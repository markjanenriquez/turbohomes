'use strict';

app
    .filter('isEmail', function() {
        return function(input) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(input);
        }
    })

.filter('findBySectionTpl', function() {
    return function(input, tpl) {
        for (var i = 0; i < input.length; i++) {
            if (input[i].tpl == tpl) {
                return input[i];
            }
        }
        return null;
    }
})

.filter('trustHtml', function($sce) {
    return $sce.trustAsHtml;
})

.filter('strLimit', function($filter) {
    return function(input, limit) {
        if (!input) return;
        if (input.length <= limit) {
            return input;
        }

        return $filter('limitTo')(input, limit) + '...';
    };
})

.filter('limitHtml', function($sce) {
    return function(input, limit) {

        var finalText = "";
        var counting = 1;
        var skipping = false;
        var addToText = false;

        input = input.replace(/<img[^>]*>/g, "");

        for (var index = 0; index < input.length; index++) {
            addToText = false;
            if (skipping) {
                finalText += input[index];
                if (input[index] === ">") {
                    skipping = false;
                }
                continue;
            } else if (counting <= limit && input[index] !== "<") {
                counting++;
                addToText = true;
            } else if (input[index] === "<") {
                skipping = true;
                addToText = true;
            }

            if (addToText) {
                finalText += input[index];
            }

        }

        return $sce.trustAsHtml(finalText);
    };
})