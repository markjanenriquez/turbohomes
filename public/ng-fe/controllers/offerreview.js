'use strict';

app.controller('offerreviewCtrl', function($scope,sellerFactory){
    $scope.listofoffers =
    [
    	{ 
    		buyersname : "Buyer 1",
    		buyersemail : "buyersemail1@mailinator.com",
    		offer : 100000,
    		created_at : new Date()
        },
        { 
    		buyersname : "Buyer 2",
    		buyersemail : "buyersemail2@mailinator.com",
    		offer : 100000,
    		created_at :  new Date()
        },
        { 
    		buyersname : "Buyer 3",
    		buyersemail : "buyersemail3@mailinator.com",
    		created_at :  new Date()
        }
    ];


});