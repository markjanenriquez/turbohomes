'use strict';

/* Controllers */

app.controller('loginCtrl', function ($scope, $auth, loginFactory, jwtHelper, store, $location, $window, toaster, forgotPasswordFactory){

    $scope.loginView = true;

    this.login = {
    	username:'',
    	password:'',
        onprogress:false,
    	submit:function(){
            
    		var login = this;
            login.onprogress = true;
    		var requestdata = {
    			email: login.username,
    			password: login.password
    		}

            
             $auth.login(requestdata).then(function(data){
             loginFactory.info(function(data){
                if(data.status === 401){
                        $auth.removeToken(); 
                        login.errorlogin = true;
                        login.errormsg = data.msg;
                        login.onprogress = false;
                }else{
                        store.set("fe_userid", data.id);
                        store.set("fe_fullname", data.fullname);
                        store.set("fe_firstname", data.firstname);
                        store.set("fe_email", data.email);
                        store.set("fe_address", data.address);
                        store.set("fe_phone", data.phone);
                        store.set("fe_profilepicture", data.picture);
                        store.set("fe_userrole", data.role);

                        if(store.get("redirectToMakeAnOffer")){
                            $window.location.href = "/dashboard#!#make_an_offer";
                            store.remove("redirectToMakeAnOffer");
                        } else if(store.get("redirectToListYourHome")) {
                            $window.location.href = "/sell-form";
                            store.remove("redirectToListYourHome");
                        } else {
                            $window.location.href = "/dashboard";
                        }

                        login.onprogress = false;
                }
             });
             }, function(error){
                console.log(error,"error")
                if(error.status === 401){
                  toaster.pop('error', error.data.error);
                  login.onprogress = false;
                }
            });

    	},
    	cancel:function() {
    		console.log('click');
        }
    };

    $scope.forgotpasswordToggle = function() {
        $scope.loginView = !$scope.loginView;
    }

    $scope.forgotpassword = {
        data: {},
        onprogress: false,
        submit: function(data) {
            if(data.email) {
                $scope.forgotpassword.onprogress = true;
                forgotPasswordFactory.sendResetLink(data, function(res) {
                    $scope.forgotpassword.onprogress = false;
                    if(res.statuscode === 401){
                        toaster.pop('error', 'Something went wrong, Please try again later');
                    } else {
                        toaster.pop('success', res.msg);
                        $scope.loginView = true;
                        $scope.forgotpassword.data  = {};
                    }
                })
            }
        }
    }
})
