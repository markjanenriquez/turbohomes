'use strict';

app.controller('blogCtrl', function($scope, $window, store, blogFtry) {

	$scope.currentPage = 1;
	$scope.totalItems = 0;
	$scope.blogList = [];
	$scope.maxSize = 5;


	var getList = function(){
		blogFtry.getBlogList($scope.currentPage, function(response){
			$scope.totalItems = response.total;
			$scope.blogList = response.data;
		})
	}

	getList();

	$scope.setPage = function(){
		getList();
	}

	$scope.readMore = function($slug,$id){
		store.set("id", $id);
		store.remove("tagid");
		store.remove("tag");
		$window.location.href=  "/blogs/" + $slug;
	}


});