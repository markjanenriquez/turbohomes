'use strict';
app.controller('updateListingCtrl', function($scope, listingFactory){

    $scope.listing = {};
    $scope.listedHomes = [];
    $scope.onprogress = false;
    var userid = $scope.userid;
    $scope.alerts = [];

    var fetchListed = function() {
        listingFactory.get(userid, function(response) {
            $scope.listedHomes = response;
        });
    }

    $scope.update_listing = function(form, listing) {
        if(form.$invalid) {
            return
        }
        $scope.alerts = [];
        $scope.onprogress = true;
        listingFactory.store(listing, userid, function(response) {
            $scope.onprogress = false;
            $scope.listing = {};
            form.$setPristine();
            if(response.statuscode == 200) {
                $scope.success = response.data
            } else {
                $scope.alerts.push({ 'type': 'danger', 'msg': 'Something went wrong, please try againd later.'});
            }
        });
    }

    fetchListed();
});