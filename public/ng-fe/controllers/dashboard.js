'use strict';
app.controller('dashboardCtrl', function($scope, $window){
    
    var varAchor = undefined; 
    $scope.goTo = function(link){
           $window.location.href = link;
    };

    $scope.setActivePills = function($ev){
        setActiveLi($ev);
    };

    var setActiveLi = function($ev){
    	var $el = angular.element($ev.currentTarget);
        var $href = $el.attr("data-target");
        var $achor = angular.element("a[href='" + $href + "']");
        varAchor = $achor;
        $achor.closest('li').addClass("active");
        var $withActive = angular.element('#sc_dashboard_menu_container .sc_dashboard_menu .dropdown.active .dropdown-menu li.active');
        $withActive.removeClass("active");
        var $withActive = angular.element('#sc_dashboard_menu_container .sc_dashboard_menu .dropdown.active');
        $withActive.removeClass("active");
    };


    $scope.setActiveTabs = function($ev){
        setActiveLi($ev);
        varAchor.closest('li.dropdown').addClass("active");
        varAchor.closest('li').addClass("active");
    };
});