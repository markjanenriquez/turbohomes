'use strict';
app.controller('contractDashboardCtrl', function($scope, contractDashboardFtry, $timeout){
	
  var user_id = localStorage.getItem('fe_userid');
	$scope.data = {
		     user_id : user_id,
         check_item : null,
         status : null
	};

	$scope.set = function($event){
		 
     var $el  = angular.element($event.currentTarget);
     
     $scope.data.check_item = $el.val();
     $scope.data.status = $event.currentTarget.checked? 1 : 0;
     
     contractDashboardFtry.setChecklist($scope.data, function(data){
        console.log('data-out')
        $timeout(function(){
          get();
           console.log('data-in')
        },1500);
     });
    
     
	};


  var get = function(){
       contractDashboardFtry.getChecklist(user_id, function(data){
            angular.forEach(data, function(value, key) {
              if(value.status == 1){
                $("#" + key).prop('checked' , true);
              }
            });
       });
  };

  get();

})