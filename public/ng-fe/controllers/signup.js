'use strict';

/* Controllers */


app.controller('signupCtrl', function ($scope, loginFactory, $timeout, toaster){

    $scope.emailVerification = false;
    $scope.verification = {};
    $scope.verificationOnProgress = false;
    $scope.emailVerificationSuccess = false;
    $scope.signup = {
        regpart2:false,
        issaving:false,
        success:false,
        alerts:[],
    	data:{
            role: 1
    	},
        validEmail: null,  
        validateEmail: function(email) {
            var self = this;

            loginFactory.validateEmail(email, function(data) {
                self.validEmail = data.valid;
            });
        },
        continue: function() {

        },
        store: function(self, data) {
            self.issaving = true;

            if(self.data.password != self.data.repassword){
                self.alerts[0] = { type: 'danger', msg: 'Password not match!' };
                self.issaving = false;
                return;
            }

            if(self.validEmail === false) {
                self.alerts[0] = { type: 'danger', msg: 'Email is already taken.' };
                self.issaving = false;
                return;
            }
            
            if(self.data.propertyaddress !== undefined && typeof self.data.propertyaddress !== 'string') {
                var temphomeaddress = [];
                        
                angular.forEach(self.data.propertyaddress.address_components, function(value, key) {
                    temphomeaddress.push(value.long_name);
                });

                self.data.propertyaddress = temphomeaddress.join(", ");
            }

            loginFactory.signup(self.data, function(data) {
                if(data.statuscode == 200) {
                    self.alerts[0] = { type: data.type, msg: data.msg };
                    self.issaving = false;
                    self.success = true;
                } else {
                    self.alerts[0] = { type: 'danger', msg: 'Server Error please try again later!' };
                    self.issaving = false;
                }
            });
        },
        submit: function(form, data){
            if(form.$invalid) {
                return;
            }

            this.store(this, data);            
        },
    	cancel:function() {
    		console.log('click');
        }
    };

    $scope.resendVerificationToggle = function() {
        $scope.emailVerification = !$scope.emailVerification;
        $scope.verification = {};
    }

    $scope.verifyEmail = function(form) {

        if(form.$invalid) {
            return;
        }

        $scope.verificationOnProgress = true;
        loginFactory.resendVerification($scope.verification, function(data) {
            $scope.verificationOnProgress = false;
            if(data.statuscode == 200) {
                $scope.emailVerification = false;
                $scope.signup.success = true;
            } else {
                toaster.pop('error', data.msg);
            }
        });
    }
    var flag = false;
    $scope.changeState = function(state, flag) {

        if(state !== 'AZ' && state) {
            toaster.pop('error', 'Sorry about that', "Unfortunately we are not able to serve your state yet but we are working hard to do so. Thank you for your interest.");
            if(!flag) {
                $scope.signup.data.state = '';
            }
        }
    }

    $scope.addressChange = function(data) {
        if(data.address_components) {
            for (var i = data.address_components.length - 1; i >= 0; i--) {
                if(data.address_components[i].short_name === 'AZ') {
                    break;
                } else if(i === 0) {
                    $scope.signup.data.propertyaddress = undefined;
                    $scope.changeState(data.address_components[i].short_name, true);
                }
            }
        }
    }

    $scope.resendEmailVerification = function() {
        if($scope.signup.data.email) {
            $scope.verificationOnProgress = true;
            loginFactory.resendVerification({ email: $scope.signup.data.email }, function(res) {
                $scope.verificationOnProgress = false;
                if(res.statuscode == 200) {
                    $scope.emailVerification = false;
                    toaster.pop('success', res.msg);
                } else {
                    toaster.pop('error', res.msg);
                }
            })
        } else {
            toaster.pop('error', "Something went wrong, Please try again later!");
        }
    }
})
