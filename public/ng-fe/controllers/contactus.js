'use strict';

app.controller('contactusCtrl', function($scope, $window, store, loginFactory, toaster) {

	$scope.form = {};

	$scope.submit = function(form, data) {
		if(!form.$invalid) {
			loginFactory.contactus(data, function(res) {
				if(res.type == 'success'){
		        	toaster.pop('success', "Message has been sent");
		        	$scope.form = {};
		        	$scope.contactusForm.$setPristine();
				} else {
		        	toaster.pop('danger', "Something went wrong, Please try again later.");
				}
			})
		}
	}

});