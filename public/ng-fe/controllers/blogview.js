'use strict';

app.controller('viewBlogCtrl', function($scope, $window, blogFtry, store) {

   var $id = store.get('id');
   
   $scope.tag = store.get("tag");

   let $tagid = $scope.tag? $scope.tag.id : null;

   $scope.blog = { main : {}, secondary : {} };

   var $getDetail = function(){
   	    blogFtry.getBlogDetail({ tagid : $tagid , id : $id }, function(data){
   	    	  console.log(data,"data")
   	    	  $scope.blog.main = data.main;
   	    	  $scope.blog.secondary = data.secondary;
   	    })
   }

   $getDetail();

   $scope.viewPost = function($slug,$id){
		store.set("id", $id);
		$window.location.href=  "/blogs/" + $slug;
   }


   $scope.viewBlogListByTag = function($id, $slug){
      store.set("tagid", $id);
      $window.location.href=  "/blogs/tag/" + $slug;
   }



});