'use strict';

/* Controllers */

app.controller('aboutyourhomeCtrl', function ($scope, $filter, jwtHelper, store, $location, $window, $timeout, FileUploader, Config, abouthomeFactory, uuid2, $q){

    var userid = $scope.userid;

    $scope.cc = {};

    var today = new Date();
    var year = today.getFullYear();
    $scope.years = [];

    for (var i = 0; i <= 50; i++) {
        year = year + i;
        $scope.years.push(year);
    }

    this.form = {
        data : {
            otherowner : [],
            userid : userid == null? 0 : userid,
            mls : 0,
            percentpayother : 0 ,
            paymentmethod: 'paypal'
        },
        validation : {},
        mask : null,
        char : null,
        via_placeholder : '',
        $init: function() {
            var self = this;
            var url = window.location.href;
            if (url.indexOf("?") >= 0) {
                self.data.custom = url.split('?custom=')[1];
                self.data.step1 = false;
                self.data.step5 = true;
                self.data.step5a = true;
            } else {
                self.data.step1 = true
            }
        },
        reset : function(){
            var form = this;
            form.data.code = undefined;
            console.log(form.data.code,"code") 
        },
        submit: function() {
            var form = this;
            
            form.validation = { 
                 contactvia : false,
                 contact : false,
                 apply : false,
                 code : false,
                 providedcode : false,
                 buyeraccess : false,
                 lockbox : false,
                 lockboxdesc : false,
                 gatecodeprov: false,
                 // providedcode: false,
                 willalarm: false,
                 
            };

            form.proceed = true;

            if(isEmpty(form.data.contactvia)){
                  form.validation.contactvia = true;
            }else{
                  if(isEmpty(form.data.contact)){
                     form.validation.contact = true;
                  }
            }

            if(!form.data.homevacant && !form.data.tenantoccupied && !form.data.permrequired && !form.data.gatecode && !form.data.alarmactivated) {
                form.validation.apply = true;
            }

            if(form.data.gatecode){
                if(isEmpty(form.data.gatecodeprov)) {
                    form.validation.gatecodeprov = true;
                } else {
                    if(form.data.gatecodeprov === "provide_gcode"){
                        if(isEmpty(form.data.providedcode)){
                            form.validation.providedcode = true;
                        }
                    }
                }
            }

            if(form.data.alarmactivated){
                if(isEmpty(form.data.willalarm)) {
                    form.validation.willalarm = true;
                } else {
                    if(form.data.willalarm === "provide_gcode"){
                        if(isEmpty(form.data.alarmcode)){
                            form.validation.alarmcode = true;
                        }
                    }
                }
            }

            if(isEmpty(form.data.buyeraccess)){
                  form.validation.buyeraccess = true;
            }else if(form.data.buyeraccess === "combination_lbox"){
                  if(isEmpty(form.data.lockbox)){
                       form.validation.lockbox = true;
                  }else{
                     if(form.data.lockbox == "provide_lbox"){
                          if(isEmpty(form.data.lockboxdesc)){
                              form.validation.lockboxdesc = true;
                          }
                     }
                  }
            }

            angular.forEach(form.validation, function(validate, key){
                  if(validate){
                      form.proceed = false;
                  }
            });

            if(form.proceed){

                // var temphomeaddress = [];
                
                // angular.forEach(form.data.homeaddress.address_components, function(value, key) {
                //     temphomeaddress.push(value.long_name);
                // });

                // form.data.homeaddress = temphomeaddress.join(", ");

                abouthomeFactory.addhome(form.data, function(data){
                    console.log(data);
                    if(data.type == 'success'){
                        form.data.step5a = false;
                        form.data.step5b = true;
                    }
                });

            }
        },
        buyernoagentchange: function($event){
            var form = this;
             if(form.data.percentpay != 'other') {
                var val = angular.element($event.currentTarget).val().replace(/,/g,"");
                form.data.buyerwithagent = (0.03 * parseInt(val)) + parseInt(val);
                var n = form.data.buyerwithagent;
                var parts = (+n).toFixed(2).split(".");
                var num = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ("." + parts[1]);
                form.data.buyerwithagent = num;
                form.data.buyerwithagentplus = n;
            }
        },
        addotherowner:function(){
            var form = this;
            form.data.otherowner.push({name:'',email:'',phone:''})
        },
        removeotherowner:function(item){
            var form = this;
            var index = form.data.otherowner.indexOf(item);
            form.data.otherowner.splice(index, 1); 
        },
        addMask : function(via){
            var form = this;
            form.data.contact = '';
            form.mask  = via != 'email'? "(999) 999-9999" : null;
            form.char = via != 'email'? "_": null;
            form.via_placeholder = via != "email"? "phone number" : via;
        },
        gotostep2:function(){
            var form = this;

            if(isEmpty(form.data.homeaddress)){
                form.validation.homeaddress = true;
            }
            else{
                form.validation.homeaddress = false;
                form.data.step1 = false;
                form.data.step2= true;
            }
        },
        gotostep3:function(){
            var form = this;

            if(isEmpty(form.data.buyernoagent)){
                form.validation.buyernoagent = true;
            }
            else{
                form.validation.buyernoagent = false;
            }
            
            if(isEmpty(form.data.buyerwithagent)){
                form.validation.buyerwithagent = true;
            }
            else{
                form.validation.buyerwithagent = false;
            }

            if(isEmpty(form.data.buyernoagent)){
                form.validation.buyernoagent = true;
            }
            else if(isEmpty(form.data.buyerwithagent)){
                form.validation.buyerwithagent = true;
            }
            else{
                form.validation.buyernoagent = false;
                form.validation.buyerwithagent = false;
                form.data.step2 = false;
                form.data.step3= true;
            }
        },
        gotostep4:function(){
              
              var form = this;
              
              form.validation = {
                 fullname : false,
                 owneremail : false,
                 otherownername : [],
                 otherowneremail : []
              }


              angular.forEach(form.data.otherowner, function(owner, key){
                    var nameBol = false;
                    var emailBol  = false;
                    if(isEmpty(owner.name)){
                        nameBol = true;
                    }
                    if(!isEmail(owner.email)){
                        emailBol = true;
                    }
                    form.validation.otherownername[key] = nameBol;
                    form.validation.otherowneremail[key] = emailBol;
              });


              if(isEmpty(form.data.fullname)){
                  form.validation.fullname = true;
              }

              if(!isEmail(form.data.owneremail)){
                  form.validation.owneremail = true;
              }

              if(!form.validation.fullname && !form.validation.owneremail && form.validation.otherownername.every(checkIfValid) && form.validation.otherowneremail.every(checkIfValid)){
                   form.data.step3 = false;
                   var url = window.location.href;
                   if (url.indexOf("?") >= 0) {
                        form.data.step5 = true;
                   } else {
                        form.data.step4 = true;
                   }
              }

              function checkIfValid(valid){
                   return valid === false;
              }
             
        }
    };

    function isEmpty(val){
        return (val === undefined || val == null || val <= 0) ? true : false;
    }


    var isEmail = function(email){
        
        if(!$filter('isEmail')(email)) {
            return false;
        }

        return true;
    };




    var buyernoagentpic = '';

    var uploader = $scope.uploader = new FileUploader({
        url: Config.BaseURL + '/api/users/fileuploader/imagefile'
    });

        // FILTERS

    uploader.filters.push({
        name: 'customFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });

        // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
        buyernoagentpic = fileItem.file.name;
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    // console.info('uploader', uploader);


    var buyerwithagentpic = '';

    var uploader2 = $scope.uploaderwith = new FileUploader({
        url: Config.BaseURL + '/api/users/fileuploader/imagefile'
    });

        // FILTERS

    uploader2.filters.push({
        name: 'customFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });

        // CALLBACKS

    uploader2.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader2.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
        buyerwithagentpic = fileItem.file.name;
    };
    uploader2.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader2.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader2.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader2.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader2.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader2.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader2.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader2.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader2.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    // console.info('uploader2', uploader2);

    var saveForm = function(form) {
        var deferred = $q.defer();

        if(typeof form.homeaddress !== 'string') {
            var temphomeaddress = [];
                    
            angular.forEach(form.homeaddress.address_components, function(value, key) {
                temphomeaddress.push(value.long_name);
            });

            form.homeaddress = temphomeaddress.join(", ");
        }

        abouthomeFactory.addpartialhome(form, function(data){
            if(data.type == 'success'){
                deferred.resolve(data);
            } else {
                deferred.reject(data);
            }
        });

        return deferred.promise;
    }

    var submitPaypal = function(form) {
        if(!form.recaptcha) {
            form.invalidRecaptcha = true;
            return;
        }

        form.invalidRecaptcha = false;
        
        $timeout(function() {
            $('#paymentForm').submit();
        });
    }

    var submitCreditCard = function(form) {

    }

    $scope.submitpayment = function(form, paymentmethod, cc) {
        var guid = uuid2.newuuid();
        form.custom = guid;
        form.paymentmethod = paymentmethod;
        saveForm(form)
            .then(function(response) {
                if(paymentmethod === 'paypal') {
                    $timeout(function() {
                        $('#paypalcustom').val(guid);
                        var ret = $('#return').val();
                        $('#return').val(ret + guid);

                        submitPaypal(form);
                    });
                } else if(paymentmethod === 'credit card') {
                    submitCreditCard(form, cc);
                }
            }, function(response) {
                console.log(response);
            });
    }


    var self = this;
    var formWatcher = $scope.$watch(function() {
        return self.form;
    }, function(newVal, oldVal) {
        self.form.$init();
        formWatcher();
    });
});