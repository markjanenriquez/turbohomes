'use strict';

app.controller('blogsByTagCtrl', function($scope, $window, store, blogFtry) {

	$scope.currentPage = 1;
	$scope.totalItems = 0;
	$scope.blogList = [];
	$scope.maxSize = 5;

	var $tagid = store.get("tagid");

	$scope.tag = null;


	var getList = function(){
		blogFtry.getBlogByTagList({id : $tagid, page : $scope.currentPage}, function(response){
			$scope.totalItems = response.total;
			$scope.blogList = response.data;
			console.log(response.data);
			store.set("tag", response.tag);
			$scope.tag = response.tag;
		})
	}

	getList();

	$scope.setPage = function(){
		getList();
	}

	$scope.readMore = function($slug,$id){
		store.set("id", $id);
		store.set("tagid", $tagid);
		store.set("tag", $scope.tag);
		$window.location.href=  "/blogs/" + $slug;
	}




});