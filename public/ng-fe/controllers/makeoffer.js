'use strict';

/* Controllers */

app.controller('makeofferCtrl', function ($scope, offerFactory, jwtHelper, store, $location, $window, $timeout, makeofferFctry, $filter){

    $scope.sections = makeofferFctry.sections;
    $scope.activeSection = angular.copy($scope.sections[0].tpl);
    $scope.activeTab = angular.copy($scope.sections[0].name);
    var userid = $scope.userid;
    $scope.onProgress = false;

	$scope.form = {
        data:{
            otherbuyer:[{name:'',email:''}],
            inclusion:{
                refrigerator: false,
                refrigeratordesc: '',
                washer: false,
                washerdesc: '',
                dryer: false,
                dryerdesc: '',
                aboveground: false,
                abovegrounddesc: '',
                other: false,
                otherdesc: ''
            },
            userid: userid,
            scpropertyaddress: undefined,
            listedmenuasnwer: undefined,
            scsellername: undefined,
            scselleraddress: undefined,
            screaltorname: undefined,
            screaltoraddress: undefined,
            scbrokerageaddress: undefined,
            scothersellername: undefined,
            scotherselleraddress: undefined,

            //<!-- buyers info -->
            // buyersname: ,
            // buyersemail: ,
            sectionbuyingmenuasnwer: undefined,
            sectionbuyingmenu2asnwer: undefined,
            sectionbuyingmenu3asnwer: undefined,
            sccurrentaddress: undefined,
            contract_date: undefined,
            sectionlistedanswer: undefined,
            sell_home_to_turbohomes: 'no',
            accept_offer_date: undefined,
            must_sell_current_house_contract: undefined,
            sectionwhomanswer: 'turbohomes',
            //sectionwhomanswer : undefined,
            whom_realtor: undefined,
            whom_realstate: undefined,
            whom_other: undefined,
            // accept_offer_date: undefined,
            // must_sell_current_house_contract: undefined,
            //<!-- financing -->
            scfinancingamt: undefined,
            sc_financing_earnest_amt: undefined,
            financingmenulistthreeanswer: undefined,
            scfinancingescrowname: undefined,
            scfinancingescrowtel: undefined,
            scfinancingescrowofficername: undefined,
            scfinancingearnestamt: undefined,
            down_payment: undefined,
            mortgage: undefined,
            planforpayingother: undefined,
            financingmenulisttwoanswer: undefined,
            type_of_financing_other: undefined,
            financingmenulistfouranswer: undefined,
            closingcostamount: undefined,
            financingmenulistsixanswer: undefined,
            financingmenulistsevenanswer: undefined,
            costnotexceed: undefined,
            importdatesmenu: undefined,
            //<!-- Important dates -->
            importlistoneanswer: undefined,
            importlistoneanswerother: undefined,
            inspectiondeadline: 10,
            inspectorselected: undefined,
            closingdate: undefined,
            discusswithattorny: undefined,
            additionalcondition: undefined
        }
    };

    $scope.selectSection = function(section, force) {
        force = force === undefined ? false : force;
        console.log(section);
        if(force || !section.disabled) {
            $scope.activeSection = angular.copy(section.tpl);
            $scope.activeTab = angular.copy(section.name);
        }
    }
})

.factory('makeofferFctry', function($http, $q, Config){
    return {
        sections: [
            { name: "Property", tpl: 'property', disabled: false, done: false, steps: 2, step: 1 },
            { name: "Buyer's Info", tpl: 'buyers-info', disabled: true, done: false, steps: 2, step: 1 },
            { name: "Financing", tpl: 'financing', disabled: true, done: false, steps: 6, step: 1 },
            { name: "Important Dates", tpl: 'import-dates', disabled: true, done: false, steps: 4, step: 1 },
            { name: "inclusions", tpl: 'inclusions', disabled: true, done: false, steps: 3, step: 1 }
        ],
        nextStep: function(form, steps, step) {
            if(!form.$invalid && steps > step) {
                return ++step;
            } else {
                return step;
            }
        },
        goBack: function(steps, step) {
            if(steps >= step && step > 0) {
                return --step;
            } else {
                return step;
            }
        },
        sectionSequence: {
            'property': 'buyers-info',
            'buyers-info': 'financing',
            'financing': 'import-dates',
            'import-dates': 'inclusions'
        },
        nextSection: function(sections, activeSection) { 
            var deferred = $q.defer();

            var sequence = this.sectionSequence;
            var prevSection = angular.copy(activeSection);
            activeSection = angular.copy(sequence[activeSection]);

            sections.map(function(value, key) {
                if(value.tpl === prevSection) {
                    value.done = true;
                } else if(activeSection === value.tpl) {
                    value.disabled = false;
                }
            });

            deferred.resolve({
                activeSection: activeSection,
                sections: sections
            });

            return deferred.promise;
        }
    }
})

.controller('propertyCtrl', function ($scope, makeofferFctry, $filter){
    $scope.steps = makeofferFctry.sections[0].steps;
    $scope.step = makeofferFctry.sections[0].step;
    var parentScope = $scope.$parent.$parent;

    $scope.nextStep = function(form) {
        $scope.step = makeofferFctry.nextStep(form, $scope.steps, $scope.step);
    }  

    $scope.goBack = function() {
        $scope.step = makeofferFctry.goBack($scope.steps, $scope.step);
    }

    $scope.listedBy = function(listedmenuasnwer, event) {
        event.preventDefault();
        $scope.form.data.listedmenuasnwer = listedmenuasnwer;

        switch (listedmenuasnwer) {
            case 'turbo':
                //realtor
                $scope.form.data.screaltorname = undefined;
                $scope.form.data.screaltoraddress = undefined;
                $scope.form.data.scbrokerageaddress = undefined;
                //other
                $scope.form.data.scothersellername = undefined,
                $scope.form.data.scotherselleraddress = undefined
                break;
            case 'realtor':
                //turbo
                $scope.form.data.scsellername = undefined;
                $scope.form.data.scselleraddress = undefined;
                //other
                $scope.form.data.scothersellername = undefined,
                $scope.form.data.scotherselleraddress = undefined
                break;
            case 'other':
                //turbo
                $scope.form.data.scsellername = undefined;
                $scope.form.data.scselleraddress = undefined;
                //realtor
                $scope.form.data.screaltorname = undefined;
                $scope.form.data.screaltoraddress = undefined;
                $scope.form.data.scbrokerageaddress = undefined;

                break;
            default:
                //turbo
                $scope.form.data.scsellername = undefined;
                $scope.form.data.scselleraddress = undefined;
                //realtor
                $scope.form.data.screaltorname = undefined;
                $scope.form.data.screaltoraddress = undefined;
                $scope.form.data.scbrokerageaddress = undefined;
                //other
                $scope.form.data.scothersellername = undefined,
                $scope.form.data.scotherselleraddress = undefined
                break;
        }
    }

    $scope.nextSection = function(form) {
        if(!form.$invalid) {
            switch ($scope.form.data.listedmenuasnwer) {
                case 'turbo':
                    if(!$filter('isEmail')($scope.form.data.scselleraddress)) {
                        alert('Invalid email');
                        return;
                    }
                    break;
                case 'realtor':
                    if(!$filter('isEmail')($scope.form.data.screaltoraddress)) {
                        alert('Invalid email');
                        return;
                    }
                    break;
                case 'other':
                    if(!$filter('isEmail')($scope.form.data.scotherselleraddress)) {
                        alert('Invalid email');
                        return;
                    }
                    break;
            }

            makeofferFctry.nextSection($scope.sections, $scope.activeSection)
                .then(function(response) {
                    parentScope.activeSection = response.activeSection;
                    parentScope.activeTab = makeofferFctry.sections[1].name;
                    parentScope.sections = response.sections;
                    makeofferFctry.sections[0].step = angular.copy(makeofferFctry.sections[0].steps);
                });
        }
    }
})

.controller('buyersinfoCtrl', function($scope, makeofferFctry, $filter){
    $scope.steps = makeofferFctry.sections[1].steps;
    $scope.step = makeofferFctry.sections[1].step;
    var parentScope = $scope.$parent.$parent;

    $scope.nextStep = function(form) {
        if($scope.step == 2) {
            if(!$filter('isEmail')($scope.form.data.buyersemail)) {
                alert('Invalid email');
                return;
            }
        }

        $scope.step = makeofferFctry.nextStep(form, $scope.steps, $scope.step);
    }  

    $scope.goBack = function() {
        $scope.step = makeofferFctry.goBack($scope.steps, $scope.step);
    }

    $scope.selectBuyingMenu = function(menu, event) {
        event.preventDefault();
        var exclude = [
            'alone',
            'notsure'
        ];

        if(exclude.indexOf(menu)) {
            $scope.form.data.otherbuyer = [{name:'',email:''}];
        }
        $scope.form.data.sectionbuyingmenuasnwer = menu;
    }

    $scope.selectBuyingMenu2 = function(menu, event) {
        event.preventDefault();
        var exclude = [
            'no',
            'notsure'
        ];

        if(exclude.indexOf(menu)) {
            $scope.form.data.sccurrentaddress = undefined;
            $scope.form.data.contract_date = undefined;
            $scope.form.data.sectionlistedanswer = undefined;
            $scope.form.data.sectionwhomanswer = undefined;
            $scope.form.data.whom_other = undefined;
            $scope.form.data.whom_realtor = undefined;
            $scope.form.data.whom_realstate = undefined;
            $scope.form.data.accept_offer_date = undefined;
            $scope.form.data.must_sell_current_house_contract = undefined;
        }

        $scope.form.data.sectionbuyingmenu2asnwer = menu;
    }

    $scope.selectBuyingMenu3 = function(menu, event) {
        event.preventDefault();
    
        if(menu === 'no') {
            $scope.form.data.contract_date = undefined;
        } else {
            $scope.form.data.sectionlistedanswer = undefined;
            $scope.form.data.sell_home_to_turbohomes = 'no';
            $scope.form.data.sectionwhomanswer = undefined;
            $scope.form.data.whom_other = undefined;
            $scope.form.data.whom_realtor = undefined;
            $scope.form.data.whom_realstate = undefined;
            $scope.form.data.accept_offer_date = undefined;
            $scope.form.data.must_sell_current_house_contract = undefined;
        }   

        $scope.form.data.sectionbuyingmenu3asnwer = menu;
    }

    $scope.selectBuyingMenu4 = function(menu, event) {
        event.preventDefault();
    
        if(menu === 'no') {
            $scope.form.data.sectionwhomanswer = undefined;
            $scope.form.data.whom_other = undefined;
            $scope.form.data.whom_realtor = undefined;
            $scope.form.data.whom_realstate = undefined;
        } else {
            $scope.form.data.sell_home_to_turbohomes = 'no';
        }   

        $scope.form.data.sectionlistedanswer = menu;
    }

    $scope.selectBuyingMenu5 = function(menu, event) {
        event.preventDefault();
        $scope.form.data.sell_home_to_turbohomes = menu;
    }

    $scope.selectBuyingMenu6 = function(menu, event) {
        event.preventDefault();
        if(menu !== 'realtor') {
            $scope.form.data.whom_realtor = undefined;
            $scope.form.data.whom_realstate = undefined;
        }
        if(menu !== 'realtor') {
            $scope.form.data.whom_other = undefined;
        }

        $scope.form.data.sectionwhomanswer = menu;
    }

    $scope.buyer = {
        add: function() {
            $scope.form.data.otherbuyer.unshift({name:'',email:''});
        },
        remove:function(index){
            $scope.form.data.otherbuyer.splice(index, 1); 
        },
    }
    
    $scope.nextSection = function(form) {
        if(!form.$invalid) {
            makeofferFctry.nextSection($scope.sections, $scope.activeSection)
                .then(function(response) {
                    parentScope.activeSection = response.activeSection;
                    parentScope.activeTab = makeofferFctry.sections[2].name;
                    parentScope.sections = response.sections;
                    makeofferFctry.sections[1].step = angular.copy(makeofferFctry.sections[0].steps);
                });
        }
    }
})

.controller('financingCtrl', function($scope, makeofferFctry, $filter){
    $scope.steps = makeofferFctry.sections[2].steps;
    $scope.step = makeofferFctry.sections[2].step;
    var parentScope = $scope.$parent.$parent;

    $scope.nextStep = function(form) {
        // if($scope.step == 2) {
        //     if(!$filter('isEmail')($scope.form.data.buyersemail)) {
        //         alert('Invalid email');
        //         return;
        //     }
        // }

        if(form.$name == 'financing1') {
            $scope.form.data.scfinancingamt = $scope.form.data.scfinancingamt.replace('$', '').replace(',','').replace(' ', '');
            $scope.form.data.scfinancingearnestamt = parseFloat($scope.form.data.scfinancingamt * 0.01).toFixed(2);
            console.log($scope.form.data.scfinancingamt);
            console.log($scope.form.data.scfinancingearnestamt);
        } else if (form.$name == 'financing2') {
            $scope.form.data.scfinancingearnestamt = $scope.form.data.scfinancingamt.replace('$', '').replace(',','').replace(' ', '');
        }

        $scope.step = makeofferFctry.nextStep(form, $scope.steps, $scope.step);
    }  

    $scope.goBack = function() {
        $scope.step = makeofferFctry.goBack($scope.steps, $scope.step);
    }
    
    $scope.nextSection = function(form) {
        if(!form.$invalid) {
            makeofferFctry.nextSection($scope.sections, $scope.activeSection)
                .then(function(response) {
                    parentScope.activeSection = response.activeSection;
                    parentScope.sections = response.sections;
                    parentScope.activeTab = makeofferFctry.sections[3].name;
                    makeofferFctry.sections[2].step = angular.copy(makeofferFctry.sections[0].steps);
                });
        }
    }

    $scope.financingmenu = function(menu, event) {
        event.preventDefault();
        if(menu == 'no') {
            $scope.form.data.scfinancingescrowname = undefined;
            $scope.form.data.scfinancingescrowtel = undefined;
            $scope.form.data.scfinancingescrowofficername = undefined;
        }
        $scope.form.data.financingmenulistthreeanswer = menu;
    }

    $scope.financingmenu2 = function(menu, event) {
        event.preventDefault();
        $scope.form.data.financingmenulisttwoanswer = menu;
    }

    $scope.financingmenu3 = function(menu, event) {
        event.preventDefault();
        $scope.form.data.financingmenulistoneanswer = menu;
    }

    $scope.financingmenu4 = function(menu, event) {
        event.preventDefault();
        $scope.form.data.financingmenulisttwoanswer = menu;
    }

    $scope.financingmenu5 = function(menu, event) {
        event.preventDefault();
        $scope.form.data.type_of_financing = menu;
    }

    $scope.financingmenu6 = function(menu, event) {
        event.preventDefault();
        $scope.form.data.financingmenulistfouranswer = menu;
    }

    $scope.financingmenu7 = function(menu, event) {
        event.preventDefault();
        $scope.form.data.financingmenulistsixanswer = menu;
    }

    $scope.financingmenu8 = function(menu, event) {
        event.preventDefault();
        $scope.form.data.financingmenulistsevenanswer = menu;
    }
})

.controller('importDatesCtrl', function($scope, makeofferFctry, $filter) {
    $scope.steps = makeofferFctry.sections[3].steps;
    $scope.step = makeofferFctry.sections[3].step;
    var parentScope = $scope.$parent.$parent;

    $scope.nextStep = function(form) {
        // if($scope.step == 2) {
        //     if(!$filter('isEmail')($scope.form.data.buyersemail)) {
        //         alert('Invalid email');
        //         return;
        //     }
        // }

        $scope.step = makeofferFctry.nextStep(form, $scope.steps, $scope.step);
    }  

    $scope.goBack = function() {
        $scope.step = makeofferFctry.goBack($scope.steps, $scope.step);
    }
    
    $scope.nextSection = function(form) {
        if(!form.$invalid) {
            makeofferFctry.nextSection($scope.sections, $scope.activeSection)
                .then(function(response) {
                    parentScope.activeSection = response.activeSection;
                    parentScope.activeTab = makeofferFctry.sections[5].name;
                    parentScope.sections = response.sections;
                    makeofferFctry.sections[3].step = angular.copy(makeofferFctry.sections[0].steps);
                });
        }
    }

    $scope.importdatesmenu = function(menu, event) {
        event.preventDefault();
        $scope.form.data.importlistoneanswer = menu;
    }

    $scope.importdatesmenu2 = function(menu, event) {
        event.preventDefault();
        $scope.form.data.inspectorselected = menu;
    }

    $scope.importdatesmenu3 = function(menu, event) {
        event.preventDefault();
        $scope.form.data.closingdate = menu;
    }
})

.controller('inclusionsCtrl', function($scope, makeofferFctry, $filter, offerFactory, $timeout) {
    $scope.steps = makeofferFctry.sections[4].steps;
    $scope.step = makeofferFctry.sections[4].step;
    var parentScope = $scope.$parent.$parent;

    $scope.nextStep = function(form) {
        $scope.step = makeofferFctry.nextStep(form, $scope.steps, $scope.step);
    }  

    $scope.goBack = function() {
        $scope.step = makeofferFctry.goBack($scope.steps, $scope.step);
    }

    var getAddress = function(address) {
        if(address) {
            if(typeof address !== 'string') {
                var temphomeaddress = [];
                        
                angular.forEach(address.address_components, function(value, key) {
                    temphomeaddress.push(value.long_name);
                });

                return temphomeaddress.join(", ");
            }
        }

        return null
    }

    $scope.saveOffer = function(form) {
        if(!form.$invalid) {
            $scope.form.data.scpropertyaddress = getAddress($scope.form.data.scpropertyaddress);
            $scope.form.data.sccurrentaddress = getAddress($scope.form.data.sccurrentaddress);
            $scope.onProgress = true;
            offerFactory.createoffer($scope.form.data, function(data){
                $scope.onProgress = false;
                if(data.type == 'success'){
                    makeofferFctry.sections.map(function(value, key) {
                        if(key !== (makeofferFctry.sections.length - 1)) {
                            value.disabled = true;
                            $scope.step = 3;
                        }
                    });
                }
            });
        }
    }

    $scope.reload = function(ev) {
        ev.preventDefault();
        window.location.replace("/dashboard#!#buyer_contract_dashboard");
        $timeout(function() {
            location.reload();
            console.log(reload);
        })
    }
});

// .controller('OfferSection3Ctrl', function($scope) {
//     $scope.init = function() {
//         $scope.form.$init();
//     };

//     $scope.test = function() {
//         console.log('test');
//     }

//     $scope.form = {
//         $state: null,
//         $endState: 5,
//         $next: function() {
//             if(this.$state != this.$endState) this.$state += 1;
//         },
//         $prev: function() {
//             if(this.$state > 1) this.$state -= 1;
//         },
//         $init: function() {
//             this.$state = 1;
//         }
//     }

//     return $scope.init();
// });