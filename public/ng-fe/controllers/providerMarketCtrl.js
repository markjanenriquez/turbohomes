'use strict';

app.controller('providerMarketCtrl', function($scope, $location) {

	switch($location.hash()) {
		case 'lender':
			$scope.filterCategory = 'Lender';
			$scope.searchText = 'Broker';
			break;
		default: 
			break;
	}

	$scope.filter = function(data) {
		var name = data.name.toUpperCase();

		if($scope.filterCategory && $scope.searchText) {
			console.log("A");
			if(data.desc === $scope.filterCategory || name.indexOf($scope.searchText.toUpperCase()) >= 0) {
				return true;
			}
		} else if(!$scope.filterCategory && !$scope.searchText) {
			return true;
		}else {

			if(($scope.filterCategory && data.desc === $scope.filterCategory) || ($scope.searchText && name.indexOf($scope.searchText.toUpperCase()) >= 0)) {
				return true;
			}
		}

		return false;
	}

	$scope.markets = [
		// { name: 'Primo Steamo', img: 'img/market/primo-steamo.png', desc: 'Automation, Security'},
		// { name: 'August Home', img: 'img/market/august.png', desc: 'Automation, Security'},
		// { name: 'Lexington Law', img: 'img/market/lexington-law.png', desc: 'Automation, Security'},
		// { name: 'Vivint Smart Home', img: 'img/market/vivint.png', desc: 'Automation, Security'},
		// { name: 'The Clean Queen', img: 'img/market/clean-queen.png', desc: 'Automation, Security'},
		{ name : 'Stratton Inspections', img : 'img/market/stratton.png', desc:'Inspector', link : 'http://www.strattoninspections.com/'},
		{ name : 'State 48 Home Inspections', img : 'img/market/state-48.png', desc:'Inspector', link : 'http://state48inspections.com/'},
		{ name : 'Great American Title', img : 'img/market/great_american_title.png', desc:'Title Company' , link : 'http://www.azgat.com/'},
		{ name : 'First American Title', img : 'img/market/first_american_title.png', desc:'Title Company', link : 'http://www.firstam.com/title/az/index.html'},
		{ name : 'State Farm', img : 'img/market/state_farm.jpg', desc:'Homeowner Insurance', link : 'http://clarksanchez.com/'},
		{ name : 'Insurance Brokers of Arizona', img : 'img/market/insurance_broker.png', desc:'Homeowner Insurance', link : 'https://insurancebrokersofaz.com/'},


		{ name : 'OneGuard Home Warranties', img : 'img/market/one-guard.png', desc:'Homeowner Warranty', link : 'https://www.oneguardhomewarranty.com/'},
		{ name : 'Peoples Mortgage Company', img : 'img/market/peoples-nav-logo.png', desc:'Lender', link : 'http://www.peoplesmortgage.com/'},
		{ name : 'Castle Appraising', img : 'img/market/castle-appraising-logo.png', desc:'Appraisals', link : 'http://castleappraising.com/'},
		{ name : 'Swift Home Inspections', img : 'img/market/HI_Theme37-header-left.png', desc:'Inspector', link : 'http://www.arizona-homeinspection.com/'},
		{ name : 'Empire West Title Agency', img : 'img/market/ewtaz.png', desc:'Title & Escrow', link : 'http://www.ewtaz.com/'},
		// { name : 'Owner, Banda Brothers DJs', img : 'img/market/HI_Theme37-header-left.png', desc:'Title & Escrow', link : 'http://www.bbdjs.com/'}
	];

});