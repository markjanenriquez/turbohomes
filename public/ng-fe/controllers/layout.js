'use strict';

app.controller("layoutCtrl", function (webNotification, Config, $document, $scope, $auth, loginFactory, notificationFactory, jwtHelper, store, $location, $window, $uibModal, $state) {

	$scope.fullname = store.get("fe_fullname");
	$scope.firstname = store.get("fe_firstname");
	$scope.address = store.get("fe_address");
	$scope.email = store.get("fe_email");
	$scope.userid = store.get("fe_userid");
	$scope.phone = store.get("fe_phone");
	$scope.isloggedin = false;
	// $scope.state = 
	$scope.disableNav = (window.location.pathname === '/dashboard');

	$scope.checkPopUP = function(){
       	loginFactory.checkPopUP({ user_id : store.get("fe_userid")}, function(data){        
            //show popup-modal
            if(!data.isShowed){   
                $('#sc_welcome_modal').modal('show');
            }
       	});
	};

	$scope.setFlag = function(user, ev){
		store.remove("redirectToMakeAnOffer");
		store.remove("redirectToListYourHome");
		if(!$scope.isloggedin){
			ev.preventDefault();
			if(user === 'buyer') {
				store.set("redirectToMakeAnOffer", true);
			} else if(user === 'seller'){
				store.set("redirectToListYourHome", true);
			}
			$window.location.href = "/login";
		}
	};

	if(!$auth.getToken()){
		$scope.isloggedin = false;
		if($window.location.href.indexOf(Config.BaseURL + "/dashboard") !== -1){
			$window.location.href = "/login";
		} 
	} else {
		$scope.isloggedin = true;
		// $scope.notificationList();
		$scope.checkPopUP();
		// if(Config.BaseURL + "/login" === $window.location.href || Config.BaseURL + "/" === $window.location.href){
		// 	$window.location.href = "/dashboard";
		// }
	}

	$scope.logout = function() {
		store.remove("fe_userid");
		store.remove("fe_fullname");
		store.remove("fe_firstname");
		store.remove("fe_userrole");
		store.remove("fe_email");
		store.remove("fe_phone");
		$auth.removeToken(); 
		$window.location.href = "/";
	}

	// anchor event
	$scope.scrollTo = function(hash){
		// var hash = $window.location.hash;
		// var hash = "#a-better-way";
		if(hash){
        	hash = hash.replace('#!', '');
        	$('html, body').animate({
        		scrollTop: ($(hash).offset().top - 150)
        	}, 800);
        }
	};

	// $document.ready(function(){
 //        $scope.scrollTo();
	// });

	///////////////////////////////
	///////// notification ////////
	///////////////////////////////
	// $scope.showWebNotification = function(notifData){
 //        webNotification.showNotification(notifData.title, {
 //                body: notifData.msg,
 //                icon: 'http://turbohomes.com/img/email-logo.png',
 //                onClick: function onNotificationClicked() {
 //                    console.log('Notification clicked.');
 //                },
 //                autoClose: 4000 //auto close the notification after 4 seconds (you can manually close it via hide function)
 //                }, function onShow(error, hide) {
 //                 if (error) {
 //                    window.alert('Unable to show notification: ' + error.message);
 //                 } else {
 //                    console.log('Notification Shown.');

 //                    setTimeout(function hideNotification() {
 //                        console.log('Hiding notification....');
 //                        hide(); //manually close the notification (you can skip $scope if you use the autoClose option)
 //                    }, 5000);
 //                 }
 //                });
	// };

	// $scope.notificationList = function(){
 //       notificationFactory.getNotif(store.get("fe_userid"), function(data){
 //            $scope.data.notificationList = data.notifications;
 //            $scope.badge = data.badge_count;
 //       });
	// };

	// $scope.readNotification = function(notificationList){
 //      notificationFactory.setNotif({ lists : notificationList}, function(data){
 //      	  //console.log(data,"data")
 //      	  if(data.statuscode === 200){
 //             $scope.notificationList();
 //      	  }
 //      })
	// };

	// $scope.data = {
	// 	 notificationList : [],
	// 	 addedList : "list"
	// };	
 
	// $scope.$watch('data.addedList', function(newVal, oldVal){
	// 	 if(newVal !== oldVal){
	// 	 	  var newList = JSON.parse(newVal).data;
 //              $scope.showWebNotification(newList);
	//      	  $scope.data.notificationList.splice(0,0, newList);
	//      	  $scope.badge = parseInt($scope.badge) + 1;
	//      }
	// },true);
})
