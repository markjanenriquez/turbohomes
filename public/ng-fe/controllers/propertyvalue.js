'use strict';

/* Controllers */


app.controller('propertyvalueCtrl', function ($scope, propertyvalueFtry, jwtHelper, store, $timeout, $sce, generalFactory, toaster){

   
    $scope.$variable = {
        onprogress : false,
        submitted : false,
        property_details : null,
        myInterval : 2000,
        noWrapSlides : false,
        active :  0,
        average_computation : 0,
        sqft_value_arr : [],
    };

    $scope.$validation = {
        address : {
            invalid : false
        }, 
        no_address_found : false, 
    };

   
    $scope.save = function(data){
        
        if (data === undefined || data.address_components === undefined) {
            return $scope.$validation.address.invalid = true;
        }

        $scope.$validation.address.invalid = false;
        $scope.$validation.no_address_found = false;
        $scope.$variable.submitted = true;

        var street_number = data.address_components[0] ? data.address_components[0].long_name : '';

        var route = data.address_components[1] ? data.address_components[1].long_name : '';

        var locality = data.address_components[3] ? data.address_components[3].long_name : '';

        var administrative_area_level_1 = data.address_components[5] ? data.address_components[5].short_name : '';

        var postal_code = data.address_components[7] ? data.address_components[7].long_name : '';

        var country = data.address_components[6] ? data.address_components[6].long_name : '';

        var postdata = {
            'street_number': street_number,
            'route': route,
            'locality': locality,
            'administrative_area_level_1': administrative_area_level_1,
            'postal_code': postal_code,
            'country': country,
        };

        $scope.$variable.onprogress = true;
        propertyvalueFtry.getReport(postdata, function(data) {
            $scope.$variable.onprogress = false;
            console.log(data, "data")
            if (data.hasOwnProperty('code')) {
                if (parseInt(data.code) <= "508") {
                    $scope.$validation.no_address_found = true;
                }
            } else {

                $scope.$variable.property_details = data;
                var comp_markers = [];
                var highest_comp = 0;
                var lowest_comp = 10000000000;
                var average_computation = 0;
                angular.forEach($scope.$variable.property_details.comparables, function(comp, key) {

                    var lastSoldPrice = parseFloat(comp.lastSoldPrice);
                    var finishedSqFt = parseFloat(comp.finishedSqFt);
                    var pricesqft = (lastSoldPrice / finishedSqFt);
                    pricesqft = (isNaN(pricesqft) ? 0 : pricesqft.toFixed(2));
                    $scope.$variable.property_details.comparables[key].pricesqft = pricesqft;
                    average_computation = parseFloat(average_computation) + parseFloat(pricesqft);

                    if (lowest_comp > pricesqft && pricesqft != 0) {
                        lowest_comp = angular.copy(pricesqft);
                    }

                    if (highest_comp < pricesqft) {
                        highest_comp = angular.copy(pricesqft);
                    }

                });

                $scope.$variable.property_details.comps_valuation_low = (lowest_comp * $scope.$variable.property_details.finishedSqFt);
                $scope.$variable.property_details.comps_valuation_average = ((average_computation / $scope.$variable.property_details.comparables.length) * $scope.$variable.property_details.finishedSqFt);
                $scope.$variable.property_details.comps_valuation_high = (highest_comp * $scope.$variable.property_details.finishedSqFt);

                $timeout(function() {
                    var img = document.getElementById('chart-img');
                    img.onload = function() {
                        $('#chart-img').attr('style', "display:none");
                        var canvas = document.getElementById('chart');
                        canvas.width = 494;
                        canvas.height = 266;
                        canvas.getContext("2d").drawImage(img, 0, 0, 494, 266);
                        $('#chart').removeAttr('style');
                    };
                });
            }
        });
    };

    // var convertCanvasToImage = function(canvas) {
    //     var image = new Image();
    //     image.src = canvas.toDataURL("image/png");
    // }
    
    // var convertImageToCanvas = function(image) {
    //     var canvas = document.createElement("canvas");
    //     canvas.width = image.width;
    //     canvas.height = image.height;
    //     canvas.getContext("2d").drawImage(image, 0, 0);

    //     convertCanvasToImage(canvas);
    // }

    // $scope.export = function(){
    //     $('#chart-img').attr('style', "display:none");
    //     domtoimage.toPng(document.getElementById('chart'))
    //         .then(function (dataUrl) {
    //             // var img = new Image();
    //             // img.src = dataUrl;
    //             // document.body.appendChild(img);
    //             console.log(dataUrl);
    //             console.log('exported');
    //         })
    //         .catch(function (error) {
    //             console.log(error);
    //             toaster.pop('error', 'Something went wrong, please try again later');
    //         });
    // }
    
});