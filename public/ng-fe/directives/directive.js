'use strict';

/* Directives */
// All the directives rely on jQuery.

app
    .directive('uiModule', ['MODULE_CONFIG','uiLoad', '$compile', function(MODULE_CONFIG, uiLoad, $compile) {
        return {
            restrict: 'A',
            compile: function (el, attrs) {
                var contents = el.contents().clone();
                return function(scope, el, attrs){
                    el.contents().remove();
                    uiLoad.load(MODULE_CONFIG[attrs.uiModule])
                        .then(function(){
                            $compile(contents)(scope, function(clonedElement, scope) {
                                el.append(clonedElement);
                            });
                        });
                }
            }
        };
    }])

    .directive('datePicker', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                console.log("ASD");
                element.datepicker({});
            }
        }
    })

    .directive('maskMoney', function($timeout) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, element, attrs, ngModel) {
              
                var precision = ("withDecimal" in attrs)? undefined : 0;
                var prefix = ("noPrefix" in attrs)? undefined : '$ ';

                scope.$watch(function () {
                    return ngModel.$modelValue;
                }, function(newValue) {
                   if(newValue){
                      let $newVal = newValue.toString();
                      ngModel.$setViewValue($newVal.replace(prefix, '').replace(/,/g,""))
                    }
                });

                $timeout(function() {
                    $(element).maskMoney({prefix: prefix, thousands:',', precision: precision, allowZero:true,});
                    $(element).focus();
                    $(element).blur();
                });
            }
        }
    });
    // .directive('gmapsAutoComplete', function() {
    //     return {
    //         restrict: 'A',
    //         link: function(scope, element, attr) {
    //             autocomplete = new google.maps.places.Autocomplete(
    //                 element,
    //                 {types: ['geocode']}
    //             );
                
    //             // When the user selects an address from the dropdown, populate the address
    //             // fields in the form.
    //             autocomplete.addListener('place_changed', fillInAddress);
    //             jQuery(element).trigger('input');

    //             function fillInAddress() {
    //                 var place = autocomplete.getPlace();
    //                 for (var i = 0; i < place.address_components.length; i++) {
    //                     var addressType = place.address_components[i].types[0];
    //                     if (componentForm[addressType]) {
    //                         var val = place.address_components[i][componentForm[addressType]];
    //                         document.getElementById(addressType).value = val;   
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // });
