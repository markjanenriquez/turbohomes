app.factory('notificationFactory', function($http, $q, Config){
    return {
        getNotif: function(user_id, callback) {
            $http({
                url: Config.BaseURL + "/api/notification/get/" + user_id,
                method:"GET",
                headers: {'Content-Type':'application/x-www-form-urlencoded'},
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        setNotif: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/notification/set",
                method:"POST",
                headers: {'Content-Type':'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        }, 
    };
   
})