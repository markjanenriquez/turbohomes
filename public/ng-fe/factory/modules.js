app.factory("modulesFactory", function($http, Config) {
  return {
    myrooms: function(sy, userid, callback) {
      $http({
        url: Config.BaseURL + "/fe/api/modules/myrooms",
        method: "POST",
        headers: {"Content-Type":"application/x-www-form-urlencoded"},
        data: $.param({sy:sy,userid:userid})
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    imagelist: function(asset, callback) {
      $http({
        url: Config.BaseURL + "/fe/api/modules/imagelist",
        method: "POST",
        headers: {"Content-Type":"application/x-www-form-urlencoded"},
        data: $.param(asset)
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    audiolist: function(asset, callback) {
      $http({
        url: Config.BaseURL + "/fe/api/modules/audiolist",
        method: "POST",
        headers: {"Content-Type":"application/x-www-form-urlencoded"},
        data: $.param(asset)
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    videolist: function(asset, callback) {
      $http({
        url: Config.BaseURL + "/fe/api/modules/videolist",
        method: "POST",
        headers: {"Content-Type":"application/x-www-form-urlencoded"},
        data: $.param(asset)
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    doculist: function(asset, callback) {
      $http({
        url: Config.BaseURL + "/fe/api/modules/doculist",
        method: "POST",
        headers: {"Content-Type":"application/x-www-form-urlencoded"},
        data: $.param(asset)
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    }
  }
})
