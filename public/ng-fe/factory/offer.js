app.factory('offerFactory', function($http, $q, Config){
    return {
         createoffer: function(student,callback){
            $http({
                url: Config.BaseURL + "/api/offer/create",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(student)
            }).then(function successCallback(response) {
              callback(response.data);
            }, function errorCallback(response) {
              callback(response.data);
            });
         }
    };
   
})