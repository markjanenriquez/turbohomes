app.factory('contractDashboardFtry', function($http, Config){
    return {
        setChecklist: function(data,callback){
            $http({
                url: Config.BaseURL + "/api/dashboard-status/set",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function successCallback(response) {
              callback(response.data);
            }, function errorCallback(response) {
              callback(response.data);
            });
        },
        getChecklist: function(user_id,callback){
            $http({
                url: Config.BaseURL + "/api/dashboard-status/get/" + user_id,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).then(function successCallback(response) {
              callback(response.data);
            }, function errorCallback(response) {
              callback(response.data);
            });
        },
    };
   
})