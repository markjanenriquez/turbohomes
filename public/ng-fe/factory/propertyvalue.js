app.factory('propertyvalueFtry', function($http, $q, Config){
    return {
         getReport: function(data,callback){
            $http({
                url: Config.BaseURL + "/api/property-value/get",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function successCallback(response) {
              callback(response.data);
            }, function errorCallback(response) {
              callback(response.data);
            });
         }
    };
   
})