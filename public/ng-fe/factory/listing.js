app.factory('listingFactory', function($http, $q, Config){
    return {
        get: function(userid,callback){
            $http({
                url: Config.BaseURL + "/api/listing/" + userid,
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function successCallback(response) {
              callback(response.data);
            }, function errorCallback(response) {
              callback(response.data);
            });
        },
        store: function(data, userid, callback) {
            $http({
                url: Config.BaseURL + "/api/listing/" + userid,
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function successCallback(response) {
              callback(response.data);
            }, function errorCallback(response) {
              callback(response.data);
            });
        }
    };
   
})