app.factory('blogFtry', function($http, Config) {
    return {
        getBlogList: function(page, callback) {
            $http({
                url: Config.BaseURL + "/api/fe/blog/list?page=" + page,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        }, 
        getBlogDetail : function(data, callback){

           let $concatUrl = "";
           if(data.tagid){
              $concatUrl =  "/" + data.tagid;
           }

           $http({
                url: Config.BaseURL + "/api/fe/blog/get/" + data.id + $concatUrl,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        getBlogByTagList : function(data, callback){
             $http({
                url: Config.BaseURL + "/api/fe/blog/list/" + data.id + "?page=" + data.page ,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            }); 
        }


    }
})