app.factory('loginFactory', function($http, $q, Config){
    return {
         studentlogin: function(student,callback){
            $http({
                url: Config.BaseURL + "/fe/api/student/login",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(student)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
         },
         signup: function(user, callback) {
            $http({
                url: Config.BaseURL + "/api/user/signup",
                method:"POST",
                headers: {'Content-Type':'application/x-www-form-urlencoded'},
                data: $.param(user)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        }, 
        validateEmail: function(email, callback) {
            $http({
                url: Config.BaseURL + "/api/user/validate-email",
                method:"POST",
                headers: {'Content-Type':'application/x-www-form-urlencoded'},
                data: $.param({ email: email })
            }).then(function(response) {
                callback(response.data);
            }, function(response) {
                callback(response.data);
            });
        },
        contactus: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/contactus",
                method:"POST",
                headers: {'Content-Type':'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function(response) {
                callback(response.data);
            }, function(response) {
                callback(response.data);
            });
        }
    };
   
})