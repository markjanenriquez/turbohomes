'use strict';

var app = angular.module('app', [
    'angular-storage',
    'ui.bootstrap',
    'angularFileUpload',
    'angular-jwt',
    'ui.router',
    'ngPasswordStrength',
    'angularMoment',
    'ngAnimate',
    'ui.mask',
    'google.places',
    'satellizer',
    'angular-web-notification',
    'vcRecaptcha',
    'angularUUID2',
    'toaster',
    'ngAnimate'
])

.config(function($httpProvider, jwtInterceptorProvider, $authProvider, Config, $interpolateProvider, $controllerProvider, $compileProvider, $provide, $stateProvider, $urlRouterProvider, jwtOptionsProvider) {


    app.controller = $controllerProvider.register;
    app.directive = $compileProvider.directive;
    app.factory = $provide.factory;
    app.service = $provide.service;
    app.constant = $provide.constant;
    app.value = $provide.value;


    jwtOptionsProvider.config({
      whiteListedDomains: ['turbohomes.dev', 'turbohomes.com', 'turbohomes.iphitech.com' ]
    });

    jwtInterceptorProvider.tokenGetter = function(jwtHelper, $http, $auth) {

        var access_token = $auth.getToken();
        if (access_token) {
            if (jwtHelper.isTokenExpired(access_token)) {
                return $http({
                    url: Config.BaseURL + '/api/user/token',
                    method: 'GET',
                    headers: {
                        Authorization: 'Bearer ' + access_token
                    },
                }).then(function(response) {
                    $auth.setToken(response.data.token);
                    return response.data.token;
                }, function(response) {
                    var error = response.data.error;
                    if (error === "token_blacklisted") {
                        return $auth.getToken();
                    } else {
                        $auth.removeToken();
                    }
                });

            }
            return access_token;
        }
    };

    $httpProvider.interceptors.push('jwtInterceptor');


    // Satellizer configuration that specifies which API
    // route the JWT should be retrieved from
    $authProvider.loginUrl = Config.BaseURL + '/api/user/login';
    $authProvider.tokenPrefix = 'fe_access';

    $interpolateProvider.startSymbol('{[{');
    $interpolateProvider.endSymbol('}]}');

    //  $urlRouterProvider
    // .otherwise('/dashboard');

    // $stateProvider


    // .state("buyer_dashboard", {
    //   url: '/dashboard',
    //   templateUrl: 'libs/houzez-child/template/buyer_dashboard.php',
    // })
    // .state("make_an_offer", {
    //   url: '/make_an_offer',
    //   templateUrl: 'libs/houzez-child/template/make_offer.html',
    //})



})