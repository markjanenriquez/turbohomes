<ul id="main_buyer_menu" class="nav nav-tabs seller_menu">
	<li class="active "><a data-toggle="tab" href="#buyer_menu1" data-id="1">1</a> <span>Get Prequalified</span></li>
	<li><a data-toggle="tab" href="#buyer_menu2" data-id="2">2</a> <span>Find a Home</span></li>
	<li><a data-toggle="tab" href="#buyer_menu3" data-id="3">3</a> <span>Make an Offer</span></li> 
	<li><a data-toggle="tab" href="#buyer_menu4" data-id="4">4</a> <span>Offer Accepted</span></li>
	<li><a data-toggle="tab" href="#buyer_menu5" data-id="5">5</a> <span>Due Diligence</span></li>
	<li><a data-toggle="tab" href="#buyer_menu6" data-id="6">6</a> <span>Closing &amp; Settlement</span></li>
</ul>

<div class="tab-content">
	<div id="buyer_menu1" class="tab-pane fade in active">		
		
		<div class="menu_header">
			<h3>Prequalify for a home loan</h3>
		</div>
		
		<div class="banner_dashboard">
			<img src="libs/houzez-child/images/Prequalify.jpg" />
		</div>
		
		<ul class="nav nav-tabs seller_menu_inner1">
			<li class="active"><a data-toggle="tab" href="#inner1_buyer_menu1">Why Prequalify</a></li>
			<li><a data-toggle="tab" href="#inner1_buyer_menu2">Price</a></li>									
			<li><a data-toggle="tab" href="#inner1_buyer_menu3">Credit Score</a></li>									
			<li><a data-toggle="tab" href="#inner1_buyer_menu4">Down Payment</a></li>									
			<li><a data-toggle="tab" href="#inner1_buyer_menu5">Select a Lender</a></li>	
		</ul>
		
		<div class="tab-content">
			
			<div id="inner1_buyer_menu1" class="tab-pane fade in active">
				<h4>Why get prequalified</h4>
				<p>One of the first things you need to do is get prequalified. Here are some of the benefits:</p>
				
				<ul>
					<li><strong>Serious Buyer</strong> - Sellers will take an offer more seriously when you are prequalified.</li>
					<li><strong>Maximum Price Point</strong> - Your lender can help you understand how much you can spend on the house.</li>
					<li><strong>Easy and Quick</strong> - Once you find the house you want to buy if you are prequalified it makes the process much easier and quicker.</li>
				</ul>
			</div>
			
			<div id="inner1_buyer_menu2" class="tab-pane fade">					
				<h4>How much should you spend?</h4>
				
				<p>Goal: Amount I can comfortably afford and still meet my goals. NOT how much will the lender give me.</p>				
				<p>
					Loan Principle <br />
					+ Loan Interest <br />
					+ Private Mortgage Insurance (PMI)* <br />
					+ Property Tax <br />
					+ Hazard Insurance <br />
					<span class="top-border">+ Home Owner’s Association Dues</span> <br />					
					<strong>Monthly Payment</strong>
				</p>
				<p>*PMI is typically required when putting less than 20% down. Your lender can help you understand these costs.</p>				
			</div>
			
			<div id="inner1_buyer_menu3" class="tab-pane fade">					
				<h3>Know Your Credit Score</h3>
				<p>If you know your credit score than great it will help, otherwise, let the first lender you contact pull it so you can find out what it is. Once you know your score you can get other lenders to give you quotes much quicker. I’m less concerned about the number of lenders pulling my score than I am about the time it takes for me to stay on the phone while they pull it. </p>
			</div>
			
			<div id="inner1_buyer_menu4" class="tab-pane fade">					
				<h3>Know Your Down Payment</h3>
				<p>To get quick quotes from lenders you will need to know how much you want to put down. Lenders can help you determine this. If you don’t have a set amount ask them to quote you for a few different down payment amounts. </p>
				<p>Jumbo Loan = Typically a loan of $417,000 or more. This will cost you more in rate or required down payment.</p>
				
				<img src="libs/houzez-child/images/down_pay_img.jpg" width="100%" />
			</div>
			
			<div id="inner1_buyer_menu5" class="tab-pane fade">					
				<h3>How to find a lender</h3>
				<p><strong>Trusted Lender</strong> - Find a lender you can trust. We have several great lenders in our Provider Marketplace or you can find many online. Check out their reviews. Rate and low fees. Often times you can save money using an online lender but many still prefer someone local.</p>
				<p><strong>What to say to prospective lenders</strong>: Lenders are like car salesmen. When you call to get a quote, some lenders will try to get you to go through the long application process so they get you invested in their process. Don’t let them. It’s not worth your time. </p>
				<p>For example, if you know you want a conventional loan you can tell the lender:</p>
				<p class="indented">Hi I’m looking for just a quick quote on a $400,000, 30-year mortgage. I have a 720-median credit score and I plan to put 3% down. Could you tell me:</p>
				<ul class="indented">
					<li>Interest Rate (APY includes the fees so it’s good for comparing lenders)</li>
					<li>All your lender fees and charges. (You want to know any lender fees, like the origination fee and if their rate includes any buy down costs.)</li>
					<li>Appraisal cost (This tends to be paid once you have an accepted offer to buy and is non-refundable if the sale falls through)</li>
				</ul>
				<p>Once you find the lender you like then have them prequalify you.</p>
				<p>In order to be pre-qualified, you will typically need to provide the lender with the following: </p>
				<ul>
					<li>Your residence history for the past two years</li>
					<li>Your employment history with dates and business addresses </li>
					<li>Two years of tax returns and W-2s (or profit and loss statements if self-employed) </li>
					<li>Copies of three months of statements for all bank and brokerage accounts, loans, credit cards, pensions, etc. </li>
					<li>Details of any real estate, vehicles, or other personal property you own, including loan balances and market value</li>
				</ul>
			</div>
			
		</div>
	</div>
	
	<div id="buyer_menu2" class="tab-pane fade">
		<div class="menu_header">
			<h3>Find a home to buy</h3>
		</div>			
		
		<div class="banner_dashboard">
			<img src="libs/houzez-child/images/Find.jpg" />			
		</div>
		
		<ul class="nav nav-tabs seller_menu_new1">
			<li class="active"><a data-toggle="tab" href="#inner1_buyer_new1">Home Search</a></li>
			<li><a data-toggle="tab" href="#inner1_buyer_new2">Set an appointment to tour </a></li>
		</ul>	
		
		<div class="tab-content">			
			<div id="inner1_buyer_new1" class="tab-pane fade in active">
				<p>With Turbo you can search for your next home on any site or by driving around. When agents list properties for sale in the MLS (Multiple Listing Service) they almost always will choose to allow it to be shared with 3rd party sites. This is how you can see pretty much any house for sale on your favorite site.</p>
			</div>
			
			<div id="inner1_buyer_new2" class="tab-pane fade">
				<p><strong>Call or text the seller</strong> - Once you find a house you want to see simply call or text the seller’s real estate agent or the seller directly to setup a time to tour the house.</p>
				<p><strong>Give seller time to get ready</strong> - You usually want to give at least a few hours or even a day’s notice when possible. Don’t be demanding. It’s not nice.</p>
				<p>You can let the seller know that you are not using an agent and that you have a Turbo Homes attorney to help with contracts and the buying process. They should be more than happy to show you the property.</p>
				<p><strong>Example text to setup a time to tour a house:</strong></p>
				<p class="indented">
					Hi Mark (Seller’s name), I would like to view the house you have listed for sale at 555 S. Main St. in Chandler. Would today between 4-5pm or tomorrow between 2-4pm work? I am not using an agent so I would need a way to access the house. Thanks! <br /><br />
					{[{fullname}]}
				</p>
				
			</div>
		</div>
	</div>
	
	<div id="buyer_menu3" class="tab-pane fade">
		
		<div class="menu_header">
			<h3>Make an offer on a home</h3>
		</div>
		
		<div class="banner_dashboard">
			<img src="libs/houzez-child/images/MakeOffer1.jpg" />
		</div>
		
		<ul class="nav nav-tabs seller_menu_inner3">
			<li class="active"><a data-toggle="tab" href="#inner3_buyer_menu1">How much to offer</a></li>
			<li><a data-toggle="tab" href="#inner3_buyer_menu2">Make an offer</a></li>
		</ul>
		
		<div class="tab-content">
			
			<div id="inner3_buyer_menu1" class="tab-pane fade in active">
				<p><strong>It is not just about price</strong> – Sellers want to know the deal will go through and on time. Here are some things that can entice them.</p>
				<ul>
					<li>High down payment, all cash offer, or not contingent on getting financing</li>
					<li>Not contingent on the sale of your house</li>
					<li>A quick close or flexible to the seller’s timing</li>
				</ul>
				<p><strong>Price</strong> – Determine the value of the home based on market comparable properties.</p>
				<ul>
					<li><strong>Property Value Report</strong> - Go to the Property Value tab to get a value report </li>
					<li><strong>Best Comparable Properties</strong> – Select the homes that have recently sold that are most like this home. Here are some key factors:</li>	
					<li class="no-bullets inside">
						<ul>
							<li><strong>Sold Date</strong> - The more recently sold the better, typically within 3 to 6 months</li>
							<li><strong>Square Feet</strong> – Typically within 500 square feet is a good rule of thumb</li>
							<li><strong># of Stories</strong> – One-story houses tend to have higher value per square foot than similar two-story houses. Only compare one-story houses with one-story houses.</li>
							<li><strong># of Bathrooms</strong> – More is better.</li> 
							<li><strong>Lot Size</strong> – Only if it is significantly different otherwise you can ignore this.</li>
							<li><strong>Location</strong> – For example, if one of the houses backs a busy street then it should be valued lower.</li>
							<li><strong>General Quality</strong> – Only pick homes that have similar level of quality. Typically if you have comparable properties within the same neighborhood by the same builder then you are off to a good start. Kitchens can be the best place to see the difference.</li>
							<li><strong>Pool</strong> – Appraisers often give limited value for a pool. Sometimes 50% of the cost.</li>
						</ul>
					</li>
					<li><strong>Adjust the Value</strong> -  Adjust the value of the comparable properties to fit your property. For example, if the comparable property is a little bigger than you would reduce its value to better fit the home you are valuing using price per square foot. You don’t need to go crazy with this. Make sure you get similar homes to compare to and then make small adjustments.</li>	 
					<li><strong>Ceiling Price</strong> – This is simple. Don’t pay more for a house that you can buy across the street for less. If a comparable house is currently listed for sale then you should not pay significantly more than that price.</li>
				</ul>
			</div>
			
			<div id="inner3_buyer_menu2" class="tab-pane fade">
				<p>Making an offer with Turbo is easy. Turbo will handle all the contracts and negotiations for you.</p>
				
				<p><strong>Step 1. Navigate to the Make an Offer Tool</strong> - The tool will take you step by step through creating the offer and send you the offer via email for final review </p>
				<p><strong>Step 2. Ask Questions</strong> - If you have questions about the offer or want to discuss it with a Turbo attorney then we are here. You can request help within the Make an Offer Tool or can just call us directly.</p>
				
				<p><strong>Step 3. Sign Electronically</strong> – Upon your final review of the offer you will be prompted to sign it electronically. This is every bit as valid as your actual signature but much more convenient.</p> 
				<p><strong>Step 4. Negotiate and Send Offer Automatically to Seller</strong> – Upon your final review and signature of the offer it will be automatically sent to the seller. If desired, a Turbo attorney will call the seller to present and negotiate your offer.</p>
				<p>If the seller replies with a counteroffer Turbo can help you understand the terms and will create the response on your behalf. It is easy when Turbo has your back.</p>
			</div>			
		</div>
	</div>
	
	<div id="buyer_menu4" class="tab-pane fade">
		
		<div class="menu_header">
			<h3>Offer Accepted</h3>
		</div>
		
		<div class="banner_dashboard">
			<img src="libs/houzez-child/images/SubmitLoan.jpg" />
		</div>
		
		<ul class="nav nav-tabs seller_menu_inner4">
			<li class="active"><a data-toggle="tab" href="#inner4_buyer_menu1">Select a Title Company</a></li>
			<li><a data-toggle="tab" href="#inner4_buyer_menu2">Open Escrow</a></li>	
			<li><a data-toggle="tab" href="#inner4_buyer_menu3">Submit Loan</a></li>	
			<li><a data-toggle="tab" href="#inner4_buyer_menu4">Appraisal</a></li>	
		</ul>
		
		<div class="tab-content">
			
			<div id="inner4_buyer_menu1" class="tab-pane fade in active">
				<p>We have some great title companies to choose from in the Provider Marketplace or you can pick your own. Select one that seems easy to work with and has a good reputation. Their fees tend to be very competitive but don’t be afraid to compare. </p>
				<ul>
					<li>Easy to work with and helpful</li>
					<li>Location of office or will they come to your house for doc signings</li>
					<li>Cost of title insurance</li>
					<li>Other fees (document, processing, recording fees)</li>
				</ul>
				<p>You can select any title agency you would like. It does not have to be the same agency that the seller uses but you and the seller can typically save some on fees if you both use the same agency. </p>
				<p>The title agency provides:</p>
				<ul>
					<li>Title insurance to help ensure the seller has the legal right to sell you the property</li> 
					<li>Assurance all necessary documents are provided and signed by both buyer and seller</li> 
					<li>Answers to questions about closing documents and settlement/funding process</li> 
					<li>Assurance the money has been exchanged appropriately</li> 
					<li>Deed recording</li> 
				</ul>
			</div>
			
			<div id="inner4_buyer_menu2" class="tab-pane fade">
				<p>As soon as you have an agreed upon contract to purchase the house you should open escrow at the title company of your choice. This means you submit to the title company: </p>				
				<ul>
					<li>The signed contract </li>
					<li>Earnest money</li>
				</ul>
				<p>Then the title company gets to work. There should be no upfront fees for this service. They are paid at closing.</p>
			</div>			
			
			<div id="inner4_buyer_menu3" class="tab-pane fade">
				<p>Once your offer to buy is accepted then submit your final loan application right away and provide your lender a copy of your signed contract and addenda. If you were prequalified it will make this step much easier. They will walk you through the rest of the loan process.</p>
				
				<p>The purchase contract also requires that you provide periodic updates on the loan to the seller. The Turbo automated reminders will help you stay on top of all the details.</p>
			</div>	
			
			<div id="inner4_buyer_menu4" class="tab-pane fade">
				<p>Lenders require that a professional appraisal is performed on the property. If the appraisal is too low then it could reduce how much the lender is willing to lend against the property. If the appraisal comes in lower than the sales price, you and the seller will still need to abide by the purchase contract. </p>
				<p>If your contract is contingent on an appraisal, one option you have is to withdraw your offer and have your earnest money deposit returned.  The appraisal can save you from paying too much for the home.</p>
				<p>Here are some additional options if you still want to buy the home, despite the low appraisal.</p>
				<ul>
					<li>Challenge the appraisal with supporting documentation like better comparable property sales</li>
					<li>Pay for a second appraisal, which may or may not come in higher (get lender approval prior to doing this)</li>
					<li>Come up with extra cash to make up the difference between the appraised value and your purchase price </li>
					<li>Renegotiate the contract, if the seller is willing </li>
					<li>Ask the seller to finance the gap between the appraisal and the sales price</li>
				</ul>
				<p>If you want to discuss your options Turbo is here to help.</p>
			</div>	
		</div>
	</div>
	
	<div id="buyer_menu5" class="tab-pane fade">
		
		<div class="menu_header">
			<h3>Due Diligence</h3>
		</div>
		
		<div class="banner_dashboard">
			<img src="libs/houzez-child/images/Inspect.jpg" />
		</div>
		
		<ul class="nav nav-tabs seller_menu_inner5">
			<li class="active"><a data-toggle="tab" href="#inner5_buyer_menu1">Contract Dates</a></li>
			<li><a data-toggle="tab" href="#inner5_buyer_menu2">Disclosures</a></li>
			<li><a data-toggle="tab" href="#inner5_buyer_menu3">Restrictions</a></li>
			<li><a data-toggle="tab" href="#inner5_buyer_menu4">Inspection</a></li>
			<li><a data-toggle="tab" href="#inner5_buyer_menu5">Insurance</a></li>
		</ul>
		
		<div class="tab-content">
			
			<div id="inner5_buyer_menu1" class="tab-pane fade in active">
				<h4>Key Contract Dates</h4>
				<p>Now that you are under contract to buy there are various deadline dates within the contract that you must adhere to. Turbo’s Contract Dashboard and Automated Reminders will help you know what to do at each stage.</p>
				<p><strong>Contract Dashboard</strong> – The dashboard is a great reference for you. It shows all the dates and deadlines required in the contract. It includes what you need to do and what you should expect from the seller.</p>
				<p><strong>Automated Reminders</strong> – We will send you automated text and/or emails to remind you of upcoming requirements.</p> 
			</div>
			
			<div id="inner5_buyer_menu2" class="tab-pane fade">	
				<h4>Seller Property Disclosures</h4>
				<p>Within a few days of going under contract the seller should typically provide you the Seller Property Disclosures. Review this closely.  Ask to see receipts for repairs to the home. The purchase contract will indicate the deadline for challenging the seller’s disclosure report or for having your own inspections conducted. </p>
			</div>
			
			<div id="inner5_buyer_menu3" class="tab-pane fade">	
				<h4>Deed Restrictions</h4>
				<p>Read the deed restrictions, also called CC&Rs (covenants, conditions and restrictions).  You might find some of the CC&Rs are very strict, especially those addressing landscaping, RV parking, play equipment, satellite antennas, and other common amenities - particularly if the subdivision is governed by a homeowner’s association.</p>
			</div>
			
			<div id="inner5_buyer_menu4" class="tab-pane fade">	
				<h4>Home Inspection</h4>
				<p>Most contracts will allow you a period of time (usually 10-14 days) to inspect the home and back out of the deal or request repairs if it is not to your satisfaction. </p>
				<p>We always recommend you hire a professional to perform a home inspection and to consider additional inspections. Our favorite inspectors can be found in the Provider Marketplace. Common inspections include:</p>
				<ul>
					<li>General</li>
					<li>Structural Pest Control (termite)</li> 
					<li>Chimney </li> 
					<li>Hazardous Materials </li> 
					<li>HVAC</li> 
					<li>Well &amp; Septic</li> 
				</ul>
				<p>Here are some simple things you can do to make sure the house is in good shape.</p>
				<ul>
					<li>Check all appliances to confirm that they work including the stove burners, oven, garbage disposal, dishwasher, washer and dryer and the water heater  </li>
					<li>Run water in all sinks and tubs</li>
					<li>Flush the toilets to make sure they drain properly  </li>
					<li>Check to see if the irrigation system works and landscaping drains properly </li>
					<li>Look behind large pictures on the wall and behind anything on the floor which conceals large areas of the wall </li>
					<li>Look for stains on the ceilings or carpets that might indicate water damage  </li>
					
				</ul>
			</div>
			
			<div id="inner5_buyer_menu5" class="tab-pane fade">	
				<h4>Homeowner’s Insurance</h4>
				<p>If you have decided to get homeowner’s insurance you will want to set that up during the inspection period to ensure you can get insurance for a reasonable price. You can also then set it up so you have coverage on day one with your new house. Make sure you understand what your insurance covers and does not cover and any deductible requirements.</p>
			</div>
		</div>
	</div>
	
	<div id="buyer_menu6" class="tab-pane fade">		
		<div class="menu_header">
			<h3>Closing &amp; Settlement</h3>
		</div>
	
	<div class="banner_dashboard">
		<img src="libs/houzez-child/images/Closing.jpg" />
	</div>
	
	<ul class="nav nav-tabs seller_menu_inner6">
		<li class="active"><a data-toggle="tab" href="#inner6_buyer_menu1">Closing &amp; Settlement Process</a></li>										
		<li><a data-toggle="tab" href="#inner6_buyer_menu2">Closing Costs</a></li>									
	</ul>
	
	<div class="tab-content">
		
		<div id="inner6_buyer_menu1" class="tab-pane fade in active">
			<p><strong>HUD 1 Statement</strong> - Typically, three days before closing the sale you should receive a HUD 1 statement that lays out how all the monies will be dispersed. This will show you exactly what fees, expenses and incoming or outgoing funds are for or required of you. Review this in detail and don’t be afraid to ask questions.</p>		
			
			<p><strong>If You Owe Money</strong> – Discuss with the Title Agency your options to pay. Typically, a wire to the Title Agency or a cashier’s check drawn on a local FDIC insured bank</p>
			
			<p><strong>The Closing Meeting and Settlement</strong> – This typically takes place at the title agency but can be done remotely with a notary. This is when you get to sign a lot of documents! The bulk of the papers are your loan documents if you’re getting a loan. The title agent will walk you through all the paperwork and help answer questions you may have. The same or another title agency will work with the seller as well to have their part of the documents signed and that monies are transferred.</p>
			
			<p>If you are unsure about closing procedures, ask questions. The Title Agency is very helpful.</p>
			<p>Once all the documents are signed the deal is still not done. You must wait until you receive confirmation of settlement or in other words that the funds have been exchanged. Then you will receive your keys!</p>
		</div>			
		
		<div id="inner6_buyer_menu2" class="tab-pane fade">
			<p>On the HUD 1 statement your closing costs will be detailed out for you. Review these and question any that seem strange. Here is a list of typical closing costs and what they are.</p>
			
			<ul>
				<li><strong>APPRAISAL FEE</strong> - This is a one-time fee that pays for an appraisal, a statement of property value required on most loans.  The appraisal is made by an independent appraiser.</li>
				<li><strong>CREDIT REPORT FEE</strong> - This one-time fee covers the cost of a credit report processed by an independent credit reporting agency.</li>
				<li><strong>DOCUMENT PREPARATION FEE</strong> - There may be a separate, one-time fee that covers preparation of the final legal papers, including the note and deed of trust.</li>
				<li><strong>LOAN DISCOUNT</strong> - Often called "Points", a loan discount is a one-time charge used to adjust the yield on the loan to what market conditions demand.  One point is equal to 1% of the loan amount.</li>
				<li><strong>LOAN ORIGINATION FEE</strong> - This fee covers the Lender’s administrative costs in processing the loan.  It is a one-time fee and is generally expressed as a percentage of the loan amount.</li>
				<li><strong>MISCELLANEOUS TITLE CHARGES</strong> - The title company may charge fees for a title search, title examination, document preparation, notary fees, recording fees, and a settlement or closing fee.  These are all one-time charges.</li>
				<li><strong>MORTGAGE INSURANCE PREMIUM</strong> - Depending on the amount of your down payment, you may be required to pay a fee or mortgage insurance (which protects the Lender against loss due to foreclosure).  You may also be required to put a certain amount for mortgage insurance into a special reserve account (called an impound account) held by the Lender. </li>
				<li><strong>PREPAID INTEREST</strong> - Depending on the day of the month your loan closes, this charge may vary from a full month to just a few days interest. If your loan closes at the beginning of the month, you will probably have to pay the maximum amount.  If your loan closes near the end of the month, you will only have to pay a few days interest.  Your first payment will usually be 30 days after the date pre-paid interest is paid through.</li>
				<li><strong>TAXES AND HAZARD INSURANCE</strong> - Based on the month you close, property taxes will be prorated between you and the Seller.  You will also need to pay an entire year’s hazard insurance premium upfront (Homeowner’s Insurance).  In addition, you may be required to put a certain amount for taxes and insurance into a special reserve account (impound account) held by the Lender.</li>
				<li><strong>TITLE INSURANCE FEES</strong> - There are two title policies; a Buyer’s title policy (which protects the new homeowner) and a Lender’s title policy (which protects the Lender against loss due to a defect in the title). These are both one-time fees.</li>
			</ul>
		</div>
	</div>
	</div>
</div>					