<div ng-controller="propertyvalueCtrl as proctrl" style="overflow: hidden;">
	

	<div class="col-sm-12">						
		
		<div class="property_search_container">			
			
			<h3 class="text-center">Enter the address of the property you want to value.</h3>
			<div class="form-group">
				<!-- <input id="autocomplete" placeholder="Enter your address" onFocus="geolocate()" ng-model="myScopeVar" type="text" class="form-control"  ng-change="proctrl.property.submit(myScopeVar)"/>	 -->

				<input type="text" g-places-autocomplete ng-model="myScopeVar" class="form-control"/>
			</div>
			<div class="text-center">
				<!-- <button type="button" class="btn btn-primary sc_get_property_report_btn" ng-click="proctrl.property.submit(myScopeVar)">Get Report</button> -->
				<button type="button" class="btn btn-primary bntorange" ng-click="proctrl.property.submit(myScopeVar)" ng-if="proctrl.property.onprogress == false">Get Report</button>
			</div>
			
			<div id="sc_spinner_container" ng-if="proctrl.property.onprogress">
				<div class="spinner">
					<div class="bounce1"></div>
					<div class="bounce2"></div>
					<div class="bounce3"></div>
				</div>
			</div>
		</div>
	</div>

	<br class="clear" />

	<form id="sc_zillow_form">
		<input type="hidden" name="action" value="sc_get_deep_comps" ng-model="proctrl.property.data.sc_get_deep_comps"/>
		<input type="hidden" id="street_number" name="street_number" ng-model="proctrl.property.data.street_number"/>
		<input type="hidden" id="route" name="route" ng-model="proctrl.property.data.route"/>
		<input type="hidden" id="locality" name="locality" ng-model="proctrl.property.data.locality"/>
		<input type="hidden" id="administrative_area_level_1" name="administrative_area_level_1" ng-model="proctrl.property.data.administrative_area_level_1"/>
		<input type="hidden" id="postal_code" name="postal_code" ng-model="proctrl.property.data.postal_code"/>
		<input type="hidden" id="country" name="country" ng-model="proctrl.property.data.country"/>
	</form>

	<br />
	
	<div class="col-sm-12">
		<!-- {[{proctrl.property.samplemsg}]} -->
		<div id="display_data" class="panel panel-default">
			<!-- <div class="panel-body ng-cloak" ng-bind-html="proctrl.property.msg" ng-if="proctrl.property.showresult"></div> -->
			<div class="panel-body ng-cloak" ng-show="proctrl.property.showresult"></div> 
			
			<div id="sc_zillow_msg_container">									
				<div class="alert alert-danger no-margin ng-cloak" ng-if="proctrl.property.showerror">No Result Found on this address!</div>
			</div>
		</div>
	</div>

</div>

