<div class="sc_dashboard menu_page_container">
	
	<p class="text-center"><strong>Important reminders to help you stay on top of what needs to be done.</strong></p>
	
	<div id="immediate_container">
		<h3>Immediate Action Needed</h3>
		<table class="table table-bordered">
			<thead>
				<th>Due Date</th>
				<th>Action</th>
				<th>Description</th>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>		
	
	<br />
	
	<div id="soon_container">
		<h3>Coming Soon</h3>
		<table class="table table-bordered">
			<thead>
				<th>Due Date</th>
				<th>Action</th>
				<th>Description</th>
			</thead>
			<tbody>
				<tr>
					<td></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
	</div>	
</div>			
