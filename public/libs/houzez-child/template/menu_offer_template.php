<?php 
	/**
		* Template Name: Dashboard - Make An Offer
		* Created by Sigfred Chamo 
	*/
	global $post;	
?>

<?php 
	get_header(); 
	get_template_part( 'template-parts/page', 'title' );		
	
	if ( ! is_user_logged_in() ) {
		wp_redirect( home_url('/login/') );
	}
	
	$user_id = get_current_user_id();	
	$user_role = get_user_role(get_current_user_id());		
?>

<section class="section-detail-content default-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="page-main">
					<div class="article-detail sc_dashboard_container">
						
						<?php include('utilities.php'); ?>						
						
						<div class="sc_dashboard menu_page_container">
							
							<p>Content Coming Soon</p> 						
							
						</div>	
						
					</div>					
				</div>
			</div>
			</div>
		</div>
		</section
		
<?php get_footer();	?>			