<div class="sc_dashboard menu_page_container">
	
	<ul id="make_offer_menu_container" class="navoffer nav-tabs">
		<li class="active"><a data-toggle="tab" href="#offer_section1" data-id="1" class="property">Property</a></li>		
		<li><a data-toggle="tab" href="#offer_section2" data-id="2" class="disabled financing">Financing</a></li> 
		<li><a data-toggle="tab" href="#offer_section3" data-id="3" class="disabled import_dates">Import Dates</a></li>
		<li><a data-toggle="tab" href="#offer_section4" data-id="4" class="disabled inclusion">Inclusions</a></li>
	</ul>
	
	<div class="tab-content">
		<div id="offer_section1" class="tab-pane fad in active">	
			
			<form id="make_offer_property_form" >
				
				<div id="property_section_one" class="col-sm-12">	
				
					<h3>Step 1</h3>
					
					<div class="form-group">
						<label for="sc_property_address">Property's Address:</label>										
						<input type="text" name="sc_property_address" id="sc_property_address" class="form-control" placeholder="Enter the address of the home you want to buy"  />
						
					</div>
					
					<div class="text-right">
						<button class="btn-primary btn btn-sm property_section_btn"> Next </button>
					</div>
				</div>
				
				<div id="property_section_two" class="col-sm-12">	
					
					<h3>Step 2</h3>
				
					<div class="form-group">
						<label>Is the house listed by:</label>
						<ul id="listed_menu" class="navoffer nav-tabs">
							<li><a data-toggle="tab" href="#listed_menu1" data-answer="turbo">Turbo Homes</a></li>
							<li><a data-toggle="tab" href="#listed_menu2" data-answer="realtor">Realtor</a></li>
							<li><a data-toggle="tab" href="#listed_menu3" data-answer="other">Other</a></li> 		
							<input type="hidden" name="listed_menu_asnwer" id="listed_menu_asnwer" />
						</ul>
						
						<div class="tab-content">
							<div id="listed_menu1" class="tab-pane fade">	
								<div class="form-group">
									<label for="sc_seller_name">Seller's Name:</label>										
									<input type="text" name="sc_seller_name" id="sc_seller_name" class="form-control"  />
								</div>
								
								<div class="form-group">
									<label for="sc_seller_address">Seller's Email:</label>										
									<input type="text" name="sc_seller_address" id="sc_seller_address" class="form-control" />
								</div>
								
								<div class="text-right">
									<button class="btn-primary btn btn-sm listed_menu1_btn"> Next </button>
								</div>
							</div>
							
							<div id="listed_menu2" class="tab-pane fade">	
								<div class="form-group">
									<label for="sc_realtor_name">Realtor's Name:</label>										
									<input type="text" name="sc_realtor_name" id="sc_realtor_name" class="form-control"  />
								</div>
								
								<div class="form-group">
									<label for="sc_realtor_address">Realtor's Email:</label>										
									<input type="text" name="sc_realtor_address" id="sc_realtor_address" class="form-control" />
								</div>
								
								<div class="form-group">
									<label for="sc_brokerage_address">Brokerage Name:</label>										
									<input type="text" name="sc_brokerage_address" id="sc_brokerage_address" class="form-control" />
								</div>
								
								<div class="text-right">
									<button class="btn-primary btn btn-sm listed_menu2_btn"> Next </button>
								</div>
							</div>
							
							<div id="listed_menu3" class="tab-pane fade">	
								<div class="form-group">
									<label for="sc_other_seller_name">Seller's Name:</label>										
									<input type="text" name="sc_other_seller_name" id="sc_other_seller_name" class="form-control"  />
								</div>
								
								<div class="form-group">
									<label for="sc_other_seller_address">Seller's Email:</label>										
									<input type="text" name="sc_other_seller_address" id="sc_other_seller_address" class="form-control" />
								</div>
								
								<div class="text-right">
									<button class="btn-primary btn btn-sm listed_menu3_btn"> Next </button>
								</div>
							</div>
						</div>
						
						<div class="text-right">
							<br />
							<button class="btn-default btn btn-sm back_to_one_btn"> Back </button>
						</div>
						
					</div>
				</div>
				
				<div id="property_section_three" class="col-sm-12">	
				
					<h3>Step 3</h3>
					
					<div class="form-group">
						<label>Are you buying the house alone or with someone else?</label>
						<ul id="section_buying_menu" class="navoffer nav-tabs">
							<li><a data-toggle="tab" href="#section_buying_menu1" data-answer="alone">Alone</a></li>
							<li><a data-toggle="tab" href="#section_buying_menu1" data-answer="someone">With Someone</a></li>
							<input type="hidden" name="section_buying_menu_asnwer" id="section_buying_menu_asnwer" />
						</ul>
						
						<div id="someone_radiobox_container">
							<div class="btn-group" data-toggle="buttons">
								<label class="btn btn-default">
									<input type="radio" name="someone_radiobox" class="someone_radiobox_btn" value="spouse" /> Spouse
								</label>
								<label class="btn btn-default">
									<input type="radio" name="someone_radiobox" class="someone_radiobox_btn" value="partner" /> Partner
								</label>
								<label class="btn btn-default">
									<input type="radio" name="someone_radiobox" class="someone_radiobox_btn" value="relative" /> Relative
								</label>
								<label class="btn btn-default">
									<input type="radio" name="someone_radiobox" class="someone_radiobox_btn" value="other" /> Other
								</label>
								<input type="hidden" id="someone_radiobox_answer" />
							</div>
						</div>
						
						<div class="tab-content">
						
							<div id="section_buying_menu1" class="tab-pane fade">									
								
								<label>Do you need to sale your current home to buy this house?</label>
								<ul id="section_buying_menu2" class="navoffer nav-tabs">
									<li><a data-toggle="tab" href="#section_buying_yes" data-answer="yes">Yes</a></li>
									<li><a data-toggle="tab" href="#section_buying_no" data-answer="no">No</a></li>
									<input type="hidden" name="section_buying_menu2_asnwer" id="section_buying_menu2_asnwer" />
								</ul>
								
								<div class="tab-content">
									<div id="section_buying_yes" class="tab-pane fade">		
										<div class="form-group">
											<label for="sc_current_address">Your current adddress:</label>										
											<input type="text" name="sc_current_address" id="sc_current_address" class="form-control" />
										</div>
										
										<div class="form-group">
											<label for="sc_current_address">Have you listed the home for sale yet?</label>		
											<ul id="section_listed_menu" class="navoffer nav-tabs">
												<li><a data-toggle="tab" href="#section_listed_yes" data-answer="yes">Yes</a></li>
												<li><a data-toggle="tab" href="#section_listed_no" data-answer="no">No</a></li>
												<input type="hidden" name="section_listed_answer" id="section_listed_answer" />
											</ul>
											
											<div class="tab-content">
												<div id="section_listed_yes" class="tab-pane fade">	
													<label for="sc_current_address">With whom?</label>		
													<ul id="section_whom_menu" class="navoffer nav-tabs">
														<li><a data-toggle="tab" href="#whom_none" data-answer="turbohomes">Turbo Homes</a></li>
														<li><a data-toggle="tab" href="#whom_menu" data-answer="realtor">Realtor</a></li>
														<li><a data-toggle="tab" href="#whom_none" data-answer="self">Self</a></li>
														<li><a data-toggle="tab" href="#whom_none" data-answer="other">Other</a></li>
														<input type="hidden" name="section_whom_answer" id="section_whom_answer" />
													</ul>
													
													<div class="tab-content">
														<div id="whom_none" class="tab-pane fade">	
															<div class="text-right">
																<button class="btn-primary btn btn-sm whom_none_btn"> Next </button>
															</div>
														</div>
														
														<div id="whom_menu" class="tab-pane fade">	
															<div class="form-group">
																<label for="sc_whom_realtor_name">Name:</label>										
																<input type="text" name="sc_whom_realtor_name" id="sc_whom_realtor_name" class="form-control"  />
															</div>
															
															<div class="form-group">
																<label for="sc_whom_realtor_brokerage">Brokerage:</label>										
																<input type="text" name="sc_whom_realtor_brokerage" id="sc_whom_realtor_brokerage" class="form-control" />
															</div>
																
															<div class="form-group">
																<label for="sc_whom_list_date">When did you list it?</label>										
																<input type="text" name="sc_whom_list_date" id="sc_whom_list_date" class="form-control"  />
															</div>
															
															
															<div class="form-group">
																<label for="sc_whom_must_sell_date">When must you sell the home?</label>										
																<input type="text" name="sc_whom_must_sell_date" id="sc_whom_must_sell_date" class="form-control"  />
															</div>																
															
															<div class="text-right">
																<button class="btn-primary btn btn-sm whom_btn"> Next </button>
															</div>
														</div>										
													</div>										
												</div>
												
												<div id="section_listed_no" class="tab-pane fade">	
													<label>Sale your home with TurboHomes</label>		
													<ul id="section_sell_th_menu" class="navoffer nav-tabs">											
														<li><a data-toggle="tab" href="#sell_th_menu" data-answer="yes">Yes</a></li>
														<li><a data-toggle="tab" href="#sell_none_menu" data-answer="no">No</a></li>
														<input type="hidden" name="section_sell_th_answer" id="section_sell_th_answer" />
													</ul>
													
													<div class="tab-content">
														<div id="sell_none_menu" class="tab-pane fade">	
															<div class="text-right">
																<button class="btn-primary btn btn-sm sell_none_btn"> Next </button>
															</div>
														</div>
														
														<div id="sell_th_menu" class="tab-pane fade">																
															<div class="form-group">
																<label for="sc_list_date_th">When do you want to list it?</label>										
																<input type="text" name="sc_list_date_th" id="sc_list_date_th" class="form-control"  />
															</div>
															
															<div class="text-right">
																<button class="btn-primary btn btn-sm sell_th_btn"> Next </button>
															</div>
														</div>			
													</div>			
												</div>
											</div>
											
										</div>
									</div>
								
									<div id="section_buying_no" class="tab-pane fade">		
										<div class="text-right">
											<button class="btn-primary btn btn-sm buying_no_btn"> Next </button>
										</div>
									</div>
								</div>
								
							</div>
							
						</div>
					</div>	
					
					<div class="text-right">
						<br />
						<button class="btn-default btn btn-sm back_to_two_btn"> Back </button>
					</div>
				</div>
				
			</form>
			
		</div>
		
		<div id="offer_section2" class="tab-pane fade">			
			
			<form id="financing_form" >
				
				<div id="financing_section_one" class="col-sm-12">	
					
					<h3>Step 1</h3>
					
					<div class="form-group">
						<label for="sc_financing_amt">How much do you want to offer?</label>										
						<input type="text" name="sc_financing_amt" id="sc_financing_amt" class="form-control" placeholder="Amount"  />
					</div>	
					
					<div class="text-right">
						<button class="btn-primary btn btn-sm financing_one_next_btn"> Next </button>
					</div>					
				</div>
				
				<div id="financing_section_two" class="col-sm-12">	
				
					<h3>Step 2</h3>
				
					<div class="form-group">
						<label>How do you plan on paying for the house?</label>	
						<ul id="financing_menu_list_one" class="navoffer nav-tabs">
							<li><a data-toggle="tab" href="#financing_all_cash" data-answer="all_cash">I'm buying all cash</a></li>
							<li><a data-toggle="tab" href="#financing_mortgage" data-answer="mortgage">I'm going to need a mortgage</a></li>		
							<input type="hidden" id="financing_menu_list_one_answer" name="financing_menu_list_one_answer" />
						</ul>
					</div>	
					
					<div class="tab-content">
						<div id="financing_all_cash" class="tab-pane fade">
							<div class="text-right">
								<button class="btn-primary btn btn-sm financing_two_next_btn"> Next </button>
							</div>
						</div>	
						
						<div id="financing_mortgage" class="tab-pane fade">	
							<label>Have you been prequalified?</label>	
							<ul id="financing_menu_list_two" class="navoffer nav-tabs">
								<li><a data-toggle="tab" href="#financing_prequalify_yes_no" data-answer="yes">Yes</a></li>
								<li><a data-toggle="tab" href="#financing_prequalify_yes_no" data-answer="no">No</a></li>
								<input type="hidden" id="financing_menu_list_two_answer" name="financing_menu_list_two_answer" />							
							</ul>
							<div class="tab-content">
								<div id="financing_prequalify_yes_no" class="tab-pane fade">
									<div class="text-right">
										<button class="btn-primary btn btn-sm financing_two_next_btn"> Next </button>
									</div>
								</div>	
							</div>	
						</div>	
					</div>	
					
					<div class="text-right">
						<br />
						<button class="btn-default btn btn-sm financing_back_to_one_btn"> Back </button>
					</div>					
				</div>	
					
				<div id="financing_section_three" class="col-sm-12">
				
					<h3>Step 3</h3>
					
					<div class="form-group">
						<label for="sc_financing_earnest_amt">How much moeny do you want to offer in earnest?</label>										
						<input type="text" name="sc_financing_earnest_amt" id="sc_financing_earnest_amt" class="form-control" placeholder="Amount"  />
					</div>	
					
					<label>Earnest money is deposited in escrow account with a title/escrow company. Do you have a title company you want to work with?</label>	
					<ul id="financing_menu_list_three" class="navoffer nav-tabs">
						<li><a data-toggle="tab" href="#financing_escrow_yes" data-answer="yes">Yes</a></li>
						<li><a data-toggle="tab" href="#financing_escrow_no" data-answer="no">No</a></li>	
						<input type="hidden" id="financing_menu_list_three_answer" name="financing_menu_list_three_answer" />
					</ul>
					
					<div class="tab-content">
						<div id="financing_escrow_yes" class="tab-pane fade">
						
							<div class="form-group">
								<label for="sc_financing_escrow_name">Name:</label>
								<input type="text" name="sc_financing_escrow_name" id="sc_financing_escrow_name" class="form-control" />
							</div>	
							
							<div class="form-group">
								<label for="sc_financing_escrow_tel">Telephone:</label>
								<input type="text" name="sc_financing_escrow_tel" id="sc_financing_escrow_tel" class="form-control" />
							</div>
							
							<div class="form-group">
								<label for="sc_financing_escrow_officer_name">Officer Name:</label>
								<input type="text" name="sc_financing_escrow_officer_name" id="sc_financing_escrow_officer_name" class="form-control" />
							</div>
						
							<div class="text-right">
								<button class="btn-primary btn btn-sm financing_three_next2_btn"> Next </button>
							</div>
						</div>
						
						<div id="financing_escrow_no" class="tab-pane fade">
							<div class="text-right">
								<button class="btn-primary btn btn-sm financing_three_next_btn"> Next </button>
							</div>
						</div>	
					</div>	
					
					<div class="text-right">
						<br />
						<button class="btn-default btn btn-sm financing_back_to_two_btn"> Back </button>
					</div>	
				</div>
					
				<div id="financing_section_four" class="col-sm-12">	
				
					<h3>Step 4 - Closing Costs</h3>
					
					<label>Do you want the seller to help cover closing costs?</label>
					<ul id="financing_menu_list_four" class="navoffer nav-tabs">
						<li><a data-toggle="tab" href="#financing_closing_yes" data-answer="yes">Yes</a></li>
						<li><a data-toggle="tab" href="#financing_closing_no" data-answer="no">No</a></li>	
						<input type="hidden" id="financing_menu_list_four_answer" name="financing_menu_list_four_answer" />
					</ul>
					
					<div class="tab-content">
						<div id="financing_closing_yes" class="tab-pane fade">							
							
							<ul id="financing_menu_list_five" class="navoffer nav-tabs">
								<li><a data-toggle="tab" href="#financing_closing_amt" data-answer="customary">Customary Amount</a></li>
								<li><a data-toggle="tab" href="#financing_closing_amt" data-answer="all">All</a></li>			
								<input type="hidden" id="financing_menu_list_five_answer" name="financing_menu_list_five_answer" />
							</ul>
							
							<div class="tab-content">
								<div id="financing_closing_amt" class="tab-pane fade">
									<div class="text-right">
										<button class="btn-primary btn btn-sm financing_four_next_btn"> Next </button>
									</div>
								</div>	
							</div>	
						</div>
						
						<div id="financing_closing_no" class="tab-pane fade">
							<div class="text-right">
								<button class="btn-primary btn btn-sm financing_four_next_btn"> Next </button>
							</div>
						</div>
					</div>
					
					<div class="text-right">
						<br />
						<button class="btn-default btn btn-sm financing_back_to_three_btn"> Back </button>
					</div>	
				</div>
					
				<div id="financing_section_five" class="col-sm-12">	
					
					<h3>Step 5 - Home Warranty</h3>
				
					<label>Do you want a home warranty included?</label>
					<ul id="financing_menu_list_six" class="navoffer nav-tabs">
						<li><a data-toggle="tab" href="#financing_warranty_yes" data-answer="yes">Yes</a></li>
						<li><a data-toggle="tab" href="#financing_warranty_no" data-answer="no">No</a></li>	
						<input type="hidden" id="financing_menu_list_six_answer" name="financing_menu_list_six_answer" />
					</ul>
					
					<div class="tab-content">
						<div id="financing_warranty_yes" class="tab-pane fade">							
							
							<label>Who should pay for the home warranty</label>
							<ul id="financing_menu_list_seven" class="navoffer nav-tabs">
								<li><a data-toggle="tab" href="#financing_warranty_answer" data-answer="seller">Seller</a></li>
								<li><a data-toggle="tab" href="#financing_warranty_answer" data-answer="buyer">Buyer</a></li>											
								<li><a data-toggle="tab" href="#financing_warranty_answer" data-answer="split">Split</a></li>	
								<input type="hidden" id="financing_menu_list_seven_answer" name="financing_menu_list_seven_answer" />
							</ul>
							
							<div class="tab-content">
								<div id="financing_warranty_answer" class="tab-pane fade">
									<div class="text-right">
										<button class="btn-primary btn btn-sm financing_five_next_btn"> Next </button>
									</div>
								</div>	
							</div>								
						</div>
						
						<div id="financing_warranty_no" class="tab-pane fade">
							<div class="text-right">
								<button class="btn-primary btn btn-sm financing_five_next_btn"> Next </button>
							</div>
						</div>
					</div>
					
					<div class="text-right">
						<br />
						<button class="btn-default btn btn-sm financing_back_to_four_btn"> Back </button>
					</div>	
				</div>
				
			</form>
			
		</div>
		
		<div id="offer_section3" class="tab-pane fade">			
			<form id="import_date_form">			
				<div id="import_section_one" class="col-sm-12">	
					<h3> Step 1 </h3>
					<p>These dates create the timeline leading to the closing of your house.</p>
					<label>When do you want to hear back from the Seller</label>
					<ul id="import_menu_list_one" class="navoffer nav-tabs">
						<li><a data-toggle="tab" href="#import_list_one_answer">24 Hours</a></li>
						<li><a data-toggle="tab" href="#import_list_one_answer">48 Hours</a></li>											
						<li><a data-toggle="tab" href="#import_list_one_answer">72 Hours</a></li>											
					</ul>
					
					<div class="tab-content">
						<div id="import_list_one_answer" class="tab-pane fade">
							<div class="text-right">
								<button class="btn-primary btn btn-sm import_one_next_btn"> Next </button>
							</div>
						</div>	
					</div>		
				</div>
				
				<div id="import_section_two" class="col-sm-12">	
					<h3> Step 2 - Seller's Disclosure </h3>
					
					<div class="form-group">								
						<input type="text" name="import_date" id="import_date" class="form-control" />						
					</div>	
					
					<div class="text-right">
						<button class="btn-primary btn btn-sm import_two_next_btn"> Next </button>
					</div>					
				</div>
				
				<div id="import_section_three" class="col-sm-12">	
					<h3> Step 3 - Due Diligence/Inspections </h3>
					<label>When do you want to hear back from the Seller</label>
					<ul id="import_menu_list_two" class="navoffer nav-tabs">
						<li><a data-toggle="tab" href="#import_list_two_yes">Yes</a></li>
						<li><a data-toggle="tab" href="#import_list_two_no">NO</a></li>	
					</ul>
					
					<div class="tab-content">
						<div id="import_list_two_yes" class="tab-pane fade">
							<div class="form-group">															
								<input type="text" name="import_date_two" id="import_date_two" class="form-control" />						
							</div>	
							
							<div class="text-right">
								<button class="btn-primary btn btn-sm import_three_next_btn"> Next </button>
							</div>
						</div>	
						
						<div id="import_list_two_no" class="tab-pane fade">
							<div class="text-right">
								<button class="btn-primary btn btn-sm import_three_next_btn"> Next </button>
							</div>
						</div>	
					</div>	
				</div>
				
				<div id="import_section_four" class="col-sm-12">
					<h3> Step 4 - Appraised </h3>
					<div class="form-group">													
						<input type="text" name="import_date_three" id="import_date_three" class="form-control" />						
					</div>	
					
					<div class="text-right">
						<button class="btn-primary btn btn-sm import_four_next_btn"> Next </button>
					</div>
				</div>
				
				<div id="import_section_five" class="col-sm-12">	
				</div>
			</form>
		</div>
		
		<div id="offer_section4" class="tab-pane fade">
			<form id="inclusion_form">
				<p>Coming Soon</p>
			</form>
		</div>
		
		
	</div>	
</div>	

