<?php 
	/**
		* Template Name: Sign Up
		* Created by Sigfred Chamo 
	*/
	global $post;
	$sticky_sidebar = houzez_option('sticky_sidebar');
	$sidebar_meta = houzez_get_sidebar_meta($post->ID);
?>

<?php 
	get_header(); 
	get_template_part( 'template-parts/page', 'title' );	
	
	if ( is_user_logged_in() ) {
		wp_redirect( home_url('/dashboard/') );
	}
	
	if (isset($_GET['validation_code'])) { $validation_code = $_GET['validation_code']; }	
	if (isset($_GET['turbo'])) { $turbo = $_GET['turbo']; }		
	
	if ( $validation_code ) {
		$user_id = get_user_id_by_validation($validation_code);
		validate_user_by_id($user_id);
	}
?>

<section class="section-detail-content default-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="page-main">
					<div class="article-detail">
						
						<?php  if ( $validation_code && $validation_code != '' && $turbo == 'home' ) : ?>
						<h3 class="text-center">Success validating your account!</h3>
						<p class="text-center">Redirecting you to your dashboard. Please wait.</p>
						<?php else : ?>	
						
						<div id="sc_form_container">
							
							<div class="panel with-nav-tabs panel-default">
								<div class="panel-heading">
									<ul class="nav nav-tabs">
										<li class="active"><a href="#buyer" data-toggle="tab"> Buyer </a></li>
										<li><a href="#seller" data-toggle="tab"> Seller </a></li>
									</ul>
								</div>
								
								<div class="panel-body">
									<div class="tab-content">
										<div id="buyer" class="tab-pane fade in active">
											<div class="container-fluid">
												<div class="row">													
													
													<form id="sc_registration_form">
														
														<div id="registration_part_one">	
															
															<h3>Welcome to the Turbo family!</h3>
															<p>Please fill out the information below so we can better serve you.</p>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="sc_full_name">Full Name:</label>										
																	<input type="text" name="sc_full_name" id="sc_full_name" class="form-control" />
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="sc_email_address">Email Address:</label>										
																	<input type="text" name="sc_email_address" id="sc_email_address" class="form-control" />
																</div>
															</div>								
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="sc_mobile_num">Mobile Number:</label>										
																	<input type="text" name="sc_mobile_num" id="sc_mobile_num" class="form-control" />
																</div>
															</div>	
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="sc_buy_date">When do you plan to buy?</label>										
																	<select id="sc_buy_date" name="sc_buy_date" class="form-control">
																		<option value="1">Within 3 months</option>
																		<option value="2">Within 6 months</option>
																		<option value="3">Within 1 year</option>
																	</select>
																</div>
															</div>	
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="sc_state">State you plan to buy in:</label>										
																	<select id="sc_state" name="sc_state" class="form-control">
																		<option value="" selected="selected">Select a State</option>
																		<option value="AL">Alabama</option>
																		<option value="AK">Alaska</option>
																		<option value="AZ">Arizona</option>
																		<option value="AR">Arkansas</option>
																		<option value="CA">California</option>
																		<option value="CO">Colorado</option>
																		<option value="CT">Connecticut</option>
																		<option value="DE">Delaware</option>
																		<option value="DC">District Of Columbia</option>
																		<option value="FL">Florida</option>
																		<option value="GA">Georgia</option>
																		<option value="HI">Hawaii</option>
																		<option value="ID">Idaho</option>
																		<option value="IL">Illinois</option>
																		<option value="IN">Indiana</option>
																		<option value="IA">Iowa</option>
																		<option value="KS">Kansas</option>
																		<option value="KY">Kentucky</option>
																		<option value="LA">Louisiana</option>
																		<option value="ME">Maine</option>
																		<option value="MD">Maryland</option>
																		<option value="MA">Massachusetts</option>
																		<option value="MI">Michigan</option>
																		<option value="MN">Minnesota</option>
																		<option value="MS">Mississippi</option>
																		<option value="MO">Missouri</option>
																		<option value="MT">Montana</option>
																		<option value="NE">Nebraska</option>
																		<option value="NV">Nevada</option>
																		<option value="NH">New Hampshire</option>
																		<option value="NJ">New Jersey</option>
																		<option value="NM">New Mexico</option>
																		<option value="NY">New York</option>
																		<option value="NC">North Carolina</option>
																		<option value="ND">North Dakota</option>
																		<option value="OH">Ohio</option>
																		<option value="OK">Oklahoma</option>
																		<option value="OR">Oregon</option>
																		<option value="PA">Pennsylvania</option>
																		<option value="RI">Rhode Island</option>
																		<option value="SC">South Carolina</option>
																		<option value="SD">South Dakota</option>
																		<option value="TN">Tennessee</option>
																		<option value="TX">Texas</option>
																		<option value="UT">Utah</option>
																		<option value="VT">Vermont</option>
																		<option value="VA">Virginia</option>
																		<option value="WA">Washington</option>
																		<option value="WV">West Virginia</option>
																		<option value="WI">Wisconsin</option>
																		<option value="WY">Wyoming</option>
																	</select>
																</div>
															</div>	
															
															<div class="col-sm-12 col-xs-12 text-right">										
																<button class="btn btn-primary" id="registration_part_one_continue_btn">Continue</button>
																<button class="btn btn-default" id="registration_part_one_cancel_btn">Cancel</button>
															</div>
															
														</div>
														
														<div id="registration_part_two">
															<h3>Just a few more questions.</h3>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Have you been preapproved for a home loan?</label>												
																	<div>
																		<div class="btn-group" data-toggle="buttons">												
																			<label class="btn btn-default sc_home_loan_answer">
																				<input type="radio" name="sc_home_loan_options" value="1" autocomplete="off"> Yes
																			</label>
																			<label class="btn btn-default sc_home_loan_answer">
																				<input type="radio" name="sc_home_loan_options" value="0" autocomplete="off"> No
																			</label>
																		</div>											
																	</div>											
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Do you also need to sell your house?</label>												
																	<div>
																		<div class="btn-group" data-toggle="buttons">												
																			<label class="btn btn-default sc_home_sell_answer">
																				<input type="radio" name="sc_home_sell_options" value="1" autocomplete="off"> Yes
																			</label>
																			<label class="btn btn-default sc_home_sell_answer">
																				<input type="radio" name="sc_home_sell_options" value="0" autocomplete="off"> No
																			</label>
																		</div>											
																	</div>											
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Have you already found a home you want to buy?</label>												
																	<div>
																		<div class="btn-group" data-toggle="buttons">												
																			<label class="btn btn-default sc_home_found_answer">
																				<input type="radio" name="sc_home_found_options" value="1" autocomplete="off"> Yes
																			</label>
																			<label class="btn btn-default sc_home_found_answer">
																				<input type="radio" name="sc_home_found_options" value="0" autocomplete="off"> No
																			</label>
																		</div>											
																	</div>											
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Are you a real estate agent?</label>												
																	<div class="form-group">
																		<div class="btn-group" data-toggle="buttons">												
																			<label class="btn btn-default sc_real_state_answer">
																				<input type="radio" name="sc_real_state_options" value="1" autocomplete="off"> Yes
																			</label>
																			<label class="btn btn-default sc_real_state_answer">
																				<input type="radio" name="sc_real_state_options" value="0" autocomplete="off"> No
																			</label>
																		</div>											
																	</div>	
																	
																	<div id="sc_real_agent_container">
																		<div class="form-group">																		
																			<input type="text" name="sc_agent_number" id="sc_agent_number" class="form-control" placeholder="License #" />
																		</div>
																		
																		<div class="form-group">																		
																			<input type="text" name="sc_agent_email" id="sc_agent_email" class="form-control" placeholder="Email Address" />
																		</div>
																	</div>
																	
																</div>
															</div>
															
															<div id="sc_spinner_container">
																<div class="spinner">
																	<div class="bounce1"></div>
																	<div class="bounce2"></div>
																	<div class="bounce3"></div>
																</div>
															</div>
															
															<div class="col-sm-12 col-xs-12 text-right final_btn_container">		
																<input type="hidden" name="account_type" value="houzez_buyer" /> 
																<input type="hidden" name="action" value="sc_registration_form_submit" /> 
																<button class="btn btn-primary" id="registration_part_two_continue_btn">All Done</button>
																<button class="btn btn-default" id="registration_part_two_cancel_btn">Back</button>
															</div>
															
														</div>
														
														<div id="sc_registration_msg_container">
															<div class="alert alert-success"></div>
															<div class="alert alert-danger"></div>
														</div>
														
													</form>		
													
												</div>
											</div>
										</div>
										
										<div id="seller" class="tab-pane fade">
											<div class="container-fluid">
												<div class="row">		
													
													<div id="seller_container">
														<form id="sc_registration_seller_form">
															
															<h3>Welcome to the Turbo family!</h3>
															<p>Please fill out the information below so we can better serve you.</p>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="sc_full_name">Full Name:</label>										
																	<input type="text" name="sc_full_name" id="sc_seller_full_name" class="form-control" />
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="sc_email_address">Email Address:</label>										
																	<input type="text" name="sc_email_address" id="sc_seller_email_address" class="form-control" />
																</div>
															</div>								
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="sc_mobile_num">Mobile Number:</label>										
																	<input type="text" name="sc_mobile_num" id="sc_seller_mobile_num" class="form-control" />
																</div>
															</div>	
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label for="sc_address_property">Address of the property:</label>										
																	<input type="text" name="sc_address_property" id="sc_address_property" class="form-control" />
																</div>
															</div>	
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Have you already found a home you want to buy?</label>												
																	<div>
																		<div class="btn-group" data-toggle="buttons">												
																			<label class="btn btn-default sc_seller_home_found_answer">
																				<input type="radio" name="sc_home_found_options" value="1" autocomplete="off"> Yes
																			</label>
																			<label class="btn btn-default sc_seller_home_found_answer">
																				<input type="radio" name="sc_home_found_options" value="0" autocomplete="off"> No
																			</label>
																		</div>											
																	</div>											
																</div>
															</div>
															
															<div class="col-sm-12">
																<div class="form-group">
																	<label>Are you a real estate agent?</label>												
																	<div class="form-group">
																		<div class="btn-group" data-toggle="buttons">												
																			<label class="btn btn-default sc_seller_real_state_answer">
																				<input type="radio" name="sc_real_state_options" value="1" autocomplete="off"> Yes
																			</label>
																			<label class="btn btn-default sc_seller_real_state_answer">
																				<input type="radio" name="sc_real_state_options" value="0" autocomplete="off"> No
																			</label>
																		</div>											
																	</div>	
																	
																	<div id="sc_seller_real_agent_container">
																		<div class="form-group">																		
																			<input type="text" name="sc_agent_number" id="sc_seller_agent_number" class="form-control" placeholder="License #" />
																		</div>
																		
																		<div class="form-group">																		
																			<input type="text" name="sc_agent_email" id="sc_seller_agent_email" class="form-control" placeholder="Email Address" />
																		</div>
																	</div>
																	
																</div>
															</div>
															
															<div id="sc_seller_spinner_container">
																<div class="spinner">
																	<div class="bounce1"></div>
																	<div class="bounce2"></div>
																	<div class="bounce3"></div>
																</div>
															</div>
															
															<div class="col-sm-12 col-xs-12 text-right seller_btn_container">
																<input type="hidden" name="account_type" value="houzez_seller" /> 
																<input type="hidden" name="action" value="sc_registration_form_submit" /> 
																<button class="btn btn-primary" id="registration_btn">Submit</button>
															</div>	
															
														</form>
														
														<div id="sc_registration_seller_msg_container">
															<div class="alert alert-success"></div>
															<div class="alert alert-danger"></div>
														</div>
													</div>	
													
												</div>												
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php endif; ?>
						
					</div>					
				</div>
			</div>
		</div>
	</div>
</section>

<script type="text/javascript">
	
	//Mask phone
	jQuery(function($){		
		$("#sc_mobile_num, #sc_seller_mobile_num").mask("(999) 999-9999");	
	});
	
	jQuery(document).ready(function($) {	
		
			
		// SUBMIT - SELLER
		$('body').on('click', '#registration_btn', function (e) {
			e.preventDefault();	
			
			var found_answer = $('.sc_seller_home_found_answer.active').text().trim();	
			var agent_answer = $('.sc_seller_real_state_answer.active').text().trim();	
			
			var full_name = $('#sc_seller_full_name').val();			
			var email = $('#sc_seller_email_address').val();			
			var mobile_num = $('#sc_seller_mobile_num').val();			
			var sc_address_property = $('#sc_address_property').val();			
			
			if ( full_name == '' ) {
				$('#sc_registration_seller_msg_container .alert-danger').fadeIn().html('Please fill up your full name.');
				$('#sc_full_name').focus();
			} else if ( email == '' ) {
				$('#sc_registration_seller_msg_container .alert-danger').fadeIn().html('Email address cannot be empty.');
				$('#sc_email_address').focus();
			} else if ( mobile_num == '' ) {
				$('#sc_registration_seller_msg_container .alert-danger').fadeIn().html('Mobile number is required.');
				$('#sc_mobile_num').focus();
			} else if ( sc_address_property == '' ) {
				$('#sc_registration_seller_msg_container .alert-danger').fadeIn().html('Kindly select a state.');
				$('#sc_state').focus();				
			} else if ( found_answer == '' || agent_answer == '' ) {
				$('#sc_registration_seller_msg_container .alert-danger').fadeIn().html('You missed a choice. Please review.');					
			} else {
				$('#sc_seller_spinner_container').fadeIn();			
				var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';	
				var data = $('#sc_registration_seller_form').serialize();					
				
				$.post(ajaxurl, data, function(response) {
					var parsed = $.parseJSON(response);	
					
					if ( parsed['error'] ) {
						$('#sc_registration_seller_msg_container .alert-danger').fadeIn().html(parsed['msg']);		
						$('#sc_seller_spinner_container').fadeOut();
						$('.seller_btn_container').fadeIn();
						} else {
						$('.seller_btn_container').fadeOut();
						$('#sc_registration_seller_msg_container .alert-success').fadeIn().html(parsed['msg']);				
						$('#sc_registration_seller_msg_container .alert-danger, #sc_registration_seller_form').fadeOut();								
					}
				});	
			}
		});
		
		
		//Next button - part two - SUBMIT - BUYER
		$('body').on('click', '#registration_part_two_continue_btn', function (e) {
			e.preventDefault();
						
			$('.final_btn_container').fadeOut();
			$('#sc_spinner_container').fadeIn();
			
			var loan_answer = $('.sc_home_loan_answer.active').text().trim();		
			var sell_answer = $('.sc_home_sell_answer.active').text().trim();		
			var found_answer = $('.sc_home_found_answer.active').text().trim();	
			var agent_answer = $('.sc_real_state_answer.active').text().trim();	
			
			if ( loan_answer == '' || sell_answer == '' || found_answer == '' || agent_answer == '' ) {
				$('#sc_registration_msg_container .alert-danger').fadeIn().html('You missed a choice. Please review.');
				$('#sc_spinner_container').fadeOut();
				$('.final_btn_container').fadeIn();
				} else {
				
				var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';	
				var data = $('#sc_registration_form').serialize();					
				
				$.post(ajaxurl, data, function(response) {
					var parsed = $.parseJSON(response);	
					
					if ( parsed['error'] ) {
						$('#sc_registration_msg_container .alert-danger').fadeIn().html(parsed['msg']);		
						$('#sc_spinner_container').fadeOut();
						$('.final_btn_container').fadeIn();
						} else {
						$('.final_btn_container').fadeOut();
						$('#sc_registration_msg_container .alert-success').fadeIn().html(parsed['msg']);				
						$('#sc_registration_msg_container .alert-danger, #registration_part_two').fadeOut();
					}
				});					
				
				
			}
		});
		
		//Cancel button - part two
		$('body').on('click', '#registration_part_two_cancel_btn', function (e) {
			e.preventDefault();			
			$('#registration_part_one').fadeIn();
			$('#registration_part_two').fadeOut();
		});
		
		//Next button - part one
		$('body').on('click', '#registration_part_one_continue_btn', function (e) {
			e.preventDefault();
			
			var full_name = $('#sc_full_name').val();			
			var email = $('#sc_email_address').val();			
			var mobile_num = $('#sc_mobile_num').val();			
			var state = $('#sc_state').val();			
			
			if ( full_name == '' ) {
				$('#sc_registration_msg_container .alert-danger').fadeIn().html('Please fill up your full name.');
				$('#sc_full_name').focus();
				} else if ( email == '' ) {
				$('#sc_registration_msg_container .alert-danger').fadeIn().html('Email address cannot be empty.');
				$('#sc_email_address').focus();
				} else if ( mobile_num == '' ) {
				$('#sc_registration_msg_container .alert-danger').fadeIn().html('Mobile number is required.');
				$('#sc_mobile_num').focus();
				} else if ( state == '' ) {
				$('#sc_registration_msg_container .alert-danger').fadeIn().html('Kindly select a state.');
				$('#sc_state').focus();
				} else if ( state != 'AZ' ) {
				$('#sc_registration_msg_container .alert-danger').fadeIn().html('We currently do not serve this state but are working hard to get there. We will let you know once we arrive.');			
				} else {
				$('#registration_part_one').fadeOut();
				$('#registration_part_two').fadeIn();
				$('#sc_registration_msg_container .alert-danger').fadeOut();
			}
			
		});
		
		//Cancel button - part one
		$('body').on('click', '#registration_part_one_cancel_btn', function (e) {
			e.preventDefault();			
			$('#sc_registration_form')[0].reset();			
		});
		
		//Check if new user is real state agent
		$('body').on('click', '.sc_real_state_answer', function (e) {	
			var answer = $(this).text().trim();			
			if ( answer == 'Yes' ) {			
				$('#sc_real_agent_container').fadeIn();
				} else {
				$('#sc_real_agent_container').fadeOut();
				$('#sc_agent_number, #sc_agent_email').val('');
			}					
		});
		
		$('body').on('click', '.sc_seller_real_state_answer', function (e) {	
			var answer = $(this).text().trim();			
			if ( answer == 'Yes' ) {			
				$('#sc_seller_real_agent_container').fadeIn();
				} else {
				$('#sc_seller_real_agent_container').fadeOut();
				$('#sc_seller_agent_number, #sc_seller_agent_email').val('');
			}					
		});		
	});
	
</script>

<?php get_footer();	?>	