<section class="section-detail-content default-page">
	<div class="container">
		<div class="row dashboardcontainer">
			<div class="col-md-12">
				<div class="page-main">
					<div class="article-detail sc_dashboard_container">
						
						<div id="dashboard_utilities" class="row">							
							<div class="col-md-2">
								<p class="display_name">Hello <a href="#" class="view_profile ng-cloak">{[{loutctrl.firstname}]}</a></p>
							</div>
							
							<div class="col-md-8">
								<div id="sc_dashboard_menu_container">
									<ul class="navoffer nav-tabs sc_dashboard_menu">
										<li class="dropdown">
											<a class="dropdown-toggle" data-toggle="dropdown" href="#">Buyer <span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="#buyer_dashboard_menu" data-toggle="pill">Buying Process</a></li>						
												<li><a href="#make_an_offer" data-toggle="pill">Make an Offer Tool</a></li>						
												<li><a href="#buyer_contract_dashboard" data-toggle="pill">Contract Dashboard</a></li>
												<li><a href="#buyer_reminders" data-toggle="pill">Reminders</a></li>			
											</ul>
										</li>
										<li class="dropdown">
											<a class="dropdown-toggle" data-toggle="dropdown" href="#">Seller <span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="#seller_dashboard_menu" data-toggle="tab">Selling Process</a></li>											
												<li><a href="/sell-form">List Property</a></li>
												<li><a href="#update_listing" data-toggle="tab">Update Listing</a></li>	
												<li><a href="#offer_review" data-toggle="tab">Offer Review</a></li>
												<li><a href="#seller_contract_dashboard" data-toggle="tab">Contract Dashboard</a></li>
												<li><a href="#seller_reminders" data-toggle="tab">Reminders</a></li>		
											</ul>
										</li>
										<li><a data-toggle="pill" href="#property_value_dashboard_menu" class="property_zillow_menu">Property Value</a></li> 
									</ul>
								</div>
							</div>
							
							<div class="col-md-2 text-right">								
								<a class="sc_logout" href="" ng-click="loutctrl.logout()">Log Out</a>
							</div>							
						</div>
						
						<div class="sc_dashboard">
							
							<div class="tab-content">
							
								<h1 class="dashboard_title text-center"></h1>
								
								<!-- Buyer Link -->
								<div id="buyer_dashboard_menu" class="tab-pane fade">		
									<?php include('buyer_dashboard.php'); ?>
								</div>
								
								<div id="make_an_offer" class="tab-pane fade">		
									<?php include('make_offer.html'); ?>									
								</div>
								
								<div id="buyer_contract_dashboard" class="tab-pane fade">		
									<?php include('buyer_contract_dashboard.php'); ?>
								</div>
								
								<div id="buyer_reminders" class="tab-pane fade">		
									<?php include('buyer_reminders.php'); ?>
								</div>
								
								<!-- Seller Link -->
								<div id="seller_dashboard_menu" class="tab-pane fade">		
									<?php include('seller_dashboard.php'); ?>
								</div>
								
								<div id="list_property" class="tab-pane fade">		
									<?php include('list_property.php'); ?>
								</div>
								
								<div id="update_listing" class="tab-pane fade">		
									<?php include('update_listing.php'); ?>
								</div>
								
								<div id="offer_review" class="tab-pane fade">		
									<?php include('offer_review.php'); ?>
								</div>
								
								<div id="seller_contract_dashboard" class="tab-pane fade">		
									<?php include('seller_contract_dashboard.php'); ?>
								</div>
								
								<div id="seller_reminders" class="tab-pane fade">		
									<?php include('seller_reminders.php'); ?>
								</div>
								
								<!-- Zillow Link -->
								<div id="property_value_dashboard_menu" class="tab-pane fade">		
									<?php include('property_dashboard.php'); ?>									
								</div>
							</div>
						</div>	
						
						<br class="clear" />
						
					</div>					
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Profile Modal -->


<!-- Buyer Modal -->
<div id="sc_welcome_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">				
				<h4 class="modal-title text-center">Welcome to Turbo Hub</h4>
			</div>
			<div class="modal-body">
				<p>You can access the Turbo Hub at anytime by logging in. The Hub walks you through the buying process and provides the other resources you will need. It’s designed so you can easily access the most important information, even the information you didn’t know you needed. Just click on the Buyer Menu tab to access the tools. It includes:</p>
				
				<ul>
					<li>The Buying Process with step by step instructions to buy a home</li>					
					<li>The Make an Offer Tool</li>
					<li>Property Value Reports</li>
					<li>Your personalized Contract Dashboard so you stay on top of critical deadlines</li>
					<li>Important reminders</li>
				</ul>
				
				<p>Take a look around.</p>
				
				<div class="text-center">
					<a href="" class="btn btn-default btn-sm">Property Value</a>
					<a href="" class="btn btn-default btn-sm">Make an Offer</a>
					<a href="" class="btn btn-default btn-sm">Go to Hub</a>				
				</div>
			</div>
			
			<!--<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>-->
		</div>
		
	</div>
</div>

<!-- Seller Modal -->
<div id="sc_welcome_seller_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">				
				<h4 class="modal-title text-center">Welcome to Turbo Hub</h4>
			</div>
			<div class="modal-body">
				<p>You can access the Turbo Hub at anytime by logging in. The Hub walks you through the selling process and provides the other resources you will need. It’s designed so you can easily access the most important information, even the information you didn’t know you needed. Just click on the Seller Menu tab to access the tools. It includes:</p>
				
				<ul>
					<li>The Selling Process with step by step instructions to sell your home</li>
					<li>List Property for Sale Tool</li>
					<li>Property Value Reports</li>
					<li>Your personalized Contract Dashboard so you stay on top of critical deadlines</li>
					<li>Important reminders</li>
				</ul>
				
				<p>Take a look around.</p>
				
				<div class="text-center">
					<a href="" class="btn btn-default btn-sm">Property Value</a>
					<a href="" class="btn btn-default btn-sm">List Property Now</a>
					<a href="" class="btn btn-default btn-sm" data-dismiss="modal">Go to Hub</a>				
				</div>
			</div>
			
			<!--<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			</div>-->
		</div>
		
	</div>
</div>


	