<div class="sc_dashboard menu_page_container main_dashboard">
	
	<h4 class="dashboard_main_title">Buyer's Dashboard</h4>
	<strong>Check off items as they are completed and see what's coming up next.</strong>
	<p class="p_title">Once you have accepted an offer to buy your home then you under contract. This is a legally binding document. You need to be aware of important dates in the contract and take appropriate action. This table summarizes some of the key dates. Turbo will also send automated reminders for these.</p>
	
	<div class="col-md-8">
		<!-- <h3>Contract Dashboard</h3>
		<table class="table table-bordered">
			<thead>
				<th>Date</th>
				<th>Action</th>
				<th>Description</th>
			</thead>
			<tbody>
				<tr>
					<td>Aug 6</td>
					<td>Loan Status Update</td>
					<td>Lender must provide seller a completed loan status update</td>
				</tr>
				<tr>
					<td> </td>
					<td> </td>
					<td> </td>
				</tr>
				<tr>
					<td> </td>
					<td> </td>
					<td> </td>
				</tr>
				<tr>
					<td> </td>
					<td> </td>
					<td> </td>
				</tr>										
			</tbody>
		</table> -->
		<h3>Get Ready</h3>
        <ul>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="get_prequalified" type="checkbox" value="get_prequalified" checked="">
                     <label for="get_prequalified"></label>
        		</div>
        	    <div class="col-sm-11">
        			<span class="or_highlight bolder_text">Get prequalified.</span> You will look like a more serious buyer. Prequal form is attached. <span class="or_highlight">Find a Lender ></span>
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="get_searching" type="checkbox" value="get_searching"  checked="">
                     <label for="get_searching"></label>
        		</div>
        	    <div class="col-sm-11">
        			<span class="or_highlight bolder_text">Get searching.</span> You can find your house any way you wish and then we help with the rest.
        		</div>
        	</li>
        </ul>
        <h3>Showing & Offers</h3>
        <ul>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="tour_a_house" type="checkbox" value="tour_a_house">
                     <label for="tour_a_house"></label>
        		</div>
        	    <div class="col-sm-11">
        			To <span class="or_highlight bolder_text">tour a house</span> you're interested in contact the selling agent or owner directly and they should be happy to show it.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="makeanoffer" type="checkbox" value="makeanoffer">
                     <label for="makeanoffer"></label>
        		</div>
        	    <div class="col-sm-11">
        			When ready to make an offer navigate to the <span class="or_highlight bolder_text">Make an Offer Tool</span> and Turbo will help you through it.
        		</div>
        	</li>
        </ul>
        <h3>Under Contract <br/> Inspection Period</h3>
        <ul>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="deliver_earnest_money" type="checkbox" value="earnest_money">
                     <label for="deliver_earnest_money"></label>
        		</div>
        	    <div class="col-sm-11">
        			Deliver <span class="or_highlight bolder_text">Earnest Money</span> to your Escrow Company and ask for a receipt.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="home_loan" type="checkbox" value="home_loan">
                     <label for="home_loan"></label>
        		</div>
        	    <div class="col-sm-11">
        			&nbsp;Apply for <span class="or_highlight bolder_text">home loan</span> within 3 days. Get them all the info right away.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="provide_loan_status_update" type="checkbox" value="loan_status_update">
                     <label for="provide_loan_status_update"></label>
        		</div>
        	    <div class="col-sm-11">
        			Ensure your lender provides a <span class="or_highlight bolder_text">Loan Status Update</span> to the seller within 10 days.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="buyer_inspect_the_home" type="checkbox" value="inspect_the_home">
                    <label for="buyer_inspect_the_home"></label>
        		</div>
        	    <div class="col-sm-11">
        			<span class="or_highlight bolder_text">Inspect the home</span> then request repairs or back out. We recommend hiring professionals right away. <span class="or_highlight">See Provicer Market</span>
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="insurance" type="checkbox" value="insurance">
                    <label for="insurance"></label>
        		</div>
        	    <div class="col-sm-11">
        			Confirm ability to obtain <span class="or_highlight bolder_text">insurance</span> on the property. Seller to deliver a 5 year insurance claims history within 5 days.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="leased_items" type="checkbox" value="leased_items">
                    <label for="leased_items"></label>
        		</div>
        	    <div class="col-sm-11">
        			Seller to deliver notice of <span class="or_highlight bolder_text">leased items</span> 
        			(e.g. solar panels) within 3 days. Decide if you are okay with the lease terms.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="seller_disclosures" type="checkbox" value="seller_disclosures">
                    <label for="seller_disclosures"></label>
        		</div>
        	    <div class="col-sm-11">
        			Seller to provide <span class="or_highlight bolder_text">Seller Disclosures</span> 
        			within 5 days. Review these in detail. Ask seller questions if needed. 
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="title_commitment" type="checkbox" value="title_commitment">
                    <label for="title_commitment"></label>
        		</div>
        	    <div class="col-sm-11">
        			Review <span class="or_highlight bolder_text">Title commitment, CC&R's and other governing documents(HOA)</span> provided by Escrow Company and cancel contract in writing within 5 days of receipt if needed.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="appraised_value" type="checkbox" value="appraised_value">
                    <label for="appraised_value"></label>
        		</div>
        	    <div class="col-sm-11">
        			You have 5 days after notice of <span class="or_highlight bolder_text">appraised value</span> to cancel the Contract and receive refund of Earnest Money.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="decision_point" type="checkbox" value="decision_point">
                    <label for="decision_point"></label>
        		</div>
        	    <div class="col-sm-11">
        	    	<p><span class="or_highlight bolder_text">Decision Point!</span></p>
        	    	If you want repairs done or to cancel the contract due to your inspections we must give the seller written notice within the inspection period to get your Earnest Money back.
        	    </div>
        	</li>
        </ul>
        <h3>Under Contract <br/> Other Items</h3>
        <ul>
            <li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="home_warranty_plan" type="checkbox" value="home_warranty_plan">
                    <label for="home_warranty_plan"></label>
        		</div>
        	    <div class="col-sm-11">
        	    	If you want a <span class="or_highlight bolder_text">Home Warranty Plan</span> set it up now. Give the invoice to the Escrow Company if the seller is to pay for it. 
        	    </div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="cant_get_the_loan" type="checkbox" value="cant_get_the_loan">
                    <label for="cant_get_the_loan"></label>
        		</div>
        	    <div class="col-sm-11">
        	    	If you <span class="or_highlight bolder_text">can't get the loan</span> you must deliver notice to Seller at least 3 days before closing to get the Earnest Money back. 
        	    </div>
        	</li>
         </ul>
        <h3>Closing</h3>
        <ul>
            <li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="buyer_final_walkthrough" type="checkbox" value="final_walkthrough">
                    <label for="buyer_final_walkthrough"></label>
        		</div>
        	    <div class="col-sm-11">
        	    	Schedule a <span class="or_highlight bolder_text">final walkthrough</span> before closing. Confirm items are in good working order.
        	    </div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="hud_1" type="checkbox" value="hud_1">
                    <label for="hud_1"></label>
        		</div>
        	    <div class="col-sm-11">
        	    	Escrow company will provide a <span class="or_highlight bolder_text">HUD 1</span> that shows who pays what and who gets what. Review this in detail.
        	    </div>
        	</li>
        	<li>
        		<div class="col-sm-1">
                    <input class="styled-checkbox" id="buyer_sign_final_docs" type="checkbox" value="sign_final_docs">
                    <label for="buyer_sign_final_docs"></label>
                </div>
                <div class="col-sm-11">
        	    	Schedule a time for your closing and to <span class="or_highlight bolder_text">sign final docs</span> with the Escrow Company. Loan docs must be signed at least 3 days before closing. Get the <span class="or_highlight bolder_text">KEYS!</span>
        	    </div>
        	</li>
         </ul>
	</div>
	
	<div class="col-md-4">
		<h3>Contact Repository</h3>
		<div id="contact_repository">
		</div>
	</div>
	
</div>	

