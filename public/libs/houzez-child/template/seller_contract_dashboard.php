<div class="sc_dashboard menu_page_container main_dashboard">
	
	<h4 class="dashboard_main_title">Seller's Dashboard</h4>
	<strong>Check off items as they are completed and see what's coming up next.</strong>
	<p class="p_title">Once you have accepted an offer to buy your home then you under contract. This is a legally binding document. You need to be aware of important dates in the contract and take appropriate action. This table summarizes some of the key dates. Turbo will also send automated reminders for these.</p>
	
	<div class="col-md-8">
		<!-- h3>Contract Dashboard</h3>
		<table class="table table-bordered">
			<thead>
				<th>Date</th>
				<th>Action</th>
				<th>Description</th>
			</thead>
			<tbody>
				<tr>
					<td>Aug 6</td>
					<td>Loan Status Update</td>
					<td>Lender must provide seller a completed loan status update</td>
				</tr>
				<tr>
					<td> </td>
					<td> </td>
					<td> </td>
				</tr>
				<tr>
					<td> </td>
					<td> </td>
					<td> </td>
				</tr>
				<tr>
					<td> </td>
					<td> </td>
					<td> </td>
				</tr>										
			</tbody>
		</table> -->
        <h3>Get Ready</h3>
        <ul>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="initial_paperwork" type="checkbox" value="initial_paperwork" checked="">
                     <label for="initial_paperwork"></label>
        		</div>
        	    <div class="col-sm-11">
        			Complete <span class="or_highlight bolder_text">initial paperwork</span> and e-sign.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="seller_disclosure_form" type="checkbox" value="seller_disclosure_form" checked="">
                     <label for="seller_disclosure_form"></label>
        		</div>
        	    <div class="col-sm-11">
        			Complete <span class="or_highlight bolder_text">Seller Disclosure form</span> and e-sign.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="temp_no" type="checkbox" value="temp_no">
                     <label for="temp_no"></label>
        		</div>
        	    <div class="col-sm-11">
        			Turbo provides temporary <span class="or_highlight bolder_text">phone # and email</span>.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="for_sale_sign" type="checkbox" value="for_sale_sign">
                     <label for="for_sale_sign"></label>
        		</div>
        	    <div class="col-sm-11">
        			Turbo puts up <span class="or_highlight bolder_text">'For Sale' sign</span> in your yard.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="photographs" type="checkbox" value="photographs">
                     <label for="photographs"></label>
        		</div>
        	    <div class="col-sm-11">
        			Professional <span class="or_highlight bolder_text">photographs</span> taken by Turbo.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="key_lockbox" type="checkbox" value="key_lockbox">
                     <label for="key_lockbox"></label>
        		</div>
        	    <div class="col-sm-11">
        			If needed, setup a <span class="or_highlight bolder_text">key lockbox</span> so potential buyers can access a key to see your house.
        		</div>
        	</li>
        </ul>
        <h3>Showing & Offers</h3>
        <ul>
        	<li class="reminder">
               <h4 class="or_highlight bolder_text">Reminder!</h4>
               <ul>
               	  <li><i class="fa fa-dot-circle-o"></i> Stage your house for every showing.</li>
               	  <li><i class="fa fa-dot-circle-o"></i> Make it easy to see. Try to accommodate times.</li>
               	  <li><i class="fa fa-dot-circle-o"></i> Be friendly. Make them want to work with you.</li>
               </ul>
            <li>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="receive_an_offer" type="checkbox" value="receive_an_offer">
                     <label for="receive_an_offer"></label>
        		</div>
        	    <div class="col-sm-11">
        	    	<p class="or_highlight bolder_text">Receive an offer</p>
        	    	If an offer goes directly to you please forward immediately.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="negotiate" type="checkbox" value="negotiate">
                     <label for="negotiate"></label>
        		</div>
        	    <div class="col-sm-11">
        			<span class="or_highlight bolder_text">Turbo Attorneys</span> will help you <span class="or_highlight bolder_text">negotiate</span> the deal and complete the paperwork.
        		</div>
        	</li>
        </ul>
        <h3>Under Contract</h3>
        <ul>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="confirm_earnest_money" type="checkbox" value="earnest_money">
                     <label for="confirm_earnest_money"></label>
        		</div>
        	    <div class="col-sm-11">
        	    	Confirm receipt of <span class="or_highlight bolder_text">Earnest Money</span>. Ask Escrow Company for this.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="review_loan_status_update" type="checkbox" value="loan_status_update">
                     <label for="review_loan_status_update"></label>
        		</div>
        	    <div class="col-sm-11">
        			Review the buyer's <span class="or_highlight bolder_text">Loan Status Update</span> to ensure loan is on track. Buyer to send within 10 days of contract.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="seller_inspect_the_home" type="checkbox" value="inspect_the_home">
                    <label for="seller_inspect_the_home"></label>
        		</div>
        	    <div class="col-sm-11">
        			Buyer has 10 days to  <span class="or_highlight bolder_text">inspect the home</span> and request repairs or back out. Your response to any request for repairs is time sensitive.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="appraisal" type="checkbox" value="appraisal">
                    <label for="appraisal"></label>
        		</div>
        	    <div class="col-sm-11">
        			Buyer has 5 days after <span class="or_highlight bolder_text">appraisal</span> to cancel.
        		</div>
        	</li>
        </ul>
        <h3>Closing</h3>
        <ul>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="sign_all_loan_docs" type="checkbox" value="sign_all_loan_docs">
                     <label for="sign_all_loan_docs"></label>
        		</div>
        	    <div class="col-sm-11">
        	    	3 days before close, <span class="or_highlight bolder_text">buyer must sign all loan docs</span> or cancel.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			<input class="styled-checkbox" id="seller_final_walkthrough" type="checkbox" value="final_walkthrough">
                    <label for="seller_final_walkthrough"></label>
        		</div>
        	    <div class="col-sm-11">
        			<span class="or_highlight bolder_text">Buyer final walkthrough</span> if they want.
        		</div>
        	</li>
        	<li>
        		<div class="col-sm-1">
        			 <input class="styled-checkbox" id="seller_sign_final_docs" type="checkbox" value="sign_final_docs">
                    <label for="seller_sign_final_docs"></label>
        		</div>
        	    <div class="col-sm-11">
        			Schedule with Escrow company to  <span class="or_highlight bolder_text">sign final docs</span> and get the <span class="or_highlight bolder_text">KEYS!</span>
        		</div>
        	</li>
        </ul>
	</div>
	
	<div class="col-md-4">
		<h3>Contact Repository</h3>
		<div id="contact_repository">
		</div>
	</div>
	
</div>	

