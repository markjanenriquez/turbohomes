<?php 
	/**
		* Template Name: Login
		* Created by Sigfred Chamo 
	*/
	global $post;
	$sticky_sidebar = houzez_option('sticky_sidebar');
	$sidebar_meta = houzez_get_sidebar_meta($post->ID);
?>

<?php 
	get_header(); 
	get_template_part( 'template-parts/page', 'title' );
	
	if ( is_user_logged_in() ) {
		wp_redirect( home_url('/dashboard/') );
	}
?>

<section class="section-detail-content default-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="page-main">
					<div class="article-detail">
						
						<div class="col-md-6">							
							<form id="sc_login_form">								
								
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12">
										<div class="form-group">
											<div class="input-group">
												<div class="input-group-addon">
													<span class="glyphicon glyphicon-user"></span>
												</div>
												<input type="text" placeholder="User Name" name="uname" class="form-control">
											</div>
										</div>
									</div>
								</div>
								
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12">
										<div class="form-group">
											<div class="input-group">
												<div class="input-group-addon">
													<span class="glyphicon glyphicon-lock"></span>
												</div>
												
												<input type="password" placeholder="Password" name="pass" class="form-control">
											</div>
										</div>
									</div>
								</div>
								
								<div class="col-xs-6 col-sm-6 col-md-6">				
									<div class="col-xs-12 col-sm-12 col-md-12">									
										<div class="form-group">
											<input type="checkbox" name="remember_me"> Remember Me
										</div>
									</div>
								</div>
								
								<div class="col-xs-6 col-sm-6 col-md-6">				
									<div class="col-xs-12 col-sm-12 col-md-12">									
										<div class="form-group">
											No account? <a href="<?php echo home_url('/sign-up/')?>"> Sign Up </a>
										</div>
									</div>
								</div>
								
								
								<div class="row">
									<div class="col-xs-12 col-sm-12 col-md-12">
										<input type="hidden" name="action" value="sc_login_action" />
										<div id="sc_spinner_container">
											<div class="spinner">
												<div class="bounce1"></div>
												<div class="bounce2"></div>
												<div class="bounce3"></div>
											</div>
										</div>
										<button type="submit" class="btn btn-primary btn_submit btn-block btn-lg"> Login </button>
									</div>
								</div>
								
								<div id="sc_login_msg_container">
									<div class="alert alert-success"></div>
									<div class="alert alert-danger"></div>
								</div>
								
							</form>							
						</div>
						
					</div>					
				</div>
			</div>
		</div>
	</div>
	</section>
	
	<div class="modal fade" id="forgot">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">				
					<h3 class="modal-title"> Recover Your Password </h3>
				</div>
				
				<div class="modal-body">
					<div class="container-fluid">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group">
									<div class="input-group">
										<div class="input-group-addon iga2">
											<span class="glyphicon glyphicon-envelope"></span>
										</div>
										<input type="email" class="form-control" placeholder="Email Address" name="email">
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class="modal-footer">
					<div class="form-group">
						<button type="submit" class="btn btn-lg btn-info sc_recover_password"> Save </button>
						
						<button type="button" data-dismiss="modal" class="btn btn-lg btn-default"> Cancel </button>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
		
		jQuery(document).ready(function($) {	
			
			//Submit login
			$('body').on('click', '.btn_submit', function (e) {
				e.preventDefault();
				$('#sc_spinner_container').fadeIn();
				var btn = $(this);
				btn.hide();
				var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';	
				var dashboard = '<?php echo home_url('/dashboard/'); ?>';	
				var data = $('#sc_login_form').serialize();			
				
				$.post(ajaxurl, data, function(response) {
					var parsed = JSON.parse(response);	
					
					if ( parsed['error'] ) {
						$('#sc_login_msg_container .alert-danger').fadeIn().html(parsed['msg']);	
						btn.fadeIn();
						$('#sc_spinner_container').hide();
						} else {
						window.location.replace(dashboard);
						$('#sc_login_msg_container .alert-danger').fadeOut();
					}
				});				
			});				
		});
		
	</script>
	
	<?php get_footer();	?>		