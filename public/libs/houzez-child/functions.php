<?php
	require_once('includes/helper.php');
	require_once('includes/zillow.php');
	
	function sc_ajax_callback() {
		
		$err_suc = array(
			'error' => false,
			'msg' => ''			
		);
		
		//Get Seller Price
		if ( isset($_POST['action']) && ($_POST['action'] == 'sc_get_seller_price') ) {
			echo get_post_meta( 1758 , 'Price', TRUE );
		}
		
		//Profile
		if ( isset($_POST['action']) && ($_POST['action'] == 'sc_logout') ) {
			wp_logout();
		}
		
		if ( isset($_POST['action']) && ($_POST['action'] == 'sc_profile_update') ) {
			
			if ( !empty($_POST) ) {
				foreach ( $_POST as $k => $p ) {
					$$k = sanitize_text_field($p);	
				}
			}			
		
			update_user_meta($user_id, 'first_name', $sc_first_name);
			update_user_meta($user_id, 'last_name', $sc_last_name);
			update_user_meta($user_id, 'contact_number', $sc_phone_num);				
			$err_suc['msg'] = 'Data updated.';		
			
			if ( $sc_password ) {
				if ( strlen($sc_password) > 6 ) {
					$validation = gen_rand_char( 20 );
					
					if ( sc_send_password_confirmation($sc_email_address, $validation) ) { 	
						update_user_meta( $user_id, 'sc_user_password_code', $validation);
						update_user_meta( $user_id, 'sc_user_temp_password', $sc_password);
					} else {
						$err_suc['error'] = true;					
						$err_suc['msg'] = 'Error: password update failed.';					
					}
				} else {
					$err_suc['error'] = true;
					$err_suc['msg'] = 'Error: password is too short.';
				}
			}
			
			echo json_encode($err_suc);
		}
		
		
		//Login
		if ( isset($_POST['action']) && ($_POST['action'] == 'sc_login_action') ) {
			if ( !empty($_POST) ) {
				foreach ( $_POST as $k => $p ) {
					$$k = sanitize_text_field($p);	
				}
			}			
			
			$creds = array();
			$creds['user_login'] = $uname;
			$creds['user_password'] = $pass;
			$creds['remember'] = $remember_me == 'on' ? true : false;
			$user = wp_signon( $creds, false );
			
			if ( is_wp_error($user) ) {
				$err_suc['error'] = true;
				$err_suc['msg'] = $user->get_error_message();	
			} else {
				$err_suc['msg'] = $user->id;
				$approved = get_user_meta( $user->id, 'sc_user_approved', TRUE );
				$temp_password = get_user_meta( $user->id, 'sc_user_temp_password', TRUE );
				if ( $approved ) {
					if ( $temp_password ) {
						$err_suc['error'] = true;
						$err_suc['msg'] = 'Your password has been updated!';	
					} else {
						wp_set_auth_cookie( $user->id, false, is_ssl() );				
					}
				} else {
					$err_suc['error'] = true;
					$err_suc['msg'] = 'Your account is not validated!';	
				}				
			}		
			
			echo json_encode($err_suc);
		}
		
		
		//Registration
		if ( isset($_POST['action']) && ($_POST['action'] == 'sc_registration_form_submit') ) {
		
			if ( !empty($_POST) ) {
				foreach ( $_POST as $k => $p ) {
					$$k = sanitize_text_field($p);	
				}
			}
			
			if ( !is_email($sc_email_address) ) {		
				$err_suc['error'] = true;
				$err_suc['msg'] .= 'Invalid email address.';			
			} elseif ( email_exists($sc_email_address) ) {		
				$err_suc['error'] = true;
				$err_suc['msg'] .= 'Email is already registered.';		
			} else {
				$password = gen_rand_char( 12 );
				$user_id = wp_create_user( $sc_email_address, $password, $sc_email_address );				
				
				if ($user_id) {	
				
					$validation = gen_rand_char( 20 );	
					
					$full_name = explode(' ', $sc_full_name);
					$fist_name = $full_name[0];
					$last_name = $full_name[1];	
					
					$userdata = array();
					$userdata['ID'] = $user_id;
					$userdata['role'] = $account_type;	
					wp_update_user($userdata);
				
					update_user_meta($user_id, 'first_name', $fist_name);	
					update_user_meta($user_id, 'last_name', $last_name);					
					update_user_meta($user_id, 'contact_number', $sc_mobile_num);					
					update_user_meta($user_id, 'state', $sc_state);					
					update_user_meta($user_id, 'home_address_property', $sc_address_property);					
					update_user_meta($user_id, 'home_loan_options', $sc_home_loan_options);					
					update_user_meta($user_id, 'home_sell_options', $sc_home_sell_options);					
					update_user_meta($user_id, 'home_found_options', $sc_home_found_options);					
					update_user_meta($user_id, 'real_state_agent', $sc_real_state_options);					
					update_user_meta($user_id, 'real_state_agent_number', $sc_agent_number);					
					update_user_meta($user_id, 'real_state_agent_email', $sc_agent_email);							
					update_user_meta($user_id, 'sc_user_validation_char', $validation);	
					update_user_meta($user_id, 'sc_user_approved', 0);	
					update_user_meta($user_id, 'sc_user_pop_up_status', 0);	
					
					$err_suc['msg'] = sc_send_email_validation($sc_email_address, $validation);					
					
				} else {
					$err_suc['error'] = true;
					$err_suc['msg'] .= 'Error creating user. Please check details and try again.';
				}
			}
			
			echo json_encode($err_suc);
		}		
		
		//Zillow API
		if ( isset($_POST['action']) && ($_POST['action'] == 'sc_get_deep_comps') ) {
			
			if ( !empty($_POST) ) {
				foreach ( $_POST as $k => $p ) {
					$$k = sanitize_text_field($p);	
				}
			}					
			
			$id = 'X1-ZWz1fx3leiotfv_2vy2l';
			$searh_address = $street_number . ' ' . $route;
			$searh_citystatezip = $locality . ' ' . $administrative_area_level_1 . ' ' . $postal_code . ' ' . $country;
			$gallery_pagination = '';
			$updated_detail_status = '';
			$gallery_content = '';
			$gallery_pagination = '';
			$gallery_arrow_btn = '';
			$image_count = 0;	
			$err_suc['gallery'] = 0;
			
			//Search data - get zpid
			$zpid = sc_zillow_search_api($id, $searh_address, $searh_citystatezip);	
			
			if ( $zpid ) {
				$deep_comp_data = sc_zillow_get_deep_comp_api($id, $zpid, '10');
				
				//Deep Comps Details - get data and comparables
				$code = $deep_comp_data['message']['code'];
				$code_text = $deep_comp_data['message']['text'];
				$principal = $deep_comp_data['response']['properties']['principal'];	
				$comparables = $deep_comp_data['response']['properties']['comparables']['comp'];	
				$sp_links = $principal['links'];
				$address = $principal['address'];
				$err_suc['latitude'] = $principal['address']['latitude'];
				$err_suc['longitude'] = $principal['address']['longitude'];	
				$zestimate = $principal['zestimate'];
				$localRealEstate = $principal['localRealEstate'];	
							
				
				if ( $code >= 500 && $code <= 510 ) {
					$err_suc['error'] = true;
					$err_suc['msg'] = $code_text;
				} else {
				
					//Updated data - get more details and images
					$updated_data = sc_zillow_get_updated_data_api($id, $zpid);
					$code = $updated_data['message']['code'];
					$code_text = $updated_data['message']['text'];	
					
					if ( $code >= 500 && $code <= 510 ) {						
						// $updated_detail_status = '<br />' . $code_text;
						$updated_detail_status = '<span class="zillow_error_msg">Some data is not available for this property</span>';
					}
				
					$latest = $updated_data['response'];					
					$neighborhood = $latest['neighborhood'];
					$school_district = $latest['schoolDistrict'];
					$elementary_school = $latest['elementarySchool'];
					$middle_school = $latest['middleSchool'];
					$home_description = $latest['homeDescription'];					
					$updated_edited_facts = $latest['editedFacts'];					
					
					if( isset( $latest['images']['count'] )) { 
						$image_count = $latest['images']['count'];
					}
					if ( $image_count == 1 ) {						
						$gallery_content = '<img src="' . $latest['images']['image']['url'] . '" class="api_img" />';
					} else if ( $image_count > 1) {	
						$gallery_pagination = '<div class="gallery_pagination">';
						$err_suc['gallery'] = 1;;
						$gallery_ctr = 1;
						
						foreach ( $latest['images']['image']['url'] as $img ) {						
							$gallery_content .= '<img src="' . $img . '" class="sc_custom_slider api_img" />';	
							$gallery_pagination .= '<button class="sc_page_btn btn" onclick="current_image(' . $gallery_ctr . ')">' . $gallery_ctr . '</button> ';
							$gallery_ctr++;
						}
						
						$gallery_arrow_btn = '	<button class="img_back img_left" onclick="next_back(-1)"> <span class="glyphicon glyphicon-chevron-left"></span> </button>
												<button class="img_back img_right" onclick="next_back(1)"> <span class="glyphicon glyphicon-chevron-right"></span> </button>';
						$gallery_pagination .= '</div>';
						
					} else {
						$image_url = get_stylesheet_directory_uri() . '/images/nopic.png';
						$gallery_content = '<img src="' . $image_url . '" class="api_img" />';
					}	
					
					$err_suc['title'] = $address['street'] . ', ' . $address['city'] . ' ' . $address['state'] .' ' . $address['zipcode'];
					$chart_img = sc_zillow_chart_api($id, $zpid, 'percent');
					$err_suc['msg'] = '	<div class="row property_report_container">
											<img src="http://www.zillow.com/widgets/GetVersionedResource.htm?path=/static/logos/Zillowlogo_200x50.gif" width="200" height="50" alt="Zillow Real Estate Search" class="zillow_img" />
											<h3 class="property_header">Property Value Report</h3>
											<p class="text-center">' . $err_suc['title'] . '</p>
											<div class="col-md-6">											
												<div class="gallery_content">
													' . $gallery_content . '													
													' . $gallery_arrow_btn . '													
												</div>
												' . $gallery_pagination . '
											</div>
											<div class="col-md-6">	
												<table class="table table-bordered">												
													<th colspan="2" class="no-padding">
														<h3 class="property_header no-margin">Property Details</h3>
													</th>
													<tr>
														<td>Finished Sq Ft</td>
														<td>' . $principal['finishedSqFt'] . '</td>
													</tr>												
													<tr>
														<td>Bedrooms</td>
														<td>' . $updated_edited_facts['bedrooms'] . '</td>
													</tr>
													<tr>
														<td>Bathrooms</td>
														<td>' . $updated_edited_facts['bathrooms'] . '</td>
													</tr>
													<tr>
														<td>Year Built</td>
														<td>' . $principal['yearBuilt'] . '</td>
													</tr>
													<tr>
														<td>Lot Size SqFt</td>
														<td>' . $principal['lotSizeSqFt'] . '</td>
													</tr>
													<tr>
														<td>Last Sold Date</td>
														<td>' . $principal['lastSoldDate'] . '</td>
													</tr>
													<tr>
														<td>Last Sold Price</td>
														<td>$' . number_format($principal['lastSoldPrice'], 2) . '</td>
													</tr>
													<tr>
														<td>Tax assessment Year</td>
														<td>' . $principal['taxAssessmentYear'] . '</td>
													</tr>
													<tr>
														<td>Tax Assessment</td>
														<td>$' . number_format($principal['taxAssessment'], 2) . '</td>
													</tr>
												</table>
											</div>
										</div>									
										
										<div id="zestimate_container">
											<div class="row">
												<div class="col-md-6">	
													<table class="table table-bordered zestimate">												
														<th colspan="3" class="no-padding">
															<h3 class="property_header no-margin">Zestimate Valuation</h3>
														</th>
														<tr>
															<td>$' . number_format($zestimate['valuationRange']['low'], 2) . '</td>														
															<td>$' . number_format($zestimate['amount'], 2) . '</td>
															<td>$' . number_format($zestimate['valuationRange']['high'], 2) . '</td>
														</tr>
														<tr>
															<td>Low</td>
															<td>Zestimate</td>
															<td>High</td>
														</tr>
													</table>
													<p>Last Updated: ' . date('M d, Y', strtotime($zestimate['last-updated'])) . '</p>
												</div>
												<div class="col-md-6">
													<table class="table table-bordered zestimate">												
														<th colspan="3" class="no-padding">
															<h3 class="property_header no-margin">Valuation Estimate Based on $/SqFt</h3>
														</th>
														<tr class="">
															<td class="comps_valuation_low"></td>														
															<td class="comps_valuation_average"></td>
															<td class="comps_valuation_high"></td>
														</tr>
														<tr>
															<td>Low</td>
															<td>Average</td>
															<td>High</td>
														</tr>
													</table>
													<p>Calculation based on comparable properties</p>
												</div>	
											</div>
										</div>
										
										<div class="row">
											<div class="col-md-6">		
												<h3 class="property_header no-margin">Comparable Properties</h3>
												<div id="zillow_map_canvas"></div>												
											</div>
											<div class="col-md-6">
												<h3 class="property_header no-margin">Change in Zestimate Values</h3>
												<div id="zillow_chart"><img src="' . $chart_img . '" /></div>
											</div>											
										</div>';
				}
				
				//Comparables					
				if ( $comparables ) {	
					
					$ctr = 1;
					$marker_ctr = 0;
					$average_computation = 0;
					$sqft_value_arr = array();
					$comp_markers_obj = [];
					$comp_markers = array();										
					
					$err_suc['msg'] .= '<div id="comparable_container" class="row">
										<h3 class="property_header">Comparable Properties ' . $updated_detail_status . '</h3>';	
					$err_suc['msg'] .= '<table class="table table-bordered table-striped comparables_table">
										<thead>											
											<th>#</th>
											<th>Comp Score</th>
											<th>Street Address</th>
											<th>SqFt</th>
											<th>Bedroom</th>
											<th>Bathroom</th>
											<th>Year Built</th>
											<th>Lot Size</th>
											<th>Stories</th>											
											<th>Last Sold Date</th>			 								
											<th>Last Sold Price</th>
											<th>$/SqFt</th>
										</thead>
										<tbody>';
					
					//Subject Property
					$main_propery_address = $address['street'] . ', <br /> ' . $address['city'] . ' ' . $address['state'] .' ' . $address['zipcode'];
					$main_propery_price_sqft = number_format($principal['lastSoldPrice'] / $principal['finishedSqFt'], 2) ;
					$err_suc['msg'] .= '<tr class="subject_property_row">
											<td> </td>
											<td> </td>
											<td class="nowrap">' . $main_propery_address . '</td>
											<td>' . $principal['finishedSqFt'] . '</td>
											<td>' . $updated_edited_facts['bedrooms'] . '</td>
											<td>' . $updated_edited_facts['bathrooms'] . '</td>
											<td>' . $updated_edited_facts['yearBuilt'] . '</td>
											<td>' . $updated_edited_facts['lotSizeSqFt'] . '</td>
											<td>' . $updated_edited_facts['numFloors'] . '</td>
											<td>' . $principal['lastSoldDate'] . '</td>
											<td>$' . number_format($principal['lastSoldPrice'], 2) . '</td>
											<td>$' . $main_propery_price_sqft . '</td>
										</tr>';	
					//Marker			
					$comp_markers[11] = array();
					array_push($comp_markers[11], $address['street']);
					array_push($comp_markers[11], $address['longitude']);
					array_push($comp_markers[11], $address['latitude']);							
					array_push($comp_markers[11], ' ');							
					array_push($comp_markers[11], get_stylesheet_directory_uri() . '/images/orange-dotless.png');							
					array_push($comp_markers_obj, $comp_markers[11]);
										
					foreach ( $comparables as $comp ) {
						$params = array(
							'zws-id'	=> $id,	
							'zpid'		=> $comp['zpid']			
						);	
			
						$ch = curl_init('http://www.zillow.com/webservice/GetUpdatedPropertyDetails.htm'); 
						curl_setopt($ch, CURLOPT_VERBOSE, 1);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
						curl_setopt($ch, CURLOPT_TIMEOUT, 30);	
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));	
						
						$response = curl_exec($ch);	
						curl_close($ch);
						
						$xml = simplexml_load_string($response);
						$json = json_encode($xml);
						$data = json_decode($json,true);	
						$code = $data['message']['code'];
						$code_text = $data['message']['text'];						
						$latest = $data['response'];
						
						
						if( $code < 500 && $ctr <= 10 ) {					
							$address = $comp['address'];
							$editedFacts = $latest['editedFacts'];
							$comp_street_address = $address['street'] . ', <br /> ' . $address['city'] . ' ' . $address['state'] .' ' . $address['zipcode'];
							
							//Valuaction computation					
							$finished_sq_ft = $editedFacts['finishedSqFt'];
							$price_sqft = $comp['lastSoldPrice'] / $finished_sq_ft;
							$average_computation +=  $price_sqft;
							array_push($sqft_value_arr, $price_sqft);							
							
							$comp_markers[$marker_ctr] = array();
							array_push($comp_markers[$marker_ctr], $address['street']);
							array_push($comp_markers[$marker_ctr], $address['longitude']);
							array_push($comp_markers[$marker_ctr], $address['latitude']);							
							array_push($comp_markers[$marker_ctr], $ctr);							
							array_push($comp_markers[$marker_ctr], get_stylesheet_directory_uri() . '/images/gray-dotless.png');							
							array_push($comp_markers_obj, $comp_markers[$marker_ctr]);
							$err_suc['msg'] .= '<tr>													
													<td>' . $ctr . '</td>
													<td>' . $comp['@attributes']['score'] . '</td>
													<td class="nowrap">' . $comp_street_address . '</td>
													<td>' . $finished_sq_ft . '</td>
													<td>' . $editedFacts['bedrooms'] . '</td>
													<td>' . $editedFacts['bathrooms'] . '</td>
													<td>' . $editedFacts['yearBuilt'] . '</td>
													<td>' . $editedFacts['lotSizeSqFt'] . '</td>
													<td>' . $editedFacts['numFloors'] . '</td>
													<td>' . $comp['lastSoldDate'] . '</td>
													<td>$' . number_format($comp['lastSoldPrice'], 2) . '</td>
													<td>$' . number_format($price_sqft, 2) . '</td>
												</tr>';
												
							$ctr++;
							$marker_ctr++;
						}
					}
					$ctr = $ctr - 1;
					$err_suc['low'] = '$' . number_format(min($sqft_value_arr) * $principal['finishedSqFt'], 2);
					$err_suc['average'] = '$' . number_format(($average_computation / $ctr) * $principal['finishedSqFt'], 2);
					$err_suc['high'] = '$' . number_format(max($sqft_value_arr) * $principal['finishedSqFt'], 2);						
					$err_suc['comp_markers'] = $comp_markers_obj;						
					
					$err_suc['msg'] .= '</tbody></table></div>';
					
					if ( $home_description != '' ) {
					
					$err_suc['msg'] .= '<div id="home_desc_container" class="row">
											<div class="col-md-12">
												<h3 class="property_header">Home Description</h3>
												<p class="text-center"> ' . $home_description . ' </p> 
											</div>
										</div>';
										
					}
										
					$err_suc['msg'] .= '<div id="other_details_container" class="row">
											<h3 class="property_header">Other Property Information</h3>
											
											<div class="col-md-6">
												<table class="table table-bordered">
													<tbody>
														<tr>
															<td>Neighborhood</td>
															<td>' . $neighborhood . '</td>
														</tr>
														<tr>
															<td>School District</td>
															<td>' . $school_district . '</td>
														</tr>
														<tr>
															<td>Elementary School</td>
															<td>' . $elementary_school . '</td>
														</tr>
														<tr>
															<td>Middle School</td>
															<td>' . $middle_school . '</td>
														</tr>
													</tbody>
												</table>
											</div>
											
											<div class="col-md-6">
												<table class="table table-bordered">
													<tbody>
														<tr>
															<td>Basement</td>
															<td>' . $updated_edited_facts['basement'] . '</td>
														</tr>	
														<tr>
															<td>Roof Type</td>
															<td>' . $updated_edited_facts['roof'] . '</td>
														</tr>
														<tr>
															<td>View</td>
															<td>' . $updated_edited_facts['view'] . '</td>
														</tr>
														<tr>
															<td>Parking Type</td>
															<td>' . $updated_edited_facts['parkingType'] . '</td>
														</tr>
														<tr>
															<td>Heating Sources</td>
															<td>' . $updated_edited_facts['heatingSources'] . '</td>
														</tr>
														<tr>
															<td>Heating System</td>
															<td>' . $updated_edited_facts['heatingSystem'] . '</td>
														</tr>
														<tr>
															<td>Appliances</td>
															<td>' . $updated_edited_facts['appliances'] . '</td>
														</tr>
														<tr>
															<td>Floor Covering</td>
															<td>' . $updated_edited_facts['floorCovering'] . '</td>
														</tr>
														<tr>
															<td>Rooms</td>
															<td>' . $updated_edited_facts['rooms'] . '</td>
														</tr>
													</tbody>
												</table>
											</div>
											
										</div>';
										
					$err_suc['msg'] .= '<div id="zillow_link_container" class="row">
											<div class="col-md-12">
												<h3 class="property_header">Zillow Links</h3>
												<ul>													
													<li><a href="' . $sp_links['graphsanddata'] . '" target="_blank"> Graphs And Data </a></li>
													<li><a href="' . $sp_links['mapthishome'] . '" target="_blank"> Map this Home </a></li>
													<li><a href="' . $sp_links['comparables'] . '" target="_blank"> Comparables </a></li>
												</ul>
											</div>
										</div>';

				}
			} else {
				$err_suc['error'] = true;
				$err_suc['msg'] = 'No data available found.';
			}
			
			echo json_encode($err_suc);
		}	
		
		exit;
	}
	
	//Profile
	add_action( 'wp_ajax_sc_profile_update', 'sc_ajax_callback' );
	add_action( 'wp_ajax_nopriv_sc_profile_update', 'sc_ajax_callback' );	
	add_action( 'wp_ajax_sc_logout', 'sc_ajax_callback' );
	add_action( 'wp_ajax_nopriv_sc_logout', 'sc_ajax_callback' );
	
	//Price
	add_action( 'wp_ajax_sc_get_seller_price', 'sc_ajax_callback' );
	add_action( 'wp_ajax_nopriv_sc_get_seller_price', 'sc_ajax_callback' );
	
	//Login
	add_action( 'wp_ajax_sc_login_action', 'sc_ajax_callback' );
	add_action( 'wp_ajax_nopriv_sc_login_action', 'sc_ajax_callback' );
	
	//Register
	add_action( 'wp_ajax_sc_registration_form_submit', 'sc_ajax_callback' );
	add_action( 'wp_ajax_nopriv_sc_registration_form_submit', 'sc_ajax_callback' );
	
	//Zillow API
	add_action( 'wp_ajax_sc_get_deep_comps', 'sc_ajax_callback' );
	add_action( 'wp_ajax_nopriv_sc_get_deep_comps', 'sc_ajax_callback' );
	
	function sc_scripts(){	
		#wp_enqueue_style( 'datepicker-style', get_template_directory_uri() . '/css/bootstrap-datepicker.min.css', array(), '' );
		wp_enqueue_script( 'masked', get_stylesheet_directory_uri() . '/js/mask_input.js', array('jquery'));
		#wp_enqueue_script( 'datepicker-js', get_stylesheet_directory_uri() . '/js/bootstrap-datepicker.min.js', array('jquery'));		
	}
	add_action( 'wp_enqueue_scripts', 'sc_scripts', 100 );	
?>