<?php
	//Get updated data	
	function sc_zillow_get_updated_data_api($id, $zpid) {
		$params = array(
			'zws-id'	=> $id,	
			'zpid'		=> $zpid			
		);	

		$ch = curl_init('http://www.zillow.com/webservice/GetUpdatedPropertyDetails.htm');
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));	
		
		$response = curl_exec($ch);	
		curl_close($ch);
		
		$xml = simplexml_load_string($response);
		$json = json_encode($xml);
		$data = json_decode($json,true);	
		return $data;		
	}
	
	//Get deep comps
	function sc_zillow_get_deep_comp_api($id, $zpid, $count) {
		$params = array(
			'zws-id'	=> $id,	
			'zpid'		=> $zpid,		
			'count'		=> $count
		);
		
		$ch = curl_init('http://www.zillow.com/webservice/GetDeepComps.htm');
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));	
		
		$response = curl_exec($ch);	
		curl_close($ch);
		
		$xml = simplexml_load_string($response);
		$json = json_encode($xml);
		$data = json_decode($json,true);
		
		return $data;
	}
	
	//Get zpid
	function sc_zillow_search_api($id, $address, $citystatezip) {
		$params = array(
			'zws-id'		=> $id,	
			'address'		=> $address,		
			'citystatezip'	=> $citystatezip		
		);				
		
		$ch = curl_init('http://www.zillow.com/webservice/GetSearchResults.htm');
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));	
		
		$response = curl_exec($ch);	
		curl_close($ch);
		
		$xml = simplexml_load_string($response);
		$json = json_encode($xml);
		$data = json_decode($json, true);	
		
		if( isset( $data['response']['results']['result']['zpid'] )) { 
			$zpid = $data['response']['results']['result']['zpid'];	
		} else  {
			$zpid = $data['response']['results']['result'][0]['zpid'];			
		}	
		
		return $zpid;
	}
	
	//CHART
	function sc_zillow_chart_api($id, $zpid, $unit_type) {
		$params = array(
			'zws-id'	=> $id,	
			'zpid'	=> $zpid,
			'unit-type'	=> $unit_type,
			'width'	=> 500,
			'height' => 300,
		);
		
		$ch = curl_init('http://www.zillow.com/webservice/GetChart.htm');
		curl_setopt($ch, CURLOPT_VERBOSE, 1);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_TIMEOUT, 30);	
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));	
		
		$response = curl_exec($ch);	
		curl_close($ch);		
		
		$xml = simplexml_load_string($response);
		$json = json_encode($xml);
		$data = json_decode($json,true);			
		
		return $data['response']['url'];		
	}