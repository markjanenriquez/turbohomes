<?php 
	//Debugger
	function pr( $var ) {		
		echo '<pre>';
		print_r( $var );
		echo '</pre>';
	}
	
	/* Generate random character */
	function gen_rand_char( $length ) {
		$characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		
		$random_character = '';
		for ($i = 0; $i < $length; $i++) {
			$random_character .= $characters[rand(0, strlen($characters) - 1)];
		}
		return $random_character;
	}
	
	/* Get User Role */
	function get_user_role( $id ) {
		global $wpdb;
		$role = $wpdb->get_var("SELECT meta_value FROM " . $wpdb->usermeta . " WHERE meta_key = 'wp_capabilities' AND user_id = " . $id);
		if(!$role) return 'non-user';
		$rarr = unserialize($role);
		$roles = is_array($rarr) ? array_keys($rarr) : array('non-user');
		return $roles[0];
	}
	
	//Send Password Confirmation
	function sc_send_password_confirmation($email, $validation) {		
		$sendername = get_bloginfo('name');						
		$sendermail = get_bloginfo('admin_email');
		$validation_link = get_home_url() . '/update-password/?validation_code=' . $validation . '&turbo=home';
		$headers = "MIME-Version: 1.0\r\n" .
				"From: ".$sendername." "."<".$sendermail.">\r\n" . 
				"Content-Type: text/html; charset=UTF-8";
		$subject = 'Turbo Homes Password Confirmation';
		$message = '<div id="body" style="background-color: #ff7900; padding: 25px;">
						<div id="container" style="margin: 0 auto; width: 80%; display: table; color: #383838; line-height: 150%; background-color: #FFF; text-align: center; font-family: Arial,Helvetica,sans-serif;">
							<div id="head" style="padding: 30px 10px 10px;">
								<img src="http://awesomarky.com/turbohomes/wp-content/uploads/2017/07/logo.png" alt="Turbo Homes Logo" />								
							</div>
							
							<div id="body" style="float: left; width: 80%; font-size: 15px;padding: 0 10% 20px;"> 								
								<p>Please click this <a href="' . $validation_link .'" target="_blank">link</a> to confirm change of password.</p>
							</div>							
						</div>
					</div>'; 		
		
		return wp_mail( $email, $subject, $message, $headers );
	}
	
	//Send Email Validation
	function sc_send_email_validation($email, $validation) {
		$sendername = get_bloginfo('name');						
		$sendermail = get_bloginfo('admin_email');
		$validation_link = get_home_url() . '/sign-up/?validation_code=' . $validation . '&turbo=home';
		$headers = "MIME-Version: 1.0\r\n" .
				"From: ".$sendername." "."<".$sendermail.">\r\n" . 
				"Content-Type: text/html; charset=UTF-8";
		$subject = 'Turbo Homes User Validation';
		$message = '<div id="body" style="background-color: #ff7900; padding: 25px;">
						<div id="container" style="margin: 0 auto; width: 80%; display: table; color: #383838; line-height: 150%; background-color: #FFF; text-align: center; font-family: Arial,Helvetica,sans-serif;">
							<div id="head" style="padding: 30px 10px 10px;">
								<img src="http://awesomarky.com/turbohomes/wp-content/uploads/2017/07/logo.png" alt="Turbo Homes Logo" />
								<h3 style="margin: 10px 0 0 0;"> Welcome to the Turbo family! </h3>
							</div>
							
							<div id="body" style="float: left; width: 80%; font-size: 15px;padding: 0 10% 20px;"> 
								
								<p>Please click this <a href="' . $validation_link .'" target="_blank">link</a> to validate your account.</p>
							</div>							
						</div>
					</div>'; 
		
		if  (wp_mail( $email, $subject, $message, $headers ) ) {			
			$msg = 'You have been registered. Please check and validate your email address.';
		} else {
			$msg = 'Email not supported. Please contact Turbo Homes.';
		}
		return $msg;
	}
	
	//Get user ID for validation
	function get_user_id_by_validation($validation) {
		global $wpdb;
		$user = reset(get_users(
			array(
				'sc_user_validation_char' => $meta_key,
				'meta_value' => $validation,
				'number' => 1,
				'count_total' => false
			)
		));
		
		return $user->ID;
	}
	
	//Get user ID for password confirmation
	function get_user_id_by_password_confirmation($validation) {
		global $wpdb;
		$user = reset(get_users(
			array(
				'sc_user_password_code' => $meta_key,
				'meta_value' => $validation,
				'number' => 1,
				'count_total' => false
			)
		));
		
		return $user->ID;
	}
	
	
	//Validate user
	function validate_user_by_id( $id ) {
		global $wpdb;
		if ( update_user_meta($id, 'sc_user_approved', 1) ) {	
			delete_user_meta( $id, 'sc_user_validation_char' );
			wp_set_auth_cookie( $id, false );
			wp_redirect( home_url( '/dashboard/' ) );  
		}	
	}	
	
	//Confirm password update
	function confirm_password_update( $id ) {
		global $wpdb;	
		$sc_password = get_user_meta( $id, 'sc_user_temp_password', TRUE ); 
		wp_set_password($sc_password, $id);		
		wp_set_auth_cookie( $id, false );
		delete_user_meta( $id, 'sc_user_temp_password' );
		delete_user_meta( $id, 'sc_user_password_code' );
		wp_redirect( home_url( '/dashboard/' ) );  		
	}		
	
	//login
	function redirect_login_page() {		
		$page_viewed = basename($_SERVER['REQUEST_URI']);  		
		if ( $page_viewed == "wp-login.php" || $page_viewed == "wp-admin" && $_SERVER['REQUEST_METHOD'] == 'GET' || $page_viewed == "wp-login.php?checkemail=confirm" ) {  			
			if ( ! is_user_logged_in() ) {
				wp_redirect( home_url( '/login/' ) );  
				exit; 
			} else {
				if ( get_user_role(get_current_user_id()) != 'administrator' ) {
					wp_redirect( home_url( '/dashboard/' ) );  
				}
			}
		}  
	}  
	add_action('init','redirect_login_page');

	//logout
	function logout_page() {
		wp_redirect( home_url( '/login/' ) );  
		exit;  
	}  
	add_action('wp_logout','logout_page');
		
	function start_buffer_output() {
        ob_start();
	}
	add_action('init', 'start_buffer_output');