/* <![CDATA[ */
jQuery(document).ready(function ($) {		
	var dollarAmount = Math.round($('#moneySliderHandle').attr('aria-valuenow'));				
	var agentFee = Math.round(dollarAmount * .03);				
	var seller = agentFee - 100;
	$('#sliderValue').text(nFormatter(dollarAmount));				
	$('#savingsAmount, #seller_savingsAmount').text(addCommas(seller));
	
	$('#moneySlider').on('moved.zf.slider', function () {
		var dollarAmount = Math.round($('#moneySliderHandle').attr('aria-valuenow'));				
		var agentFee = Math.round(dollarAmount * .03);				
		var seller = agentFee - 100;
		$('#sliderValue').text(nFormatter(dollarAmount));				
		$('#savingsAmount, #seller_savingsAmount').text(addCommas(seller));
	});	
	// var data = {'action': 'sc_get_seller_price'}	
	// $.post('http://awesomarky.com/turbohomes/wp-admin/admin-ajax.php', data, function(response) {
	
	// 	var dollarAmount = Math.round($('#moneySliderHandle').attr('aria-valuenow'));				
	// 	var agentFee = Math.round(dollarAmount * .03);				
	// 	var seller = agentFee - response;
	// 	$('#sliderValue').text(nFormatter(dollarAmount));				
	// 	$('#savingsAmount, #seller_savingsAmount').text(addCommas(seller));
	
	// 	$('#moneySlider').on('moved.zf.slider', function () {
	// 		var dollarAmount = Math.round($('#moneySliderHandle').attr('aria-valuenow'));				
	// 		var agentFee = Math.round(dollarAmount * .03);				
	// 		var seller = agentFee - response;
	// 		$('#sliderValue').text(nFormatter(dollarAmount));				
	// 		$('#savingsAmount, #seller_savingsAmount').text(addCommas(seller));
	// 	});	
		
	// });	
});
/* ]]> */	