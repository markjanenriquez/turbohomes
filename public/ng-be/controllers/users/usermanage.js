'use strict';

/* Controllers */
app.controller('usermanageCtrl', function($scope, Config, anchorSmoothScroll, $location, usermanageFactory, $uibModal, $state, store) {

    // $localStorage.page = 0;
    // $localStorage.keyword = null;

    $scope.page = 0
    $scope.keyword = null;
    $scope.sortBy = "status";
    $scope.order = "asc";
    $scope.maxSize = 5;
    $scope.index = 0;
    $scope.dataList = [];

    $scope.alerts = [];

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    var userlist = function() {
        $scope.listloading = true;
        var listquery = {
            page: $scope.page,
            keyword: $scope.keyword,
            sortBy: $scope.sortBy,
            order: $scope.order
        };

        usermanageFactory.list(listquery, function(data) {
            //console.log(data.dataList,"data")
            $scope.dataList = data.dataList;
            $scope.listofusers = data.data;
            $scope.bigTotalItems = data.total;
            $scope.listloading = false;
            $scope.bigCurrentPage = $scope.page;
            $scope.index = data.from;
        });

    };

    $scope.setRole = function(role){
        var strRole = "";
        switch(role){
            case 0:
              strRole = "Administrator";
            break;
            case 1:
              strRole = "Buyer";
            break;
            case 2:
              strRole = "Seller";
            break;
            case 3:
              strRole = "Both";
            break;
            case 7:
              strRole = "Superadmin";
            break;
        }
        return strRole;
    }; 

    $scope.columns = [{
        col: "ID",
        value: "id"
    }, {
        col: "Full Name",
        value: "firstname"
    }, {
        col: "Role",
        value: "role"
    }, {
        col: "Status",
        value: "status"
    }];

    $scope.sorting = {};

    var assignSorting = function() {
        angular.forEach($scope.columns, function(column) {
            $scope.sorting[column.value] = {
                sortBy: column.value,
                order: column.value == "status" ? "asc" : "desc"
            };
        });
    };

    assignSorting();

    $scope.sort = function(sorting) {
        var sortBy = sorting.sortBy;
        var order = sorting.order;
        $scope.sortBy = sortBy;
        $scope.sorting[sortBy]['order'] = order == "asc" ? "desc" : "asc";
        $scope.order = $scope.sorting[sortBy]['order'];
        userlist();
    };

    userlist();

    $scope.search = function(searchkeyword) {
        $scope.keyword = searchkeyword;
        userlist();
    };

    $scope.reset = function() {
        $scope.page = 0
        $scope.keyword = null;
        $scope.sortBy = "status";
        $scope.order = "asc";
        userlist();
    };

    $scope.setPage = function(pageNo) {
        $scope.page = pageNo;
        userlist();
    };

    $scope.edituser = function(userid) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'useredit',
            controller: usereditCtrl,
            resolve: {
                userid: function() {
                    return userid;
                }
            }
        });
    };

    var usereditCtrl = function($scope, $uibModalInstance, userid) {
        // $localStorage.userid = userid;
        store.set('userid', userid);
        $scope.ok = function() {
            $state.go('useredit');
            $uibModalInstance.dismiss('cancel');
        }
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        }

    };

    $scope.deleteuser = function(userid) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'userdelete',
            controller: userdeleteCtrl,
            resolve: {
                userid: function() {
                    return userid;
                }
            }
        });
    };

    var deletealert = function(datatype, msg) {
        $scope.alerts[0] = {
            type: datatype,
            msg: msg
        };
    }

    var userdeleteCtrl = function($scope, $uibModalInstance, userid) {
        var deletequery = {
            'userid': userid
        };
        $scope.ok = function() {
            usermanageFactory.delete(deletequery, function(data) {
                deletealert(data.type, data.msg);
                $uibModalInstance.dismiss('cancel');
                userlist();
            });
        }
        $scope.cancel = function() {
            $uibModalInstance.dismiss('cancel');
        }

    };


}); //end of controller