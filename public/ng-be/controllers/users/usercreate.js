'use strict';

/* Controllers */
app.controller('usercreateCtrl', function ($scope, Config, anchorSmoothScroll, $location, usercreateFactory, FileUploader) {

  var profilepicname = '';

  $scope.alerts = [];

  $scope.user = {

  };

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

	var isallfilled = false;

	$scope.saveuser = function(user){
    console.log(user,"user")
    useridvalidation(user.userid);
    passwordvalidation(user.password);
    repasswordvalidation(user.password, user.repassword);
    lastnamevalidation(user.lastname);
    firstnamevalidation(user.firstname);
    addressvalidation(user.address);

    if(isallfilled == true){
      user['photo'] = profilepicname;

      $scope.saveloading = true;

      usercreateFactory.create(user, function(data) {
        if(data.statuscode) {
          $scope.alerts[0] = { type: data.type, msg: data.msg };
          uploader.uploadAll()
          $location.hash('gototop');
          anchorSmoothScroll.scrollTo('gototop');
          $scope.saveloading = false;
          angular.element(".profilepic").attr("src","/img/defaultprofilepicture.png");
          clearallfield();
        }
        else{
          $scope.alerts[0] = { type: 'danger', msg: 'Server Error please try again later!' };
          $location.hash('gototop');
          anchorSmoothScroll.scrollTo('gototop');
          $scope.saveloading = false;
        }
      });

    }
    else{
      $location.hash('gototop');
      anchorSmoothScroll.scrollTo('gototop');
    }

	};

  $scope.useridchange = function(userid){
    useridvalidation(userid);
  };

  $scope.usernamechange = function(username){
    usernamevalidations(username);
  };

  $scope.passwordchange = function(password){
    passwordvalidation(password);
  };

  $scope.repasswordchange = function(password, repassword){
    repasswordvalidation(password, repassword);
  };

  $scope.lastnamechange = function(lastname){
    lastnamevalidation(lastname);
  };

  $scope.firstnamechange = function(firstname){
    firstnamevalidation(firstname);
  };

  $scope.addresschange = function(address){
    addressvalidation(address);
  };

	var usernamevalidations = function(username) {
    var postusername = {
      'username': username
    };

		if(username == '' || username == undefined || username == null){
			$scope.usernameclasstype = 'has-error';
			$scope.usernamemsg = 'Username is required!'
			$scope.usernameicontype = 'fa fa-times-circle-o';
			$location.hash('gototop');
          anchorSmoothScroll.scrollTo('gototop');

			isallfilled = false;
			$scope.boxtype = 'box-danger'
		}
		else{
      $scope.userspinner = true;
      usercreateFactory.checkusername(postusername, function(data){

        if(data.statuscode == 200){
          $scope.usernameclasstype = 'has-success';
          $scope.usernamemsg = ''
          $scope.usernameicontype = 'fa fa-check';

          isallfilled = true;
          $scope.boxtype = 'box-success'
        }
        else{
          $scope.usernameclasstype = 'has-error';
          $scope.usernamemsg = 'Username is required!'
          $scope.usernameicontype = 'fa fa-times-circle-o';
          $location.hash('gototop');
              anchorSmoothScroll.scrollTo('gototop');

          isallfilled = false;
          $scope.boxtype = 'box-danger'
        }
      });
			
		}

	};

  var useridvalidation = function(userid){

    var postuserid = {
      'userid': userid
    };

    if(userid == '' || userid == undefined || userid == null) {
      $scope.useridclasstype = 'has-error';
      $scope.useridmsg = 'User ID is required!';
      $scope.useridicontype = 'fa fa-times-circle-o';

      isallfilled = false;
      $scope.boxtype = 'box-danger'
    }
    else {
      $scope.userspinner = true;
      usercreateFactory.checkuserid(postuserid, function(data){

        if(data.statuscode == 200){
          $scope.useridclasstype = 'h';
          $scope.useridmsg = ''
          $scope.useridicontype = '';

          isallfilled = true;
          $scope.boxtype = 'box-success'
          $scope.userspinner = false;
        }
        else{
          $scope.useridclasstype = 'has-error';
          $scope.useridmsg = data.msg;
          $scope.useridicontype = 'fa fa-times-circle-o';

          isallfilled = false;
          $scope.boxtype = 'box-danger';
          $scope.userspinner = false;
        }
      });
    }

  };

  var passwordvalidation = function(password){{

    if(password == '' || password == undefined || password == null){
      $scope.passwordclasstype = 'has-error';
      $scope.passwordmsg = 'Password is required!';
      $scope.passwordicontype = 'fa fa-times-circle-o';

      isallfilled = false;
      $scope.boxtype = 'box-danger';
    }
    else{
      if(password.length < 6){
        $scope.passwordclasstype = 'has-error';
        $scope.passwordmsg = '';
        $scope.passwordicontype = 'fa fa-times-circle-o';

        isallfilled = false;
        $scope.boxtype = 'box-danger';
      }
      else{
        $scope.passwordclasstype = '';
        $scope.passwordmsg = '';
        $scope.passwordicontype = '';

        isallfilled = true;
        $scope.boxtype = 'box-success';
      }
    }

  }};

  var repasswordvalidation = function(password,repassword){{

    if(repassword == '' || repassword == undefined || repassword == null){
      $scope.repasswordclasstype = 'has-error';
      $scope.repasswordmsg = 'Re-enter Password is required!';
      $scope.repasswordicontype = 'fa fa-times-circle-o';

      isallfilled = false;
      $scope.boxtype = 'box-danger';
    }
    else{
      if(repassword.length < 6){
        $scope.repasswordclasstype = 'has-error';
        $scope.repasswordmsg = '';
        $scope.repasswordicontype = 'fa fa-times-circle-o';

        isallfilled = false;
        $scope.boxtype = 'box-danger';
      }
      else{
        if(repassword != password){
          $scope.repasswordclasstype = 'has-error';
          $scope.repasswordmsg = 'Password does not Match!';
          $scope.repasswordicontype = 'fa fa-times-circle-o';

          isallfilled = false;
          $scope.boxtype = 'box-danger';
        }
        else{
          $scope.repasswordclasstype = '';
          $scope.repasswordmsg = '';
          $scope.repasswordicontype = '';

          isallfilled = true;
          $scope.boxtype = 'box-success';
        }
      }
    }

  }};

  var lastnamevalidation = function(lastname){

    if(lastname == '' || lastname == undefined || lastname == null) {
      $scope.lastnameclasstype = 'has-error';
      $scope.lastnamemsg = 'is required!';
      $scope.lastnameicontype = 'fa fa-times-circle-o';

      isallfilled = false;
      $scope.boxtype = 'box-danger';
    }
    else {
      $scope.lastnameclasstype = '';
      $scope.lastnamemsg = '';
      $scope.lastnameicontype = '';

      isallfilled = true;
      $scope.boxtype = 'box-success';
    }

  };

  var firstnamevalidation = function(firstname){

    if(firstname == '' || firstname == undefined || firstname == null) {
      $scope.firstnameclasstype = 'has-error';
      $scope.firstnamemsg = 'is required!';
      $scope.firstnameicontype = 'fa fa-times-circle-o';

      isallfilled = false;
      $scope.boxtype = 'box-danger';
    }
    else {
      $scope.firstnameclasstype = '';
      $scope.firstnamemsg = '';
      $scope.firstnameicontype = '';

      isallfilled = true;
      $scope.boxtype = 'box-success';
    }

  };

  var addressvalidation = function(address){

    if(address == '' || address == undefined || address == null) {
      $scope.addressclasstype = 'has-error';
      $scope.addressmsg = 'is required!';
      $scope.addressicontype = 'fa fa-times-circle-o';

      isallfilled = false;
      $scope.boxtype = 'box-danger';
    }
    else {
      $scope.addressclasstype = '';
      $scope.addressmsg = '';
      $scope.addressicontype = '';

      isallfilled = true;
      $scope.boxtype = 'box-success';
    }

  };

  var clearallfield = function(){
    $scope.user.userid = '';
    $scope.user.email = '';
    $scope.user.password = '';
    $scope.user.repassword = '';
    $scope.user.lastname = '';
    $scope.user.firstname = '';
    $scope.user.middlename = '';
    $scope.user.address = '';
    $scope.user.contact = '';
    $scope.user.role = 0;
    $scope.user.gender = 0;
  }

  var uploader = $scope.uploader = new FileUploader({
    url: Config.BaseURL + '/api/users/fileuploader/imagefile'
  });

        // FILTERS

  uploader.filters.push({
      name: 'customFilter',
      fn: function(item /*{File|FileLikeObject}*/, options) {
          return this.queue.length < 10;
      }
  });

        // CALLBACKS

  uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
      console.info('onWhenAddingFileFailed', item, filter, options);
  };
  uploader.onAfterAddingFile = function(fileItem) {
      console.info('onAfterAddingFile', fileItem);
      profilepicname = fileItem.file.name;
  };
  uploader.onAfterAddingAll = function(addedFileItems) {
      console.info('onAfterAddingAll', addedFileItems);
  };
  uploader.onBeforeUploadItem = function(item) {
      console.info('onBeforeUploadItem', item);
  };
  uploader.onProgressItem = function(fileItem, progress) {
      console.info('onProgressItem', fileItem, progress);
  };
  uploader.onProgressAll = function(progress) {
      console.info('onProgressAll', progress);
  };
  uploader.onSuccessItem = function(fileItem, response, status, headers) {
      console.info('onSuccessItem', fileItem, response, status, headers);
  };
  uploader.onErrorItem = function(fileItem, response, status, headers) {
      console.info('onErrorItem', fileItem, response, status, headers);
  };
  uploader.onCancelItem = function(fileItem, response, status, headers) {
      console.info('onCancelItem', fileItem, response, status, headers);
  };
  uploader.onCompleteItem = function(fileItem, response, status, headers) {
      console.info('onCompleteItem', fileItem, response, status, headers);
  };
  uploader.onCompleteAll = function() {
      console.info('onCompleteAll');
  };

  console.info('uploader', uploader);

}); //end of controller
