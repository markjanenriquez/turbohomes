'use strict';

/* Controllers */

app.controller('tagCtrl', function($scope, $uibModal, tagFtry, $timeout){

      $scope.page = 0
      $scope.keyword = null
      $scope.sortBy = "created_at";
      $scope.order = "asc";
      $scope.maxSize = 5;
      $scope.index = 0;
      $scope.dataList = [];

      $scope.alerts = [];

      $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
      };

      var tagList = function(){
        
        $scope.isLoading = true;

        var data = {
           page: $scope.page,
           keyword: $scope.keyword,
           sortBy : $scope.sortBy,
           order : $scope.order
        };

        tagFtry.list(data, function(data){
           console.log(data.dataList,"data")
           $scope.dataList = data.dataList;
           $scope.tagList = data.data;
           $scope.bigTotalItems = data.total;
           $scope.isLoading = false;
           $scope.bigCurrentPage = $scope.page;
           $scope.index = data.from;
        });

      };

      $scope.columns = [
         { col : "Name", value : "name"},
         { col : "Slug", value : "slug"},
         { col : "Date Created", value : "created_at"},   
      ];

      $scope.sorting = {};

      var assignSorting = function(){
          angular.forEach($scope.columns, function(column){
                $scope.sorting[column.value] = {
                     sortBy : column.value,
                     order : column.value == "created_at"? "asc" : "desc"
                };
          });
      };

      assignSorting();

      $scope.sort = function(sorting){ 
        var sortBy = sorting.sortBy;
        var order = sorting.order;
        $scope.sortBy = sortBy;
        $scope.sorting[sortBy]['order'] = order == "asc"? "desc" : "asc"; 
        $scope.order = $scope.sorting[sortBy]['order'];
        tagList();
      };

      tagList();

      $scope.search = function (searchkeyword) {
        $scope.keyword = searchkeyword;
        tagList();
      };

      $scope.reset = function(){
        $scope.page = 0;
        $scope.keyword = null;
        $scope.sortBy = "created_at";
        $scope.order = "asc";
        tagList();
      };

      $scope.setPage = function (pageNo) {
        $scope.page = pageNo;
        tagList();
      };

      $scope.tagCreateOrUpdate = function(tag){
            $uibModal.open({
                animation: true,
                templateUrl: 'tag',
                controller: function($scope, $uibModalInstance, tag){
                      
                      $scope.field = {};
                      $scope.title = "New Tag";
                      $scope.form = {

                      }
                      $scope.action = "add";

                      if(tag){
                         $scope.field = tag;
                         $scope.title = "Update Tag";
                         $scope.action = "edit";
                         tagFtry.show(tag.id, function(data){
                             $scope.field = data;
                         })
                      }

                      $scope.save = function(form){

                          form.slug.$setValidity('existed', true);

                          if(form.$invalid){
                                return;
                          }
                          
                          if($scope.action == "add"){
                            tagFtry.create($scope.field, function(response){
                                if(response.statuscode == 500){
                                  form.slug.$setValidity('existed', false);
                                }else{
                                  $uibModalInstance.close(response);
                                }
                                
                            });
                         }else{
                            tagFtry.update($scope.field, function(response){
                                if(response.statuscode == 500){
                                  form.slug.$setValidity('existed', false);
                                }else{
                                  $uibModalInstance.close(response);
                                }
                                
                            });
                         }
                      }

                      $scope.cancel = function(){
                          $uibModalInstance.dismiss('cancel');
                      }


                      $scope.slugify = function(name){
                         $scope.field.slug = "";
                         if(name){
                            $scope.field.slug = name
                            .replace(/^\s\s*/, '') // Trim start
                            .replace(/\s\s*$/, '') // Trim end
                            .toLowerCase() // Camel case is bad
                            .replace(/[^a-z0-9_\-~!\+\s]+/g, '') // Exchange invalid chars
                            .replace(/[\s]+/g, '-');
                         }
                      };

                      // $scope.$watch('field.slug', function(newVal, oldVal){
                        
                      //       if(newVal){
                      //         $timeout(function(){
                      //              tagFtry.check(newVal, function(response){
                      //                 $scope.form.Form.slug.$setValidity('existed', response.isExisted); 
                      //              })
                      //         })     
                      //       }
                          
                      // })

                      $scope.setError = function(form, field){
                          if(form[field].$invalid && (form[field].$dirty || form[field].$touched || form.$submitted)){
                              return true;
                          }

                          return false;
                      }


                },
                resolve: {
                  tag: function () {
                    return tag;
                  }
                }
            }).result.then(function (response) {
                 showAlert(response);
                 tagList();
            }, function () {
                 console.log('Modal dismissed at: ' + new Date());
            });
      }


      $scope.delete = function(id){
         $uibModal.open({
             animation : true,
             templateUrl : 'tagDelete',
             controller : function($scope, $uibModalInstance, id){
                  $scope.ok = function(){
                      tagFtry.destroy({ id : id},function(response){
                        $uibModalInstance.close(response);
                      })
                  };

                  $scope.cancel = function(){
                      $uibModalInstance.dismiss('cancel');
                  };
             },
             resolve: {
                  id : function () {
                    return id;
                  }
             }
         }).result.then(function (response) {
                showAlert(response);
                tagList();
          }, function () {
               console.log('Modal dismissed at: ' + new Date());
          })
       }

      var showAlert = function(response){
          $scope.alerts[0] = { type: response.type, msg: response.msg };
      };

})