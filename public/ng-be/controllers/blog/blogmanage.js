'use strict';

/* Controllers */

app.controller('blogmanageCtrl', function($scope, $uibModal, $state, blogFtry){
     
      $scope.page = 0
      $scope.keyword = null
      $scope.sortBy = "created_at";
      $scope.order = "asc";
      $scope.maxSize = 5;
      $scope.index = 0;
      $scope.dataList = [];

      $scope.alerts = [];

      $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
      };

      var blogList = function(){
        
        $scope.isLoading = true;

        var data = {
           page: $scope.page,
           keyword: $scope.keyword,
           sortBy : $scope.sortBy,
           order : $scope.order
        };

        blogFtry.list(data, function(data){
           //console.log(data.dataList,"data")
           $scope.dataList = data.dataList;
           $scope.blogList = data.data;
           $scope.bigTotalItems = data.total;
           $scope.isLoading = false;
           $scope.bigCurrentPage = $scope.page;
           $scope.index = data.from;
        });

      };

      $scope.columns = [
         { col : "Title", value : "title"},
         { col : "Slug", value : "slug"},
         { col : "Date Created", value : "created_at"},   
      ];

      $scope.sorting = {};

      var assignSorting = function(){
          angular.forEach($scope.columns, function(column){
                $scope.sorting[column.value] = {
                     sortBy : column.value,
                     order : column.value == "created_at"? "asc" : "desc"
                };
          });
      };

      assignSorting();

      $scope.sort = function(sorting){ 
        var sortBy = sorting.sortBy;
        var order = sorting.order;
        $scope.sortBy = sortBy;
        $scope.sorting[sortBy]['order'] = order == "asc"? "desc" : "asc"; 
        $scope.order = $scope.sorting[sortBy]['order'];
        blogList();
      };

      blogList();

      $scope.search = function (searchkeyword) {
        $scope.keyword = searchkeyword;
        blogList();
      };

      $scope.reset = function(){
        $scope.page = 0;
        $scope.keyword = null;
        $scope.sortBy = "created_at";
        $scope.order = "asc";
        blogList();
      };

      $scope.setPage = function (pageNo) {
        $scope.page = pageNo;
        blogList();
      };

      $scope.edit = function(id){
         $uibModal.open({
             animation : true,
             templateUrl : 'blogedit',
             controller : function($scope, $uibModalInstance, id){
                  $scope.ok = function(){
                      $uibModalInstance.close(id);
                  };

                  $scope.cancel = function(){
                      $uibModalInstance.dismiss('cancel');
                  };
             },
             resolve: {
                  id : function () {
                    return id;
                  }
             }
         }).result.then(function (id) {
                $state.go("blogedit", { "id": id});
          }, function () {
               console.log('Modal dismissed at: ' + new Date());
          });
      }

      $scope.delete = function(id){
         $uibModal.open({
             animation : true,
             templateUrl : 'blogdelete',
             controller : function($scope, $uibModalInstance, id){
                  $scope.ok = function(){
                      blogFtry.destroy({ id : id},function(response){
                        $uibModalInstance.close(response);
                      })
                  };

                  $scope.cancel = function(){
                      $uibModalInstance.dismiss('cancel');
                  };
             },
             resolve: {
                  id : function () {
                    return id;
                  }
             }
         }).result.then(function (response) {
                showAlert(response);
                blogList();
          }, function () {
               console.log('Modal dismissed at: ' + new Date());
          });
      }

      var showAlert = function(response){
          $scope.alerts[0] = { type: response.type, msg: response.msg };
      };

})