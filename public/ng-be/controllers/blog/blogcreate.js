'use strict';

/* Controllers */

app.controller('blogcreateCtrl', function($scope, $timeout, FileUploader, $location, $auth, anchorSmoothScroll, blogFtry, Config){
     
     $scope.field = {
        tags : [],
      };

      $scope.options = {
        language: 'en',
        allowedContent: true,
        entities: false
      };

     $scope.form = null;
     $scope.alerts = [];

     $scope.tags = [];

     $scope.boxtype = "box-default";

     var getTagList = function(){
         blogFtry.getTag(function(data){
           console.log(data,"data");
           $scope.tags = data;
         })
     };

     getTagList();

     $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
     };

     $scope.save = function(form){
       
       form.slug.$setValidity('existed', true);
       
       if(form.$invalid){
          $scope.alerts[0] = { type: "error", msg: "Please Specify all the details before submitting"};
          //$location.hash('gototop');
          anchorSmoothScroll.scrollTo('gototop');
          return false;
       }

       blogFtry.create($scope.field, function(response){
            console.log(response,"response")
            if(response.statuscode === 200){
                uploader.uploadAll();
                resetAll();
                resetCkEditor();
            }
            if(response.statuscode !== 500){
                $scope.alerts[0] = { type: response.type, msg: response.msg};
            }else{
                form.slug.$setValidity('existed', false);
            }
            anchorSmoothScroll.scrollTo('gototop');
       });
     };


     $scope.reset = function(){
          resetAll(); 
          resetCkEditor();
     };

     var resetAll = function(){
         $scope.field = {  tags : [] };
         $scope.form.$setPristine();
         $scope.form.$setUntouched();
         angular.element("#profilepic").css({
            "background-image" : "url(/img/icon-no-image.png)"
         })
         getTagList();
     };

     $scope.setError = function(form, field){
     
        if(form[field].$invalid && (form[field].$dirty || form[field].$touched || form.$submitted)){
            return true;
        }

        return false;
    }

    var resetCkEditor = function(){
         $timeout(function(){
              let $name = null;
              for(name in CKEDITOR.instances){
                  $name = name;
              }
              let $editor = CKEDITOR.instances[$name];
              $editor.on("focus", function(event){
                
                 if(!$scope.form.content.$touched){
                  $scope.form.content.$touched = true;
                  }
              
              })
              $scope.form.$setPristine();
              $scope.form.$setUntouched();
          },100)
          
    }

     $scope.onReady = function () {
        resetCkEditor();
     };

    $scope.slugify = function(name){
       $scope.field.slug = "";
       if(name){
          $scope.field.slug = name
          .replace(/^\s\s*/, '') // Trim start
          .replace(/\s\s*$/, '') // Trim end
          .toLowerCase() // Camel case is bad
          .replace(/[^a-z0-9_\-~!\+\s]+/g, '') // Exchange invalid chars
          .replace(/[\s]+/g, '-');
       }
    };
    
    var uploader = $scope.uploader = new FileUploader({
        url: Config.BaseURL + '/api/blog/upload',
        headers: { 'Authorization': 'Bearer ' + $auth.getToken() },
    });

          // FILTERS

    uploader.filters.push({
        name: 'customFilter',
        fn: function(item /*{File|FileLikeObject}*/, options) {
            return this.queue.length < 10;
        }
    });

          // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item /*{File|FileLikeObject}*/, filter, options) {
        console.info('onWhenAddingFileFailed', item, filter, options);
    };
    uploader.onAfterAddingFile = function(fileItem) {
        console.info('onAfterAddingFile', fileItem);
        console.log(fileItem.file.name,"fileName");
        $scope.field.featured_image = fileItem.file.name;
    };
    uploader.onAfterAddingAll = function(addedFileItems) {
        console.info('onAfterAddingAll', addedFileItems);
    };
    uploader.onBeforeUploadItem = function(item) {
        console.info('onBeforeUploadItem', item);
    };
    uploader.onProgressItem = function(fileItem, progress) {
        console.info('onProgressItem', fileItem, progress);
    };
    uploader.onProgressAll = function(progress) {
        console.info('onProgressAll', progress);
    };
    uploader.onSuccessItem = function(fileItem, response, status, headers) {
        console.info('onSuccessItem', fileItem, response, status, headers);
    };
    uploader.onErrorItem = function(fileItem, response, status, headers) {
        console.info('onErrorItem', fileItem, response, status, headers);
    };
    uploader.onCancelItem = function(fileItem, response, status, headers) {
        console.info('onCancelItem', fileItem, response, status, headers);
    };
    uploader.onCompleteItem = function(fileItem, response, status, headers) {
        console.info('onCompleteItem', fileItem, response, status, headers);
    };
    uploader.onCompleteAll = function() {
        console.info('onCompleteAll');
    };

    console.info('uploader', uploader);

})