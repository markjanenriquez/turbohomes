'use strict';

/* Controllers */

app.controller('resetPasswordCtrl', function ($scope, $auth,$filter, $state, forgotPasswordFactory, jwtHelper, store, $location, $window){
	
    $scope.field = {
       password : '',
       retype : '',
       id : ''
    };

    $scope.getUserID = function(id){
       $scope.field.id = id;
    };
    

    $scope.validation = {
       retype : { 
         status : true,
         msg : '',
       },
       password : {
          status : true,
          msg : ''
       }      
    };

    $scope.success = false;
   
    var isValid = function(field){
        
        $scope.validation.password.msg = "";
        $scope.validation.password.status = true; 

        if(field.password === ''){
          $scope.validation.password.msg = "New Password is empty.";
          $scope.validation.password.status = false;   
        } else if(field.password.length < 6){
          $scope.validation.password.msg = "Min length is 6 character.";
          $scope.validation.password.status = false;  
        }

        $scope.validation.retype.msg = "";
        $scope.validation.retype.status = true; 
      
        if(field.retype === ''){
          $scope.validation.retype.msg = "New retype is empty.";
          $scope.validation.retype.status = false;   
        }else if(field.retype.length < 6){
          $scope.validation.retype.msg = "Min length is 6 character.";
          $scope.validation.retype.status = false;  
        }else if(field.password !== field.retype){
          $scope.validation.retype.msg = "Password Mismatch.";
          $scope.validation.retype.status = false;  
        }
        console.log($scope.validation,"validation")

        if($scope.validation.retype.status === true && $scope.validation.password.status){
             return $scope.success = true;
        }else{
             return $scope.success = false;
        }
    }

    $scope.submit = function(field){
        
        if(!isValid(field)){
           return false;
        }

        forgotPasswordFactory.resetPassword(field, function(data){
              console.log(data,"data")
              if(data.statuscode === 401){
                 $scope.success = false;
              }else{
                 $scope.success = true;
              }
        });

        
    };

  
    var access_token = $auth.getToken();
    if(access_token){
        $window.location.href = "/admin/main";
    }

})
