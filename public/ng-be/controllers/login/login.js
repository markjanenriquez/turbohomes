'use strict';

/* Controllers */

app.controller('loginCtrl', function ($scope, $auth, $state, loginFactory, jwtHelper, store, $location, $window){
	
	$scope.loginmsg = true;
	$scope.errorlogin = false;
    $scope.user = { email : "", password : "" };

	$scope.userlogin = function(user){
	    $auth.login(user).then(function(data){
     	     loginFactory.info(function(data){
                     console.log(data,"list")
                     if(data.status === 401){
                        $auth.removeToken();
                     	$scope.loginmsg = false;
					    $scope.errorlogin = true;
					    $scope.errormsg =data.msg;
                     }else{
                     	store.set("userid", data.id);
				        store.set("fullname", data.fullname);
				        store.set("profilepicture", data.picture);
				        store.set("userrole", data.role);
				        $window.location.href = "/admin/main";
                    }
		     });
     	}).catch(function(response) {
            console.log(response);
            if(response.status === 401){
                $scope.loginmsg = false;
                $scope.errorlogin = true;
                $scope.errormsg = response.data.error;   
            }
            // Handle errors here, such as displaying a notification
            // for invalid email and/or password.
        });
	};
   
    var access_token = $auth.getToken();
    if(access_token){
        $window.location.href = "/admin/main";
    }

})
