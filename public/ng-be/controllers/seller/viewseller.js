'use strict';

/* Controllers */
app.controller('viewsellerCtrl', function ($scope, Config, anchorSmoothScroll, $location, sellermanageFactory, $uibModal, $state, $stateParams, offermanageFactory, toaster) {

  $scope.seller = {
    data:{
      id: $stateParams.id
    },
    viewdata:{

    },
    fetch:function(){
      var seller = this;
      sellermanageFactory.view(seller.data, function(data){
         seller.viewdata = data;
         console.log(seller.viewdata);
         seller.viewdata.contactvia = camelize(seller.viewdata.contactvia);
         seller.viewdata.apply = camelize(seller.viewdata.apply);

          if(seller.viewdata.contract_status !== $scope.constant.pending) {
              seller.emailstatus();
          }
      });
    },
    emailstatus : function(){
        var seller = this;
        offermanageFactory.getemailstatus(seller.data, 'listedhome', function(data){
           seller.emailstatuslist = data;
        });
    }
  };

  $scope.constant = {
    pending: 'PENDING'
  }

  var camelize = function (str) {
    if(str) {
       return str.replace(/\b\w/g, chr => chr.toUpperCase()).replace(" ", ""); 
    }

    return str;
  };

  $scope.setContract = function() {
        var modalInstance = $uibModal.open({
            size: 'md',
            templateUrl: 'setContract.html',
            controller: function($scope, $uibModalInstance, seller) {
                $scope.data = {};

                $scope.setContract = function(form) {
                    console.log($scope.data,"data")
                    if(!form.$invalid){
                        $scope.data.homeid = seller.id;

                        offermanageFactory.setcontract($scope.data, function(response) {
                            $scope.cancel();
                        })
                    }
                }

                $scope.cancel = function() {
                    $uibModalInstance.dismiss('cancel');
                }

            },
            resolve: {
                seller: function() {
                    return $scope.seller.viewdata;
                }
            }
        });

        modalInstance.result.finally(function() {
            $scope.seller.fetch();
            $scope.seller.emailstatus();
        });
    }

    $scope.viewContract = function() {
        var modalInstance = $uibModal.open({
            size: 'md',
            templateUrl: 'viewContract.html',
            controller: function($scope, $uibModalInstance, seller) {
                $scope.contract = {};
                $scope.minDate = new Date();

                var loadData = function() {
                    offermanageFactory.getContract(seller.id, 'listedhome', function(data) {
                        $scope.contract = data;
                    });
                }

                loadData();

                 $scope.updateContract = function(form) {
                    if(!form.$invalid){
                        offermanageFactory.editcontract($scope.contract, function(response) {
                           $scope.cancel();
                        })
                    }
                }
                
                $scope.cancel = function() {
                    $uibModalInstance.dismiss('cancel');
                }
            },
            resolve: {
                seller: function() {
                    return $scope.seller.viewdata;
                }
            }
        });

        modalInstance.result.finally(function() {
            $scope.seller.fetch();
            $scope.seller.emailstatus();
        });
    }

    $scope.sendEmail = function(key) {
        offermanageFactory.sendEmail({key: key, id: $stateParams.id, type: 'listedhome'}, function(response) {
            if(response.type === 'success') {
                toaster.pop('success', response.msg);
                $scope.seller.emailstatus();
            } else {
                toaster.pop('error', 'Something went wrong, please try again later!');
            }
        });
    }

  return $scope.seller.fetch();
    

}); //end of controller
