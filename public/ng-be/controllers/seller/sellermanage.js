'use strict';

/* Controllers */
app.controller('sellermanageCtrl', function ($scope, Config, anchorSmoothScroll, $location, sellermanageFactory, $uibModal, $state) {

  // $localStorage.page = 0;
  // $localStorage.keyword = null;
  
  $scope.page = 0
  $scope.keyword = null;
  $scope.sortBy = "created_at";
  $scope.order = "desc";
  $scope.maxSize = 5;
  $scope.index = 0;
  $scope.dataList = [];

  $scope.alerts = [];

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

  var sellerlist = function(){
  
    $scope.listloading = true;

    var listquery = {
       page: $scope.page,
       keyword: $scope.keyword,
       sortBy : $scope.sortBy,
       order : $scope.order
    };
    
    sellermanageFactory.list(listquery, function(data){
              console.log(data.dataList,"data")
              $scope.dataList = data.dataList;
              $scope.listofseller = data.data;
              $scope.bigTotalItems = data.total;
              $scope.listloading = false;
              $scope.bigCurrentPage = $scope.page;
              $scope.index = data.from;
              //console.log(data.data,"data")
    });

  };

  $scope.columns = [
     { col : "Seller's Name", value : "fullname"},
     { col : "Seller's Email", value : "owneremail"},
     { col : "Contact", value : "ownerphone"},
     { col : "Date", value : "created_at"},
     { col : "Paypal Transaction Status", value : "status"}   
  ];

  $scope.sorting = {};

  var assignSorting = function(){
      angular.forEach($scope.columns, function(column){
            $scope.sorting[column.value] = {
                 sortBy : column.value,
                 order : column.value == "created_at"? "asc" : "desc"
            };
      });
  };

  assignSorting();


  $scope.sort = function(sorting){ 
    var sortBy = sorting.sortBy;
    var order = sorting.order;
    $scope.sortBy = sortBy;
    $scope.sorting[sortBy]['order'] = order == "asc"? "desc" : "asc"; 
    $scope.order = $scope.sorting[sortBy]['order'];
    sellerlist();   
  };
  
  sellerlist();
 
  $scope.search = function (searchkeyword) {
    $scope.keyword = searchkeyword;
    //userlist();
    sellerlist();
  };

  $scope.reset = function(){
    $scope.page = 0;
    $scope.keyword = null;
    $scope.sortBy = "created_at";
    $scope.order = "asc";
    //userlist();
    sellerlist();
  };

  $scope.setPage = function (pageNo) {
    $scope.page = pageNo;
    //userlist();
    sellerlist();
  };

  $scope.editseller = function(id){
         $uibModal.open({
             animation : true,
             templateUrl : 'useredit',
             controller : function($scope, $uibModalInstance, id){
                  $scope.ok = function(){
                      $uibModalInstance.close(id);
                  };

                  $scope.cancel = function(){
                      $uibModalInstance.dismiss('cancel');
                  };
             },
             resolve: {
                  id : function () {
                    return id;
                  }
             }
         }).result.then(function (id) {
                $state.go("editseller", { "id": id});
          }, function () {
               console.log('Modal dismissed at: ' + new Date());
          });
  }

  $scope.deleteseller = function(id){
    var modalInstance = $uibModal.open({
        templateUrl: 'sellerdelete',
        controller: sellerdeleteCtrl,
        resolve: {
          id: function () {
            return id;
          }
        }
    }).result.then(function () {
        sellerlist();
    }, function () {
         console.log('Modal dismissed at: ' + new Date());
    });
  };

  var deletealert = function(datatype, msg){
    $scope.alerts[0] = { type: datatype, msg: msg };
  }

  var sellerdeleteCtrl = function ($scope, $uibModalInstance, id) {
    var deletequery = {
      'id':id
    };
    $scope.ok = function() {
      sellermanageFactory.delete(deletequery, function(data){
        deletealert(data.type, data.msg);
        $uibModalInstance.close();
      });
    }
    $scope.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    }
    
  };

 

}); //end of controller
