'use strict';
app.controller('sellercreateCtrl', function($scope, $timeout, $filter, uuid2, store, anchorSmoothScroll, sellercreateFactory){
	
	var guid = uuid2.newuuid();
	
	$scope.field = {
		userid : store.get('userid'),
        otherowner : [],
        term_agreement : true,
        paymentmethod : "paypal",
        custom : guid,
        status : "PENDING",
        mls : 0,
	    percentpayother : 0,
        buyernoagent : "$ " + parseFloat(0).toFixed(2),
        buyerwithagent : "$ " + parseFloat(0).toFixed(2),
    };

    $scope.alerts = []; 

	$scope.form = {};

    let checkApply = function(field){
        $scope.field.apply = null;
        if(field.homevacant || field.tenantoccupied || field.permrequired || field.gatecode || field.alarmactivated){
             $scope.field.apply = true;
        }
    }

    $scope.setApply = function(){
        checkApply($scope.field);
    }

	$scope.setMask = function(via){
            $scope.field.contact = '';
            $scope.mask  = via != 'email'? "(999) 999-9999" : null;
            $scope.char = via != 'email'? "_": null;
            $scope.via_placeholder = via != "email"? "phone number" : via;
    }

	let isLoaded = {
		 buyernoagent : false,
		 buyerwithagent : false,
	}

	$scope.setError = function(form, field){
        
        if(form[field].$invalid && (form[field].$dirty || form[field].$touched || form.$submitted)){
            return true;
        }

        return false;
    };

    $scope.setBuyerWAgentVal = function($evt){
	   let val = angular.element($evt.currentTarget).val().replace(/,/g,"").replace("$ ","");
	   if($scope.field.percentpay != 'other') {
            let computedVal = (0.03 * parseInt(val)) + parseInt(val);
            var parts = (+computedVal).toFixed(2).split(".");
            var finalVal = "$ " + parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ("." + parts[1]);
            $scope.field.buyerwithagent = finalVal;
            $scope.field.buyerwithagentplus = computedVal;
       }
    };

    $scope.checkIfEmpty = function(col, event){
    	$timeout(function(){
    		if(isLoaded[col]){
    		   let val = event? $('[name="' + col + '"]').val().replace("$ ","") : $scope.field[col];
    		   validatePrice(parseInt(val), col);
    	    }
    	    isLoaded[col] = true;
    	})
    };

    var validatePrice = function(val, col){
    	$scope.form[col].$setValidity('required', val === 0 || isNaN(val)? false : true); 
    };
    
    $scope.validateEmail = function(email, col){
        $scope.form[col].$setValidity('email', $filter('isEmail')(email));
    };

    $scope.save = function(form){
    	 
    	 
         validatePrice(parseInt($scope.field['buyernoagent']), 'buyernoagent');
         validatePrice(parseInt($scope.field['buyerwithagent']), 'buyerwithagent');
         
         if(form.$invalid){
         	  $scope.alerts[0] = { type: "error", msg: "Please Specify all the details before submitting"};
              anchorSmoothScroll.scrollTo('gototop');
              return;
    	 }

    	 if(typeof $scope.field.homeaddress !== 'string') {
           
            var temphomeaddress = [];
                    
            angular.forEach($scope.field.homeaddress.address_components, function(value, key) {
                temphomeaddress.push(value.long_name);
            });

            $scope.field.homeaddress = temphomeaddress.join(", ");
        }

        sellercreateFactory.addpartialhome($scope.field, function(data){
        	if(data.statuscode === 200){
        		sellercreateFactory.addhome($scope.field, function(data){
        			 if(data.statuscode === 200){
        			 	 $scope.alerts[0] = { type: data.type, msg: "Home successfully saved!"};
	        			 guid = uuid2.newuuid();
	        			 resetAll();
        			 }else{
        			 	$scope.alerts[0] = { type: data.type, msg: data.msg };
        			 }
        			 anchorSmoothScroll.scrollTo('gototop');
        		})
        	}else{
        		  $scope.alerts[0] = { type: data.type, msg: data.msg };
        		  anchorSmoothScroll.scrollTo('gototop');
        	}
        }) 	 
    	 
    };

    $scope.addOtherOwner = function(){
       $scope.field.otherowner.push({name : '', email : '', phone : ''});
    };

    $scope.removeOtherOwner = function(index){
       $scope.field.otherowner.splice(index, 1);
    };

    $scope.closeAlert = function(index) {
        $scope.alerts.splice(index, 1);
    };

    var resetAll = function(){
         $scope.field = {
			userid : store.get('userid'),
	        otherowner : [],
	        term_agreement : true,
	        paymentmethod : "paypal",
	        custom : guid,
	        status : "PENDING",
	        mls : 0,
	        percentpayother : 0,
            buyernoagent : "$ " + parseFloat(0).toFixed(2),
            buyerwithagent : "$ " + parseFloat(0).toFixed(2),
         };
         $scope.form.$setPristine();
         $scope.form.$setUntouched();
    };

    $scope.reset = function(){
       $scope.alerts = []; 
       resetAll();
    };
})