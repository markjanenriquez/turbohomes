'use strict';

/* Controllers */

app.controller('forgotPasswordCtrl', function ($scope, $auth,$filter, $state, forgotPasswordFactory, jwtHelper, store, $location, $window){
	
    
    $scope.field = {
       email : ''
    };

    $scope.validation = {
       status : true,
       msg : '',      
    };

    $scope.success = false;

    $scope.submit = function(field){
        
        if(!isEmail(field.email)){
            return false;
        }

        forgotPasswordFactory.sendResetLink(field, function(data){
              console.log(data,"data")
              if(data.statuscode === 401){
                 $scope.success = false;
                 $scope.validation = {
                        msg : data.msg,
                        status : false
                 };
              }else{
                 $scope.success = true;
                 $scope.validation = {
                   status : true,
                   msg : '',      
                };
              }
        });

        
    }

    var isEmail = function(email){
      
      if(email === ''){
        $scope.validation = {
            msg : "Please enter your email address",
            status : false
        };
        return false;
      }else if(!$filter('isEmail')(email)){
        $scope.validation = {
            msg : "Please enter a valid email address",
            status : false
        };
        return false;
      }
       
       return true;

    };

    var access_token = $auth.getToken();
    if(access_token){
        $window.location.href = "/admin/main";
    }

})
