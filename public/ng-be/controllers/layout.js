'use strict';

app.controller("layoutCtrl", function ($scope, jwtHelper, $auth, store, $location, $window, $state) {

	
    if(!$auth.getToken()){
           store.remove("userid");
           store.remove("username");
           store.remove("userrole");
           $window.location.href = "/admin";
    }

	$scope.fullname = store.get("fullname");
	$scope.profilepicture = store.get("profilepicture");
	if(store.get('userrole') == 7) {
		$scope.userrole = "Super User";
	} else if(store.get('userrole') == 0) {
		$scope.userrole = "Administrator";
	} else if (store.get('userrole') == 1) {
		$scope.userrole = "Teacher";
	}
	// console.log($scope.userrole);

	$scope.profile = function() {
		// $localStorage.userid = store.get("userid");
		$state.go('profile');
	}

	$scope.logout = function(){
		$auth.removeToken(); 
		store.remove("userid");
		store.remove("fullname");
		store.remove("userrole");
		$window.location.href = "/admin";
	};

	$scope.getClass = function (path) {
	  if ($location.path().substr(0, path.length) === path) {
	    return 'active';
	  } else {
	    return '';
	  }
	};


})
