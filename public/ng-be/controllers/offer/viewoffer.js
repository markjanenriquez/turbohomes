'use strict';

/* Controllers */
app.controller('viewofferCtrl', function($scope, Config, anchorSmoothScroll, $location, offermanageFactory, $uibModal, $state, $stateParams, toaster) {

    $scope.offer = {
        data: {
            id: $stateParams.id
        },
        viewdata: {

        },
        fetch: function() {
            var offer = this;
            offermanageFactory.view(offer.data, function(data) {
                offer.viewdata = data;

                if(offer.viewdata.status !== $scope.constant.pending) {
                    offer.emailstatus();
                }
            });
        },
        emailstatus : function(){
            var offer = this;
            offermanageFactory.getemailstatus(offer.data, 'offer', function(data){
               offer.emailstatuslist = data;
            });
        }
    };

    $scope.constant = {
        turbo: 'Turbo Homes',
        realtor: 'Realtor',
        notsure: 'Not Sure',
        other: 'Other',
        alone: 'Alone',
        someone: 'With Someone',
        yes: 'Yes',
        no: 'No',
        turbohomes: 'Turbo Homes',
        self: 'Self',
        buyer: 'Buyer',
        seller: 'Seller',
        split: 'Split Evenly',
        conventional: 'Conventional',
        fha: 'FHA',
        va: 'VA',
        usda: 'USDA',
        assumption: 'Assumption',
        seller_carryback: 'Seller Carryback',
        'true': 'Yes',
        'false': 'No',
        pending: 'PENDING'
    }

    $scope.transformlisted = function(item) {
        if (item == 'turbo') {
            return 'Turbo Homes'
        } else if (item == 'realtor') {
            return 'Realtor'
        }
    }

    $scope.checklisted = function(item) {
        if (item == 'turbo') {
            return true;
        } else if (item == 'realtor') {
            return true;
        } else {
            return false;
        }
    }

    $scope.setContract = function() {
        var modalInstance = $uibModal.open({
            size: 'md',
            templateUrl: 'setContract.html',
            controller: function($scope, $uibModalInstance, offer) {
                $scope.data = {};

                $scope.setContract = function(form) {
                    console.log($scope.data,"data")
                    if(!form.$invalid){
                        $scope.data.offerid = offer.id;

                        offermanageFactory.setcontract($scope.data, function(response) {
                            $scope.cancel();
                        })
                    }
                }

                $scope.cancel = function() {
                    $uibModalInstance.dismiss('cancel');
                }

            },
            resolve: {
                offer: function() {
                    return $scope.offer.viewdata;
                }
            }
        });

        modalInstance.result.finally(function() {
            $scope.offer.fetch();
            $scope.offer.emailstatus();
        });
    }

    $scope.viewContract = function() {
        var modalInstance = $uibModal.open({
            size: 'md',
            templateUrl: 'viewContract.html',
            controller: function($scope, $uibModalInstance, offer) {
                $scope.contract = {};
                $scope.minDate = new Date();

                var loadData = function() {
                    offermanageFactory.getContract(offer.id, 'offer', function(data) {
                        $scope.contract = data;
                    });
                }

                loadData();

                 $scope.updateContract = function(form) {
                    if(!form.$invalid){
                        offermanageFactory.editcontract($scope.contract, function(response) {
                           $scope.cancel();
                        })
                    }
                }
                
                $scope.cancel = function() {
                    $uibModalInstance.dismiss('cancel');
                }
            },
            resolve: {
                offer: function() {
                    return $scope.offer.viewdata;
                }
            }
        });

        modalInstance.result.finally(function() {
            $scope.offer.fetch();
            $scope.offer.emailstatus();
        });
    }

    $scope.sendEmail = function(key) {
        offermanageFactory.sendEmail({key: key, id: $stateParams.id, type: 'offer'}, function(response) {
            if(response.type === 'success') {
                toaster.pop('success', response.msg);
                $scope.offer.emailstatus();
            } else {
                toaster.pop('error', 'Something went wrong, please try again later!');
            }
        });
    }

    // $scope.offer.emailstatus();

    return $scope.offer.fetch();


}); //end of controller