'use strict';

app.controller('offercreateCtrl', function($scope, $filter, $timeout, offercreateFactory, store, anchorSmoothScroll){
	console.log('offercreateCtrl')
	
	$scope.form = {};
	
	$scope.field = {
        userid : store.get('userid'),
        scfinancingamt : "$ " + parseInt(0),
        scfinancingearnestamt : "$ " + parseInt(0).toFixed(2),
        sc_financing_earnest_amt : "$ " + parseInt(0).toFixed(2),
        down_payment : "$ " + parseInt(0),
        mortgage : "$ " + parseInt(0),
        planforpayingother : "$ " + parseInt(0),
        closingcostamount : "$ " + parseInt(0),
        costnotexceed : "$ " + parseInt(0),
        inspectiondeadline : 10,
        closingdate : '45',
    };

	$scope.alerts = [];

    $scope.isDatePickerLoaded = {
       contractDate : false,
       acceptOfferDate : false,
       mustSellDate : false,
       listOneOtherDate : false,
       closingDateOther : false,
    }; 

    let isLoaded = {
        scfinancingamt : false,
        scfinancingearnestamt : false,
        sc_financing_earnest_amt : false,
        down_payment : false,
        mortgage : false,
        closingcostamount : false,
        costnotexceed : false,
        inspectiondeadline : false,
    };
    
    $scope.save = function(form){
    	
        $scope.isDatePickerLoaded = {
            contractDate : true,
            acceptOfferDate : true,
            mustSellDate : true,
            listOneOtherDate : true,
            closingDateOther : true,
        }; 

        validatePrice(parseInt($scope.field['scfinancingamt']), 'scfinancingamt');
        validatePrice(parseInt($scope.field['scfinancingearnestamt']), 'scfinancingearnestamt');
        validatePrice(parseInt($scope.field['sc_financing_earnest_amt']), 'sc_financing_earnest_amt');
        validatePrice(parseInt($scope.field['down_payment']), 'down_payment');
        validatePrice(parseInt($scope.field['mortgage']), 'mortgage');
        if($scope.field['financingmenulistfouranswer'] === 'yes'){
            validatePrice(parseInt($scope.field['closingcostamount']), 'closingcostamount');
        }
        if($scope.field['financingmenulistsixanswer'] === 'yes'){
            validatePrice(parseInt($scope.field['costnotexceed']), 'costnotexceed');   
        }
        if(form.$invalid){
            $scope.alerts[0] = { type: "error", msg: "Please Specify all the details before submitting"};
            anchorSmoothScroll.scrollTo('gototop');
    		return;
    	}
    	
    	if(typeof $scope.field.scpropertyaddress !== 'string') {
           
            var temphomeaddress = [];
                    
            angular.forEach($scope.field.scpropertyaddress.address_components, function(value, key) {
                temphomeaddress.push(value.long_name);
            });

            $scope.field.scpropertyaddress = temphomeaddress.join(", ");
        }

        if($scope.field.sccurrentaddress !== null && $scope.field.sccurrentaddress !== undefined && typeof $scope.field.sccurrentaddress !== 'string'){

            var temphomeaddress = [];
                    
            angular.forEach($scope.field.sccurrentaddress.address_components, function(value, key) {
                temphomeaddress.push(value.long_name);
            });

            $scope.field.sccurrentaddress = temphomeaddress.join(", ");
        }

    	console.log($scope.field, "field")
    	
        offercreateFactory.createoffer($scope.field, function(data){
    		 if(data.statuscode === 200){
                 $scope.alerts[0] = { type: data.type, msg: "Offer successfully saved!"};
                 resetAll();
             }else{
                $scope.alerts[0] = { type: data.type, msg: data.msg };
             }
             anchorSmoothScroll.scrollTo('gototop');
             console.log(data,"data")
    	})
    };

    $scope.setEarnestMoney = function($evt){
        let val = $($evt.currentTarget).val();
        let offerValue = val.replace("$ ", "").replace(/,/g,"");
        let computedValue = parseFloat(offerValue * 0.01).toFixed(2);
        let parts = computedValue.split(".");
        let finalVal = "$ " + parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ("." + parts[1]);
        $scope.field.scfinancingearnestamt = finalVal;
        $scope.field.sc_financing_earnest_amt = finalVal;
        $scope.field.down_payment = "$ " + parseInt(offerValue * 0.02).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $scope.field.mortgage = "$ " + parseInt(offerValue * 0.97).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    $scope.setOtherEarnestMoney = function($evt){
        let val = $($evt.currentTarget).val();
        $scope.field.scfinancingearnestamt = val;
    };
	
	$scope.setError = function(field){
        
        if($scope.form[field].$invalid && ($scope.form[field].$dirty || $scope.form[field].$touched || $scope.form.$submitted)){
            return true;
        }

        return false;
    };

    $scope.checkIfEmpty = function(col, event){
        $timeout(function(){
            if(isLoaded[col]){
               let val = event? $('[name="' + col + '"]').val().replace("$ ","") : $scope.field[col];
               validatePrice(parseInt(val), col);
            }
            isLoaded[col] = true;
        })
    };

    var validatePrice = function(val, col){
        $scope.form[col].$setValidity('required', val === 0 || isNaN(val)? false : true); 
    };

    $scope.validateEmail = function(email, col){
        $scope.form[col].$setValidity('email', $filter('isEmail')(email));
    };
    

    $scope.addAnotherBuyer = function(){
       addOtherBuyer();
    };

    $scope.removeAnotherBuyer = function(index){
        $scope.field.otherbuyer.splice(index, 1);
    }; 

    let addOtherBuyer = function(){
    	$scope.field.otherbuyer.push({
             name : '',
             email : ''
        })
    } 

    $scope.$watch('field.sectionbuyingmenuasnwer', function(newVal, olVal){
         if(newVal){
            $scope.field.otherbuyer = [];
            console.log(olVal,newVal)
            if(newVal === "someone"){
                addOtherBuyer();
            }
         }
    })

    var resetAll = function(){
        
        $scope.field = {
            userid : store.get('userid'),
            scfinancingamt : "$ " + parseInt(0),
            scfinancingearnestamt : "$ " + parseInt(0).toFixed(2),
            sc_financing_earnest_amt : "$ " + parseInt(0).toFixed(2),
            down_payment : "$ " + parseInt(0),
            mortgage : "$ " + parseInt(0),
            planforpayingother : "$ " + parseInt(0),
            closingcostamount : "$ " + parseInt(0),
            costnotexceed : "$ " + parseInt(0),
            inspectiondeadline : 10,
            closingdate : '45',
        };

        $scope.isDatePickerLoaded = {
           contractDate : false,
           acceptOfferDate : false,
           mustSellDate : false,
           listOneOtherDate : false,
           closingDateOther : false,
        }; 
        
        $scope.form.$setPristine();
        $scope.form.$setUntouched();
    };

    $scope.reset = function(){
       $scope.alerts = []; 
       resetAll();
    };

})