'use strict';

app.controller('offereditCtrl', function($scope, $filter, $timeout, offereditFactory, store, anchorSmoothScroll, $stateParams, moment){
	console.log('offereditCtrl')
    
    let dateList = {
        contract_date : null,
        accept_offer_date : null,
        must_sell_current_house_contract : null,
        importlistoneanswerother : null,
        closingdateother : null,
    };

    let inclusionList = [
       'refrigerator',
       'washer',
       'dryer',
       'aboveground',
       'other',
    ];

    let fetchDetail = function(){
        offereditFactory.fetch({ id : $stateParams.id}, function(data){
             //console.log(data);
             $scope.field = data;
             checkDate(data);
             setInclusion(data);
             $scope.field.scfinancingamt = "$ " + parseFloat(data.scfinancingamt).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
             $scope.field.scfinancingearnestamt = "$ " + parseFloat(data.scfinancingearnestamt).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
             $scope.field.sc_financing_earnest_amt = "$ " + parseFloat(data.sc_financing_earnest_amt).toFixed(2).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
             $scope.field.down_payment = "$ " + parseFloat(data.down_payment).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
             $scope.field.mortgage = "$ " + parseFloat(data.mortgage).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
             $scope.field.planforpayingother = "$ " + parseFloat(data.planforpayingother).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
             $scope.field.closingcostamount = "$ " + parseFloat(data.closingcostamount).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
             $scope.field.costnotexceed = "$ " + parseFloat(data.costnotexceed).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
             $scope.field.discusswithattorny = (data.discusswithattorny === "true");

        })
    }

    fetchDetail();

    let checkDate = function(data){
        $.each(dateList, function(key, item){
             if(data[key]){
                  dateList[key] = data[key];
             }
        })
    }

    let setDate = function(){
        $.each(dateList,function(key, item){
            if(item){
                $scope.field[key] = item;
            }
        })
    }

    let setInclusion = function(data){
       $scope.field['inclusion'] = {};
       $.each(inclusionList, function(key, item){
           if(data[item]){
              $scope.field['inclusion'][item] = (data[item] === "true");
              $scope.field['inclusion'][item+'desc'] = data[item+'desc'];
           }
       })
    }
    
    $timeout(function(){
       setDate();
    }, 1500)
	
	$scope.form = {};

	$scope.alerts = [];

    $scope.isDatePickerLoaded = {
       contractDate : false,
       acceptOfferDate : false,
       mustSellDate : false,
       listOneOtherDate : false,
       closingDateOther : false,
    }; 

    let isLoaded = {
        scfinancingamt : false,
        scfinancingearnestamt : false,
        sc_financing_earnest_amt : false,
        down_payment : false,
        mortgage : false,
        closingcostamount : false,
        costnotexceed : false,
        inspectiondeadline : false,
    };
    
    $scope.save = function(form){
    	
        $scope.isDatePickerLoaded = {
            contractDate : true,
            acceptOfferDate : true,
            mustSellDate : true,
            listOneOtherDate : true,
            closingDateOther : true,
        }; 

        validatePrice(parseInt($scope.field['scfinancingamt']), 'scfinancingamt');
        validatePrice(parseInt($scope.field['scfinancingearnestamt']), 'scfinancingearnestamt');
        validatePrice(parseInt($scope.field['sc_financing_earnest_amt']), 'sc_financing_earnest_amt');
        validatePrice(parseInt($scope.field['down_payment']), 'down_payment');
        validatePrice(parseInt($scope.field['mortgage']), 'mortgage');
        if($scope.field['financingmenulistfouranswer'] === 'yes'){
            validatePrice(parseInt($scope.field['closingcostamount']), 'closingcostamount');
        }
        if($scope.field['financingmenulistsixanswer'] === 'yes'){
            validatePrice(parseInt($scope.field['costnotexceed']), 'costnotexceed');   
        }
        if(form.$invalid){
            $scope.alerts[0] = { type: "error", msg: "Please Specify all the details before submitting"};
            anchorSmoothScroll.scrollTo('gototop');
    		return;
    	}
    	
    	if(typeof $scope.field.scpropertyaddress !== 'string') {
           
            var temphomeaddress = [];
                    
            angular.forEach($scope.field.scpropertyaddress.address_components, function(value, key) {
                temphomeaddress.push(value.long_name);
            });

            $scope.field.scpropertyaddress = temphomeaddress.join(", ");
        }
        
        if($scope.field.sccurrentaddress !== null && typeof $scope.field.sccurrentaddress !== 'string'){

            var temphomeaddress = [];
                    
            angular.forEach($scope.field.sccurrentaddress.address_components, function(value, key) {
                temphomeaddress.push(value.long_name);
            });

            $scope.field.sccurrentaddress = temphomeaddress.join(", ");
        }

    	console.log($scope.field, "field")
    	
        offereditFactory.edit($scope.field, function(data){
    		 if(data.statuscode === 200){
                 $scope.alerts[0] = { type: data.type, msg: "Offer successfully updated!"};
             }else{
                $scope.alerts[0] = { type: data.type, msg: data.msg };
             }
             anchorSmoothScroll.scrollTo('gototop');
             console.log(data,"data")
    	})
    };

    $scope.setEarnestMoney = function($evt){
        let val = $($evt.currentTarget).val();
        let offerValue = val.replace("$ ", "").replace(/,/g,"");
        let computedValue = parseFloat(offerValue * 0.01).toFixed(2);
        let parts = computedValue.split(".");
        let finalVal = "$ " + parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ("." + parts[1]);
        $scope.field.scfinancingearnestamt = finalVal;
        $scope.field.sc_financing_earnest_amt = finalVal;
        $scope.field.down_payment = "$ " + parseInt(offerValue * 0.02).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
        $scope.field.mortgage = "$ " + parseInt(offerValue * 0.97).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    };

    $scope.setOtherEarnestMoney = function($evt){
        let val = $($evt.currentTarget).val();
        $scope.field.scfinancingearnestamt = val;
    };
	
	$scope.setError = function(field){
        
        if($scope.form[field].$invalid && ($scope.form[field].$dirty || $scope.form[field].$touched || $scope.form.$submitted)){
            return true;
        }

        return false;
    };

    $scope.checkIfEmpty = function(col, event){
        $timeout(function(){
            if(isLoaded[col]){
               let val = event? $('[name="' + col + '"]').val().replace("$ ","") : $scope.field[col];
               validatePrice(parseInt(val), col);
            }
            isLoaded[col] = true;
        })
    };

    var validatePrice = function(val, col){
        $scope.form[col].$setValidity('required', val === 0 || isNaN(val)? false : true); 
    };

    $scope.validateEmail = function(email, col){
        $scope.form[col].$setValidity('email', $filter('isEmail')(email));
    };
    

    $scope.addAnotherBuyer = function(){
       addOtherBuyer();
    };

    $scope.removeAnotherBuyer = function(index){
        $scope.field.otherbuyer.splice(index, 1);
    }; 

    let addOtherBuyer = function(){
    	$scope.field.otherbuyer.push({
             name : '',
             email : ''
        })
    } 

    $scope.$watch('field.sectionbuyingmenuasnwer', function(newVal, olVal){
         if(newVal){
            if(newVal === "someone"){
                $.each($scope.field.otherbuyer, function(key, item){
                    $scope.field.otherbuyer[key] = {
                        name : item.buyersname || item.name,
                        email : item.buyersemail || item.email
                    }
                })
            }
         }
    })

})