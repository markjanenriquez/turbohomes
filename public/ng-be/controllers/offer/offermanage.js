'use strict';

/* Controllers */
app.controller('offermanageCtrl', function ($scope, Config, anchorSmoothScroll, $location, offermanageFactory, $uibModal, $state) {

  // $localStorage.page = 0;
  // $localStorage.keyword = null;
  $scope.page = 0
  $scope.keyword = null
  $scope.sortBy = "created_at";
  $scope.order = "asc";
  $scope.maxSize = 5;
  $scope.index = 0;
  $scope.dataList = [];

  $scope.alerts = [];

  $scope.closeAlert = function(index) {
    $scope.alerts.splice(index, 1);
  };

  var offerlist = function(){
    
    $scope.listloading = true;

    var listquery = {
       page: $scope.page,
       keyword: $scope.keyword,
       sortBy : $scope.sortBy,
       order : $scope.order
    };

    offermanageFactory.list(listquery, function(data){
       console.log(data.dataList,"data")
       $scope.dataList = data.dataList;
       $scope.listofoffer = data.data;
       $scope.bigTotalItems = data.total;
       $scope.listloading = false;
       $scope.bigCurrentPage = $scope.page;
       $scope.index = data.from;
    });

  };

  $scope.columns = [
     { col : "Property", value : "scpropertyaddress"},
     { col : "Buyer's Name", value : "buyersname"},
     { col : "Buyer Email", value : "buyersemail"},
     { col : "House Listed By", value : "listed_by"},
     { col : "Date", value : "created_at"},   
  ];

  $scope.sorting = {};

  var assignSorting = function(){
      angular.forEach($scope.columns, function(column){
            $scope.sorting[column.value] = {
                 sortBy : column.value,
                 order : column.value == "created_at"? "asc" : "desc"
            };
      });
  };

  assignSorting();

  $scope.sort = function(sorting){ 
    var sortBy = sorting.sortBy;
    var order = sorting.order;
    $scope.sortBy = sortBy;
    $scope.sorting[sortBy]['order'] = order == "asc"? "desc" : "asc"; 
    $scope.order = $scope.sorting[sortBy]['order'];
    offerlist();   
  };

  offerlist();

  $scope.search = function (searchkeyword) {
    $scope.keyword = searchkeyword;
    offerlist();
  };

  $scope.reset = function(){
    $scope.page = 0;
    $scope.keyword = null;
    $scope.sortBy = "created_at";
    $scope.order = "asc";
    offerlist();
  };

  $scope.setPage = function (pageNo) {
    $scope.page = pageNo;
    offerlist();
  };

  $scope.editoffer = function(id){
         $uibModal.open({
             animation : true,
             templateUrl : 'useredit',
             controller : function($scope, $uibModalInstance, id){
                  $scope.ok = function(){
                      $uibModalInstance.close(id);
                  };

                  $scope.cancel = function(){
                      $uibModalInstance.dismiss('cancel');
                  };
             },
             resolve: {
                  id : function () {
                    return id;
                  }
             }
         }).result.then(function (id) {
                $state.go("editoffer", { "id": id});
          }, function () {
               console.log('Modal dismissed at: ' + new Date());
          });
  }

  $scope.deleteoffer = function(offerid){
    var modalInstance = $uibModal.open({
        animation: true,
        templateUrl: 'offerdelete',
        controller: offerdeleteCtrl,
        resolve: {
          offerid: function () {
            return offerid;
          }
        }
    });
  };

  var deletealert = function(datatype, msg){
    $scope.alerts[0] = { type: datatype, msg: msg };
  }

  var offerdeleteCtrl = function ($scope, $uibModalInstance, offerid) {
    var deletequery = {
      'offerid':offerid
    };
    $scope.ok = function() {
      offermanageFactory.delete(deletequery, function(data){
        deletealert(data.type, data.msg);
        $uibModalInstance.dismiss('cancel');
        offerlist();
      });
    }
    $scope.cancel = function() {
      $uibModalInstance.dismiss('cancel');
    }
    
  };

 

}); //end of controller
