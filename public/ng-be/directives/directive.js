'use strict';

/* Directives */
// All the directives rely on jQuery.


app
    .directive('uiModule', ['MODULE_CONFIG', 'uiLoad', '$compile', function(MODULE_CONFIG, uiLoad, $compile) {
        return {
            restrict: 'A',
            compile: function(el, attrs) {
                var contents = el.contents().clone();
                return function(scope, el, attrs) {
                    el.contents().remove();
                    uiLoad.load(MODULE_CONFIG[attrs.uiModule])
                        .then(function() {
                            $compile(contents)(scope, function(clonedElement, scope) {
                                el.append(clonedElement);
                            });
                        });
                }
            }
        };
    }])

    .directive('datePicker', function($timeout) {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
               var startDate = scope.$eval(attr.minDate);
               element.datepicker({
                   "startDate" : startDate,
                   "format" : 'yyyy-mm-dd',
                   "forceParse" : false
               });
            }
        }
    })
    
    .directive('exportTable', function() {
        return {
            restrict: 'C',
            link:  function($scope, elm, attr){
                 $scope.$on('export-pdf', function(e, d){
                      elm.tableExport({type:'pdf', escape:false});
                 });
                 $scope.$on('export-excel', function(e, d){
                       elm.tableExport({type:'excel', escape:false,ignoreColumn: [5,6]});
                 });
                 $scope.$on('export-doc', function(e, d){
                     elm.tableExport({type: 'doc', escape:false});
                 });
                }
        }
    })

    .directive('maskMoney', function($timeout) {
        return {
            require: 'ngModel',
            restrict: 'A',
            link: function(scope, element, attrs, ngModel) {
              
                var precision = ("withDecimal" in attrs)? undefined : 0;
                var prefix = ("noPrefix" in attrs)? undefined : '$ ';

                scope.$watch(function () {
                    return ngModel.$modelValue;
                }, function(newValue) {
                   if(newValue){
                      let $newVal = newValue.toString();
                      ngModel.$setViewValue($newVal.replace(prefix, '').replace(/,/g,""))
                    }
                });

                $timeout(function() {
                    $(element).maskMoney({prefix: prefix, thousands:',', precision: precision, allowZero:true,});
                    $(element).focus();
                    $(element).blur();
                });
            }
        }
    })


