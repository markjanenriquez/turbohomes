app.directive('ensureUnique', ['$http', function($http) {
  return {
    require: 'ngModel',
    link: function(scope, ele, attrs, c) {
      scope.$watch(attrs.ngModel, function() {
        $http({
          method: 'POST',
          url: '/api/students/checkuniqueid',
          data: {'field': c.$viewValue} //c.$viewValue gets the value of the field
        }).success(function(data, status, headers, cfg) {
          c.$setValidity('unique', data.isUnique);
        }).error(function(data, status, headers, cfg) {
          // c.$setValidity('unique', false);
          console.log('there is an error in userid validation');
        });
      });
    }
  }
}]);
