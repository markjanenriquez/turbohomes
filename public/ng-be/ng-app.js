// 'use strict';

var app = angular.module('app', [
    'angular-jwt',
    'angular-storage',
    'ui.load',
    'ui.bootstrap',
    'ui.router',
    'ngPasswordStrength',
    'directives.customvalidation.customValidationTypes',
    'angularFileUpload',
    'angularMoment',
    'ngJsonExportExcel',
    'satellizer',
    'ngMessages',
    'ui.select2',
    'ng.ckeditor',
    'toaster',
    'ngAnimate',
    'google.places',
    'ckeditor',
    'ui.mask',
    'angularUUID2',
])

.run(function($rootScope, $auth, jwtHelper, store, $location, $window, generalFactory, $state) {

        $rootScope.$on('$stateChangeStart', function(event, next) {

            if (!$auth.getToken()) {
                store.remove("userid");
                store.remove("username");
                store.remove("userrole");
                $window.location.href = "/admin";
            }

            //console.log(jwtHelper.getTokenExpirationDate($auth.getToken()))
        });

    // .state("propertycreate", {
    //   url: '/property/create',
    //   controller: 'propertyCreateCtrl',
    //   templateUrl: '/admin/property/propertycreate',
    //   resolve: {
    //     deps: ['uiLoad',
    //     function(uiLoad){
    //       return uiLoad.load([
    //         '/ng-be/controllers/property/propertycreate.js',
    //         // '/ng-be/factory/seller/sellermanage.js'
    //         ]);
    //     }]
    //   },
    // })

    
    })
    .config(function($httpProvider, jwtInterceptorProvider, $authProvider, Config, $interpolateProvider, $controllerProvider, $compileProvider, $provide, $stateProvider, $urlRouterProvider, jwtOptionsProvider) {

        app.controller = $controllerProvider.register;
        app.directive = $compileProvider.directive;
        app.factory = $provide.factory;
        app.service = $provide.service;
        app.constant = $provide.constant;
        app.value = $provide.value;

        jwtOptionsProvider.config({
            whiteListedDomains: ['turbohomes.dev', 'turbohomes.com', 'turbohomes.iphitech.com' ]
        });

        jwtInterceptorProvider.tokenGetter = function(jwtHelper, $http, $auth) {

            var access_token = $auth.getToken();
            if (access_token) {
                if (jwtHelper.isTokenExpired(access_token)) {
                    return $http({
                        url: Config.BaseURL + '/api/user/token',
                        method: 'GET',
                        headers: {
                            Authorization: 'Bearer ' + access_token
                        },
                    }).then(function(response) {
                        $auth.setToken(response.data.token);
                        return response.data.token;
                    }, function(response) {
                        var error = response.data.error;
                        if (error === "token_blacklisted") {
                            return $auth.getToken();
                        } else {
                            $auth.removeToken();
                        }
                    });

                }
                return access_token;
            }
        };

        $httpProvider.interceptors.push('jwtInterceptor');


        // Satellizer configuration that specifies which API
        // route the JWT should be retrieved from
        $authProvider.loginUrl = Config.BaseURL + '/api/user/login';
        $authProvider.tokenPrefix = 'be_access';

        $interpolateProvider.startSymbol('{[{');
        $interpolateProvider.endSymbol('}]}');

        $urlRouterProvider
            .otherwise('/admin');

        $stateProvider

        
         .state("dashboard", {
            url: 'admin',
            controller: 'dashboardCtrl',
            templateUrl: '/admin/dashboard',
            resolve: {
                deps: ['uiLoad',
                    function(uiLoad) {
                        return uiLoad.load([
                            '/ng-be/controllers/dashboard.js',
                        ]);
                    }
                ]
            }
        })

        // start usercreate
        .state("usercreate", {
                url: '/usercreate',
                controller: 'usercreateCtrl',
                templateUrl: '/admin/admin/usercreate',
                authorization: true,
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/ng-be/controllers/users/usercreate.js',
                                '/ng-be/factory/users/usercreate.js'
                            ]);
                        }
                    ]
                }
            })
            // end usercreate

        // start usercreate
        .state("usermanage", {
                url: '/usermanage',
                controller: 'usermanageCtrl',
                templateUrl: '/admin/admin/usermanage',
                authorization: true,
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/ng-be/controllers/users/usermanage.js',
                                '/ng-be/factory/users/usermanage.js'
                            ]);
                        }
                    ]
                }
            })
            // end usercreate

        // start useredit
        .state("useredit", {
                url: '/useredit',
                controller: 'usereditCtrl',
                templateUrl: '/admin/admin/useredit',
                authorization: true,
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/ng-be/controllers/users/useredit.js',
                                '/ng-be/factory/users/useredit.js'
                            ]);
                        }
                    ]
                }
            })
            // end useredit

        // start useredit
        .state("profile", {
                url: '/profile',
                controller: 'usereditCtrl',
                templateUrl: '/admin/admin/userprofile',
                authorization: true,
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/ng-be/controllers/users/useredit.js',
                                '/ng-be/factory/users/useredit.js'
                            ]);
                        }
                    ]
                }
            })
            // end useredit



         .state("createoffer", {
            url: '/offer/create',
            controller: 'offercreateCtrl',
            templateUrl: '/admin/offer/createoffer',
            resolve: {
                deps: ['uiLoad',
                    function(uiLoad) {
                        return uiLoad.load([
                            '/ng-be/controllers/offer/offercreate.js',
                            '/ng-be/factory/offer/offercreate.js'
                        ]);
                    }
                ]
            },
        })

        .state("manageoffer", {
            url: '/offer/manage',
            controller: 'offermanageCtrl',
            templateUrl: '/admin/offer/manageoffer',
            resolve: {
                deps: ['uiLoad',
                    function(uiLoad) {
                        return uiLoad.load([
                            '/ng-be/controllers/offer/offermanage.js',
                            '/ng-be/factory/offer/offermanage.js'
                        ]);
                    }
                ]
            },
        })

        .state("editoffer", {
            url: '/offer/edit/:id',
            controller: 'offereditCtrl',
            templateUrl: '/admin/offer/editoffer',
            resolve: {
                deps: ['uiLoad',
                    function(uiLoad) {
                        return uiLoad.load([
                            '/ng-be/controllers/offer/offeredit.js',
                            '/ng-be/factory/offer/offeredit.js'
                        ]);
                    }
                ]
            },
        })

        .state("viewoffer", {
            url: '/offer/view/:id',
            controller: 'viewofferCtrl',
            templateUrl: '/admin/offer/viewoffer',
            resolve: {
                deps: ['uiLoad',
                    function(uiLoad) {
                        return uiLoad.load([
                            '/ng-be/controllers/offer/viewoffer.js',
                            '/ng-be/factory/offer/offermanage.js'
                        ]);
                    }
                ]
            },
        })

        //  .state("createoffertool", {
        //     url: '/offer/createoffertool',
        //     // controller: 'createoffertoolCtrl',
        //     templateUrl: '/admin/offer/createoffertool',
        //     // resolve: {
        //     //     deps: ['uiLoad',
        //     //         function(uiLoad) {
        //     //             return uiLoad.load([
        //     //                 '/ng-be/controllers/offer/offermanage.js',
        //     //                 '/ng-be/factory/offer/offermanage.js'
        //     //             ]);
        //     //         }
        //     //     ]
        //     // },
        // })
         
        .state("createseller", {
            url: '/seller/create',
            controller: 'sellercreateCtrl',
            templateUrl: "/admin/seller/sellercreate",
            resolve: {
                deps: ['uiLoad',
                    function(uiLoad) {
                        return uiLoad.load([
                            '/ng-be/controllers/seller/sellercreate.js',
                            '/ng-be/factory/seller/sellercreate.js'
                        ]);
                    }
                ]
            }
        })

        .state("manageseller", {
            url: '/seller/manage',
            controller: 'sellermanageCtrl',
            templateUrl: "/admin/seller/sellermanage",
            resolve: {
                deps: ['uiLoad',
                    function(uiLoad) {
                        return uiLoad.load([
                            '/ng-be/controllers/seller/sellermanage.js',
                            '/ng-be/factory/seller/sellermanage.js'
                        ]);
                    }
                ]
            }
        })

        .state("editseller", {
            url: '/seller/edit/:id',
            controller: 'sellereditCtrl',
            templateUrl: '/admin/seller/selleredit',
            resolve: {
                deps: ['uiLoad',
                    function(uiLoad) {
                        return uiLoad.load([
                            '/ng-be/controllers/seller/selleredit.js',
                            '/ng-be/factory/seller/selleredit.js',
                        ]);
                    }
                ]
            },
        })

        .state("viewseller", {
            url: '/seller/view/:id',
            controller: 'viewsellerCtrl',
            templateUrl: '/admin/seller/viewseller',
            resolve: {
                deps: ['uiLoad',
                    function(uiLoad) {
                        return uiLoad.load([
                            '/ng-be/controllers/seller/viewseller.js',
                            '/ng-be/factory/seller/sellermanage.js',
                            '/ng-be/factory/offer/offermanage.js'
                        ]);
                    }
                ]
            },
        })

        // start blogcreate
        .state("blogcreate", {
                url: '/blogcreate',
                controller: 'blogcreateCtrl',
                templateUrl: '/admin/blog/blogcreate',
                authorization: true,
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/ng-be/controllers/blog/blogcreate.js',
                                '/ng-be/factory/blog/blog.js'
                            ]);
                        }
                    ]
                }
            })
         .state("blogmanage", {
                url: '/blogmanage',
                controller: 'blogmanageCtrl',
                templateUrl: '/admin/blog/blogmanage',
                authorization: true,
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/ng-be/controllers/blog/blogmanage.js',
                                '/ng-be/factory/blog/blog.js'
                            ]);
                        }
                    ]
                }
         })
         .state("blogedit", {
                url: '/blogedit/:id',
                controller: 'blogeditCtrl',
                templateUrl: '/admin/blog/blogedit',
                authorization: true,
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/ng-be/controllers/blog/blogedit.js',
                                '/ng-be/factory/blog/blog.js'
                            ]);
                        }
                    ]
                }
         })
        
        // end blogcreate

        // start blogcreate
        .state("tag", {
                url: '/tag',
                controller: 'tagCtrl',
                templateUrl: '/admin/blog/tag',
                authorization: true,
                resolve: {
                    deps: ['uiLoad',
                        function(uiLoad) {
                            return uiLoad.load([
                                '/ng-be/controllers/blog/tag.js',
                                '/ng-be/factory/blog/tag.js'
                            ]);
                        }
                    ]
                }
            })
        // end blogcreate
        

    });