'use strict';

app
    .filter('trustHtml', function($sce) { 
        return $sce.trustAsHtml; 
    })

    .filter('strLimit',function($filter) {
      return function(input, limit) {
          if (! input) return;
          if (input.length <= limit) {
              return input;
          }

          return $filter('limitTo')(input, limit) + '...';
      };
    })

    .filter('isEmail', function() {
        return function(input) {
            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            return regex.test(input);
        }
    })