app.factory('generalFactory', function ($http, Config) {
  return {
    tokenrefresh: function(token, callback) {
      var asset = { token: token};
      $http({
        url: Config.BaseURL + "/users/tokenrefresh",
        method:"POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(asset)
			}).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    }
  }
})
