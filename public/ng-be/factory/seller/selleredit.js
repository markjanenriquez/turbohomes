app.factory('sellereditFactory', function($http, $q, Config){
    return {
        view : function(listquery ,callback) {
          $http({
            url: Config.BaseURL + "/api/seller/view/"+listquery.id,
            method:"POST",
            headers: {'Content-Type':'application/x-www-form-urlencoded'},
            data: $.param(listquery)
          }).then(function successCallback(response) {
              callback(response.data);
          }, function errorCallback(response) {
              callback(response.data);
          });
        },
        edit : function(data ,callback) {
          $http({
            url: Config.BaseURL + "/api/seller/edit",
            method:"POST",
            headers: {'Content-Type':'application/x-www-form-urlencoded'},
            data: $.param(data)
          }).then(function successCallback(response) {
              callback(response.data);
          }, function errorCallback(response) {
              callback(response.data);
          });
        },
        set_status : function(data ,callback) {
          $http({
            url: Config.BaseURL + "/api/seller/set_status",
            method:"POST",
            headers: {'Content-Type':'application/x-www-form-urlencoded'},
            data: $.param(data)
          }).then(function successCallback(response) {
              callback(response.data);
          }, function errorCallback(response) {
              callback(response.data);
          });
        },
    };
   
})