app.factory('sellermanageFactory', function ($http, Config) {
  return {

    list: function(listquery ,callback) {
      $http({
        url: Config.BaseURL + "/api/seller/list?page=" + listquery.page,
        method:"POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        data: $.param(listquery)
			}).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    view: function(listquery ,callback) {
      $http({
        url: Config.BaseURL + "/api/seller/view/"+listquery.id,
        method:"POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        data: $.param(listquery)
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    delete: function(deletequery ,callback) {
      $http({
        url: Config.BaseURL + "/api/seller/delete",
        method:"POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        data: $.param(deletequery)
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    }

  }
})
