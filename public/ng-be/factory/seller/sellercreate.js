app.factory('sellercreateFactory', function($http, $q, Config){
    return {
        addhome: function(data,callback){
            $http({
                url: Config.BaseURL + "/api/seller/create",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function successCallback(response) {
              callback(response.data);
            }, function errorCallback(response) {
              callback(response.data);
            });
        },
        addpartialhome: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/seller/create-partial",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function successCallback(response) {
              callback(response.data);
            }, function errorCallback(response) {
              callback(response.data);
            });
        }
    };
   
})