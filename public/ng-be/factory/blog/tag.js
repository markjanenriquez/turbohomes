app.factory('tagFtry', function($http, Config) {
    return {
        create: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/tag/create",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        list: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/tag/list?page=" + data.page,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        show: function(id, callback) {
            $http({
                url: Config.BaseURL + "/api/tag/show/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        update: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/tag/update",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        destroy: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/tag/destroy",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        }, 
        // check: function(slug, callback) {
        //     $http({
        //         url: Config.BaseURL + "/api/tag/check/" + slug,
        //         method: "GET",
        //         headers: {
        //             'Content-Type': 'application/x-www-form-urlencoded'
        //         }
        //     }).then(function successCallback(response) {
        //         callback(response.data);
        //     }, function errorCallback(response) {
        //         callback(response.data);
        //     });
        // },       
    }
})