app.factory('blogFtry', function($http, Config) {
    return {
        create: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/blog/create",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        list: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/blog/list?page=" + data.page,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        show: function(id, callback) {
            $http({
                url: Config.BaseURL + "/api/blog/show/" + id,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        update: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/blog/update",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        destroy: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/blog/destroy",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        }, 
        getTag :function(callback){
           $http({
                url: Config.BaseURL + "/api/blog/tag/get",
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        }
        // check: function(id, callback) {
        //     $http({
        //         url: Config.BaseURL + "/api/blog/check/" + id,
        //         method: "GET",
        //         headers: {
        //             'Content-Type': 'application/x-www-form-urlencoded'
        //         }
        //     }).then(function successCallback(response) {
        //         callback(response.data);
        //     }, function errorCallback(response) {
        //         callback(response.data);
        //     });
        // },      
    }
})