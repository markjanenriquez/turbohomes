app.factory('usereditFactory', function ($http, Config) {
  return {

    view: function(userid, callback) {
      $http({
        url: Config.BaseURL + "/api/user/view",
        method:"POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        data: $.param(userid)
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    update: function(user, callback) {
      $http({
        url: Config.BaseURL + "/api/user/update",
        method:"POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(user)
			}).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    checkuserid: function(postuserid, callback) {
      $http({
        url: Config.BaseURL + "/api/users/checkuserid",
        method:"POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        data: $.param(postuserid)
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    resetpassword: function(userid, callback) {
      $http({
        url: Config.BaseURL + "/api/user/resetpassword",
        method:"POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        data: $.param({userid:userid})
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    changepassword: function(pwd, callback) {
      $http({
        url: Config.BaseURL + "/api/user/changepassword",
        method:"POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        data: $.param(pwd)
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    sendResetLink: function(data, callback){
            $http({
                url: Config.BaseURL + "/api/user/password/forgot",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
    },

  }
})
