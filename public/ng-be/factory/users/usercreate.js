app.factory('usercreateFactory', function ($http, Config) {
  return {

     create: function(user, callback) {
      $http({
        url: Config.BaseURL + "/api/user/create",
        method:"POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
				data: $.param(user)
			}).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    checkuserid: function(postuserid, callback) {
      $http({
        url: Config.BaseURL + "/api/user/check/id",
        method:"POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        data: $.param(postuserid)
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    },
    checkusername: function(postusername, callback) {
      $http({
        url: Config.BaseURL + "/api/user/check/username",
        method:"POST",
        headers: {'Content-Type':'application/x-www-form-urlencoded'},
        data: $.param(postusername)
      }).then(function successCallback(response) {
          callback(response.data);
      }, function errorCallback(response) {
          callback(response.data);
      });
    }

  }
})
