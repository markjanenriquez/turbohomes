app.factory('offermanageFactory', function($http, Config) {
    return {
        list: function(listquery, callback) {
            $http({
                url: Config.BaseURL + "/api/offer/list?page=" + listquery.page,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(listquery)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        view: function(listquery, callback) {
            $http({
                url: Config.BaseURL + "/api/offer/view/" + listquery.id,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(listquery)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        delete: function(deletequery, callback) {
            $http({
                url: Config.BaseURL + "/api/offer/delete",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(deletequery)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        setcontract: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/offer/contract",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        editcontract: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/offer/contract/edit",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        getContract: function(id, type, callback) {
            $http({
                url: Config.BaseURL + "/api/offer/contract/" + id + "/" + type,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        getemailstatus: function(data, type, callback) {
            $http({
                url: Config.BaseURL + "/api/email/status/get/" + data.id + '/' + type,
                method: "GET",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        sendEmail: function(data, callback) {
            $http({
                url: Config.BaseURL + "/send/mail",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        }
    }
})