app.factory('offereditFactory', function($http, $q, Config){
    return {
        fetch: function(listquery, callback) {
            $http({
                url: Config.BaseURL + "/api/offer/view/" + listquery.id,
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(listquery)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
        edit: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/offer/edit",
                method: "POST",
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        },
    };
   
})