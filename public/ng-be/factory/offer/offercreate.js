app.factory('offercreateFactory', function($http, $q, Config){
    return {
         createoffer: function(data, callback){
            $http({
                url: Config.BaseURL + "/api/offer/create",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function successCallback(response) {
              callback(response.data);
            }, function errorCallback(response) {
              callback(response.data);
            });
         }
    };
   
})