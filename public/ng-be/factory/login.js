app.factory('loginFactory', function($http, $q, Config, $httpParamSerializer){
    return {
         info: function(callback){
            $http({
                url: Config.BaseURL + "/api/user/info",
                method: "GET",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
         },
         signup: function(user, callback) {
            $http({
                url: Config.BaseURL + "/api/user/signup",
                method:"POST",
                headers: {'Content-Type':'application/x-www-form-urlencoded'},
                data: $.param(user)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
        }, 
        validateEmail: function(email, callback) {
            $http({
                url: Config.BaseURL + "/api/user/validate-email",
                method:"POST",
                headers: {'Content-Type':'application/x-www-form-urlencoded'},
                data: $.param({ email: email })
            }).then(function(response) {
                callback(response.data);
            }, function(response) {
                callback(response.data);
            })
        },
        checkPopUP : function(data, callback){
            $http({
                url : Config.BaseURL + "/api/pop-up-showed/set",
                method : "POST",
                headers: {'Content-Type':'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function(response) {
                callback(response.data);
            }, function(response) {
                callback(response.data);
            })
        },
        resendVerification: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/user/resend-verification",
                method:"POST",
                headers: {'Content-Type':'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function(response) {
                callback(response.data);
            }, function(response) {
                callback(response.data);
            });
        },
        contactus: function(data, callback) {
            $http({
                url: Config.BaseURL + "/api/contactus",
                method:"POST",
                headers: {'Content-Type':'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function(response) {
                callback(response.data);
            }, function(response) {
                callback(response.data);
            });
        }
    };
   
})