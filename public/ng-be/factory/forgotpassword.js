app.factory('forgotPasswordFactory', function($http, $q, Config, $httpParamSerializer){
    return {
         sendResetLink: function(data, callback){
            $http({
                url: Config.BaseURL + "/api/user/password/forgot",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
         },
         resetPassword: function(data, callback){
            $http({
                url: Config.BaseURL + "/api/user/resetpassword",
                method: "POST",
                headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                data: $.param(data)
            }).then(function successCallback(response) {
                callback(response.data);
            }, function errorCallback(response) {
                callback(response.data);
            });
         }
    };
   
})