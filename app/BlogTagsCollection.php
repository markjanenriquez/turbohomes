<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlogTagsCollection extends Model
{
    protected $fillable = [
    	'blog_id',
    	'tag_id'
    ];
}
