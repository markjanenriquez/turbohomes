<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offers extends Model
{
    protected $fillable = [
		'userid',
		'scpropertyaddress',
		'listedmenuasnwer',
		'scsellername',
		'scselleraddress',
		'screaltorname',
		'screaltoraddress',
		'scbrokerageaddress',
		'scothersellername',
		'scotherselleraddress',
		'buyersname',
		'buyersemail',
		'sectionbuyingmenuasnwer',
		'sectionbuyingmenu2asnwer',
		'sectionbuyingmenu3asnwer',
		'sccurrentaddress',
		'contract_date',
		'sectionlistedanswer',
		'sectionwhomanswer',
		'sell_home_to_turbohomes',
		'accept_offer_date',
		'must_sell_current_house_contract',
		'scfinancingamt',
		'sc_financing_earnest_amt',
		'financingmenulistthreeanswer',
		'scfinancingescrowname',
		'scfinancingescrowtel',
		'scfinancingescrowofficername',
		'scfinancingearnestamt',
		'down_payment',
		'mortgage',
		'planforpayingother',
		'financingmenulisttwoanswer',
		'type_of_financing_other',
		'financingmenulistfouranswer',
		'closingcostamount',
		'financingmenulistsixanswer',
		'financingmenulistsevenanswer',
		'costnotexceed',
		'importdatesmenu',
		'importlistoneanswer',
		'importlistoneanswerother',
		'inspectiondeadline',
		'inspectorselected',
		'closingdate',
		'closingdateother',
		'refrigerator',
		'refrigeratordesc',
		'washer',
		'washerdesc',
		'dryer',
		'dryerdesc',
		'aboveground',
		'abovegrounddesc',
		'other',
		'otherdesc',
		'discusswithattorny',
		'additionalcondition',
		'type_of_financing'
	];

    public $timestamps = true;
}
