<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DashboardCheckList extends Model
{
    protected $fillable = [
     'user_id',
     'check_item',
     'status'
    ];
}
