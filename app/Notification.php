<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = ['sender_id','receiver_id','title','msg','status'];
    public $timestamps = true;
}
