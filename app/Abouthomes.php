<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Abouthomes extends Model
{
    protected $fillable = [
		'userid',
		'homeaddress',
		'features',
		'mls',
		'percentpay',
		'percentpayother',
		'buyernoagent',
		'buyerwithagent',
		'buyerwithagentplus',
		'fullname',
		'owneremail',
		'ownerphone',
		'term_agreement',
		'contactvia',
		'contact',
		'apply',
		'code',
		'providedcode',
		'buyeraccess',
		'lockbox',
		'lockboxdesc',
		'specialinstruction',
		'paymentmethod',
		'custom',
		'transaction_data',
		'status',
		'homevacant',
        'tenantoccupied',
        'permrequired',
        'gatecode',
        'gatecodeprov',
        'willalarm',
        'alarmcode',
	];
	
    public $timestamps = true;
}
