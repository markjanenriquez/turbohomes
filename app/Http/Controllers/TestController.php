<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use JWTAuth;
use JWTFactory;
use Tymon\JWTAuthExceptions\JWTException;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use Zillow\ZillowClient;
use App\DashboardCheckList;

class TestController extends Controller
{
    public function set(){
    	

        $key = env('APP_KEY');
        $token = array(
              "userid" => 28,
              "iat" => time(),
              "exp" => time() + 86400
        );

        return $jwt = JWT::encode($token, $key);
        
       
     
       
        
    }

    public function get($jwt){
        $key = env('APP_KEY');
        $decoded = JWT::decode($jwt, $key, array('HS256'));
        $expires_at = date('d M Y h:i:s A', $decoded->exp);

        $created_at = date('d M Y h:i:s A', $decoded->iat);
        $today = Carbon::now()->format('d M Y h:i:s A');
        $checkExpired = strtotime($today) >= strtotime($expires_at);
        return response()->json(compact('created_at','expires_at','today','checkExpired'));
    }

    public function getZillow(){
        
        $id = env('ZILLOW_ID');
        $client = new ZillowClient($id);
        
        $get_search_results = $client->GetSearchResults(['id' => $id, 'address' => '2114 Bigelow Ave N Seattle', 'citystatezip' 
            => 'WA 98109']);

        if(!isset( $get_search_results['results'])){
           return response()->json($get_search_results['message']);
        }

        $result = $get_search_results['results']['result'];
       
        $get_deep_comps = $client->GetDeepComps(['id' => $id, 'zpid' => $result['zpid'], 'count'=> 10]);
         
        $principal = $get_deep_comps['properties']['principal'];
        $zestimate = $principal['zestimate'];
        $comparables = $get_deep_comps['properties']['comparables'];

        $get_updated_property_details = $client->GetUpdatedPropertyDetails(['id' => $id, 'zpid' => $result['zpid']]);
        
        $editedFacts = $get_updated_property_details['editedFacts'];
        $images = $get_updated_property_details['images'];

        $data['images'] = $images;
        $data['finishedSqFt'] = $editedFacts['finishedSqFt'];
        $data['bedrooms'] = $editedFacts['bedrooms'];
        $data['bathrooms'] = $editedFacts['bathrooms'];
        $data['yearBuilt'] = $editedFacts['yearBuilt'];
        $data['lotSizeSqFt'] = $editedFacts['lotSizeSqFt'];
        $data['lastSoldDate'] = $principal['lastSoldDate'];
        $data['lastSoldPrice'] = $principal['lastSoldPrice'];
        $data['taxAssessmentYear'] = $principal['taxAssessmentYear'];
        $data['taxAssessment'] = $principal['taxAssessment'];
        $data['zestimate']['amount'] = $zestimate['amount'];
        $data['zestimate']['valuationRange']['low'] = $zestimate['valuationRange']['low'];
        $data['zestimate']['valuationRange']['high']  = $zestimate['valuationRange']['high'];
        $data['zestimate']['last-updated'] = $zestimate['last-updated'];
        $data['comp'] = $comparables['comp'];

        
        $get_chart = $client->GetChart(['id' => $id, 'zpid' => $result['zpid'], 'unit-type' => 'percent']);

        $data['chart'] =  $get_chart['url'];

        return response()->json(compact('data','get_deep_comps'));
    }

    public function index(){
          
         $model = DashboardCheckList::where(['user_id' => 28])->get();
         $data = [];
         foreach ($model as $key => $item) {
             $data[$item->check_item]['status'] = $item->status;     
         }

         return response()->json(compact('data'));

    }
}
