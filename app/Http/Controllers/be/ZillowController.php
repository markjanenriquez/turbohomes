<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Zillow\ZillowClient;

class ZillowController extends Controller
{
    public function __construct()
    {
       // Apply the jwt.auth middleware to all methods in this controller
       // except for the authenticate method. We don't want to prevent
       // the user from retrieving their token if they don't already have it
       $this->middleware('jwt.auth');
    }


    public function getReport(Request $request){

    
      	$id = env('ZILLOW_ID');
        $client = new ZillowClient($id);

        $address_data = $request->all();

        $address = $address_data['street_number']." ".$address_data['route']." ".$address_data['locality'];
        $citystatezip = $address_data['administrative_area_level_1']." ".$address_data['postal_code']." ".$address_data['country'];
        
        $get_search_results = $client->GetDeepSearchResults(['id' => $id, 'address' => $address, 'citystatezip' => $citystatezip]);
         
        if(isset($get_search_results['message'])){
           return response()->json($get_search_results['message']);
        }

        $result = $get_search_results['results']['result'];
       
        $get_deep_comps = $client->GetDeepComps(['id' => $id, 'zpid' => $result['zpid'], 'count'=> 25]);
        
        if(isset($get_deep_comps['message'])){
            return response()->json($get_deep_comps['message']);
        }
         
        $principal = $get_deep_comps['properties']['principal'];
        $address = $principal['address'];
        $zestimate = $principal['zestimate'];
        $comparables = $get_deep_comps['properties']['comparables'];

        foreach ($comparables['comp'] as $key => $value) {
            $comparables['comp'][$key]['score'] = intval($comparables['comp'][$key]['@attributes']['score']);
        }
        
        $get_updated_property_details = $client->GetUpdatedPropertyDetails(['id' => $id, 'zpid' => $result['zpid']]);

        if(!isset($get_updated_property_details['message'])){
            // return response()->json($get_updated_property_details['message']);
            $editedFacts = $get_updated_property_details['editedFacts'];
            if(isset($get_updated_property_details['images'])){
                $images = $get_updated_property_details['images'];
                if($images['count'] > 1){
                    foreach ($images['image']['url'] as $key => $url) {
                        $data['images'][$key]['url'] = $url;
                    }
                } else {
                    $data['images'][0]['url'] = $images['image']['url'];
                }
            }
        }

        $data['title'] = $address['street'] . ', ' . $address['city'] . ' ' . $address['state'] .' ' . $address['zipcode'];
        $data['latitude'] = $principal['address']['latitude'];
        $data['longitude'] = $principal['address']['longitude']; 
        $data['finishedSqFt'] = $principal['finishedSqFt'];
        $data['bedrooms'] = $principal['bedrooms'];
        $data['bathrooms'] = isset($principal['bathrooms']) ? $principal['bathrooms'] : 'No Number of Bathrooms Specified';
        $data['totalRooms'] = isset($principal['totalRooms'])? $principal['totalRooms'] : 'No Number of Rooms Specified';
        $data['yearBuilt'] = isset($principal['yearBuilt'])? $principal['yearBuilt'] :'No Specified';
        $data['lotSizeSqFt'] = $principal['lotSizeSqFt'];
        $data['lastSoldDate'] = isset($principal['lastSoldDate'])? $principal['lastSoldDate'] : "";
        $lastSoldPrice = isset($principal['lastSoldPrice'])? $principal['lastSoldPrice'] : 0;
        $data['lastSoldPrice'] = $lastSoldPrice;
        $data['taxAssessmentYear'] = $principal['taxAssessmentYear'];
        $data['taxAssessment'] = $principal['taxAssessment'];
        $data['zestimate']['amount'] = $zestimate['amount'];
        $data['zestimate']['valuationRange']['low'] = $zestimate['valuationRange']['low'];
        $data['zestimate']['valuationRange']['high']  = $zestimate['valuationRange']['high'];
        $data['zestimate']['last-updated'] = date('D, M d Y', strtotime($zestimate['last-updated']));
        $data['comparables'] = $comparables['comp'];
        $data['main_propery_address'] = $address['street'] . ', <br /> ' . $address['city'] . ' ' . $address['state'] .' ' . $address['zipcode'];
        $data['main_propery_price_sqft'] = number_format($lastSoldPrice / $principal['finishedSqFt'], 2);
        $data['links'] = $principal['links'];
        $get_chart = $client->GetChart(['id' => $id, 'zpid' => $result['zpid'], 'unit-type' => 'percent']);
   
        $data['chart'] =  $get_chart['url'];

        return response()->json($data);
    }
}
