<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notification;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;
use DB;
use App\Libraries\TimeElapsed;

class NotificationController extends Controller
{
      

      public function __construct()
      {
       // Apply the jwt.auth middleware to all methods in this controller
       // except for the authenticate method. We don't want to prevent
       // the user from retrieving their token if they don't already have it
       $this->middleware('jwt.auth');
      }

      public function get($user_id){
          
          $time_elapsed = new TimeElapsed();
          $notifications = Notification::where(['receiver_id' => $user_id])->orderBy('created_at','desc')->get();
          foreach ($notifications as $key => $notification) {
             
                $notifications[$key]['time_elapsed'] = $time_elapsed->ago(date_create($notification->created_at));
          }
          $badge_count = Notification::where(['receiver_id' => $user_id, 'status' => 'unread'])->count();

          return response()->json(compact('notifications','badge_count'));

      }


    public function set(Request $request){
          
          
        if($request->isMethod('post')){
          	  
          	$lists = $request->input('lists');
          	
            if(isset($lists) && count($lists)) {  
              	DB::beginTransaction();
              	  
              	foreach ($lists as $key => $list) {
              	  	if($list['status'] == "unread") {
              	  	    $notification = Notification::find($list['id']);
              	  	    $notification->status = "read";
              	  	    if(!$notification->save()){
              	  	   	    DB::rollBack();
              	  	        return response()->json([
                                'type' => 'danger',
                                'msg' => 'Something went wrong please try again. Offer not save!',
                                'statuscode' => 409
                            ]);
              	  	    }
              	  	}
              	}

              	DB::commit();
            }
            
	          return response()->json([
	                'type' => 'success',
	                'msg' => 'Notification Successfuly Saved!',
	                'statuscode' => 200
	          ]);
          }  

      }    
}
