<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Guid;
use DB;
use App\Abouthomes;
use App\Offers;
use Carbon\Carbon;
use App\Contract;
use App\EmailStatus;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;

class EmailStatusController extends Controller
{
    public function __construct()
    {
       // Apply the jwt.auth middleware to all methods in this controller
       // except for the authenticate method. We don't want to prevent
       // the user from retrieving their token if they don't already have it
       $this->middleware('jwt.auth');
    }

    public function get($id, $type){
    	$response = array();

        switch ($type) {
            case 'listedhome':
                $response = array(
                    'onceundercontract' => [
                        'type' => 'Under Contract',
                        'status' => 'Pending',
                        'date_sent' => null
                    ],
                    'earnestmoneydue' => [
                        'type' => 'Earnest Money Due',
                        'status' => 'Pending',
                        'date_sent' => null
                    ], 
                    'inspectionperiod' => [
                        'type' => 'Inspection Period',
                        'status' => 'Pending',
                        'date_sent' => null
                    ], 
                    'closing' => [
                        'type' => 'Closing',
                        'status' => 'Pending',
                        'date_sent' => null
                    ], 
                    'checkingin' => [
                        'type' => 'Checking In',
                        'status' => 'Pending',
                        'date_sent' => null
                    ]
                );

                $conditionCol = 'contracts.homeid';
                break;
            
            default:

                $response = array(
                    'undercontract' => [
                        'type' => 'Under Contract',
                        'status' => 'Pending',
                        'date_sent' => null
                    ],
                    'last3daysinspection' => [
                        'type' => '3 Days left in inspection period',
                        'status' => 'Pending',
                        'date_sent' => null
                    ], 
                    'lastdayofinspection' => [
                        'type' => 'Last day of inspection period',
                        'status' => 'Pending',
                        'date_sent' => null
                    ], 
                    'dayafterinspectionperiod' => [
                        'type' => 'March to close',
                        'status' => 'Pending',
                        'date_sent' => null
                    ], 
                    'fivedaysbeforeclosedate' => [
                        'type' => '5 Days left to close',
                        'status' => 'Pending',
                        'date_sent' => null
                    ]
                );

                $conditionCol = 'contracts.offerid';
                break;
        }

        $list = DB::table('emailstatus')
            ->join('contracts', 'contracts.id', '=', 'emailstatus.contract_id')
            ->select('emailstatus.*','contracts.offerid','contracts.homeid','contracts.contractdate','contracts.inspectiondate','contracts.closingdate')
            ->where($conditionCol,"=",$id)->first();


        if($list) {
            foreach ($response as $key => $value) {

                $response[$key]['status'] = $list->$key ? "SENT":"PENDING";
                $response[$key]['date_sent'] = !$list->$key ? null : date('Y-m-d', strtotime($list->$key));
            }
        }

        $dataRes = array();
        foreach ($response as $key => $value) {
            $value['model'] = $key;
            $dataRes[] = $value;
        }

        return response()->json($dataRes);

    }

    private function get_seller_layout($contract, $key) {
        $data = array(
            'email_layout' => 'backend.email.seller.'
        );

        $today = Carbon::now()->format('d-m-Y');
        $contract_date = Carbon::parse($contract['contractdate'])->format('d-m-Y');
        $after_three_days_date = Carbon::parse($contract_date)->addDays(3)->format('d-m-Y'); 
        $end_inspection_date = Carbon::parse($contract['inspectiondate'])->format('d-m-Y');
        $closing_date = Carbon::parse($contract['closingdate'])->format('d-m-Y');
        $five_days_before_closing_date = Carbon::parse($closing_date)->subDays(5)->format('d-m-Y');
        $ten_days_after_listing_date = '';
        if(!empty($contract['homeid'])) {
            $abouthomes = DB::table("abouthomes")->select("created_at AS listingdate", "owneremail")->where('id', $contract['homeid'])->first();
            $listing_date = Carbon::parse($abouthomes->listingdate)->format('d-m-Y');
            $ten_days_after_listing_date = Carbon::parse($listing_date)->addDays(10)->format('d-m-Y');
        }

        $data['receiver_email'] = $abouthomes->owneremail;
        switch ($key) {
            case 'onceundercontract':
                $data['email_layout'] .= $key;
                $data['subject'] = "Turbohomes - You are under contract";
                break;
            case 'earnestmoneydue':
                $data['email_layout'] .= $key;
                $data['subject'] = "Turbohomes - 3 days in...Earnest Money is due";
                break;
            case 'inspectionperiod':
                $data['email_layout'] .= $key;
                $data['subject'] = "Turbohomes - Inspection period ends today";
                break;
            case 'closing':
                $data['email_layout'] .= $key;
                $data['subject'] = "Turbohomes - The day is near. It’s time to close";
                break;
            case 'checkingin':
                $data['email_layout'] .= $key;
                $data['subject'] = "Turbohomes - Checking in";
                break;
            
            default:
                # code...
                break;
        }
        return $data;
    }

    private function get_buyer_layout($contract, $key) {
        $data = [
            'email_layout' => 'backend.email.buyer.'
        ];

        $offer = DB::table("offers")->select("*")->where('id', $contract['offerid'])->first();
        $inspection3daysleft = $offer->inspectiondeadline - 3;

        $today = Carbon::now()->format('d-m-Y');
        $contract_date = Carbon::parse($contract['contractdate'])->format('d-m-Y');
        $inspection3daysleft = Carbon::parse($contract_date)->addDays($inspection3daysleft)->format('d-m-Y'); 
        $end_inspection_date = Carbon::parse($contract['inspectiondate'])->format('d-m-Y');
        $day_after_inspection = Carbon::parse($contract['inspectiondate'])->addDays(1)->format('d-m-Y');
        $five_days_before_closing_date = Carbon::parse($contract['closingdate'])->subDays(5)->format('d-m-Y');

        $data['receiver_email'] = $offer->buyersemail;

        switch ($key) {
            case 'undercontract':
                $data['email_layout'] .= $key;
                $data['subject'] = "Turbohomes - You are under contract";
                break;
            case 'last3daysinspection':
                $data['email_layout'] .= $key;
                $data['subject'] = "Turbohomes - 3 days left in your Inspection Period";
                break;
            case 'lastdayofinspection':
                $data['email_layout'] .= $key;
                $data['subject'] = "Last day of the Inspection Period";
                break;
            case 'dayafterinspectionperiod':
                $data['email_layout'] .= $key;
                $data['subject'] = "Turbohomes - The march to close";
                break;
            case '5daysbeforeclosedate':
                $data['email_layout'] .= $key;
                $data['subject'] = "Turbohomes - 5 days left to close";
                break;
            
            default:
                # code...
                break;
        }
        return $data;
    }

    public function sendMail(Request $request) {
          
        $data = $request->all();

        if($data['type'] === 'offer') {
            $contract = Contract::where([ 'offerid' => $data['id'] ])->first();
            $response = $this->get_buyer_layout($contract, $data['key']);
        } else if($data['type'] === 'listedhome') {
            $contract = Contract::where([ 'homeid' => $data['id'] ])->first();
            $response = $this->get_seller_layout($contract, $data['key']);
        }

        $emailstat = EmailStatus::where('contract_id', $contract['id'])->first();

        if(!$emailstat) {
            $emailstat = new EmailStatus();
            $guid = new Guid;
            $emailstat->id = $guid->GUID();
            $emailstat->contract_id = $contract['id'];
        }

        $date = date('Y-m-d');
        switch ($data['key']) {
            case 'undercontract':
                $emailstat->undercontract = $date;
                break;
            case 'last3daysinspection':
                $emailstat->last3daysinspection = $date;
                break;
            case 'lastdayofinspection':
                $emailstat->lastdayofinspection = $date;
                break;
            case 'dayafterinspectionperiod':
                $emailstat->dayafterinspectionperiod = $date;
                break;
            case 'fivedaysbeforeclosedate':
                $emailstat->fivedaysbeforeclosedate = $date;
                break;
            case 'earnestmoneydue':
                $emailstat->earnestmoneydue = $date;
                break;
            case 'inspectionperiod':
                $emailstat->inspectionperiod = $date;
                break;
            case 'closing':
                $emailstat->closing = $date;
                break;
            case 'checkingin':
                $emailstat->checkingin = $date;
                break;
            case 'onceundercontract':
                $emailstat->onceundercontract = $date;
                break;
        }

        if(!$emailstat->save()) {

            return response()->json([
                'type' => 'danger',
                'msg' => 'Something went wrong please try again. Blog not save!',
                'statuscode' => 409
            ]);
        }

        if (!empty($response)) {
            Mail::to($response['receiver_email'])
                ->send(new SendMail($response['email_layout'], $response['subject']));
        }

        return response()->json([
            'type' => 'success',
            'msg' => 'Email successfully sent.',
            'statuscode' => 200
        ]);
    }
}
