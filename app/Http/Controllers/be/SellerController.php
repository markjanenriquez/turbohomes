<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Abouthomes;
use App\Otherowner;
use DB; 
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use App\Libraries\TimeElapsed;
use App\Users;
use App\Notification;
use Carbon\Carbon;
use App\Events\eventTrigger;


class SellerController extends Controller
{
     
    public function __construct()
    {
	   // Apply the jwt.auth middleware to all methods in this controller
	   // except for the authenticate method. We don't want to prevent
	   // the user from retrieving their token if they don't already have it
	   $this->middleware('jwt.auth',['except'=>['create', 'create_partial']]);
    }

    public function create_partial(Request $request) {
    
        $data = array(
            'type' => 'danger',
            'msg' => 'Something went wrong please try again. Your home not save!',
            'statuscode' => 409
        );

        DB::beginTransaction();

        $fields = [
            'buyernoagent',
            'buyerwithagent',
            'buyerwithagentplus',
            'features',
            'fullname',
            'homeaddress',
            'mls',
            'owneremail',
            'ownerphone',
            'paymentmethod',
            'percentpay',
            'percentpayother',
            'term_agreement',
            'custom',
            'userid',
            'contact',
            'contactvia'
        ];

        $requestData = $request->only($fields);
        $otherowner = !empty($request->input('otherowner')) ? $request->input('otherowner') : array();

        $save = new Abouthomes();
        $save->fill($requestData);
        if($save->save($requestData)){
            foreach ($otherowner as $otherbuyer) {
                $saveotherbuyer = new Otherowner();
                $saveotherbuyer->abouthomesid = $save->id;
                $saveotherbuyer->name = $otherbuyer['name'];
                $saveotherbuyer->email = $otherbuyer['email'];
                $saveotherbuyer->phone = $otherbuyer['phone'];

                if(!$saveotherbuyer->save()){
                    DB::rollBack();
                    return response()->json($data);
                }
            }
        }

        $data = array(
            'type' => 'success',
            'msg' => 'Success!',
            'statuscode' => 200
        );

        DB::commit();

        return response()->json($data);
    }

    public function create(Request $request){
    
        $data = array(
            'type' => 'danger',
            'msg' => 'Something went wrong please try again. Your home not save!',
            'statuscode' => 409
        );

        DB::beginTransaction();

        $fields = [
            'homevacant',
            'tenantoccupied',
            'permrequired',
            'gatecode',
            'gatecodeprov',
            'providedcode',
            'willalarm',
            'alarmcode',
            'buyeraccess',
            'lockbox',
            'lockboxdesc',
            'code',
            'contact',
            'contactvia',
            'specialinstruction'
        ];

        $requestData = $request->only($fields);
        $custom = $request->input('custom');

        $save = Abouthomes::where('custom', $custom);

        if($save->update($requestData)){
            $home = $save->first();

            // Mail::to($home->owneremail)->send(new SendMail('backend.email.seller.atpaymentto'));

            $buyers = Users::where(['role'=>1])->orWhere(['role'=>3])->get();
            
            foreach ($buyers as $key => $buyer) {
              
                if($request->input('userid') != $buyer->id){
                    $notif = array();
                    $notif['sender_id'] = $request->input('userid');
                    $notif['receiver_id'] = $buyer->id;
                    $notif['status'] = "unread";
                    $notif['title'] = "New Home Listed";
                    $notif['msg'] = "New home has been listed on ".$home->homeaddress."! Listed by seller ". $home->fullname." with an price of $".number_format($home->buyernoagent, 2)."(w/out agent) / $".number_format($home->buyerwithagent, 2)."(w/ agent).";
                    $notification = new Notification();
                    $notification->fill($notif);
                    if(!$notification->save($notif)){              
                        DB::rollBack();
                        return response()->json([
                            'type' => 'danger',
                            'msg' => 'Something went wrong please try again. Offer not save!',
                            'statuscode' => 409
                        ]);
                    }
                    $notif['id'] = $notification->id;
                    $time_elapsed = new TimeElapsed();
                    $notif['time_elapsed'] = $time_elapsed->ago(date_create(date('Y-m-d H:i:s')));
                    // event(new eventTrigger($notif));
                }
            }

            $data['type'] = 'success';
            $data['msg'] = 'Your home Successfuly Saved!';
            $data['statuscode'] = 200;
            $data['seller_email'] = $home->owneremail;
        }

        DB::commit();

        return response()->json($data);
    }

    public function list(Request $request) {

        $keyword = $request->input('keyword');
        $sortBy = $request->input('sortBy');
        $order = $request->input('order');

        if($keyword == '' || $keyword == null){
           $list = DB::table('abouthomes')
              ->select("id","fullname","owneremail","ownerphone","created_at", "status")
              ->orderBy($sortBy,$order);
        }else{
           $list = DB::table('abouthomes')
              ->select("id","fullname","owneremail","ownerphone","created_at", "status")
              ->where("fullname","LIKE","%{$keyword}%")
              ->orWhere("owneremail","LIKE","%{$keyword}%")
              ->orWhere("ownerphone","LIKE","%{$keyword}%")
              ->orderBy($sortBy,$order);
        }

        $sellerlist = $list->paginate(10)->toArray();
        $sellerlist['dataList'] = $list = DB::table('abouthomes')->select("*")->get();   
    
        return response()->json($sellerlist);
    }

    public function view($id) {

        $query = ['id' => $id];
        $query2 = ['abouthomesid' => $id];
        $result = Abouthomes::where($query)->first();

        $otherbuyer = Otherowner::where($query2)->get();

        $result->otherbuyer = $otherbuyer;

        return $result;
    }

    public function edit(Request $request){
         
        $data = array(
            'type' => 'danger',
            'msg' => 'Something went wrong please try again. Home not save!',
            'statuscode' => 409
        );
       
        DB::beginTransaction();

        $fields = [
            'buyernoagent',
            'buyerwithagent',
            'buyerwithagentplus',
            'features',
            'fullname',
            'homeaddress',
            'mls',
            'owneremail',
            'ownerphone',
            'paymentmethod',
            'percentpay',
            'percentpayother',
            'term_agreement',
            'custom',
            'userid',
            'contact',
            'contactvia',
            'homevacant',
            'tenantoccupied',
            'permrequired',
            'gatecode',
            'gatecodeprov',
            'providedcode',
            'willalarm',
            'alarmcode',
            'buyeraccess',
            'lockbox',
            'lockboxdesc',
            'code',
            'contact',
            'contactvia',
            'specialinstruction'
        ];

        $requestData = $request->only($fields);

        $id = $request->input('id');
        $model = Abouthomes::find($id);

        if(!$model->update($requestData)){
            DB::rollBack();
            return response()->json($data);
        }

        $checkotherowner = Otherowner::where([ 'abouthomesid' => $id ])->get();

        foreach ($checkotherowner as $key => $otherowner) {
                if(!$otherowner->delete()){
                   DB::rollBack();
                   return response()->json($data);
                }
        }

         $otherowner = !empty($request->input('otherowner')) ? $request->input('otherowner') : array();

        foreach ($otherowner as $otherbuyer) {
            $saveotherbuyer = new Otherowner();
            $saveotherbuyer->abouthomesid = $model->id;
            $saveotherbuyer->name = $otherbuyer['name'];
            $saveotherbuyer->email = $otherbuyer['email'];
            $saveotherbuyer->phone = $otherbuyer['phone'];

            if(!$saveotherbuyer->save()){
                DB::rollBack();
                return response()->json($data);
            }
        }

        $data['type'] = 'success';
        $data['msg'] = 'Your home Successfuly Saved!';
        $data['statuscode'] = 200;
        
        DB::commit();

        return response()->json($data);

    }

    public function set_status(Request $request){
         
         $id = $request->input('id');
         $model = Abouthomes::find($id);
         $data = ['status' => $request->status];
         if(!$model->update($data)){
            return response()->json([ 
                'type' => 'danger',
                'msg' => 'Something went wrong please try again. Payment status not updated!',
                'statuscode' => 409
            ]);
         }

         return response()->json([ 
                'type' => 'success',
                'msg' => 'Payment Status Updated!',
                'statuscode' => 200
         ]);
    }

    public function destroy(Request $request) {
      
        $id = $request->input('id');

        $checkseller = Abouthomes::where([ 'id' => $id ])->first();
      
        $checkotherowner = Otherowner::where([ 'abouthomesid' => $id ])->get();
      
        $data = array(
            'type' => 'danger',
            'msg' => 'Something went wrong, please try again later',
        );

        if($checkseller && $checkotherowner){
          
            if(!$checkseller->delete()){
                return json_encode($data);
            }

            foreach ($checkotherowner as $key => $otherowner) {
                if(!$otherowner->delete()){
                    return response()->json($data);
                }
            }
          
            $data['type'] = 'success';
            $data['msg'] = 'Home Details Successfuly deleted!';
          
        }

        return response()->json($data);
    }

    public function get_seller_listing($userid) {
        $list = DB::table('abouthomes')
              ->select("homeaddress")
              ->where('userid', $userid)
              ->get();

        return response()->json($list);
    }

    public function update_listed_home(Request $request, $userid) {

        $address = $request->address;
        $price = $request->price;
        $other = $request->other;

        $home = Abouthomes::where('userid', $userid)
                    ->select("fullname","owneremail")
                    ->where('homeaddress', $address)
                    ->get();

        if(!$home) {
            return response()->json([
                'type' => 'danger',
                'msg' => 'Something went wrong please try again. Offer not save!',
                'statuscode' => 401
            ]);
        }

        $data = array(
            'address' => $address,
            'price' => $price,
            'other' => $other,
            'fullname' => $home[0]->fullname,
            'owneremail' => $home[0]->owneremail
        );

        Mail::to(env('email'))->send(new SendMail('backend.email.seller.list-update', 'Listed Home Update - ' . $home[0]->fullname, $data));

        return response()->json([
            'data' => $data,
            'statuscode' => 200
        ]);
    }
}
