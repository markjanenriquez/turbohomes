<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB; //query builder cusomt query
use Hash; //password hasing
use App\Users; //model Users
use App\Usersinfos;
use App\PasswordReset;
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;
use Zillow;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use App\Libraries\Guid;
use Carbon\Carbon;
use Firebase\JWT\JWT;
use App\Contactus;


class UsersController extends Controller
{
    
    public function __construct()
    {
       // Apply the jwt.auth middleware to all methods in this controller
       // except for the authenticate method. We don't want to prevent
       // the user from retrieving their token if they don't already have it
       $this->middleware('jwt.auth', [
            'except' => [
                'authenticate',
                'token',
                'signup',
                'accountactivation',
                'passwordforgot',
                'resetpassword',
                'checkresettoken',
                'validate_email',
                'resend_email_verification',
                'contactus'
            ]
        ]);
    }
    
    public function authenticate(Request $request)
    {
        $credentials = $request->only('email', 'password');

        try {
            // verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'Invalid Credentials'], 401);
            }
        } catch (JWTException $e) {
            // something went wrong
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        // if no errors are encountered we can return a JWT
        return response()->json(compact('token'));
    }

    public function token(){
        
        $token = JWTAuth::getToken();
        
        if(!$token){
             return response()->json(['error' => 'token_not_provided'], 401);
        }
        
        try{
              $token = JWTAuth::refresh($token);
        } catch (\Tymon\JWTAuth\Exceptions\TokenBlacklistedException $e) {
              return response()->json(['error' => 'token_blacklisted'], 401);
        } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {
              return response()->json(['error' => 'token_invalid'], $e->getStatusCode());
        } 
        
        return response()->json(compact('token'));
    }


    public function getAuthenticatedUser()
    {
      $data = array();
      try {

        if (! $user = JWTAuth::parseToken()->authenticate()) {
            return response()->json(['status'=> 401, 'msg' => 'User not Found.'], 401);
        }else if($user->activated == 0){
            return response()->json(['status'=> 401, 'msg' => 'Account is not activated. Please check your email.'], 401);
        }else{
               $info = Usersinfos::where(['id'=> $user->id])->first();
               if($info){
                   $data = $info;
                   $data['fullname'] = $info->firstname . ' ' . $info->lastname;
                   $data['phone'] = $info->mobilenumber;
                   $data['picture'] = ($info->photo == '' || $info->photo == null)? 'defaultprofilepicture.png' : $info->photo;  
                   $data['role'] = $user->role;
                   $data['email'] = $user->email;
               }
        }

      } catch (\Tymon\JWTAuth\Exceptions\TokenExpiredException $e) {

        return response()->json(['error' => 'token_expired'], $e->getStatusCode());

      } catch (\Tymon\JWTAuth\Exceptions\TokenInvalidException $e) {

        return response()->json(['error' => 'token_invalid'], $e->getStatusCode());

      } catch (\Tymon\JWTAuth\Exceptions\JWTException $e) {

        return response()->json(['error' => 'token_absent'], $e->getStatusCode());

      }

      // the token is valid and we have found the user via the sub claim
      return response()->json($data);
    }


    public function signup(Request $request){
      
        $appkey = env('APP_KEY');

        $data = array(
          'type' => 'danger',
          'msg' => 'Something went wrong, please try again later.',
          'statuscode' => 422
        );

        $guid = new Guid;
        $userid = $guid->GUID();

        if ($request->isMethod('post')){

            DB::beginTransaction();

            $userid = $request->input('userid');
            $password = $request->input('password');
            $email = $request->input('email');
            $role = $request->input('role');
            $lastname = ucwords(strtolower($request->input('lastname')));
            $firstname = ucwords(strtolower($request->input('firstname')));
            $middlename = $request->input('middlename');
            $address = $request->input('address');
            $contact = $request->input('contact');
            $gender = $request->input('gender');
            $photo = $request->input('photo');
            $mobilenumber = $request->input('mobilenumber');
            $plantobuy = $request->input('plantobuy');
            $state = $request->input('state');
            $propertyaddress = $request->input('propertyaddress');
            $homeloan = $request->input('homeloan');
            $homesell = $request->input('homesell');
            $homefound = $request->input('homefound');
            $realstateagent = $request->input('realstateagent');
            $agentnumber = $request->input('agentnumber');
            $agentemail = $request->input('agentemail');

            $validation = Users::where('email', $email)
                            ->first();
            if($validation) {
                return response()->json($data);
            }

            $saveuser = new Users();
            $saveuser->email = $email;
            $saveuser->role = $role;
            $saveuser->status = 1;
            $saveuser->password = Hash::make($password);

            if(!$saveuser->save()){
                DB::rollBack();

                return response()->json($data);
            }

            $saveuserinfo = new Usersinfos();
            $saveuserinfo->id = $saveuser->id;
            $saveuserinfo->firstname = $firstname;
            $saveuserinfo->middlename = $middlename;
            $saveuserinfo->lastname = $lastname;
            $saveuserinfo->gender = $gender;
            $saveuserinfo->address = $address;
            $saveuserinfo->contact = $contact;
            $saveuserinfo->photo = $photo;
            $saveuserinfo->mobilenumber = $mobilenumber;
            $saveuserinfo->plantobuy = $plantobuy;
            $saveuserinfo->homeloan = $homeloan;
            $saveuserinfo->homesell = $homesell;
            $saveuserinfo->homefound = $homefound;
            $saveuserinfo->realstateagent = $realstateagent;
            $saveuserinfo->agentnumber = $agentnumber;
            $saveuserinfo->agentemail = $agentemail;
            $saveuserinfo->state = $state;
            $saveuserinfo->propertyaddress = $propertyaddress;

            if(!$saveuserinfo->save()){
                DB::rollBack();

                return response()->json($data);
            }

            //1 day - 86400
            $token = array(
                "userid" => $saveuser->id,
                "iat" => time(),
                "exp" => time() + 86400
            );

            $jwt = JWT::encode($token, $appkey);
          
            $title = 'Turbo Homes Email Activation';
            $content = 'Activation';
            $name = $firstname;
            $urlactivation = url('/users/activation/'.$jwt);
            $data = ['name' => $name, 'token' => $urlactivation];
          
            Mail::to($email)->send(new SendMail("backend.email.emailconfirmation",'Account Activation', $data));
          
            $strRole = "";
          
            switch($role){
                case 1:
                    $strRole = "Buyer";
                    break;
                case 2:
                    $strRole = "Seller";
                    break;
                case 3:
                    $strRole = "Both Buyer and Seller";
                    break;
            }

            Mail::to('hello@turbohomes.com')
                ->send(new SendMail(
                    "backend.email.newcostumernotification",
                    'New Customer Registered', 
                    [
                        'name' =>  $firstname." ".$lastname,
                        'email' => $email, 
                        'mobileno' => $mobilenumber,
                        'role' => $strRole
                    ]
                ));

            $data = array(
                'type' => 'success',
                'msg' => 'User successfully saved!',
                'statuscode' => 200
            );

            DB::commit();
        }

        return response()->json($data);
    }

    public function accountactivation($token){
        
        $appkey = env('APP_KEY');
        try{ 
          
          $decoded = JWT::decode($token, $appkey, array('HS256'));
          $id = $decoded->userid;
          $query = ['id' => $id];
          $checkuser = Users::where($query)->first();
          $checkuser->activated = 1;

          if($checkuser->save()){
              if($checkuser->role == 1){
                 Mail::to($checkuser->email)->send(new SendMail('backend.email.buyer.registration'));
               }
              return redirect('/activated');
          } else {
              return redirect('/activation/expired');
          }
        }catch (\Exception $e) {
            return redirect('/activation/expired');
        }
    }

    public function create(Request $request){

      //$data = array();
      //$guid = new Guid;
      // $userid = $guid->GUID();

      if ($request->isMethod('post')){

        DB::beginTransaction();

        $userid = $request->input('userid');
        $password = $request->input('password');
        $email = $request->input('email');
        $role = $request->input('role');
        $lastname = ucwords(strtolower($request->input('lastname')));
        $firstname = ucwords(strtolower($request->input('firstname')));
        $middlename = $request->input('middlename');
        $address = $request->input('address');
        $contact = $request->input('contact');
        $gender = $request->input('gender');
        $photo = $request->input('photo');

        $saveuser = new Users();
        $saveuser->email = $email;
        $saveuser->role = $role;
        $saveuser->status = 1;
        $saveuser->activated = 1;
        $saveuser->password = Hash::make($password);

        if(!$saveuser->save()){              
                DB::rollBack();
                return response()->json([
                    'type' => 'danger',
                    'msg' => 'Something went wrong please try again. Offer not save!',
                    'statuscode' => 409
                ]);
         } 

        $saveuserinfo = new Usersinfos();
        $saveuserinfo->id = $saveuser->id;
        $saveuserinfo->firstname = $firstname;
        $saveuserinfo->middlename = $middlename;
        $saveuserinfo->lastname = $lastname;
        $saveuserinfo->gender = $gender;
        $saveuserinfo->address = $address;
        $saveuserinfo->contact = $contact;
        $saveuserinfo->photo = $photo;

        if(!$saveuserinfo->save()){   
            
            DB::rollBack();

            return response()->json([
                'type' => 'danger',
                'msg' => 'Something went wrong please try again. Offer not save!',
                'statuscode' => 409
            ]);
        }
      }
      
      DB::commit();
      return response()->json([
        'type' => 'success',
        'msg' => 'User Successfuly Saved!',
        'statuscode' => 200
      ]);

    }


    public function checkuserid(Request $request){
      if ($request->isMethod('post')){
        $userid = $request->input('userid');

        $query = ['userid' => $userid];
        $checkuser = Users::where($query)->first();
        if($checkuser){
            $data['type'] = 'danger';
            $data['msg'] = 'User ID is not available!';
            $data['statuscode'] = 409;
        }
        else{
            $data['type'] = 'success';
            $data['msg'] = 'User ID is Available!';
            $data['statuscode'] = 200;
        }

        return response()->json($data);

      }
    }

    public function checkusername(Request $request){
      if ($request->isMethod('post')){
        $username = $request->input('username');

        $query = ['username' => $username];
        $checkuser = Users::where($query)->first();
        if($checkuser){
            $data['type'] = 'danger';
            $data['msg'] = 'Username is not available!';
            $data['statuscode'] = 409;
        }
        else{
            $data['type'] = 'success';
            $data['msg'] = 'Username is Available!';
            $data['statuscode'] = 200;
        }

        return response()->json($data);

      }
    }


    public function list(Request $request){
   
        if ($request->isMethod('post')){

        $keyword = $request->input('keyword');
        $sortBy = $request->input('sortBy');
        $order = $request->input('order');

        if($keyword == '' || $keyword == null){

          $list = DB::table('users')
            ->join('usersinfos', 'users.id', '=', 'usersinfos.id')
            ->select('users.id','usersinfos.firstname','usersinfos.lastname','users.role','users.status')
            ->where('users.role',"!=",7)
            ->orderBy($sortBy,$order);

        }
        else{

          $list = DB::table('users')
            ->join('usersinfos', 'users.id', '=', 'usersinfos.id')
            ->select('users.id','usersinfos.firstname','usersinfos.lastname','users.role','users.status')
            ->where('users.role',"!=",7)
            ->where('firstname', 'LIKE', "%{$keyword}%")
            ->orWhere('middlename', 'LIKE', "%{$keyword}%")
            ->orWhere('lastname', 'LIKE', "%{$keyword}%")
            ->orderBy($sortBy,$order);

        }
        
        $userlist = $list->paginate(10)->toArray();
        $userlist['dataList'] = DB::table('users')
            ->join('usersinfos', 'users.id', '=', 'usersinfos.id')
            ->select('usersinfos.*','users.*')
            ->get();
            
        return response()->json($userlist);

       }

    }


    public function view(Request $request){
       
       if ($request->isMethod('post')){
        $userid = $request->input('userid');

        $userlist = DB::table('users')
            ->join('usersinfos', 'users.id', '=', 'usersinfos.id')
            ->select('users.id', 'users.email', 'users.role', 'users.status', 'usersinfos.firstname', 'usersinfos.middlename', 'usersinfos.lastname', 'usersinfos.contact', 'usersinfos.address', 'usersinfos.gender', 'usersinfos.photo')
            ->where('users.id', $userid)
            ->first();
        if($userlist){
          return response()->json($userlist);
        }

      }

    }

    public function update(Request $request){       
      
      $data = array();

      if ($request->isMethod('post')){
        $userid = $request->input('id');
        $password = $request->input('password');
        $email = $request->input('email');
        $role = $request->input('role');
        $status = $request->input('status');
        $lastname = ucwords(strtolower($request->input('lastname')));
        $firstname = ucwords(strtolower($request->input('firstname')));
        $username = $firstname . " ". $lastname;
        $middlename = ucwords(strtolower($request->input('middlename')));
        $address = $request->input('address');
        $contact = $request->input('contact');
        $gender = $request->input('gender');
        $photo = $request->input('photo');

        if($userid == 1) {
          $role = 7;
          $username = "Superagent";
        }

        $query = ['id' => $userid];
        $checkuser = Users::where($query)->first();
        $checkuserifo = Usersinfos::where($query)->first();
        if($checkuser && $checkuserifo){
            $checkuser->email = $email;
            $checkuser->role = $role;
            $checkuser->status = $status;
            $checkuser->username = $username;
            $checkuserifo->id = $checkuser->id;
            $checkuserifo->firstname = $firstname;
            $checkuserifo->middlename = $middlename;
            $checkuserifo->lastname = $lastname;
            $checkuserifo->gender = $gender;
            $checkuserifo->address = $address;
            $checkuserifo->contact = $contact;
            $checkuserifo->photo = $photo;
        }
        else {
            $data['type'] = 'danger';
            $data['msg'] = 'User is not on database!';
            $data['statuscode'] = 409;
        }

        if($checkuser->save() && $checkuserifo->save()){
          
          $data['type'] = 'success';
          $data['msg'] = 'User Successfuly updated!';
          $data['statuscode'] = 200;
    
        }
        else{

          $data['type'] = 'danger';
          $data['msg'] = 'Something went wrong please try again. User not updated!';
          $data['statuscode'] = 409;
        }

      }
        return response()->json($data);
    }

    public function destroy(Request $request){
        
        if ($request->isMethod('post')){
          $userid = $request->input('userid');

          $query = ['id' => $userid];
          $checkuser = Users::where($query)->first();
          $checkuserifo = Usersinfos::where($query)->first();

          if($checkuser && $checkuserifo){
              if($checkuser->delete() && $checkuserifo->delete()){
                $data['type'] = 'success';
                $data['msg'] = 'User Successfuly deleted!';
                $data['statuscode'] = 200;
              }
              else{
                $data['type'] = 'danger';
                $data['msg'] = 'Something went wrong please try again later!';
                $data['statuscode'] = 409;
            }

          }
          else{
              $data['type'] = 'danger';
              $data['msg'] = 'Something went wrong please try again later!';
              $data['statuscode'] = 409;
          }
       }

       return response()->json($data);
    }

    public function changepassword(Request $request) {
     
      if($request->isMethod('post')) {
        $userid = $request->input('userid');
        $old = $request->input('old');
        $new = $request->input('new');

        $query = ["id" => $userid];
        $find = Users::where($query)->first();
        if($find == true) {
          if(Hash::check($old, $find->password)) {
            $find->password = Hash::make($new);

            if($find->save()) {
              $data['type'] = 'success';
              $data['msg'] = 'Password successfully changed.';
              $data['statuscode'] = 200;
            }
          } else {
            $data['type'] = 'danger';
            $data['msg'] = 'Old password is incorrect';
            $data['statuscode'] = 409;
          }
        }
      }
      return response()->json($data);
    }

    public function upload(Request $request){
        if(!$request->hasFile('file')){
          return response()->json(['error' => "No file found"], 401);
        }
        
        $file = $request->file('file');
        if(!$file->move(public_path()."/img", $file->getClientOriginalName())) {
            return response()->json(['error' => 'Error saving the file.'], 401);
        }
    }

    public function passwordforgot(Request $request){
       
        DB::beginTransaction();
        $email = $request->email;
        $user = Users::where(['email' => $email])->first();
        
        if(!$user){
          return response()->json([
                'type' => 'error', 
                'msg' => 'Email is not registered. Please enter a registered email.',
                'statuscode' =>  401]);
        }

        $user_info = Usersinfos::find($user->id);

        $name = $user_info->firstname;

        //2 hr - 7200
        $appkey = env('APP_KEY');
        $jwt = array(
            "userid" => $user->id,
            "email" => $email,
            "iat" => time(),
            "exp" => time() + 7200
        );

        $token  = JWT::encode($jwt, $appkey);
        $password_reset = new PasswordReset();
        $data = ['email' => $email, 'token' => $token, 'created_at' => date('Y-m-d H:i:s')];
        $password_reset->fill($data);
        if(!$password_reset->save($data)){
            DB::rollBack();
            return response()->json([
                'type' => 'danger',
                'msg' => 'Something went wrong please try again.',
                'statuscode' =>  401]);
       }

       $urlactivation = url('/user/password/reset/'.$token);
       $data = ['name' => $name, 'token' => $urlactivation];
          
       Mail::to($email)->send(new SendMail("backend.email.forgotpassword.forgotpassword",'Forgot Password Confirmaton', $data));

       DB::commit();

       return response()->json([
                'type' => 'success',
                'msg' => 'Reset Password link has been sent to your email.',
                'statuscode' => 200]);

    }

    public function checkresettoken($token){
       
        $appkey = env('APP_KEY');
        try{ 
          
          $decoded = JWT::decode($token, $appkey, array('HS256'));
          $password_reset = PasswordReset::where(['token' => $token])->first();
          
          if(!$password_reset){
                 return redirect('/forgot/password/expired');
          }

          $userid = $decoded->userid;

          return view("backend/index/resetpassword",['userid' => $userid]);
          
        }catch (\Exception $e) {
            return redirect('/forgot/password/expired');
        }
    }

    public function resetpassword(Request $request){
      
      $id = $request->id;
      $password = Hash::make($request->password);
      DB::beginTransaction();
      
      $user = Users::find($id);
      $user->password = $password;
      
      if(!$user->save()){
          DB::rollBack();
          return response()->json([
                'type' => 'danger',
                'msg' => 'Something went wrong please try again.',
                'statuscode' =>  401]);
      }


  
      $password_reset = PasswordReset::where(['email' => $user->email]);

      if(!$password_reset->delete()){
          DB::rollBack();
          return response()->json([
                'type' => 'danger',
                'msg' => 'Something went wrong please try again.',
                'statuscode' =>  401]);
      }
      
      DB::commit();

      return response()->json([
                'type' => 'success',
                'msg' => 'Your Password reset.',
                'statuscode' => 200]);

    }

    public function validate_email(Request $request) {
        $email = $request->input('email');

        $valid = Users::where([ 'email' => $email ])->first();

        if($valid) {
            return [ 'valid' => false ];
        } 

        return [ 'valid' => true ];
    }
   
    public function resend_email_verification(Request $request) {
        $email = $request->input('email');

        $user = Users::where([ 'email' => $email, 'activated' => 0 ])->first();

        if($user) {

            $userinfo = Usersinfos::where([ 'id' => $user->id ])->first();
            if($userinfo) {
                $appkey = env('APP_KEY');
                //1 day - 86400
                $token = array(
                    "userid" => $user->id,
                    "iat" => time(),
                    "exp" => time() + 86400
                );

                $jwt = JWT::encode($token, $appkey);
              
                $title = 'Turbo Homes Email Activation';
                $content = 'Activation';
                $name = $userinfo->firstname;
                $urlactivation = url('/users/activation/'.$jwt);
                $data = ['name' => $name, 'token' => $urlactivation];
              
                Mail::to($email)->send(new SendMail("backend.email.emailconfirmation",'Account Activation', $data));

                return response()->json([
                'type' => 'success',
                'msg' => 'Email verification has been successfully sent.',
                'statuscode' =>  200]);
            }
        } 

        return response()->json([
                'type' => 'danger',
                'msg' => 'Email is not registered.',
                'statuscode' =>  401]);
    }

    public function contactus(Request $request) {
        $data = $request->only(['name', 'email', 'subject', 'message']);

        DB::beginTransaction();

        $form = Contactus::create($data);

        if(!$form){
              
            DB::rollBack();

            return response()->json([
                'type' => 'danger',
                'msg' => 'Something went wrong please try again. Message not save!',
                'statuscode' => 409
            ]);
        }

        Mail::to('hello@turbohomes.com')
            ->send(new SendMail(
                "backend.email.contactus",
                'A message from a customer', 
                $data
            ));

        DB::commit();
        return response()->json([
            'type' => 'success',
            'msg' => 'Successfully submitted.',
            'statuscode' => 200
        ]);
    }
}
