<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\PopUpShowed;
class PopUpShowedController extends Controller
{
     public function __construct()
    {
       // Apply the jwt.auth middleware to all methods in this controller
       // except for the authenticate method. We don't want to prevent
       // the user from retrieving their token if they don't already have it
       $this->middleware('jwt.auth');
    }

    public function set(Request $request){

    	  $response = [];
    	  $data = $request->all();
    	  $model = PopUpShowed::where($data)->first();
    	  if($model){
              $response['isShowed'] = true;
    	  }else{
    	  	 
    	  	 $model = new PopUpShowed($data);
    	  	 if(!$model->save()){
    	  	 	   return response()->json([
                    'type' => 'danger',
                    'msg' => 'Something went wrong please try again.',
                    'statuscode' => 409
                ]);
    	  	 }

    	  	 $response['isShowed'] = false;
    	  }

    	  return response()->json($response);
    }
}
