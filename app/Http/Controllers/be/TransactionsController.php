<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\PaypalIPN;
use DB;
use App\Abouthomes;

class TransactionsController extends Controller
{
    
    public function listener()
    {
       // Set this to true to use the sandbox endpoint during testing:
		$enable_sandbox = true;

		$ipn = new PaypalIPN();
		if ($enable_sandbox) {
		    $ipn->useSandbox();
		}

		$verified = $ipn->verifyIPN();
		$data = array(
            'type' => 'danger',
            'msg' => 'Something went wrong please try again. Your home not save!',
            'statuscode' => 409
        );

		if($verified){
	        DB::beginTransaction();

	        $save = Abouthomes::where('custom', $_POST['custom'])->first();
	        if($save->update([ 'transaction_data' => json_encode($_POST), 'status' => $_POST['payment_status'] ])){
	        	$data = $save;
	        }

	        DB::commit();
	    }

        return response()->json($data);

		// Reply with an empty 200 response to indicate to paypal the IPN was received correctly
		header("HTTP/1.1 200 OK");

	}
    
}
