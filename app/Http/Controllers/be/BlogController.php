<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Blog;
use App\Tag;
use App\BlogTagsCollection;
use DB;
class BlogController extends Controller
{
	 public function __construct()
    {
       // Apply the jwt.auth middleware to all methods in this controller
       // except for the authenticate method. We don't want to prevent
       // the user from retrieving their token if they don't already have it
       $this->middleware('jwt.auth', [
            'except' => [
                'getBlogList',
                'getBlogDetail',
                'getBlogListByTag'
            ]
        ]);
    }

    public function createBlog(Request $request){
    	  
    	$data = $request->only('title','slug','content','featured_image');
          
        DB::beginTransaction();

        $check = $this->checkBlogSlug($data['slug']);
        if(!$check){
            return response()->json([
                'type' => 'danger',
                'msg' => 'Slug already existed',
                'statuscode' => 500
            ]);
        }

        $blog = Blog::create($data);

    	if(!$blog){
              
            DB::rollBack();

            return response()->json([
                'type' => 'danger',
                'msg' => 'Something went wrong please try again. Blog not save!',
                'statuscode' => 409
            ]);
    	}

    	$blog_tags_collections = new BlogTagsCollection();
    	$tags = $request->tags;

    	foreach ($tags as $key => $tag) {
    	 	 
    	 	$tag_data = [
            	"blog_id" => $blog->id,
             	"tag_id" => $tag
	    	];

	    	if(!$blog_tags_collections::create($tag_data)){
	    	 	DB::rollBack();
				return response()->json([
	                'type' => 'danger',
	                'msg' => 'Something went wrong please try again. Blog not save!',
	                'statuscode' => 409
	            ]);
	    	}
    	}

    	DB::commit();
	    return response()->json([
            'type' => 'success',
            'msg' => 'Blog successfully save.',
            'statuscode' => 200
        ]);

    }

    public function listBlog(Request $request){

        $keyword = $request->input('keyword');
        $sortBy = $request->input('sortBy');
        $order = $request->input('order');

        if($keyword == '' || $keyword == null){
           $list = DB::table('blogs')
              ->select("id","title","slug","created_at")
              ->orderBy($sortBy,$order);
        }else{
           $list = DB::table('blogs')
              ->select("id","title","slug","created_at")
              ->where("title","LIKE","%{$keyword}%")
              ->orWhere("slug","LIKE","%{$keyword}%")
              ->orWhere("content","LIKE","%{$keyword}%")
              ->orderBy($sortBy,$order);
        }

        $blogList = $list->paginate(10)->toArray();
        $blogList['dataList'] = DB::table('blogs')->select("*")->get();   
        return response()->json($blogList);
    }


    public function showBlog($id){
         $data = Blog::select("id","title","slug","content","featured_image")->where('id', $id)->get()->toArray();
         $data = $data[0];
         $tagsCollections = BlogTagsCollection::select('tag_id')->where('blog_id', $id)->get();
         $data['tags'] = [];
         foreach ($tagsCollections as $key => $tag) {
              array_push($data['tags'], $tag->tag_id);
         }
        return response()->json($data);
    }

    public function updateBlog(Request $request){
		
		$data = $request->only('title','slug','content','featured_image');
	      
	    DB::beginTransaction();

	    $blog = Blog::find($request->id);

        $check = $this->checkBlogSlug($data['slug']);
        if(!$check && $blog->slug != $data['slug']){
            return response()->json([
                'type' => 'danger',
                'msg' => 'Slug already existed',
                'statuscode' => 500
            ]);
        }

		if(!$blog->update($data)){
	          
	        DB::rollBack();

	        return response()->json([
	            'type' => 'danger',
	            'msg' => 'Something went wrong please try again. Blog not updated!',
	            'statuscode' => 409
	        ]);

		}

		$del_tags_collections = BlogTagsCollection::where('blog_id',$blog->id)->get();
       
		foreach	($del_tags_collections as $key => $tag){
		 	if(!$tag->delete()){
		 	  	DB::rollBack();
	            return response()->json([
	                'type' => 'danger',
	                'msg' => 'Something went wrong please try again. Blog not updated!',
	                'statuscode' => 409
	            ]);
		 	 }
		}

		$blog_tags_collections = new BlogTagsCollection();
		$tags = $request->tags;

		foreach ($tags as $key => $tag) {
		 	 
		 	$tag_data = [
	         	"blog_id" => $blog->id,
	         	"tag_id" => $tag
	    	];

	    	if(!$blog_tags_collections::create($tag_data)){
	    	 	
	    	 	DB::rollBack();

	            return response()->json([
	                'type' => 'danger',
	                'msg' => 'Something went wrong please try again. Blog not save!',
	                'statuscode' => 409
	            ]);
	    	}
		}

		DB::commit();
	    return response()->json([
	            'type' => 'success',
	            'msg' => 'Blog successfully updated.',
	            'statuscode' => 200
	    ]);
    }


    public function destroyBlog(Request $request){
    	
    	DB::beginTransaction();
    	
    	$blog = Blog::find($request->id);
    	if(!$blog->delete()){
    		DB::rollBack();
		    return response()->json([
		    	'type' => 'danger',
		        'msg' => 'Something went wrong please try again. Blog not deleted!',
		        'statuscode' => 409
		    ]);
    	}

		$del_tags_collections = BlogTagsCollection::where(['blog_id' => $blog->id])->get();
		foreach ($del_tags_collections as $key => $tag){
			if(!$tag->delete()){
				DB::rollBack();
			    return response()->json([
			    	'type' => 'danger',
			        'msg' => 'Something went wrong please try again. Blog not deleted!',
			        'statuscode' => 409
			    ]);
			}
	    }
    	
    	DB::commit();

	    return response()->json([
	        'type' => 'success',
	        'msg' => 'Blog successfully deleted.',
	        'statuscode' => 200
	    ]);

    }

    public function getTag(){
    	return Tag::all();
    }

    public function createTag(Request $request){
    	
    	$data = $request->all();

    	$check = $this->checkTagSlug($data['slug']);
    	if(!$check){
    		return response()->json([
		    	'type' => 'danger',
		        'msg' => 'Slug already existed',
		        'statuscode' => 500
		    ]);
    	}
        
        $tag = new Tag();
    	
    	if(!$tag->create($data)){
    		return response()->json([
		    	'type' => 'danger',
		        'msg' => 'Something went wrong please try again. Tag not save!',
		        'statuscode' => 409
		    ]);
    	}

        return response()->json([
	        'type' => 'success',
	        'msg' => 'Tag successfully save.',
	        'statuscode' => 200
	    ]);
    }

    public function listTag(Request $request){

    	$keyword = $request->input('keyword');
        $sortBy = $request->input('sortBy');
        $order = $request->input('order');

        if($keyword == '' || $keyword == null){
           $list = DB::table('tags')
              ->select("id","name","slug","created_at")
              ->orderBy($sortBy,$order);
        }else{
           $list = DB::table('tags')
              ->select("id","name","slug","created_at")
              ->where("name","LIKE","%{$keyword}%")
              ->orWhere("slug","LIKE","%{$keyword}%")
              ->orderBy($sortBy,$order);
        }

        $tagList = $list->paginate(10)->toArray();
        $tagList['dataList'] = DB::table('tags')->select("*")->get();   
        return response()->json($tagList);
    }

    public function showTag($id){
  		return Tag::find($id);
    }

    public function updateTag(Request $request){
    	
    	$data = $request->only('name','slug');

    	$tag = Tag::find($request->id);

    	$check = $this->checkTagSlug($data['slug']);
    	if(!$check && $tag->slug != $data['slug']){
    		return response()->json([
		    	'type' => 'danger',
		        'msg' => 'Slug already existed',
		        'statuscode' => 500
		    ]);
    	}
       
    	if(!$tag->update($data)){
    		return response()->json([
		    	'type' => 'danger',
		        'msg' => 'Something went wrong please try again. Tag not updated!',
		        'statuscode' => 409
		    ]);
    	}

        return response()->json([
	        'type' => 'success',
	        'msg' => 'Tag successfully updated.',
	        'statuscode' => 200
	    ]);
    }

    public function destroyTag(Request $request){
    	
        $tag = Tag::find($request->id);
    	
    	if(!$tag->delete()){
    		return response()->json([
		    	'type' => 'danger',
		        'msg' => 'Something went wrong please try again. Tag not deleted!',
		        'statuscode' => 409
		    ]);
    	}

        return response()->json([
	        'type' => 'success',
	        'msg' => 'Tag successfully deleted.',
	        'statuscode' => 200
	    ]);
    }

    private function checkBlogSlug($slug){
    	$blog = Blog::where(['slug' => $slug])->first();
    	$isExisted = true;
    	if($blog){
    		$isExisted = false;
    	} 
    	return $isExisted;
    }

    private function checkTagSlug($slug){
    	$tag = Tag::where(['slug' => $slug])->first();
    	$isExisted = true;
    	if($tag){
    		$isExisted = false;
    	} 
    	return $isExisted;
    }

    public function uploadFeaturedImage(Request $request){
        if(!$request->hasFile('file')){
          return response()->json(['error' => "No file found"], 401);
        }
        
        $file = $request->file('file');
        if(!$file->move(public_path()."/img", $file->getClientOriginalName())) {
            return response()->json(['error' => 'Error saving the file.'], 401);
        }
    }

    public function getBlogList(){
          $list = DB::table("blogs")->select("*");
          $data = $list->paginate(5)->toArray();
          return response()->json($data);
    }

    public function getBlogDetail($id, $tagid = null){
        $data = array();
        $data['main'] = Blog::find($id);
        $all = Blog::all();
        if($tagid){
            $all = DB::table('blog_tags_collections')
            ->join('blogs', 'blogs.id', '=', 'blog_tags_collections.blog_id')
            ->select('blogs.*')
            ->where("blog_tags_collections.tag_id", $tagid)->get();
            $data['tag'] = Tag::find($tagid);
        }
        $data['secondary'] = array();
        foreach ($all as $key => $item) {
            if($item->id == $id && $all->count() > 1){
               if($key == 0){
                    $data['secondary']['next'] = $all[$key + 1];
               }else if($key == $all->count() - 1){
                    $data['secondary']['prev'] = $all[$key - 1];
               }else{
                    $data['secondary']['prev'] = $all[$key - 1];
                    $data['secondary']['next'] = $all[$key + 1];
               }
            }
        }
        $data['main']['tags'] = DB::table('blog_tags_collections')
            ->join('tags', 'tags.id', '=', 'blog_tags_collections.tag_id')
            ->select('tags.*')
            ->where("blog_tags_collections.blog_id", $id)->get();
        return response()->json($data); 
    }

    public function getBlogListByTag($id){
         
         $list = DB::table('blog_tags_collections')
            ->join('blogs', 'blogs.id', '=', 'blog_tags_collections.blog_id')
            ->select('blogs.*')
            ->where("blog_tags_collections.tag_id", $id);
         
         $data = $list->paginate(5)->toArray();
         $data['tag'] = Tag::find($id);

         return response()->json($data);
    }
}
