<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Guid;
use App\Libraries\TimeElapsed;
use App\Abouthomes;
use DB; 
use JWTAuth;
use Tymon\JWTAuthExceptions\JWTException;

// models
use App\Offers;
use App\Offersotherbuyers;
use App\Contract;
use App\Users;
use App\Notification;
use App\EmailStatus;

use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use Carbon\Carbon;
use App\Events\eventTrigger;

class OfferController extends Controller
{
    public function __construct()
    {
       // Apply the jwt.auth middleware to all methods in this controller
       // except for the authenticate method. We don't want to prevent
       // the user from retrieving their token if they don't already have it
       $this->middleware('jwt.auth');
    }

    public function create(Request $request){
        
        $data = array();

        if($request->isMethod('post')) {

            DB::beginTransaction();

            $allrequest = $request->all();

            $inclusion = $request->input('inclusion');

            // inclusions
            $allrequest['refrigerator'] = isset($inclusion['refrigerator'])? $inclusion['refrigerator'] : null;
            $allrequest['refrigeratordesc'] = isset($inclusion['refrigeratordesc'])? $inclusion['refrigeratordesc'] : null ;
            $allrequest['washer'] = isset($inclusion['washer'])? $inclusion['washer'] : null ;
            $allrequest['washerdesc'] = isset($inclusion['washerdesc'])? $inclusion['washerdesc'] : null;
            $allrequest['dryer'] = isset($inclusion['dryer'])? $inclusion['dryer'] : null;
            $allrequest['dryerdesc'] = isset($inclusion['dryerdesc'])? $inclusion['dryerdesc'] : null;
            $allrequest['aboveground'] = isset($inclusion['aboveground'])? $inclusion['aboveground'] : null;
            $allrequest['abovegrounddesc'] = isset($inclusion['abovegrounddesc'])? $inclusion['abovegrounddesc'] : null;
            $allrequest['other'] = isset($inclusion['other'])? $inclusion['other'] : null;
            $allrequest['otherdesc'] = isset($inclusion['otherdesc'])? $inclusion['otherdesc'] : null;

            $save = new Offers();
            $save->fill($allrequest);

            if(!$save->save($allrequest)){              

                DB::rollBack();

                return response()->json([
                    'type' => 'danger',
                    'msg' => 'Something went wrong please try again. Offer not save!',
                    'statuscode' => 409
                ]);
            }

            $otherowner = !empty($request->input('otherbuyer')) ? $request->input('otherbuyer') : array();
            
            foreach ($otherowner as $otherbuyer) {

                $saveotherbuyer = new Offersotherbuyers();
                $saveotherbuyer->offerid = $save->id;
                $saveotherbuyer->buyersname = $otherbuyer['name'];
                $saveotherbuyer->buyersemail = $otherbuyer['email'];

                if(!$saveotherbuyer->save()){
                    DB::rollBack();

                    return response()->json([
                        'type' => 'danger',
                        'msg' => 'Something went wrong please try again. Offer not save!',
                        'statuscode' => 409
                    ]);
                }

            }

            $listedmenuasnwer = $request->input('listedmenuasnwer');
            // if($listedmenuasnwer == "turbo"){
            //      $seller_email = $request->input('scselleraddress');
            //      $user = Users::where(['email'=> $seller_email])->first();
            //      $notif = array();
            //      $notif['sender_id'] = $request->input('userid');
            //      $notif['receiver_id'] = $user->id;
            //      $notif['status'] = "unread";
            //      $notif['title'] = "New Offer Request";
            //      $notif['msg'] = "You have new offer! Offer by buyer ". $request->input("buyersname")." with an offer money of $".number_format($request->input("scfinancingamt"), 2).".";
            //      $notification = new Notification();
            //      $notification->fill($notif);
            //      if(!$notification->save($notif)){              

            //           DB::rollBack();

            //           return response()->json([
            //               'type' => 'danger',
            //               'msg' => 'Something went wrong please try again. Offer not save!',
            //               'statuscode' => 409
            //           ]);
            //     }
            //     $notif['id'] = $notification->id;
            //     $time_elapsed = new TimeElapsed();
            //     $notif['time_elapsed'] = $time_elapsed->ago(date_create(date('Y-m-d H:i:s')));
            //     event(new eventTrigger($notif));
            // }

            DB::commit();

            return response()->json([
                'type' => 'success',
                'msg' => 'Offer Successfuly Saved!',
                'statuscode' => 200
            ]);

        }
    }

  public function list(Request $request){
     
      if ($request->isMethod('post')){

        $keyword = $request->input('keyword');
        $sortBy = $request->input('sortBy');
        $order = $request->input('order');

        if($sortBy == "listed_by"){
           $sortBy = "buyersemail";
        }

        if($keyword == '' || $keyword == null){

          $list = DB::table('offers')
             ->select("id","scpropertyaddress","buyersname","buyersemail","created_at")
             ->orderBy($sortBy,$order);
        }
        else{

          $list = DB::table('offers')
            ->select("id","scpropertyaddress","buyersname","buyersemail","created_at")
            ->where("buyersname", "LIKE", "%{$keyword}%")
            ->orWhere("buyersemail", "LIKE","%{$keyword}%")
            ->orWhere("listedmenuasnwer", "LIKE", "%{$keyword}%")
            ->orWhere("scpropertyaddress", "LIKE", "%{$keyword}%")
            ->orderBy($sortBy,$order);

        }

        $offerlist = $list->paginate(10)->toArray();
        $offerlist ['dataList'] = $list = DB::table('offers')
              ->select("*")->get();  
        return response()->json($offerlist);

      }
    }

    public function view($id){

      $query = ['id' => $id];

      $query2 = ['offerid' => $id];

      $result = Offers::where($query)->first();

      $otherbuyer = Offersotherbuyers::where($query2)->get();
      // var_dump($otherbuyer);
      $result->otherbuyer = $otherbuyer;

      return $result;
    }

    public function edit(Request $request){
        
        if($request->isMethod('post')) {

            DB::beginTransaction();
             
            $id = $request->input('id'); 
            $allrequest = $request->all();

            $inclusion = $request->input('inclusion');

            // inclusions
            $allrequest['refrigerator'] = isset($inclusion['refrigerator'])? $inclusion['refrigerator'] : null;
            $allrequest['refrigeratordesc'] = isset($inclusion['refrigeratordesc'])? $inclusion['refrigeratordesc'] : null ;
            $allrequest['washer'] = isset($inclusion['washer'])? $inclusion['washer'] : null ;
            $allrequest['washerdesc'] = isset($inclusion['washerdesc'])? $inclusion['washerdesc'] : null;
            $allrequest['dryer'] = isset($inclusion['dryer'])? $inclusion['dryer'] : null;
            $allrequest['dryerdesc'] = isset($inclusion['dryerdesc'])? $inclusion['dryerdesc'] : null;
            $allrequest['aboveground'] = isset($inclusion['aboveground'])? $inclusion['aboveground'] : null;
            $allrequest['abovegrounddesc'] = isset($inclusion['abovegrounddesc'])? $inclusion['abovegrounddesc'] : null;
            $allrequest['other'] = isset($inclusion['other'])? $inclusion['other'] : null;
            $allrequest['otherdesc'] = isset($inclusion['otherdesc'])? $inclusion['otherdesc'] : null;

            $model = Offers::find($id);
            
            if(!$model->update($allrequest)){              

                DB::rollBack();

                return response()->json([
                    'type' => 'danger',
                    'msg' => 'Something went wrong please try again. Offer not updated!',
                    'statuscode' => 409
                ]);
            }

            $offerowners = Offersotherbuyers::where([ 'offerid' => $id ])->get();
            foreach ($offerowners as $key => $otherowner) {
                  if(!$otherowner->delete()){
                      DB::rollBack();
                      return response()->json([
                          'type' => 'danger',
                          'msg' => 'Something went wrong please try again. Offer not updated!',
                          'statuscode' => 409
                      ]);
                  }
            }
            
            $otherowner = !empty($request->input('otherbuyer')) ? $request->input('otherbuyer') : array();
            
            foreach ($otherowner as $otherbuyer) {

                $saveotherbuyer = new Offersotherbuyers();
                $saveotherbuyer->offerid = $model->id;
                $saveotherbuyer->buyersname = $otherbuyer['name'];
                $saveotherbuyer->buyersemail = $otherbuyer['email'];

                if(!$saveotherbuyer->save()){
                    DB::rollBack();

                    return response()->json([
                        'type' => 'danger',
                        'msg' => 'Something went wrong please try again. Offer not save!',
                        'statuscode' => 409
                    ]);
                }

            }

            DB::commit();

            return response()->json([
                'type' => 'success',
                'msg' => 'Offer Successfuly Updated!',
                'statuscode' => 200
            ]);

        }
    }
    
    public function destroy(Request $request) {
     	
     	$offerid = $request->input('offerid');

     	$offers = Offers::where([ 'id' => $offerid ])->first();
     	$offerowners = Offersotherbuyers::where([ 'offerid' => $offerid ])->get();
     	$data = array(
     		'type' => 'danger',
     		'msg' => 'Something went wrong, please try again later',
     	);

     	if($offers && $offerowners){
         	if(!$offers->delete()){
             return response()->json($data);
          }

          foreach ($offerowners as $key => $otherowner) {
                  if(!$otherowner->delete()){
                      return response()->json($data);
                  }
          }
          
          $data['type'] = 'success';
          $data['msg'] = 'Offer Details Successfully deleted!';
          
      }

      return response()->json($data);
    }

    public function emailundercontract() {
    	Mail::send('backend.email.buyer.undercontract', array(), function ($message)
        {
            $message->from('enriquez.mark.jan@gmail.com', 'Turbo Homes');
      		$message->to('turbobuyer123@gmail.com');
        });
    }

    public function threedaysleftinspection() {
    	Mail::send('backend.email.buyer.last3daysinspection', array(), function ($message)
        {
            $message->from('enriquez.mark.jan@gmail.com', 'Turbo Homes');
      		$message->to('turbobuyer123@gmail.com');
        });
    }

    public function lastdayinspection() {
    	Mail::send('backend.email.buyer.lastdayofinspection', array(), function ($message)
        {
            $message->from('enriquez.mark.jan@gmail.com', 'Turbo Homes');
      		$message->to('turbobuyer123@gmail.com');
        });
    }

    public function dayafterinspection() {
    	Mail::send('backend.email.buyer.dayafterinspectionperiod', array(), function ($message)
        {
            $message->from('enriquez.mark.jan@gmail.com', 'Turbo Homes');
      		$message->to('turbobuyer123@gmail.com');
        });
    }

    public function fivedaysclosing() {
    	Mail::send('backend.email.buyer.5daysbeforeclosedate', array(), function ($message)
        {
            $message->from('enriquez.mark.jan@gmail.com', 'Turbo Homes');
      		$message->to('turbobuyer123@gmail.com');
        });
    }

    public function contract(Request $request) {
        DB::beginTransaction();
        $guid = new Guid;

        $allrequest = $request->all();

        if(!empty($allrequest['offerid'])) {
            $offer = Offers::where([ 'id' => $allrequest['offerid'] ])->first();
            $offer->status = "UNDER CONTRACT";
            $emailTemplate = 'backend.email.buyer.';
            $receiver_email = $offer->buyersemail;
            if(!$offer->save()){              
                DB::rollBack();
                return response()->json([
                    'type' => 'danger',
                    'msg' => 'Something went wrong please try again. Offer not save!',
                    'statuscode' => 409
                ]);
            }
        } else if(!empty($allrequest['homeid'])) {
            $home = Abouthomes::where([ 'id' => $allrequest['homeid'] ])->first();
            $home->contract_status = "UNDER CONTRACT";
            $emailTemplate = 'backend.email.seller.';
            $receiver_email = $home->owneremail;
            if(!$home->save()){              
                DB::rollBack();
                return response()->json([
                    'type' => 'danger',
                    'msg' => 'Something went wrong please try again. Offer not save!',
                    'statuscode' => 409
                ]);
            }
        } else {    
            DB::rollBack();
            return response()->json([
                'type' => 'danger',
                'msg' => 'Something went wrong please try again. Offer not save!',
                'statuscode' => 409
            ]);
        }

        $contract = new Contract();
        $contract->id = $guid->GUID();
        $contract->contractdate = date('Y-m-d', strtotime($allrequest['contractdate']));
        $contract->inspectiondate = date('Y-m-d', strtotime($allrequest['inspectiondate']));
        $contract->closingdate = date('Y-m-d', strtotime($allrequest['closingdate']));
        $contract->offerid = empty($allrequest['offerid']) ? null : $allrequest['offerid'];
        $contract->homeid = empty($allrequest['homeid']) ? null : $allrequest['homeid'];
        if(!$contract->save()){              
            DB::rollBack();
            return response()->json([
                'type' => 'danger',
                'msg' => 'Something went wrong please try again. Offer not save!',
                'statuscode' => 409
            ]);
        }

        Mail::to($receiver_email)
          ->send(new SendMail($emailTemplate . 'undercontract', 'Turbohomes - You are under contract'));
        
        $emailstat = new EmailStatus();
        $emailstat->id = $guid->GUID();
        $emailstat->contract_id = $contract->id;
        $emailstat->undercontract = $allrequest['contractdate'];
        if(!$emailstat->save()){              
            DB::rollBack();
            return response()->json([
                'type' => 'danger',
                'msg' => 'Something went wrong please try again. Offer not save!',
                'statuscode' => 409
            ]);
        }

        DB::commit();

        return response()->json([
            'type' => 'success',
            'msg' => 'Offer Successfuly Saved!',
            'statuscode' => 200
        ]);
    }

    public function get_contract($id, $type) {
        if($type === 'offer') {
            $contract = Contract::where([ 'offerid' => $id ])->first();
        } else {
            $contract = Contract::where([ 'homeid' => $id ])->first();
        }

        if(!$contract) {
            return null;
        }

        return $contract->toArray();
    }

    public function editContract(Request $request) {
       
        $id = $request->id;
        $data = $request->only('contractdate','inspectiondate','closingdate');
        $contract = Contract::find($id);
        
        if(!$contract->update($data)){              
            return response()->json([
                'type' => 'danger',
                'msg' => 'Something went wrong please try again. Offer not updated!',
                'statuscode' => 409
            ]);
        }

        return response()->json([
            'type' => 'success',
            'msg' => 'Offer Successfuly Updated!',
            'statuscode' => 200
        ]);
    }

    // public function emailsample() {

    //     $c = Offers::where([])->first();

    //     if($c) {
    //         Mail::to($c->scselleraddress)->send(new SendMail('backend.email.buyer.registration'));
    //         Mail::to($c->scselleraddress)->send(new SendMail('backend.email.buyer.undercontract'));
    //         Mail::to($c->scselleraddress)->send(new SendMail('backend.email.buyer.last3daysinspection'));
    //         Mail::to($c->scselleraddress)->send(new SendMail('backend.email.buyer.lastdayofinspection'));
    //         Mail::to($c->scselleraddress)->send(new SendMail('backend.email.buyer.dayafterinspectionperiod'));
    //         Mail::to($c->scselleraddress)->send(new SendMail('backend.email.buyer.5daysbeforeclosedate'));

    //         Mail::to($c->scselleraddress)->send(new SendMail('backend.email.seller.atpaymentto'));
    //         Mail::to($c->scselleraddress)->send(new SendMail('backend.email.seller.checkingin'));
    //         Mail::to($c->scselleraddress)->send(new SendMail('backend.email.seller.closing'));
    //         Mail::to($c->scselleraddress)->send(new SendMail('backend.email.seller.earnestmoneydue'));
    //         Mail::to($c->scselleraddress)->send(new SendMail('backend.email.seller.inspectionperiod'));
    //         Mail::to($c->scselleraddress)->send(new SendMail('backend.email.seller.onceundercontract'));

    //         $c = Contract::where('offerid', $c->id)->first();

    //         if($c) {
    //             $emailstat = EmailStatus::where('contract_id', $c->id)->first();

    //             if($emailstat) {
    //                 $stat = null;
    //                 $emailstat->undercontract = $stat;
    //                 $emailstat->last3daysinspection = $stat;
    //                 $emailstat->lastdayofinspection = $stat;
    //                 $emailstat->dayafterinspectionperiod = $stat;
    //                 $emailstat->fivedaysbeforeclosedate = $stat;

    //                 $emailstat->atpaymentto = $stat;
    //                 $emailstat->checkingin = $stat;
    //                 $emailstat->closing = $stat;
    //                 $emailstat->earnestmoneydue = $stat;
    //                 $emailstat->inspectionperiod = $stat;
    //                 $emailstat->onceundercontract = $stat;
    //             } else {
    //                 $emailstat = new EmailStatus();
    //                 $guid = new Guid;
    //                 $emailstat->id = $guid->GUID();
    //                 $emailstat->contract_id = $c->id;
    //             }

    //             if(!$emailstat->save()) {
    //                 return 'error';
    //             }

    //             return 'success';
    //         }

    //     }
    // }
}
