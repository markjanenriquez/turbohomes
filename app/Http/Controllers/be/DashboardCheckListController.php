<?php

namespace App\Http\Controllers\be;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\DashboardCheckList;
use DB;

class DashboardCheckListController extends Controller
{
    public function __construct()
    {
       // Apply the jwt.auth middleware to all methods in this controller
       // except for the authenticate method. We don't want to prevent
       // the user from retrieving their token if they don't already have it
       $this->middleware('jwt.auth');
    }


    public function setCheckList(Request $request){
    	 
    	  DB::beginTransaction();
    	  $data = $request->all();
          $check_data = $request->only('user_id','check_item');
          $checkIfAdded = DashboardCheckList::where($check_data)->first(); 
          if(!$checkIfAdded){
             $model = new DashboardCheckList($data);
             if(!$model->save()){

             	DB::rollBack();

                return response()->json([
                    'type' => 'danger',
                    'msg' => 'Something went wrong please try again.',
                    'statuscode' => 409
                ]);
             }  
    	  }else{
    	  	 $id = $checkIfAdded->id;
    	  	 $status = $data['status'];
    	     $model = DashboardCheckList::find($id);
             if(!$model->update([ 'status' => $status ])){

             	DB::rollBack();

                return response()->json([
                    'type' => 'danger',
                    'msg' => 'Something went wrong please try again.',
                    'statuscode' => 409
                ]);
             }  	 
    	  }

    	  DB::commit();
    	  return response()->json([
                    'type' => 'success',
                    'msg' => 'Dashboard CheckList Status has been successfully updated.',
                    'statuscode' => 200
          ]);
    }

    public function getChecklist($user_id){
    	 
         $model = DashboardCheckList::where(['user_id' => $user_id])->get();
         $data = [];
         
         foreach ($model as $key => $item) {
             $data[$item->check_item]['status'] = $item->status;     
         }

         return response()->json($data);
    }


}
