<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PopUpShowed extends Model
{
   protected $table = 'pop_up_showed';
   protected $fillable = [
      'user_id'
    ];
}
