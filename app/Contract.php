<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
     public $timestamps = true;
     public $incrementing = false;
     protected $fillable = [
 		  "contractdate",
 		  "inspectiondate",
 		  "closingdate"
     ];
}
