<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Carbon\Carbon;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;
use App\EmailStatus;
use App\Libraries\Guid;

class sendMails extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'send:mail';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'for sending email under contract';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(){
        parent::__construct();
    }

    private function get_seller_layout($contract) {
        $data = array(
            'email_layout' => 'backend.email.seller.'
        );

        $today = Carbon::now()->format('d-m-Y');
        $contract_date = Carbon::parse($contract->contractdate)->format('d-m-Y');
        $after_three_days_date = Carbon::parse($contract_date)->addDays(3)->format('d-m-Y'); 
        $end_inspection_date = Carbon::parse($contract->inspectiondate)->format('d-m-Y');
        $closing_date = Carbon::parse($contract->closingdate)->format('d-m-Y');
        $five_days_before_closing_date = Carbon::parse($closing_date)->subDays(5)->format('d-m-Y');
        $ten_days_after_listing_date = '';
        if(!empty($contract->homeid)) {
            $abouthomes = DB::table("abouthomes")->select("created_at AS listingdate", "owneremail")->where('id', $contract->homeid)->first();
            if($abouthomes) {
                $listing_date = Carbon::parse($abouthomes->listingdate)->format('d-m-Y');
                $ten_days_after_listing_date = Carbon::parse($listing_date)->addDays(10)->format('d-m-Y');

                // if($contract_date == $today){
                //     $data['field'] = 'onceundercontract';
                //     $data['email_layout'] .= $data['field'];
                //     $data['subject'] = "Turbohomes - You are under contract";
                // } else 
                if($after_three_days_date == $today){
                    $data['field'] .= "earnestmoneydue";
                    $data['email_layout'] .= $data['field'];
                    $data['subject'] = "Turbohomes - 3 days in...Earnest Money is due";
                } else if($end_inspection_date == $today){
                    $data['field'] = 'inspectionperiod';
                    $data['email_layout'] .= $data['field'];
                    $data['subject'] = "Turbohomes - Inspection period ends today";
                } else if($five_days_before_closing_date == $today){
                    $data['field'] = 'closing';
                    $data['email_layout'] .= $data['field'];
                    $data['subject'] = "Turbohomes - The day is near. It’s time to close";
                } else if($ten_days_after_listing_date == $today){
                    $data['field'] = 'checkingin';
                    $data['email_layout'] .= $data['field'];
                    $data['subject'] = "Turbohomes - Checking in";
                }
            }
        }

        return $data;
    }

    private function get_buyer_layout($contract) {
        $data = [
            'email_layout' => 'backend.email.buyer.'
        ];

        $offer = DB::table("offers")->select("*")->where('id', $contract->offerid)->first();
        if($offer) {
            $inspection3daysleft = $offer->inspectiondeadline - 3;

            $today = Carbon::now()->format('d-m-Y');
            $contract_date = Carbon::parse($contract->contractdate)->format('d-m-Y');
            $inspection3daysleft = Carbon::parse($contract_date)->addDays($inspection3daysleft)->format('d-m-Y'); 
            $end_inspection_date = Carbon::parse($contract->inspectiondate)->format('d-m-Y');
            $day_after_inspection = Carbon::parse($contract->inspectiondate)->addDays(1)->format('d-m-Y');
            $five_days_before_closing_date = Carbon::parse($contract->closingdate)->subDays(5)->format('d-m-Y');

            $data['receiver_email'] = $offer->buyersemail;
            // if($contract_date == $today){
            //     $data['field'] = 'undercontract';
            //     $data['email_layout'] .= $data['field'];
            //     $data['subject'] = "Turbohomes - You are under contract";
            // } else 
            if($inspection3daysleft == $today){
                $data['field'] = 'last3daysinspection';
                $data['email_layout'] .= $data['field'];
                $data['subject'] = "Turbohomes - 3 days left in your Inspection Period";
            } else if($end_inspection_date == $today){
                $data['field'] = 'lastdayofinspection';
                $data['email_layout'] .= $data['field'];
                $data['subject'] = "Last day of the Inspection Period";
            } else if($day_after_inspection == $today){
                $data['field'] = 'dayafterinspectionperiod';
                $data['email_layout'] .= $data['field'];
                $data['subject'] = "Turbohomes - The march to close";
            } else if($five_days_before_closing_date == $today){
                $data['email_layout'] .= "5daysbeforeclosedate";
                $data['field'] = 'fivedaysbeforeclosedate';
                $data['subject'] = "Turbohomes - 5 days left to close";
            }
        }

        return $data;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $contracts = DB::table("contracts")->select("*")->get();

        foreach ($contracts->toArray() as $key => $contract) {
            if(!empty($contract->offerid)) {
                $response = $this->get_buyer_layout($contract);
            } else if(!empty($contract->homeid)) {
                $response = $this->get_seller_layout($contract);
            }

            if(isset($response) && isset($response['field']) && !is_null($response['field'])) {

                $emailstat = EmailStatus::where('contract_id', $contract->id)->first();

                if($emailstat) {
                    $date = date('Y-m-d');
                    if($key === 'buyer') {
                        switch ($response['field']) {
                            case 'undercontract':
                                $emailstat->undercontract = $date;
                                break;
                            case 'last3daysinspection':
                                $emailstat->last3daysinspection = $date;
                                break;
                            case 'lastdayofinspection':
                                $emailstat->lastdayofinspection = $date;
                                break;
                            case 'dayafterinspectionperiod':
                                $emailstat->dayafterinspectionperiod = $date;
                                break;
                            case 'fivedaysbeforeclosedate':
                                $emailstat->fivedaysbeforeclosedate = $date;
                                break;
                            case 'earnestmoneydue':
                                $emailstat->earnestmoneydue = $date;
                                break;
                            case 'inspectionperiod':
                                $emailstat->inspectionperiod = $date;
                                break;
                            case 'closing':
                                $emailstat->closing = $date;
                                break;
                            case 'checkingin':
                                $emailstat->checkingin = $date;
                                break;
                        }
                    }
                } else {
                    $emailstat = new EmailStatus();
                    $guid = new Guid;
                    $emailstat->id = $guid->GUID();
                    $emailstat->contract_id = $contract->id;
                }

                if(!$emailstat->save()) {
                    echo 'error\n';
                }

                Mail::to($response['receiver_email'])
                    ->send(new SendMail($response['email_layout'], $response['subject']));
            }
        } 
        echo "Cron has been successfully executed.\n";
    }
}
