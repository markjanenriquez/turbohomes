<?php
use App\Events\eventTrigger;
use App\Mail\SendMail;
use Illuminate\Support\Facades\Mail;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Frontend

Route::get('/landingpage', function () {
    return view('frontend/landingpage');
});

Route::get('/privacyandpolicy', function () {
    return view('frontend/privacyandpolicy');
});

Route::get('/termsandcondition', function () {
    return view('frontend/termsandcondition');
});

Route::get('/', function () {
    return view('frontend/index');
});

Route::get('/login', function () {
    return view('frontend/login');
});

Route::get('/signup', function () {
    return view('frontend/signup');
});

Route::get('/users/activation/{token}','be\UsersController@accountactivation');

Route::get('/activated', function () {
    return view('frontend/activated');
});

Route::get('/activation/expired', function () {
    return view('frontend/expired');
});

// buyer
Route::get('/how-to-buy-with-turbo-homes', function () {
    return view('frontend/buyer/how-to-buy-with-turbo-homes');
});

// buyer
Route::get('/dashboard', function () {
    return view('frontend/buyer/makeanoffer');
});

//seller
Route::get('/sell', function () {
    return view('frontend/seller/sell');
});

//seller form
Route::get('/sell-form', function () {
    return view('frontend/seller/sell-form');
});

Route::get('/faqs', function () {
    return view('frontend/faqs');
});

//about

Route::get('/about', function () {
    return view('frontend/about/about');
});

Route::get('/testimonials', function () {
    return view('frontend/about/testimonials');
});

Route::get('/careers', function () {
    return view('frontend/about/careers');
});

Route::get('/contact-us', function () {
    return view('frontend/contactus');
});

Route::get('/blogs', function () {
    return view('frontend/blogs');
});
Route::get('/blogs/{slug}', function () {
    return view('frontend/blogview');
});
Route::get('/blogs/tag/{slug}', function () {
    return view('frontend/blogsbytag');
});

// Backend

Route::get('/user/password/reset/{token}', 'be\UsersController@checkresettoken');

Route::get('/forgot/password/expired', function(){
    return view("backend/index/expired");
});

Route::get('/provider-market', function() {
    return view('frontend/provider-market');
});

// Backend
Route::group(['prefix' => 'admin'], function(){
    Route::get('/', function () {
   	    return view('backend/index/login');
    });
    Route::get('/forgotpassword', function () {
        return view('backend/index/forgotpassword');
    });
    Route::get('/main', function () {
        return view('backend/layout/layout');
    });
    Route::get('/{param1}/{param2}', function ($param1, $param2) {
        return view('backend/'.$param1.'/'.$param2);
    });
});

Route::group(['prefix' => 'api','namespace' => 'be'], function(){

    //USERS
    Route::group(['prefix' => 'user'] , function(){
        Route::post('login','UsersController@authenticate');
        Route::post('signup','UsersController@signup');
        Route::get('info','UsersController@getAuthenticatedUser');
        Route::get('token','UsersController@token');
        Route::post('create','UsersController@create');
        Route::post('list','UsersController@list');
        Route::post('view','UsersController@view');
        Route::post('update','UsersController@update');
        Route::post('delete','UsersController@destroy');
        Route::post('changepassword','UsersController@changepassword');
        Route::post('resetpassword','UsersController@resetpassword');
        Route::post('upload','UsersController@upload');
        Route::post('check/id','UsersController@checkuserid');
        Route::post('check/username','UsersController@checkusername');
        Route::post('password/forgot','UsersController@passwordforgot');
        Route::post('validate-email', 'UsersController@validate_email');
        Route::post('resend-verification', 'UsersController@resend_email_verification');
    });

     //OFFERS
    Route::group(['prefix' => 'offer'],function(){
	    Route::post('create','OfferController@create');
	    Route::post('list','OfferController@list');
	    Route::post('view/{id}','OfferController@view');
        Route::post('edit','OfferController@edit');
	    Route::post('delete','OfferController@destroy');
	    Route::post('contract', 'OfferController@contract');
        Route::get('contract/{id}/{type}', 'OfferController@get_contract');
        Route::post('contract/edit','OfferController@editContract');
	});

	//SELLERS
    Route::group(['prefix' => 'seller'], function(){
	    Route::post('create','SellerController@create');
	    Route::post('list','SellerController@list');
	    Route::post('view/{id}','SellerController@view');
        Route::post('edit', 'SellerController@edit');
	    Route::post('delete','SellerController@destroy');
        Route::post('create-partial','SellerController@create_partial');
        Route::post('set_status','SellerController@set_status');
	});

	//NOTIFICATIONS
	Route::group(['prefix' => 'notification'], function(){
        Route::post('set','NotificationController@set');
        Route::get('get/{user_id}','NotificationController@get');
	});

    //EMAIL STATUS
    Route::group(['prefix' => 'email/status'],function(){
        Route::get('get/{id}/{type}', 'EmailStatusController@get');
    });


    //DASHBOARD CHECKLIST STATUS
    Route::group(['prefix' => 'dashboard-status'], function(){
        Route::post('set', 'DashboardCheckListController@setCheckList');
        Route::get('get/{user_id}', 'DashboardCheckListController@getChecklist');
    });


    //PROPERTY VALUE
    Route::group(['prefix' => 'property-value'], function(){
        Route::post('get', "ZillowController@getReport");
    });

    //POP UP SHOWED
    Route::group(['prefix' =>'pop-up-showed'], function(){
        Route::post('set', "PopUpShowedController@set");
    });

    //listing
    Route::group(['prefix' =>'listing'], function(){
        Route::get('/{userid}', "SellerController@get_seller_listing");
        Route::post('/{userid}', "SellerController@update_listed_home");
    });

    //BLOG
    Route::group(['prefix' => 'blog'], function(){
        Route::post('create', 'BlogController@createBlog');
        Route::get('show/{id}', 'BlogController@showBlog');
        Route::post('list', 'BlogController@listBlog');
        Route::post('update', 'BlogController@updateBlog');
        Route::post('destroy', 'BlogController@destroyBlog');
        Route::get('tag/get','BlogController@getTag');
        Route::post('upload','BlogController@uploadFeaturedImage');
    });

    //TAG
    Route::group(['prefix' => 'tag'], function(){
        Route::post('create', 'BlogController@createTag');
        Route::get('show/{id}', 'BlogController@showTag');
        Route::post('list', 'BlogController@listTag');
        Route::post('update', 'BlogController@updateTag');
        Route::post('destroy', 'BlogController@destroyTag');
    });

    //FE BLOG
    Route::group(['prefix' => 'fe'], function(){
        Route::group(['prefix' => 'blog'], function(){
            Route::get('list', 'BlogController@getBlogList');
            Route::get('get/{id}/{tagid?}', 'BlogController@getBlogDetail');
            Route::get('list/{id}', 'BlogController@getBlogListByTag');
        });
    });

    Route::post('/transaction/ipn','TransactionsController@listener');
    Route::post('/contactus','UsersController@contactus');
});


// Route::get('sendMail', function(){

//        Illuminate\Support\Facades\Mail::to('nathancalaus@mailinator.com')->send(new App\Mail\SendMail('backend.email.seller.atpaymentto'));
// });

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/test', function(){
    return view('eventListener');
});

Route::get('/trigger/{user_id}/{msg}/{title}', function($user_id,$msg,$title){
    event(new eventTrigger(['msg'=> $msg, 'sender_id'=> 1,'receiver_id'=> $user_id,'title'=>$title,'time_elapsed'=>'Today 1secondsago']));
});

Route::get('/send/mail/{folder}/{layout}', function($folder,$layout){
    Mail::to('Jayford@myglobalteam.com.au')->send(new SendMail("backend.email.$folder.$layout"));
    echo "Sent";
});

Route::get('/send/registration/email', function(){
    Mail::to("remaloufeleciano@mailinator.com")->send(new SendMail("backend.email.emailconfirmation",'subject',['name'=>'Kenji Lizuka','token' => url('/account/activated/stgsshirdHJFHHVJdjjsmnvjhsjasfanfma.jvkdjgkldvdkgeg.fhdjsfhjs.fsdhfjsdhfjasghsf.dsahjfgasjfvsahjfbajgjsfb.')]));
});

// Route::get('/send/mail/scott', function() {
//     // return new \App\Mail\SendMail('backend.email.seller.onceundercontract', 'test');
//     // exit();
//     $emails = [
//         [
//             "Turbohomes - You are under contract",
//             "backend.email.buyer.undercontract"
//         ],
//         [
//             "Turbohomes - 3 days left in your Inspection Period",
//             "backend.email.buyer.last3daysinspection"
//         ],
//         [
//             "Last day of the Inspection Period",
//             "backend.email.buyer.lastdayofinspection"
//         ],
//         [
//             "Turbohomes - The march to close",
//             "backend.email.buyer.dayafterinspectionperiod"
//         ],
//         [
//             "Turbohomes - 5 days left to close",
//             "backend.email.buyer.5daysbeforeclosedate"
//         ],

//         [
//             "Turbohomes - You are under contract",
//             "backend.email.seller.onceundercontract"
//         ],
//         [
//             "Turbohomes - 3 days in...Earnest Money is due",
//             "backend.email.seller.earnestmoneydue"
//         ],
//         [
//             "Turbohomes - Inspection period ends today",
//             "backend.email.seller.inspectionperiod"
//         ],
//         [
//             "Turbohomes - The day is near. It’s time to close",
//             "backend.email.seller.closing"
//         ],
//         [
//             "Turbohomes - Checking in",
//             "backend.email.seller.checkingin"
//         ]
//     ];

//     foreach ($emails as $key => $value) {
//         Mail::to("turbobuyer123@gmail.com")->send(new SendMail($value[1], $value[0]));
//     }

//     return 'success';
// });

Route::get('/set','TestController@set');
Route::get('/get/{jwt}','TestController@get');

Route::get('/show/email/{folder}/{layout}', function($folder,$layout){
    return view('backend/email/'.$folder."/".$layout);
});

Route::get('/show/forgot/password', function(){
    return view('backend/email/forgotpassword/forgotpassword',[ 'data' => [ 'name' => "Name", 'token' => "Token"]]);
});


Route::get('/show/email/confirmation', function(){
    return view('backend/email/emailconfirmation',[ 'data' => [ 'name' => "Name", 'token' => "Token"]]);
});

Route::get('/show/new/email', function(){
    return view('backend/email/newcostumernotification',[ 'data' => [ 'role' => "Buyer", 'email' => 'sample@mailinator.com', 'name' => "Sample Name", 'mobileno' => "(1213) 212 212"]]);
});


Route::get('/send/new/email', function(){
    Mail::to("kenjilizuka14@gmail.com")->send(new SendMail("backend.email.newcostumernotification",'New Customer Registered', [ 'role' => "Buyer", 'email' => 'sample@mailinator.com', 'name' => "Sample Name", 'mobileno' => "(1213) 212 212"]));
});

Route::get('/show/zillow','TestController@getZillow');

Route::get('/testIndex', 'TestController@index');

Route::get('/blog', function(){
    return view('backend/blog/blogcreate');
});

Route::post('/send/mail','be\EmailStatusController@sendMail');


// Make An Offer
