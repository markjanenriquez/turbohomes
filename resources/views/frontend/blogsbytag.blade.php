@extends('frontend.layout.app')

@section('title', 'Blogs')

@section('page-js')
	<script src="/ng-fe/controllers/blogsbytag.js" type="text/javascript"></script>
	<script src="/ng-fe/factory/blog.js" type="text/javascript"></script>
@endsection

@section('content')
	<div class="content-container main-page" id="blogs" ng-controller="blogsByTagCtrl">
	    <div class="row">
			<div class="col-md-12 no-pad-left">
				<div class="col-md-7 col-md-offset-1 no-pad-left m-top-20">
		    		<ul class="breadcrumb">
	    				<li><a href="/">Home</a></li>
	    				<li><a href="/blogs">Blogs</a></li>
	   					<li class="active" ng-bind="tag.name"></li>
					</ul>
	    		</div>
				<div class="col-md-7 col-md-offset-1 blogList" ng-repeat="blog in blogList" >
					<div class="card">
	                    <!-- <img class="card-img-top" ng-src="/img/{[{ blog.featured_image }]}"> -->
	                    <div class="blog-cover" style="background-image: url('/img/{[{ blog.featured_image }]}')"></div>
	                    <div class="card-block">
	                        <h1 class="card-title mt-3" ng-bind="blog.title"></h1>
	                        <div class="meta">
	                            <small><i class="fa fa-clock-o fa-fw"></i> Last updated <span am-time-ago="blog.updated_at"></span></small>
	                        </div>
	                         <div class="card-text" ng-bind-html="blog.content | limitHtml : 400">
                             </div>
	                    </div>
	                    <div class="card-footer clearfix">
	                    	<small><i class="fa fa-calendar fa-fw"></i> <span ng-bind="blog.created_at |
                          amDateFormat:'ddd, MMM. Do YYYY'"></span></small>
	                        <button class="btn btn-primary pull-right" ng-click="readMore(blog.slug,blog.id)">Read More <i class="fa fa-angle-right" aria-hidden="true"></i></button>
	                    </div>
	                </div>
			    </div>
			</div>
			<div class="col-md-7 col-md-offset-1 no-pad-left">
				<div class="text-center">
					<ul uib-pagination total-items="totalItems" items-per-page="5" ng-model="currentPage" max-size="maxSize" class="pagination-md" boundary-link-numbers="true" ng-change="setPage()"></ul>
				</div>
			</div>
		</div>
	</div>
@endsection