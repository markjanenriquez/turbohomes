@extends('frontend.layout.app')

@section('title', 'Provider Market')

@section('page-js')
	<script src="/ng-fe/controllers/providerMarketCtrl.js" type="text/javascript"></script>
@endsection

@section('content')
	<div class="container content-container" ng-controller="providerMarketCtrl">
		<div class="provider-market">
			<div class="row">
				<div class="col-sm-12">
					<aside>
						<div class="p-20 search-lockup">
							<form role="search" id="searchform" name="searchform" class="searchform">
								<!-- <input type="hidden" name="customize_messenger_channel" value="preview-1">
								<input type="hidden" name="customize_changeset_uuid" value="7caa3352-8b58-4f8b-a6dd-b5f771a789a5"> -->
								<div class="p-relative f-20">
									<input value="" name="search" id="search" class="search" type="text" placeholder="Search" ng-model="searchText">
									<select ng-model="filterCategory" style="position: absolute;top: 0;right: 42px;height: 42px;background-color: transparent;border: 1px solid #e5e5e5;">
										<option></option>
										<option>Inspector</option>
										<option>Title Company</option>
										<option>Homeowner Insurance</option>
										<option>Homeowner Warranty</option>
										<option>Lender</option>
										<option>Appraisals</option>
										<option>Title & Escrow</option>
									</select>
									<button class="search-toggle" type="submit"></button>
								</div>
							</form>
						</div>
					</aside>
				</div>

				<div class="col-sm-12">
					<div class="market-lockup f-bold" ng-repeat="market in markets | filter: filter">
						<div class="market-img">
							<a href="#"><img ng-src="{[{market.img}]}" alt="" ></a>
						</div>
						<div class="market-name">
							<a href="#"><span class="p-color" ng-bind="market.name"></span></a>
						</div>
						<div class="market-desc">
							<span ng-bind="market.desc"></span>
						</div>
						<div class="market-link">
							<a href="{[{ market.link }]}" class="p-color" target="_blank">Learn More</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
@endsection