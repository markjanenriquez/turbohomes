@extends('frontend.layout.app')

@section('title', 'How to Buy with Turbo Homes') 

@section('page-css')
    <link href="/libs/houzez-child/lib/assets/stylesheets/vendors64e9.css?v=1-0-6400-31987" rel="stylesheet" />
    <link href="/libs/houzez-child/lib/assets/stylesheets/app64e9.css?v=1-0-6400-31987" rel="stylesheet" />
    <link href="/libs/houzez-child/static-style.css" rel="stylesheet" /> 
@section('pagecss')
    
@section('page-js')
    <script type="text/javascript" src="/libs/houzez-child/lib/assets/javascripts/vendor/foundation.min.js"></script>

    <script>
        $(document).ready(function () {
            console.log("log")
            $(document).foundation();

            function nFormatter(num) {
                if (num >= 1000000) {
                    return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                }
                if (num >= 1000) {
                    return (num / 1000).toFixed(0).replace(/\.0$/, '') + 'K';
                }
                return num;
            }
            
            function addCommas(num) {
                num += '';
                x = num.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
            }
            
            var getSavings = function (principle, rate, term) {
                var M; //monthly mortgage payment
                var P = principle * .03; //principle agent fee / initial amount borrowed for agent fee
                var I = rate / 100 / 12; //monthly interest rate
                var N = term * 12; //number of payments months
                
                //monthly mortgage payment
                M = P * I * (Math.pow(1 + I, N)) / (Math.pow(1 + I, N) - 1);
                var totalSavings = M * N;
                return totalSavings;
            };
            
            $('#moneySlider').on('moved.zf.slider', function () {
                var dollarAmount = Math.round($('#moneySliderHandle').attr('aria-valuenow'));
                // var savingsAmount = Math.round(getSavings(dollarAmount, 4.05, 30));
                var agentFee = Math.round(dollarAmount/100);
                var maxRebate = 10000;
                var buyerAgentFee = (agentFee <= maxRebate)? agentFee : maxRebate; 
                console.log(buyerAgentFee,"buyerAgentFee")
                var commissionInterest = buyerAgentFee * 0.04 * 30;
                var savingsAmount = Math.round(buyerAgentFee + commissionInterest);
                // var agentFee = Math.round(dollarAmount * .04 * 30);
                // var commissionInterest = savingsAmount - agentFee;
                $('#sliderValue').text(nFormatter(dollarAmount));
                $('#savingsAmount').text(addCommas(savingsAmount));
                $('#buyerAgentFee').text(addCommas(buyerAgentFee));
                //$('#sellerAgentFee').text(addCommas(agentFee));
                $('#commissionInterest').text(addCommas(commissionInterest));
            });
        });
    </script>
@endsection

@section('content')
    <div id="buy-with-turbo" class="content-container main-page">
        <!--Video Section-->
        <section class="content-section video-section">
            <div class="imgbanner" style="background-image: url('img/howtobuy-bg-1.png');">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12 m-t-20 mt-115">
                            <p class="featured-header">A better way to buy a home.</p>
                        </div>
                    </div>
                    <div class="row">
                    <br>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <!-- <a class="buttonlink buttonlink-left" href="#" ng-click="scrollTo('#a-better-way');">HOW TO BUY</a> -->
                                <a class="buttonlink buttonlink-left" href="#" ng-click="scrollTo('#a-better-way');">HOW IT WORKS <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>

                             <div class="col-sm-6">
                                <a class="buttonlink buttonlink-right" href="/signup">SIGN UP <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--Video Section Ends Here-->

        <div class="row maincolor kicker-section">
            <div class="container">            
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <p class="kicker featured-header">Get paid to buy your next house.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row container-gray-light" id="home-slider">
            <div class="container">            
                <div class="col-sm-12 mpc-column">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                    @include('frontend.includes.buyer-slider')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row maincolor kicker-section">
            <div class="container">            
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <p class="kicker f-bold featured-header">Why use Turbo?</p>
                            <p class="kicker">Compare what we provide to what your typical agent provides. While they get paid 3%, we pay you. We rebate to you up to $10,000.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            
        </div>

        <main id="main" class="main featured-section" role="main">
    
            <!-- Buyer Package -->
            <div id="buyer-package" data-magellan-target="buyer-package"></div>

            <div class="row margin-top-large" id="a-better-way">
                <div class="column text-center">
                    <h4 class="featured-header">A Better Way to Buy</h4>
                    <p class="m-b-60">Turbo Homes provides you all the tools you need to buy your next home. Our technology and real people support makes it simple. Check out how we help you through the process.</p>
                </div>
            </div>

            <div class="featured-container">
                <div class="bulletcons-inline margin-bottom-medium">
                    
                    <div class="gray-bg">
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="m-b-30"><strong>Step 1:</strong> Sign up with Turbo and start searching for your next home.</p>
                            </div>
                        </div>

                        <div class="row m-b-80">
                            <div class="col-sm-4">
                                <div class="icon">
                                    <img src="img/icons/icon6.png" />
                                </div>
                                <div class="text">
                                    <h5 class="bulletcon-inline-heading">Search and Tour Homes from Any Site and Still Save </h5>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="icon">
                                    <img src="img/icons/icon2.png" />
                                </div>
                                <div class="text">
                                    <h5 class="bulletcon-inline-heading">Home Buying Guide for Every Step</h5>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="icon">
                                    <img src="img/icons/icon3.png" />
                                </div>
                                <div class="text">
                                    <h5 class="bulletcon-inline-heading">Home Value Reports</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <p class="m-b-30"><strong>Step 2:</strong> Make an offer.</p>
                        </div>
                    </div>

                    <div class="row m-b-80">
                        <div class="col-sm-4">
                            <div class="icon">
                                <img src="img/icons/icon1.png" />
                            </div>
                            <div class="text">
                                <h5 class="bulletcon-inline-heading">Offer Creation Tool</h5>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="icon">
                                <img src="img/icons/icon5.png" />
                            </div>
                            <div class="text">
                                <h5 class="bulletcon-inline-heading">Turbo Real Estate Attorneys Negotiate and Handle the Paperwork</h5>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="icon">
                                <img src="img/icons/icon3.png" />
                            </div>
                            <div class="text">
                                <h5 class="bulletcon-inline-heading">Electronic Signatures (Makes it Easy) </h5>
                            </div>
                        </div>
                    </div>
                    
                    <div class="gray-bg">
                        <div class="row">
                            <div class="col-sm-12">
                                <p class="m-b-30"><strong>Step 3:</strong> Once the offer is accepted it’s time for inspections….</p>
                            </div>
                        </div>

                        <div class="row m-b-80">
                            <div class="col-sm-6">
                                <div class="icon">
                                    <img src="img/icons/automated-contract.png" />
                                </div>
                                <div class="text">
                                    <h5 class="bulletcon-inline-heading">Automated Contract Alerts</h5>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="icon">
                                    <img src="img/icons/icon4.png" />
                                </div>
                                <div class="text">
                                    <h5 class="bulletcon-inline-heading">Trusted Service Providers</h5>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <p class="m-b-0"><strong>Step 4:</strong> Get the Keys and Move In!</p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row"> <div class="column text-center margin-bottom-large margin-top-small">           
                    <h4 class="margin-bottom-small text-gray-light have_question">Have questions? Call (602) 845-9990<span class="nowrap"></span></h4>
                    <a href="/signup" target="_parent" class="buttonlink" onclick="trackClick('Sign Up')" style="font-size: 28px;line-height: 1;padding: 14.300px 44px;display: inline-block;width: auto;height: auto">Get Started <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                </div>
            </div>
        </main>
    </div>
@endsection


