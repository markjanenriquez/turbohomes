@extends('frontend.layout.app')

@section('title', 'Dashboard') 

@section('page-js')

    <!-- <script src="components/html2canvas/build/html2canvas.min.js" type="text/javascript"></script> -->
    <script src="components/dom-to-image/dist/dom-to-image.min.js" type="text/javascript"></script>

    <script src="/ng-fe/controllers/makeoffer.js" type="text/javascript"></script>
    <script src="/ng-fe/controllers/update-listing.js" type="text/javascript"></script>
    <script src="/ng-fe/controllers/propertyvalue.js" type="text/javascript"></script>
    <script src="/ng-fe/controllers/offerreview.js" type="text/javascript"></script>
    <script src="/ng-fe/controllers/contract-dashboard.js" type="text/javascript" ></script>
    <script src="/ng-fe/controllers/dashboard.js" type="text/javascript" ></script>

    <!-- Factory -->
    <script src="/ng-fe/factory/offer.js" type="text/javascript"></script>
    <script src="/ng-fe/factory/seller.js" type="text/javascript"></script>
    <script src="/ng-fe/factory/contract-dashboard.js" type="text/javascript"></script>
    <script src="/ng-fe/factory/propertyvalue.js" type="text/javascript"></script>
    <script src="/ng-fe/factory/listing.js" type="text/javascript"></script>


    <script type="text/javascript">
        $(document).ready(function() {
            var hash = window.location.hash.replace("/","");
            hash = hash.replace("#!", "");
            hash = hash? hash : "#buyer_dashboard_menu";
            var aSelector = 'a[href="' + hash + '"]';
            console.log(hash);
            $('#sc_dashboard_menu_container ' + aSelector).tab('show');
            $('.dashboard_title').html($(aSelector).text()); 
            $(aSelector).closest("li.dropdown").addClass("active");

            jQuery('body').on('click', '.sc_get_property_report_btn', function (e) {
                e.preventDefault(); 
                jQuery(this).hide();
                sc_get_deep_comps();
            });
            
            var ajaxurl = '/'; 
            function sc_get_deep_comps() {
                jQuery('#sc_spinner_container').fadeIn();
                jQuery('#display_data').fadeOut();
                var data = jQuery('#sc_zillow_form').serialize();
                
                jQuery.post(ajaxurl, data, function(response) {
                    var parsed = jQuery.parseJSON(response);
                    jQuery('#display_data, .sc_get_property_report_btn').fadeIn();          
                    
                    if ( parsed['error'] ) {                
                        jQuery('#sc_zillow_msg_container .alert-danger').fadeIn().html(parsed['msg']);
                        jQuery('#display_data .panel-body').html('').fadeOut();
                        } else {
                        jQuery('#display_data .panel-body').html(parsed['msg']).fadeIn();
                        jQuery('#sc_zillow_msg_container .alert-danger').fadeOut();
                        
                        //map               
                        map_initialize( parsed['latitude'], parsed['longitude'], parsed['title'], parsed['comp_markers'] );     
                        
                        //gallery
                        if ( parsed['gallery'] ) {
                            show_sc_image(1);
                        }
                        
                        jQuery('.comps_valuation_low').html(parsed['low']);
                        jQuery('.comps_valuation_average').html(parsed['average']);
                        jQuery('.comps_valuation_high').html(parsed['high']);
                    }
                    
                    jQuery('#sc_spinner_container').fadeOut();
                }); 
            } 
        });
    </script>

    <script type="text/javascript">
        
        jQuery('body').on('click', '.sc_get_property_report_btn', function (e) {
            e.preventDefault(); 
            jQuery(this).hide();
            sc_get_deep_comps();
        });
        
        function sc_get_deep_comps() {
            jQuery('#sc_spinner_container').fadeIn();
            jQuery('#display_data').fadeOut();
            var data = jQuery('#sc_zillow_form').serialize();
            
            jQuery.post('http://turbohomes.com/wp-admin/admin-ajax.php', data, function(response) {
                var parsed = jQuery.parseJSON(response);
                jQuery('#display_data, .sc_get_property_report_btn').fadeIn();          
                
                if ( parsed['error'] ) {                
                    jQuery('#sc_zillow_msg_container .alert-danger').fadeIn().html(parsed['msg']);
                    jQuery('#display_data .panel-body').html('').fadeOut();
                    } else {
                    jQuery('#display_data .panel-body').html(parsed['msg']).fadeIn();
                    jQuery('#sc_zillow_msg_container .alert-danger').fadeOut();
                    
                    //map               
                    map_initialize( parsed['latitude'], parsed['longitude'], parsed['title'], parsed['comp_markers'] );     
                    
                    //gallery
                    if ( parsed['gallery'] ) {
                        show_sc_image(1);
                    }
                    
                    jQuery('.comps_valuation_low').html(parsed['low']);
                    jQuery('.comps_valuation_average').html(parsed['average']);
                    jQuery('.comps_valuation_high').html(parsed['high']);
                }
                
                jQuery('#sc_spinner_container').fadeOut();
            }); 
        }
    </script>

    <script type="text/javascript">     
        //Image gallery 
        var slideIndex = 1;
        function next_back(n) {
            show_sc_image(slideIndex += n);
        }
        
        function current_image(n) {
            show_sc_image(slideIndex = n);
        }
        
        function show_sc_image(n) {
            var i;
            var x = document.getElementsByClassName("sc_custom_slider");
            var dots = document.getElementsByClassName("sc_page_btn");
            if (n > x.length) {slideIndex = 1}    
            if (n < 1) {slideIndex = x.length}
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";  
            }
            for (i = 0; i < dots.length; i++) {
                dots[i].className = dots[i].className.replace(" sc_hightlight", "");
            }       
            $(x[slideIndex-1]).fadeIn('slow');
            dots[slideIndex-1].className += " sc_hightlight";
        }
        
        //MAP && MARKERS
        var map;        
        var marker, i;
        var global_markers = [];    
        var placeSearch, autocomplete;
        var componentForm = {
            street_number: 'short_name',
            route: 'long_name',
            locality: 'long_name',
            administrative_area_level_1: 'short_name',
            country: 'long_name',
            postal_code: 'short_name'
        };
        
        
        //Map
        function map_initialize( latitude, longitude, title, comp_markers) {
            geocoder = new google.maps.Geocoder();
            var latlng = new google.maps.LatLng( latitude, longitude);
            var map_options = {
                zoom: 15,
                center: latlng,     
                // mapTypeId: google.maps.MapTypeId.ROADMAP
                mapTypeId: 'roadmap'
            }
            map = new google.maps.Map(document.getElementById("zillow_map_canvas"), map_options);       
            
            //add marker            
            comparable_marker( comp_markers );
        }
        
        //Marker for comparables
        function comparable_marker( markers ) {
            
            for (var i = 0; i < markers.length; i++) {
                // obtain the attribues of each marker
                
                var trailhead_name = markers[i][0];         
                var lng = parseFloat(markers[i][1]);            
                var lat = parseFloat(markers[i][2]);
                var label = markers[i][3].toString();
                var icon = markers[i][4].toString();
                
                var myLatlng = new google.maps.LatLng(lat, lng);
                
                var contentString = "<div class='sc_map_marker_title'><h3 class='no-margin'>" + trailhead_name + "</h3></div>"; 
                
                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map,               
                    icon: icon,
                    label: {
                        text: label,
                        color: 'white',
                    }
                });
                
                marker['infowindow'] = contentString;
                
                global_markers[i] = marker;
                var infowindow = new google.maps.InfoWindow();
                
                google.maps.event.addListener(global_markers[i], 'click', function() {
                    infowindow.setContent(this['infowindow']);
                    infowindow.open(map, this);             
                });
            }
        }          
        
        function fillInAddress() {
            // Get the place details from the autocomplete object.
            var place = autocomplete.getPlace();
            
            // for (var component in componentForm) {
            // document.getElementById(component).value = '';
            // document.getElementById(component).disabled = false;
            // }
            
            // Get each component of the address from the place details
            // and fill the corresponding field on the form.
            for (var i = 0; i < place.address_components.length; i++) {
                var addressType = place.address_components[i].types[0];
                if (componentForm[addressType]) {
                    var val = place.address_components[i][componentForm[addressType]];
                    document.getElementById(addressType).value = val;   
                }
            }
        }
        
        // Bias the autocomplete object to the user's geographical location,
        // as supplied by the browser's 'navigator.geolocation' object.
        function geolocate() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var geolocation = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };
                    var circle = new google.maps.Circle({
                        center: geolocation,
                        radius: position.coords.accuracy
                    });
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }
    </script>

    <script type="text/javascript">
        
        $('body').on('click', '.sc_get_property_report_btn', function (e) {
            e.preventDefault(); 
            $(this).hide();
            sc_get_deep_comps();
        });
        
        function sc_get_deep_comps() {
            $('#sc_spinner_container').fadeIn();
            $('#display_data').fadeOut();
            var data = $('#sc_zillow_form').serialize();
            
            $.post(ajaxurl, data, function(response) {
                var parsed = $.parseJSON(response);
                $('#display_data, .sc_get_property_report_btn').fadeIn();           
                
                if ( parsed['error'] ) {                
                    $('#sc_zillow_msg_container .alert-danger').fadeIn().html(parsed['msg']);
                    $('#display_data .panel-body').html('').fadeOut();
                    } else {
                    $('#display_data .panel-body').html(parsed['msg']).fadeIn();
                    $('#sc_zillow_msg_container .alert-danger').fadeOut();
                    
                    //map               
                    map_initialize( parsed['latitude'], parsed['longitude'], parsed['title'], parsed['comp_markers'] );     
                    
                    //gallery
                    if ( parsed['gallery'] ) {
                        show_sc_image(1);
                    }
                    
                    $('.comps_valuation_low').html(parsed['low']);
                    $('.comps_valuation_average').html(parsed['average']);
                    $('.comps_valuation_high').html(parsed['high']);
                }
                
                $('#sc_spinner_container').fadeOut();
            }); 
        }
    </script>

    <script type="text/javascript">
        
        $(document).ready(function($) { 
           
            $('body').on('click', '.sc_profile_update_btn', function (e) {      
                e.preventDefault();
                var btn = $(this);
                btn.text('Processing');
                
                var profile_form = $('#sc_profile_form');
                var disabled = profile_form.find(':input:disabled').removeAttr('disabled');
                var data = profile_form.serialize();            
                disabled.attr('disabled','disabled');           
                
                $.post(ajaxurl, data, function(response) {
                    if ( $('#sc_profile_password').val() != '' && $('#sc_profile_password').val().length > 6 ) { 
                    
                        $.post(ajaxurl, {'action' : 'sc_logout'}, function(response) {  
                            location.reload();
                        });
                        
                    } else {                
                        var parsed = JSON.parse(response);
                        if ( parsed['error'] ) {
                            $('#sc_profile_msg_container .alert-danger').fadeIn().html(parsed['msg']).delay(3000).fadeOut();            
                        
                            
                        } else {
                            $('#sc_profile_msg_container .alert-success').fadeIn().html(parsed['msg']).delay(3000).fadeOut();           
                        }
                    }
                    btn.text('Update');
                });
            });
            
            $('body').on('click', '.view_profile', function (e) {       
                e.preventDefault();
                $('#sc_profile_modal').modal('show');
                $("#sc_profile_phone_num").mask("(999) 999-9999");  
            });
            
        
            // $('body').on('click', '.dropdown-menu li a, .property_zillow_menu', function () {
            //     var strDisplay = "block";
            //     if($(this).text().indexOf("Contract Dashboard") !== -1){
            //         strDisplay = "none";
            //     }
            //     $('.dashboard_title').html($(this).text()).css({"display" : strDisplay});
            // }); 
             
            
            $('body').on('click', '#main_seller_menu > li > a', function () {               
                seller_check_finish_step( $(this).data('id') )
            });
            
            $('body').on('click', '#main_buyer_menu > li > a', function () {                
                buyer_check_finish_step( $(this).data('id') )
            });
            
            function seller_check_finish_step( clicked_id ) {
                $('#main_seller_menu > li > a').each(function(index,val) {
                    var id = $(this).data('id');
                    
                    if ( id <= clicked_id ) {
                        $(this).addClass('sc_finish');
                        } else {
                        $(this).removeClass('sc_finish');
                    }
                    
                });
            }
            
            function buyer_check_finish_step( clicked_id ) {
                $('#main_buyer_menu > li > a').each(function(index,val) {
                    var id = $(this).data('id');
                    
                    if ( id <= clicked_id ) {
                        $(this).addClass('sc_buyer_finish');
                        } else {
                        $(this).removeClass('sc_buyer_finish');
                    }
                    
                });
            }        
            
        });
    </script>
@endsection

@section('content')
    <div class="content-container">
        <div class="row mt-40"></div>
        
        @include('frontend.dashboard.structure')
    </div>        
    
@endsection
