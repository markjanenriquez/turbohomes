<!DOCTYPE html>
<html data-ng-app="app"  lang="{{ app()->getLocale() }}">
    <head>
        <title>Turbo Homes | Landing Page</title>

        @include('frontend.includes.head')
        
        @yield('page-css')
    </head>
    <body>
    <div id="contact-us" class="content-container"> 
    <div class="row" style="background-color:#ffffff">
        <div class="container">            
            <div class="col-sm-12 col-xs-12 mpc-column">                            
                <div class="container">
                    <section class="section">
                        <div class="row">
                            <div class="col-md-6 col-xl-9 col-sm-12"> 
                             <h1>Welcome To The Revolution</h1>
                             <p>
                                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
                            </p>
                            <p>  
                                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.

                            </p>                 
                            <div class="col-sm-6">
                                <!-- <a class="buttonlink buttonlink-left" href="#" ng-click="scrollTo('#a-better-way');">HOW TO BUY</a> -->
                                <a class="buttonlink buttonlink-left" href="/">Learn More <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>

                            <div class="col-sm-6">
                                <a class="buttonlink buttonlink-right" href="/signup">Sign Up <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                        <div class="col-md-6 col-xl-9 col-sm-12">
                                <img class = "landingimg"src="/img/seller_photos_32.jpg" alt="" style="width:100%;display: block;margin: auto;transform: translateX(15px) translateY(25px);">      
                        </div>
                    </div>
                </section> 
            </div>
        </div>
    </div>
</div>
</div>
</div>  
    </body>
    <script src="/components/jquery/dist/jquery.min.js"></script>
    <script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
 </html>   