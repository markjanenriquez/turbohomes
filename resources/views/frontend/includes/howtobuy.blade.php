		<main id="main" class="main" role="main">
			
			<!-- Buyer Package -->
			<div id="buyer-package" data-magellan-target="buyer-package"></div>
			
			<div class="row margin-top-large">
				<div class="column text-center">
					<h4>A Better Way to Buy</h4>
					<p class="margin-bottom-medium">Turbo Homes provides you all the tools you need to buy your next home. Our technology and real people support makes it simple.</p>
				</div>
			</div>
			
			<div class="bulletcons-inline margin-bottom-medium">
				<ul>
					<li class="bulletcon-inline">
						<div class="bulletcon-inline-icon">
							<img src="assets/Images/icons/icon2.png" />
						</div>
						<div class="bulletcon-inline-content">
							<h5 class="bulletcon-inline-heading">Home buying guide for every step</h5>
						</div>
					</li>
					<li class="bulletcon-inline">
						<div class="bulletcon-inline-icon">
							<img src="assets/Images/icons/icon3.png" />
						</div>
						<div class="bulletcon-inline-content">
							<h5 class="bulletcon-inline-heading">Home value reports</h5>
						</div>
					</li>
					<li class="bulletcon-inline">
						<div class="bulletcon-inline-icon">
							<img src="assets/Images/icons/icon5.png" />
						</div>
						<div class="bulletcon-inline-content">
							<h5 class="bulletcon-inline-heading">Real estate attorney help with all paperwork</h5>
						</div>
					</li>
					<li class="bulletcon-inline">
						<div class="bulletcon-inline-icon">
							<img src="assets/Images/icons/File.svg" />
						</div>
						<div class="bulletcon-inline-content">
							<h5 class="bulletcon-inline-heading">Automated Contract Alerts</h5>
						</div>
					</li>
					<li class="bulletcon-inline">
						<div class="bulletcon-inline-icon">
							<img src="assets/Images/icons/icon6.png" />
						</div>
						<div class="bulletcon-inline-content">
							<h5 class="bulletcon-inline-heading">Search and buy homes from any site and still save</h5>
						</div>
					</li>
					<li class="bulletcon-inline">
						<div class="bulletcon-inline-icon">
							<img src="assets/Images/icons/icon1.png" />
						</div>
						<div class="bulletcon-inline-content">
							<h5 class="bulletcon-inline-heading">Offer creation tool</h5>
						</div>
					</li>
					<li class="bulletcon-inline">
						<div class="bulletcon-inline-icon">
							<img src="assets/Images/icons/icon4.png" />
						</div>
						<div class="bulletcon-inline-content">
							<h5 class="bulletcon-inline-heading">Trusted service providers</h5>
						</div>
					</li>
					<li class="bulletcon-inline">
						<div class="bulletcon-inline-icon">
							<img src="assets/Images/icons/icon3.png" />
						</div>
						<div class="bulletcon-inline-content">
							<h5 class="bulletcon-inline-heading">Electronic Signatures</h5>
						</div>
					</li>
				</ul>
			</div>
			
			<div class="row phone">
				<div class="column text-center margin-bottom-large margin-top-small">           
					<h4 class="margin-bottom-small text-gray-light">Have questions? Call 555-555-5555<span class="nowrap"></span></h4>
					<a href="/signup" target="_parent" class="buttonlink buttonlink-right" onclick="trackClick('Sign Up')">Get Started</a>
				</div>
			</div>
		</main>
		
		<script type="text/javascript" src="/libs/houzez-child/lib/assets/javascripts/vendor/foundation.min.js"></script>
		
		<script>
			$(function () {
				$(document).foundation();
			});
		</script>
		
		<script>
			function resizeIframe(obj) {
				obj.style.height = obj.contentWindow.document.body.scrollHeight + 'px';
			}
			function trackClick(data){
				location.href="http://thomes.dev/signup";
			}
		</script>

		<script>
			function nFormatter(num) {
				if (num >= 1000000) {
					return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
				}
				if (num >= 1000) {
					return (num / 1000).toFixed(0).replace(/\.0$/, '') + 'K';
				}
				return num;
			}
			
			function addCommas(num) {
				num += '';
				x = num.split('.');
				x1 = x[0];
				x2 = x.length > 1 ? '.' + x[1] : '';
				var rgx = /(\d+)(\d{3})/;
				while (rgx.test(x1)) {
					x1 = x1.replace(rgx, '$1' + ',' + '$2');
				}
				return x1 + x2;
			}
			
			var getSavings = function (principle, rate, term) {
				var M; //monthly mortgage payment
				var P = principle * .03; //principle agent fee / initial amount borrowed for agent fee
				var I = rate / 100 / 12; //monthly interest rate
				var N = term * 12; //number of payments months
				
				//monthly mortgage payment
				M = P * I * (Math.pow(1 + I, N)) / (Math.pow(1 + I, N) - 1);
				var totalSavings = M * N;
				return totalSavings;
			};
			
			$('#moneySlider').on('moved.zf.slider', function () {
				var dollarAmount = Math.round($('#moneySliderHandle').attr('aria-valuenow'));
				var savingsAmount = Math.round(getSavings(dollarAmount, 4.05, 30));
				var agentFee = Math.round(dollarAmount * .03);
				var commissionInterest = savingsAmount - agentFee;
				$('#sliderValue').text(nFormatter(dollarAmount));
				$('#savingsAmount').text(addCommas(savingsAmount));
				$('#buyerAgentFee').text(addCommas(agentFee));
				$('#sellerAgentFee').text(addCommas(agentFee));
				$('#commissionInterest').text(addCommas(commissionInterest));
			});
			  console.log("log1")
		</script>
		
	</body>
</html>