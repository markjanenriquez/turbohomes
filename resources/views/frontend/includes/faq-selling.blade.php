<ul class="accordion margin-bottom-collapsed" data-accordion data-multi-expand="true" data-allow-all-closed="true">
    <li class="text-center f-24 f-primary p-b-30">
        Listing My Home
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Does selling my home with Turbo Homes really save me money?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Yes. Our low fees net Turbo Homes Sellers more on their home sale than if they paid traditional agent fees of 6%. Of course, the exact savings depends on the list price of the home and whether the buyer is represented by an agent. But in all cases, using Turbo Homes costs significantly less than using a real estate agent, which saves you more. And if you are looking for something to do with all the money you save, check out our <a href="/blog" target="_blank">Turbo Homes Blog Post</a> on how you can help others with the money you save not paying commissions.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            How do I list my home with Turbo Homes?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Go to the Turbo Homes Sign Up page and enter your information to get started. While there you will enter information about your home. You will also be able to use our complimentary home valuation finder, which will help you see what your home might be worth. There is no obligation if you enter your information and don’t proceed to listing your house. If you are not interested, we won't hound you, harass you, or continually following up with you like you may have experienced with traditional agents.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Does saving my home profile obligate me to sell my home with Turbo Homes?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Nope. You'll be obligated to nothing. You will get access to a customizable personalized Turbo Homes dashboard. What you choose to do next is up to you.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Compared to industry average, how quickly do homes sell on Turbo Homes?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>On average, Turbo Homes homes sell faster than similar homes in the same price range and area. This is a direct result of Turbo Homes's proprietary marketing approach that targets buyers directly and represented buyers alike.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            What if I want to sell a home that is currently being rented out?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Not a problem. We understand that selling a house when tenants are still living there can be tricky. The key is to stick with consistent and predictable days/times to show the house. This makes life easier for your renters and decreases friction between you and your renters.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Will Turbo Homes take photos for my property?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Heck Yes. Every Turbo Homes seller is provided with an industry leading home photographer. You have the opportunity to review and approve each photo before it goes up on your home profile.
            Please note, the photographer is there to photograph your home. The photographer does not “stage” your home. You will want to make sure your home is decluttered or “staged”. If you need assistance with “staging” your home, contract one of Turbo Homes Preferred Staging Vendors.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            How do I determine my home's for sale price?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>We recommend using the complementary home value tool. Of course, while the tool provides you with a reasonable estimate of your home’s value. Read this <a href="/blog" target="_blank">Turbo Homes Blog Post</a> to learn more about how to set the right price for your home.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            When does my home profile go up on Turbo Homes.com?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>In short, whenever you're ready. Just complete the final review of your home profile and approve your professional home photos that we provide and publish your Turbo Homes listing. That's it. Your home will then go live on Turbo Homes.com. Simultaneously, we'll launch a campaign marketing your home to buyers.</p>
        </div>
    </li>

    <li class="text-center f-24 f-primary p-t-30 p-b-30">
        Marketing My Home
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            How do I manage private showings that buyers schedule?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>All this happens in the Turbo Homes dashboard you get access to when you work with us. Use the Turbo Homes electronic calendar to indicate when your home is available for showings. Buyers will request showing times, which you can accept or decline and reschedule. You (or your authorized Co-Seller) should be at your  house ahead of any buyers to meet them, show them around, and answer any questions that come up. Of course, you're welcome to host an open house too. Check out our <a href="/blogs"></a>Turbo Homes Blog Post on how to make your open house a success. </p>
        </div>
    </li>

     <li class="text-center f-24 f-primary p-t-30 p-b-30">
        Closing on My Home
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            What happens when I enter Escrow with a buyer? Who helps me?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>You will work with Turbo Homes legal team to help you prepare closing documents. During the escrow period, you will be contacted by a few more people as part of the process, including the escrow officer, but Turbo Homes is there to prepare you for the process. Our <a href="/blog" target="_blank">Turbo Homes Blog Post</a> about the escrow period will help orient you with the escrow period.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            How will I make sense of all the paperwork? 
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Turbo Homes has partnered with local attorneys to help you with paperwork.  Should you need additional support, you are also able to discuss any questions with Turbo Homes's legal team.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            What happens if a buyer is using an agent?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>You can sell your house to someone who shows up with an agent. A buyer’s agent will typically want a seller to pay them a commission. A buyer’s agent’s commission could be as much as 3% of the home’s purchase price. Keep in mind, though, paying a buyer’s agent a commission is not the law of the land. Commissions are negotiable.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            What if I don’t sell my home right away?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Our goal is for every home to sell quickly. Because the real estate market fluctuates, selling times also fluctuate.  It is possible that your home, despite best efforts, will not sell immediately. If that is the case, you may want to consider making some changes to your listing. Some ideas on how to maximize your home’s salability are in this <a href="/blog" target="_blank">Turbo Homes Blog Post</a>. If after [TIME_PERIOD] your home has not sold, you choose to extend the listing and continue to market the home, or to cancel your Turbo Homes listing. Regardless, Turbo Homes will never pressure you to go in a direction that you are not comfortable with.</p>
        </div>
    </li>
</ul>