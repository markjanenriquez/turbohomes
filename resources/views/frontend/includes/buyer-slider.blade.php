<svg style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
	<defs>
		<symbol id="icon-home" viewBox="0 0 32 32">
			<title>home</title>
			<path d="M31.776 16l-7.168-7.168v-4.48c0-0.448-0.352-0.8-0.8-0.8s-0.8 0.352-0.8 0.8v2.88l-5.76-5.76c-0.32-0.32-0.768-0.512-1.248-0.512s-0.928 0.192-1.248 0.512l-14.528 14.528c-0.32 0.32-0.32 0.832 0 1.12 0.32 0.32 0.832 0.32 1.12 0l14.528-14.528c0.064-0.064 0.16-0.064 0.224 0l14.528 14.528c0.16 0.16 0.352 0.224 0.576 0.224 0.192 0 0.416-0.064 0.576-0.224 0.288-0.32 0.288-0.8 0-1.12z"></path>
			<path d="M26.4 15.648c-0.448 0-0.8 0.352-0.8 0.8v12.736c0 0.128-0.064 0.224-0.096 0.224h-6.304v-6.56c0-0.8-0.64-1.44-1.44-1.44h-3.52c-0.8 0-1.44 0.64-1.44 1.44v6.56h-6.304c-0.032 0-0.096-0.096-0.096-0.224v-12.736c0-0.448-0.352-0.8-0.8-0.8s-0.8 0.352-0.8 0.8v12.736c0 1.024 0.768 1.824 1.696 1.824h7.104c0.448 0 0.8-0.352 0.8-0.8v-7.2h3.2v7.2c0 0.448 0.352 0.8 0.8 0.8h7.104c0.928 0 1.696-0.832 1.696-1.824v-12.736c0-0.448-0.352-0.8-0.8-0.8z"></path>
		</symbol>
		<symbol id="icon-question" viewBox="0 0 32 32">
			<title>question</title>
			<path d="M16 0c-8.832 0-16 7.168-16 16s7.168 16 16 16 16-7.168 16-16c0-8.832-7.168-16-16-16zM16 30.4c-7.936 0-14.4-6.464-14.4-14.4s6.464-14.4 14.4-14.4 14.4 6.464 14.4 14.4-6.464 14.4-14.4 14.4z"></path>
			<path d="M15.936 9.12c-1.76 0-3.040 0.704-3.808 1.568l1.376 1.472c0.576-0.672 1.312-1.088 2.208-1.088 1.056 0 1.728 0.576 1.728 1.632 0 0.896-0.48 1.408-1.248 2.112-1.216 1.12-1.728 1.632-1.728 3.392v0.896h2.080v-0.576c0-1.152 0.288-1.536 1.248-2.4 1.248-1.12 2.048-1.952 2.048-3.584 0-2.112-1.568-3.424-3.904-3.424z"></path>
			<path d="M14.24 20.384h2.528v2.496h-2.528v-2.496z"></path>
		</symbol>
	</defs>
</svg>

<main id="main" class="main" role="main">
	
	<!-- How much can you save? -->
	<div class="container-gray-light">
		<div class="row m-b-20">
			<div class="column text-center m-t-25 m-b-20">
				<h2 class="featured-header"><span style = " text-decoration: line-through;">Save</span> Make thousands with Turbo!</h2>
				<p class="margin-bottom-medium slider-p">Buying with Turbo Homes costs you nothing. We rebate to you up to $10,000 of the commission that the seller pays. Slide to the price of your home to see how much you could gain with Turbo compared to using a typical agent.</p>
				<div class="slider slider-large" id="moneySlider" data-slider data-start="100000" data-end="1500000" data-step="5000" data-initial-start="500000">
					<span class="slider-handle" id="moneySliderHandle" data-slider-handle role="slider" tabindex="1">
						<img src="img/icons/slider.svg" style="font-size: 2rem;height: 100%;padding: 0.5rem;width: 100%;overflow: hidden;" />
						<span class="slider-value">
							<sup>$</sup><span id="sliderValue"></span>
						</span>
					</span>
					<span class="slider-fill" data-slider-fill></span>
					<input type="hidden">
				</div>
			</div>
		</div>
		<div class="row margin-bottom-large display-b">
			<div class="column text-center">
				<ul class="tidbits">
					<li class="tidbit">
						<small style="font-size: 30px">Buyers</small>
						<span class="tidbit-callout">
							Save $<span id="savingsAmount"></span>
						</span>								
					</li>
				</ul>
				<div class="row">
					<div class="column text-center">
						<b>Buyer&rsquo;s Agent Rebate:</b>
						<br class="hide-for-medium">
						  <span class="strikethrough-inner">$<span id="buyerAgentFee"></span></span>
						<br>
						<b>Interest Paid on Rebate:</b>
						<br class="hide-for-medium">
						  <span class="strikethrough-inner">$<span id="commissionInterest"></span></span>
					    <br />
						<small>
							<div>
								Savings may vary. These Savings are based on the seller offering the 
							</div>
							<div>
								typical 3%  commission and the buyer getting a 4%, 30 year mortgage
							</div>
						</small>
					</div>
				</div>	
			</div>
		</div>
	</div>
</main>