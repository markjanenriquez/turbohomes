<ul class="accordion margin-bottom-collapsed" data-accordion data-multi-expand="true" data-allow-all-closed="true">
    <li class="text-center f-24 f-primary p-b-30">
        Finding a home
    </li>

    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            How do I find a Turbo Homes?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>We advertise directly on the internet to buyers just like you. Turbo Homes are on Google, Zillow, Trulia, Realtor, and other places buyers search. For folks that still like to use a real estate agent, Turbo Homes are also listed on the Multiple Listing Service (MLS). </p>
        </div>
    </li>

    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Do I need a real estate agent?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Not unless you want one. The choice to us one is entirely your own. If you aren’t working with a real estate agent, you don’t have to pay a buyer’s agent’s commission. If you are working with an agent, you are responsible for paying your agent’s commission. You can arrange with the seller to have the buyer’s agent’s commission financed as part of the homes purchase price, but it will result in a higher overall purchase price of the home. If you feel more comfortable using a real estate agent to buy a house we are here for you too. Send us a message to let us know you want to work with a real estate agent and we will have one of our realtors get in contact with you. </p>
        </div>
    </li>

    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            How do I see a Turbo Homes that I am interested in?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>To see a home in person, you’ll want to schedule a showing with the seller using Turbo Homes interactive scheduling tool. Turbo Homes scheduling tool allows you to select a date and time to view a home. Once you choose the date and time, the seller will receive your appoint request and accept it.</p>
        </div>
    </li>

    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            How do I ask a question about a Turbo Homes I am interested in?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Questions are asked directly to the seller. You can get in touch with the seller by simply clicking [INSERT WHAT THEY SHOULD CLICK] on the relevant property’s information page. </p>
        </div>
    </li>

    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Can Turbo Homes find me a home?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Um yeah…but we can’t represent you in the purchase of a home just quite yet. Soon though. Very soon. In the meantime, we can get you in touch with some good people who can help you out. </p>
        </div>
    </li>

    <li class="text-center f-24 f-primary p-b-30">
        Buying a Home
    </li>

    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Can I make an offer using Turbo Homes?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Yes, you can. Turbo Homes has made the offer process simple. We simplified the paperwork and with a few simple prompts you can quickly submit an offer. If you and the seller are on the same page, we can put the finishing touches on the contract and get on to closing.</p>
        </div>
    </li>

    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Where do I get financing for the home?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Before the seller can accept your offer, you’ll need to get financing in place. You can work with any lender you want. Some preferred lenders are listed in our Preferred Vendors section. </p>
        </div>
    </li>

    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            What happens when I make an offer?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>When you make an offer, your offer is submitted to the seller. The seller can accept your offer as is or the seller can “counter” your offer with different terms. You then can accept, reject, or counter the seller’s offer—and so on and so forth. When you and the seller agree on terms, seller and buyer will sign the purchase contract and then the inspection period and any other requirements will begin to run. Your Turbo Homes dashboard lists important dates by which things must be completed. Completing the relevant tasks by the required dates is extremely important. Failing to timely meet the dates could result in the loss of escrow fees or even cancellation of the contract. So, keep on top of tasks and their respective due dates.</p>
        </div>
    </li>

    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            How do inspections work?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Just like they always do. After your offer is accepted you will have an inspection contingency period to ensure the condition of the property is in line with your expectations. A list of preferred inspectors is on our Preferred Vendors section. You can use any inspector you want though. </p>
        </div>
    </li>

    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            What if I need to sell my home to buy my new home?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>In real estate this is known as a “contingency”. Contingencies are established when an offer is made and contingencies also help define the timeline for escrow. See our Turbo Homes Blog post discussing contingencies. And to keep track of your timelines, due dates, and upcoming action items, check your Turbo Homes dashboard.</p>
        </div>
    </li>
</ul>