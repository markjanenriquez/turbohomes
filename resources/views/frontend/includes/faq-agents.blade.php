<h4 class="text-center">Coming Soon</h4>

<!-- <ul class="accordion margin-bottom-collapsed" data-accordion data-multi-expand="true" data-allow-all-closed="true">
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            What does it mean that Homie is a brokerage?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Here are a few of the things you should know when working with a Homie customer.</p>
            <ul>
                <li>We have a listing agreement, which protects the buyer agent commission.</li>
                <li>We offer limited representation for our sellers and buyers.</li>
                <li>We charge sellers a flat fee. This includes help with negotiations, contracts, and addendums. If the Seller has chosen to list on the MLS, the commission agreement stipulated therein will apply.</li>
                <li>When working with Homie sellers: All contract documents go directly to Homie; all appointments requiring access to the home go directly to the seller, including showings, inspections, appraisal access, etc.</li>
                <li>Our buyers use automated tour scheduling software to schedule to tour the home.</li>
            </ul>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Do Homie buyers sign a buyer-broker agreement?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Before buyers can send offers using Homie software, they must sign a limited buyer-broker agreement. These buyers use our real estate attorneys to help them write up the purchase contract and any necessary addenda and counteroffers. Our software also shows our customers when and how to schedule home inspections, order home warranty plans, progress to closing and meet contract deadlines.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            How does Homie&rsquo;s Tour Scheduler work?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>If your client wants to view a Homie-listed home, you can schedule a tour through our automated tour scheduling tool. Simply click or tap the &ldquo;Request a Tour&rdquo; button on the listing page for the home you are interested in and select your preferred time to view the home. The seller will confirm the time and you will both receive a notification that the appointment is set.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            What if I receive requests to tour homes I&rsquo;ve listed from Homie buyers?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Our buyers use Homie&rsquo;s software to schedule home tours. When you receive a tour request through email or text, simply select the time that works for you and your seller or choose a different time using Homie&rsquo;s software.</p>
        </div>
    </li>
</ul> -->