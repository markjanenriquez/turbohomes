<ul class="accordion margin-bottom-collapsed" data-accordion data-multi-expand="true" data-allow-all-closed="true">
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            What is Turbo Homes?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Simplicity with a touch of technology, Turbo Homes is the revolutionary new way of buying or selling a home. Our technology allows us to automate the entire transaction for both buyers and sellers, which means that you can now say goodbye to the lengthy travels to the agent’s office and the ton of paperwork you need to. Additionally, rather than paying 6% on agent commissions, which is roughly $18,000 on a $300,000 home, you only pay a low fee with zero commision.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            How much do agent commision fees go?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>The typical agent commission fee when they sign a contract  is 6% of the home’s selling price. At most brokerages, this is further divided into 3% each for the seller agent and the buyer agent. We wanted to eliminate this practice, which is why Turbo homes is create. We simply ask for a small fee and 0% commissions, and the platform is free for buyers</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            How much does Turbo Homes cost? 
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Sellers are only required to pay a flat fee that is payable in two installments, $199 during set up, and  $699 after sale. No commissions or hidden charges</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Does Turbo Homes provide paperwork support?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Of course we do. With our licensed real estate attorneys at your service, all of the paperwork can be done in a breeze.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Does Turbo Homes handle home loans?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Not yet but we have some great providers in the provider market.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Where does Turbo Homes operate?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>At the moment Turbo Homes is available in Arizona, in the Phoenix metropolitan area. Although we plan on future expansions.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            What is the Provider Marketplace?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>The Provider Marketplace is a system created by Turbo Homes to allow you quick access to the most reliable and best service providers to successfully buy or sell your home, such as homeowner associations, insurance companies, home inspectors, appraisers, and anybody else required.</p>
        </div>
    </li>
    <li class="accordion-item" data-accordion-item>
        <a href="#" class="accordion-title">
            <svg class="icon icon-question icon-gray-light"><use xlink:href="#icon-question"></use></svg>
            Does Turbo Homes also assist in lot selling and buying?
        </a>
        <div class="accordion-content" data-tab-content>
            <p>Yes, so long as the lot in question has a legal description or tax id.</p>
        </div>
    </li>
</ul>