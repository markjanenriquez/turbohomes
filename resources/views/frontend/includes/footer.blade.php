
<footer class="footer-margin-0">
    <div class="row">
        <div class="omz"> 
            <div class="col-lg-12" style="background: #000;height: 91px;">
                <div class="col-sm-1 mpc-column">
                </div>

                <div class="col-sm-3 mpc-column">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                	<p style="color:#fff;">Turbo Homes - All Rights Reserved</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-1 mpc-column">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                	<p ><a href="/contact-us" style="color:#fff;">Contact Us</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 mpc-column">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                	<p style="color:#fff;">
                                        <a href="/termsandcondition" target="_blank" style="color:#fff;">Terms and Conditions</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-2 mpc-column">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                    <p style="color:#fff;">
                                        <a href="/privacyandpolicy" target="_blank" style="color:#fff;">Privacy Policy</a>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-2 mpc-column">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="top-bar-right social-media">
                                    <div class="top-contact">
                                        <ul>
                                            <li class="top-bar-social">
                                                <a href="#">Follow us</a>
                                                <a target="_blank" class="btn-facebook" href="https://www.facebook.com/">
                                                    <i class="fa fa-facebook-square"></i>
                                                </a>
                                                <!-- 
                                                <a target="_blank" class="btn-twitter" href="http://twitter.com/">
                                                    <i class="fa fa-twitter"></i>
                                                </a> -->
                                               <!--  
                                                <a target="_blank" class="btn-linkedin" href="http://linkedin.com/">
                                                    <i class="fa fa-linkedin"></i>
                                                </a> -->
                                               <!--
                                                <a target="_blank" class="btn-google-plus" href="http://googleplus.com/">
                                                    <i class="fa fa-google-plus"></i>
                                                </a> -->
                                                <a target="_blank" class="btn-instagram" href="http://instagram.com/">
                                                    <i class="fa fa-instagram"></i>
                                                </a>
                                            </li>                        
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-1 mpc-column">
                </div>

            </div>
        </div>
    </div>
    <!-- /.row -->
</footer>

<toaster-container toaster-options="{ 'time-out': 5000, 'position-class': 'toast-bottom-right' }"></toaster-container>

<!-- modals -->
<script type="text/ng-template" id="loginmodal.html">
	<div ng-include="'/femodal/loginmodal.html'"></div>
</script>


<!-- updated links - JEEVON -->
<script src="/js/app.js" type="text/javascript"></script>
<script src="/components/jquery/dist/jquery.min.js"></script>
<script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/components/select2/dist/js/select2.full.min.js"></script>

<!-- InputMask -->
<script src="/plugins/input-mask/jquery.inputmask.js"></script>
<script src="/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<!-- custom input mask -->
<script src="/dist/js/inputmask.js"></script>
<!-- date-range-picker -->
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script> -->
<script src="/plugins/daterangepicker/daterangepicker.js"></script>
<!-- bootstrap color picker -->
<script src="/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="/plugins/iCheck/icheck.min.js"></script>
<!-- FastClick -->
<script src="/plugins/fastclick/fastclick.min.js"></script>
<!-- ChartJS 1.0.1 -->
<script src="/plugins/chartjs/Chart.min.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<!-- <script src="/dist/js/pages/dashboard2.js"></script> -->

<script src="http://pupunzi.com/mb.components/mb.YTPlayer/demo/inc/jquery.mb.YTPlayer.js"></script>

<script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- iCheck 1.0.1 -->
<script src="/plugins/iCheck/icheck.min.js"></script>

<script src="/components/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>

<!-- Components -->
<script src="/components/angular/angular.min.js" type="text/javascript"></script>
<script src="/components/angular-jwt/dist/angular-jwt.min.js" type="text/javascript"></script>
<script src="/components/a0-angular-storage/dist/angular-storage.min.js" type="text/javascript"></script>
<script src="/components/angular-ui-router/release/angular-ui-router.min.js" type="text/javascript"></script>
<script src="/other_components/angular-ui-load/ui-load.js" type="text/javascript"></script>
<script src="/components/angular-ui-router/release/angular-ui-router.min.js" type="text/javascript"></script>

<script src="/components/angular-bootstrap/ui-bootstrap.min.js" type="text/javascript"></script>
<script src="/components/angular-bootstrap/ui-bootstrap-tpls.min.js" type="text/javascript"></script>
<script src="/components/ng-password-strength/dist/scripts/ng-password-strength.js"></script>
<script src="/components/lodash/lodash.min.js"></script>
<script src="/components/angular-file-upload/dist/angular-file-upload.min.js"></script>
<script src="/components/moment/min/moment.min.js"></script>
<script src="/components/angular-moment/angular-moment.min.js"></script>
<script src="/components/angular-animate/angular-animate.min.js"></script>
<script src="/components/angular-google-places-autocomplete/src/autocomplete.js"></script>
<script src="/components/angular-ui-mask/dist/mask.min.js"></script>

<script src="/libs/houzez-child/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="/libs/houzez-child/js/mask_input.js" type="text/javascript"></script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDU8UPQsS0i1sJTCUAVGRs6xSqJVJaA04k&libraries=places"></script>
<link rel="stylesheet" href="/components/angular-google-places-autocomplete/src/autocomplete.css">

<!-- Satellizer Js -->
<script src="/components/satellizer/dist/satellizer.min.js"></script>

<!-- Angular Web Push Notifications -->
<script type="text/javascript" src="/components/simple-web-notification/web-notification.js"></script>
<script type="text/javascript" src="/components/angular-web-notification/angular-web-notification.js"></script>

<!-- angular form ui validation -->
<script src="/dist/js/angular-ui-form-validation.js"></script>
    
<script src="/components/angular-recaptcha/release/angular-recaptcha.min.js" type="text/javascript"></script>
<script src="/components/angular.uuid2/dist/angular-uuid2.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/1.1.0/toaster.min.js"></script>

<!-- Angular app & config -->
<script src="/ng-fe/ng-app.js" type="text/javascript"></script>
<script src="/ng-fe/ng-config.js" type="text/javascript"></script>
<script src="/ng-fe/directives/directive.js" type="text/javascript"></script>
<script src="/ng-fe/filter/filter.js" type="text/javascript"></script>

<!-- Controllers -->
<script src="/ng-fe/controllers/layout.js" type="text/javascript"></script>
<script src="/ng-fe/controllers/login.js" type="text/javascript"></script>
<script src="/ng-fe/controllers/signup.js" type="text/javascript"></script>
<!-- <script src="/ng-fe/controllers/makeoffer.js" type="text/javascript"></script> -->

<!-- Factory -->
<script src="/ng-be/factory/forgotpassword.js" type="text/javascript"></script>
<script src="/ng-fe/factory/notification.js" type="text/javascript"></script>
<script src="/ng-be/factory/login.js" type="text/javascript"></script>

<script src="/ng-be/factory/general-factory.js" type="text/javascript"></script>

<script src="/components/wow/dist/wow.js" type="text/javascript"></script>

<script src="/ng-fe/controllers/providerMarketCtrl.js" type="text/javascript"></script>

<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-111184546-1"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-111184546-1');
</script> -->

<!-- inline script -->
<script type="text/javascript">

	$(document).ready(function () {
        $('#myCarousel').carousel({
            interval: 6000,
            cycle: true
        });
    
		$(".player").YTPlayer();
	});
</script>


