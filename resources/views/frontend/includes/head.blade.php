
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="csrf-token" content="{{ csrf_token() }}">

<!-- Google Tag Manager -->
<script>
	(function(w,d,s,l,i){
		w[l]=w[l]||[];
		w[l].push({
			'gtm.start': new Date().getTime(),
			 event:'gtm.js'
			});
		var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer'? '&l='+l:'';
            j.async = true;
            j.src = 'https://www.googletagmanager.com/gtm.js?id='+ i + dl;
            f.parentNode.insertBefore(j,f);
            console.log(j.src,"src", dl, l)
    })(window,document,'script','dataLayer','GTM-5SVH8PX');
</script>
<!-- End Google Tag Manager -->

<!-- updated links - JEEVON -->
<link rel="stylesheet" href="/components/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="/components/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/components/Ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="/components/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css">
<link rel="stylesheet" href="/components/angular-bootstrap/ui-bootstrap-csp.css">
<link rel="stylesheet" href="/components/wow/css/libs/animate.css">
<link rel="stylesheet" href="/components/select2/dist/css/select2.min.css">
<link rel="stylesheet" href="/components/jquery.mb.ytplayer/dist/css/jquery.mb.YTPlayer.min.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/1.1.0/toaster.min.css" rel="stylesheet" />

{{-- <link rel="stylesheet" href="/dist/css/AdminLTE.css"> --}}
<link rel="stylesheet" href="/dist/css/fecss/custom.css">
<link rel="stylesheet" href="/dist/css/fecss/gen-classes.css">
<link rel="stylesheet" href="/dist/css/fecss/makeoffer.css">
<link rel="stylesheet" href="/dist/css/full-slider.css">
<link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/css/main.css">

<style type="text/css">
	.row{max-width: : none !important};
</style>

<!--OMZ-->
<link rel="shortcut icon" href="/img/favicon-01.png">
