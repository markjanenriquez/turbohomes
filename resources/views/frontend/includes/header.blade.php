<div class="row turboheader top-bar  hide-top-bar-mobile">
    <div class="container">
        <div class="col-sm-12">
            <div class="top-bar-left  ">
                <div class="top-contact">
                    <ul class="">
                        <li class="top-bar-phone"><a href="tel:(602) 845-9990"><i class="fa fa-phone"></i> <span>(602) 845-9990</span></a></li>
                        <li class="top-bar-contact"><a href="mailto:hello@turbohomes.com"><i class="fa fa-envelope-o"></i>  <span>hello@turbohomes.com</span></a></li>
                    </ul>
                </div>

            </div>

            <div class="top-bar-right">
                <div class="top-contact">
                    <ul class="">
                        <li class="top-bar-social">
                            <a target="_blank" class="btn-facebook" href="https://www.facebook.com/Real-Turbo-Homes-312410112516817/"><i class="fa fa-facebook-square"></i></a>
                            <a target="_blank" class="btn-instagram" href="https://www.instagram.com/realturbohomes/"><i class="fa fa-instagram"></i></a>
                        </li>                        
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>
<nav class="navbar navbar-default custom navbar-fixed-top">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img alt="Brand" src="/img/logo-01.png">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" ng-if="!disableNav">
            <ul class="nav navbar-nav navbar-right">
                <!-- <li><a href="/">Home</a></li> -->
                {{-- <li class="ng-cloak" ng-if="loutctrl.isloggedin == true">
                    <a href="" class="haschild notif_wrapper" ng-click="readNotification(data.notificationList)" ng-mouseover="readNotification(data.notificationList)">
                       <i class="fa fa-globe fa-2x"></i>
                       <span class="notif_badge" ng-if="badge > 0" ng-bind="badge"></span>
                    </a>
                
                    <input type="text" style="display: none" id="inputList" ng-model="data.addedList">
                    
                    <ul id="ulNotif" class="list-group" style="box-shadow: 2px 2px 2px #888888;">
                       <div id="divNotif" style="overflow: auto; height : 250px;padding:0px 1px 0px 0px;width:100%">
                        <li style="height:auto;clear:both;border-radius : 0px !important;padding:0px;width:100%" class="list-group-item" ng-repeat="list in data.notificationList">
                            <a href="" style="clear: both;padding:12px;margin:0px;width:100%">
                            <h4 class="list-group-item-heading" ng-bind="list.title">First List Group Item Heading</h4>
                            <small ng-bind="list.time_elapsed">24 mins ago</small>
                            <p class="list-group-item-text" ng-bind="list.msg">List Group Item Text</p>
                            </a>
                        </li>
                       </div>
                       <li style="height : 30px;border: solid 1px #ddd;"></li> 
                    </ul>
                </li> --}}
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Buyer <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/how-to-buy-with-turbo-homes">How to Buy with Turbo Homes</a></li>
                        <li><a href="/dashboard#!#make_an_offer" ng-click="setFlag('buyer', $event);">Make an Offer</a></li>
                        <li><a href="/faqs#buying">Buyer FAQs</a></li>
                        <li><a href="/provider-market">Provider Market</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Seller <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <!-- <li><a href="/sell">How to Sell with Turbo Homes</a></li> -->
                        <li><a href="/sell-form" ng-click="setFlag('seller', $event);">List Your Home</a></li>
                        <li><a href="/faqs?#selling">Seller FAQs</a></li>
                        <li><a href="/provider-market">Provider Market</a></li>
                    </ul>
                </li>
                <!-- <li><a href="/blogs">Blog</a></li>
                <li><a href="/contact-us">Contact Us</a></li> -->
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/blogs">Blogs</a></li>
                        <li><a href="/contact-us">Contact Us</a></li>
                        <li><a href="/faqs?#general">FAQs</a></li>
                    </ul>
                </li>
                <li class="ng-cloak" ng-if="isloggedin == false">
                    <a href="/signup">Sign Up</a>
                </li>
                <li class="ng-cloak" ng-if="isloggedin == false">
                    <a href="/login">Login</a>
                </li>

                <li ng-if="isloggedin == true">
                    <a href="#" class="dropdown-toggle ng-cloak" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{[{fullname}]} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/dashboard">Turbo Hub</a></li>
                        <li><a href="#" ng-click="logout()">Log Out</a></li>
                    </ul>
                </li>            
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>

<nav class="navbar navbar-default mobile">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-2" aria-expanded="false" ng-if="!disableNav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">
                <img alt="Brand" src="/img/logo-01.png">
            </a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2" ng-if="!disableNav">
            <ul class="nav navbar-nav navbar-right">
                <!-- <li><a href="/">Home</a></li> -->
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Buyer <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/how-to-buy-with-turbo-homes">How to Buy with Turbo Homes</a></li>
                        <li><a href="/faqs#buying">Buyer FAQs</a></li>
                        <li><a href="/provider-market">Provider Market</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Seller <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/sell">How to Sell with Turbo Homes</a></li>
                        <li ng-if="isloggedin == true"><a href="/sell-form">List Your Home</a></li>
                        <li><a href="/faqs?#selling">Seller FAQs</a></li>
                        <li><a href="/provider-market">Provider Market</a></li>
                    </ul>
                </li>
                 <li>
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">About<span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/blogs">Blogs</a></li>
                        <li><a href="/contact-us">Contact Us</a></li>
                        <li><a href="/faqs?#general">FAQs</a></li>
                    </ul>
                </li>
                <li class="ng-cloak" ng-if="isloggedin == false">
                    <a href="/signup">Sign Up</a>
                </li>
                <li class="ng-cloak" ng-if="isloggedin == false">
                    <a href="/login">Login</a>
                </li>

                <li class="ng-cloak" ng-if="isloggedin == true">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">{[{fullname}]} <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><a href="/dashboard">Turbo Hub</a></li>
                        <li><a href="#" ng-click="logout()">Log Out</a></li>
                    </ul>
                </li>  
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>