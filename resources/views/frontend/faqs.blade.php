@extends('frontend.layout.app')

@section('title', 'Frequently Asked Questions') 

@section('page-css')
    <link href="/libs/houzez-child/lib/assets/stylesheets/vendors64e9.css?v=1-0-6400-31987" rel="stylesheet" />
    <link href="/libs/houzez-child/lib/assets/stylesheets/app64e9.css?v=1-0-6400-31987" rel="stylesheet" />
@endsection 

@section('page-js')
    <script type="text/javascript" src="/libs/houzez-child/lib/assets/javascripts/vendor/foundation.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $(document).foundation();

            var url = window.location.href;
            var iframe = $('iframe#myIframe');

            if(url.indexOf('#buying') > 0) {
                $('.faq-buying', iframe.contents()).parent().click();
            } else if(url.indexOf('#selling') > 0) {
                $('.faq-selling', iframe.contents()).parent().click();
            } else {
                $('.faq-general', iframe.contents()).parent().click();
            }

            //fix for lack of deep linking in foundation tabs
            function tabDeepLink(selector) {
                $(selector).each(function () {
                    var $tabs = $(this);

                    // match page load anchor
                    var anchor = window.location.hash;
                    anchor = anchor.replace("#!", "");

                    if (anchor.length && $tabs.find('[href="' + anchor + '"]').length) {
                        $tabs.foundation('selectTab', $(anchor));
                        // roll up a little to show the header
                        var offset = $tabs.offset();
                        $(document).ready(function () {
                            $('html, body').animate({ scrollTop: 0 }, 300);
                        });
                    }

                    // append the hash on click
                    $tabs.on('change.zf.tabs', function () {
                        var anchor = $tabs.find('.tabs-title.is-active a').attr('href');
                        history.pushState({}, "", anchor);
                    });
                });
            }

            tabDeepLink('.tabs');

            $('.tab-btn').click(function() {
                $(document).ready(function () {
                    $('html, body').animate({ scrollTop: 0 }, 300);
                });
            });

        })

    </script>

@endsection

@section('content')
    
    <svg style="position: absolute; width: 0; height: 0; overflow: hidden;" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
        <defs>
            <symbol id="icon-question" viewBox="0 0 32 32">
            <title>question</title>
            <path d="M16 0c-8.832 0-16 7.168-16 16s7.168 16 16 16 16-7.168 16-16c0-8.832-7.168-16-16-16zM16 30.4c-7.936 0-14.4-6.464-14.4-14.4s6.464-14.4 14.4-14.4 14.4 6.464 14.4 14.4-6.464 14.4-14.4 14.4z"></path>
            <path d="M15.936 9.12c-1.76 0-3.040 0.704-3.808 1.568l1.376 1.472c0.576-0.672 1.312-1.088 2.208-1.088 1.056 0 1.728 0.576 1.728 1.632 0 0.896-0.48 1.408-1.248 2.112-1.216 1.12-1.728 1.632-1.728 3.392v0.896h2.080v-0.576c0-1.152 0.288-1.536 1.248-2.4 1.248-1.12 2.048-1.952 2.048-3.584 0-2.112-1.568-3.424-3.904-3.424z"></path>
            <path d="M14.24 20.384h2.528v2.496h-2.528v-2.496z"></path>
            </symbol>
        </defs>
    </svg>
    <div class="content-container">
        <main id="main" class="main" role="main">
            <div class="row" id="faq-row">
                <div class="column heading-group margin-bottom-small">
                    <h1 class="heading-group-title">Frequently Asked Question</h1>
                    <ul class="tabs" data-tabs id="faq-tabs" data-deep-link="true" data-update-history="true">
                        <li class="tabs-title">
                            <a href="#general" class="faq-general tab-btn">
                                General
                            </a>
                        </li>
                        <li class="tabs-title">
                            <a href="#buying" class="faq-buying tab-btn">
                                Buying
                            </a>
                        </li>
                        <li class="tabs-title">
                            <a href="#selling" class="faq-selling tab-btn">
                                Selling
                            </a>
                        </li>
                        <li class="tabs-title">
                            <a href="#agents" class="tab-btn">
                                Agents
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="container">
                <div class="row margin-bottom-large">
                    <div class="column">
                        <div class="tabs-content" data-tabs-content="faq-tabs">

                            <!-- General -->
                            <div class="tabs-panel" id="general">
                                @include('frontend.includes.faq-general')
                            </div>

                            <!-- Buying -->
                            <div class="tabs-panel" id="buying">
                                @include('frontend.includes.faq-buying')
                            </div>

                            <!-- Selling -->
                            <div class="tabs-panel" id="selling">
                                @include('frontend.includes.faq-selling')
                            </div>

                            <!-- Agents -->
                            <div class="tabs-panel" id="agents">
                                @include('frontend.includes.faq-agents')
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </main>
    </div>
@endsection
