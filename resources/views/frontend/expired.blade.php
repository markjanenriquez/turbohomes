<!DOCTYPE html>
<html data-ng-app="app">
<head>
	<title>Turbo Homes | Expired Token</title>
    @include('frontend.includes.head')
    
</head>
<body class="layout-top-nav" ng-controller="layoutCtrl as loutctrl">
	<div class="wrapper">
        @include('frontend.includes.header')

            <div class="row min-height-700 mt-20">

                <div class="col-md-12">
                    <div class="signupsuccess">
                        <h3>Error!</h3>
                        <p>Your Turbo Homes Activation token is expired or invalid!.</p>
                    </div>
                </div>
            </div>

        @include('frontend.includes.footer')

    </div>
</body>
</html>
