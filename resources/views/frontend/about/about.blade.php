<!DOCTYPE html>
<html data-ng-app="app">
<head>
	<title>Turbo Homes | About Us</title>
    @include('frontend.includes.head')
    
</head>
<body class="layout-top-nav" ng-controller="layoutCtrl as loutctrl">
	<div class="wrapper">
    @include('frontend.includes.header')

	<!--  Content -->

	<div class="row" style="background-color:#ffffff">
        <div class="container" style="padding-top: 50px !important;padding-bottom: 50px !important;">            
            <div class="col-sm-12 mpc-column">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <h1 style="text-align:center; font-size:60px; font-weight:700; color:#4E4E4E;">Who is Turbo Homes?</h1>
                                <br>
                                <p style="text-align:center; font-size:18px;">
                                	Simplicity backed with technology. Turbo Homes gives you the latest technology in real state, the most innovative software available, and the most professional customer service, all in an effort to give you, our valued customers,  ease in buying and selling a home.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-12 mpc-column">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <div class="container">
	                                <div class="img2">
	                                    <img src="img/sell-slider-bg-1024x683.png">
	                                </div>
	                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

	<!--  Content -->
    @include('frontend.includes.footer')
    </div>
</body>
</html>