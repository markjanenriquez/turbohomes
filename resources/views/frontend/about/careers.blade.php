<!DOCTYPE html>
<html data-ng-app="app">
<head>
	<title>Turbo Homes | About Us</title>
    @include('frontend.includes.head')
</head>
<body class="layout-top-nav" ng-controller="layoutCtrl as loutctrl">
	<div class="wrapper">
    @include('frontend.includes.header')
	<!--  Content -->
	
	<div class="row" style="background-color:#ffffff">
        <div class="container" style="padding-top: !important;padding-bottom: 40px !important;">            
            <div class="col-sm-12 mpc-column">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <h1 style="text-align:center; font-size:60px; font-weight:700; color:#4E4E4E;">Careers</h1>
                                <br>
                                <p style="font-size:22px;color:#4E4E4E">
                                	<strong>Are you someone who sees the big picture?</strong>
                                </p>
                                <p style="font-size:19px;color:#4E4E4E">
                                	To transform and simplify the whole real estate process is what <b><i>Turbo Homes</i></b> strives to achieve. With all the technology and assets that we have, the only thing needed is people who sees the big picture, problem solvers, and creative people to make this revolution possible.
                                </p>
                                &nbsp;
                                <p style="font-size:22px;color:#4E4E4E">
                                	<strong>Are you a People Person?</strong>
                                </p>
                                <p style="font-size:19px;color:#4E4E4E">
                                	All work no play makes Jack a dull boy. Hardworking as we are, we are also open to a good laugh. A Healthy office is a smiling office, and if an office is healthy, business is healthy. We want a team player who can work with a smile, and we believe that such people will make this company both incredible and fun.
                                </p>
                                &nbsp;
                                <p style="font-size:22px;color:#4E4E4E">
                                	<strong>Are you Passionate?</strong>
                                </p>
                                <p style="font-size:19px;color:#4E4E4E">
                                	We want people who sees their career with us as more than just a source of income, but a chance to change the world one step at a time. We aim to tackle challenges to achieve our goal, and only the most passionate people can keep up with the energy and creativity needed.
                                </p>

	                             <img src="img/the-place-to-start-your-real-estate-career.jpg" height="100%" width="100%">
                           		
                           		<p style="font-size:27px;color:#4E4E4E; text-align:center;">
                                	<br>
                                	<strong>OPEN POSITIONS</strong>
                                </p>
	                             &nbsp;
                                <p style="font-size:25px;color:#4E4E4E">
                                	<strong>Sales Representative</strong>
                                </p>
                                <p style="font-size:19px;color:#4E4E4E">
                                	We’re looking for a results-driven sales representative to actively seek out and engage customer prospects. You will provide complete and appropriate solutions for every customer in order to boost top-line revenue growth, customer acquisition levels and profitability.
                                </p>
                                &nbsp;
                                <p style="font-size:20px;color:#4E4E4E">
                                	<strong>Responsibilities</strong>
                                </p>

                                <ul style="font-size:19px;color:#4E4E4E">
								    <li>Present, promote and sell products/services using solid arguments to existing and prospective customers</li>
									<li>Perform cost-­benefit and needs analysis of existing/potential customers to meet their needs</li>
									<li>Establish, develop and maintain positive business and customer relationships</li>
									<li>Reach out to customer leads through cold calling</li>
									<li>Expedite the resolution of customer problems and complaints to maximize satisfaction</li>
									<li>Achieve agreed upon sales targets and outcomes within schedule</li>
									<li>Coordinate sales effort with team members and other departments</li>
									<li>Analyze the territory/market’s potential, track sales and status reports</li>
									<li>Supply management with reports on customer needs, problems, interests, competitive activities, and potential for new products and services.</li>
									<li>Keep abreast of best practices and promotional trends</li>
									<li>Continuously improve through feedback</li>
								</ul>

								<p style="font-size:20px;color:#4E4E4E">
                                	<strong>Requirements</strong>
                                </p>

                                <ul style="font-size:19px;color:#4E4E4E">
								    <li>Proven work experience as a sales representative</li>
									<li>Excellent knowledge of MS Office</li>
									<li>Familiarity with BRM and CRM practices along with ability to build productive business professional relationships</li>
									<li>Highly motivated and target driven with a proven track record in sales</li>
									<li>Excellent selling, communication and negotiation skills</li>
									<li>Prioritizing, time management and organizational skills</li>
									<li>Ability to create and deliver presentations tailored to the audience needs</li>
									<li>Relationship management skills and openness to feedback</li>
									<li>BS/BA degree or equivalent</li>
								</ul>

								<p style="font-size:20px;color:#4E4E4E">
                                	<br>
                                	Interested applicants please send your updated resume to <b style="color:#FF7900;">recruitment@turbohomes.com</b>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>


	<!--  Content -->
    @include('frontend.includes.footer')
    </div>
</body>
</html>