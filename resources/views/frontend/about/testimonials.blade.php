<!DOCTYPE html>
<html data-ng-app="app">
<head>
	<title>Turbo Homes | About Us</title>
    @include('frontend.includes.head')
</head>
<body class="layout-top-nav" ng-controller="layoutCtrl as loutctrl">
	<div class="wrapper">
    @include('frontend.includes.header')
	<!--  Content -->


<div class="row" style="background-color:#ffffff">
        <div class="container" style="padding-top: 50px !important;padding-bottom: 50px !important;">  
	
   			 <div class="col-sm-12 mpc-column">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <div class="container">
<section class="section team-section text-center pb-3">

    <!--Section heading-->
    <h1 style="text-align:center; font-size:60px; font-weight:700; color:#4E4E4E;">Testimonials</h1>
    <hr>
    <!--Grid row-->
    <div class="row text-center">

        <!--Grid column-->
        <div class="col-md-6 mb-r">
            
            <div class="testimonial">
                <!--Avatar-->
                <div class="avatar">
                    <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(1).jpg" class="roundedc6ircle z-depth-1 img-fluid">
                </div>
                
                <!--Content-->
                <h4 class="mb-3">Catherine Dally</h4>
                <h6 class="mb-3 font-bold grey-text">Seller</h6>
                <p><i class="fa fa-quote-left"></i> I would have given up on selling my home if not for Turbo Homes.</p>
            </div>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-6 mb-r">
            <div class="testimonial">
                <!--Avatar-->
                <div class="avatar">
                    <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(8).jpg" class="rounded-circle z-depth-1 img-fluid">
                </div>

                <!--Content-->
                <h4 class="mb-3">Matthew Bonney</h4>
                <h6 class="mb-3 font-bold grey-text">Buyer</h6>
                <p><i class="fa fa-quote-left"></i> My experience with Turbo Homes definitely exceeded all expectations.</p>
            </div>
        </div>
        <!--Grid column--> 
    </div>
    <!--Grid row-->
    <hr>
</section>
	                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>     



            <div class="col-sm-12 mpc-column">
                <div class="vc_column-inner ">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element ">
                            <div class="wpb_wrapper">
                                <div class="container">
<section class="section team-section text-center pb-3">
    <!--Grid row-->
    <div class="row text-center">

         <!--Grid column-->
        <div class="col-md-6 mb-r">
            <div class="testimonial">
                <!--Avatar-->
                <div class="avatar">
                    <img src="https://mdbootstrap.com/img/Photos/Avatars/img%20(10).jpg" class="rounded-circle z-depth-1 img-fluid">
                </div>
                <!--Content-->
                <h4 class="mb-3">Martha Wallace</h4>
                <h6 class="mb-3 font-bold grey-text">Buyer</h6>
                <p><i class="fa fa-quote-left"></i> My family tends to move a lot because of my work. Everything was a hassle before I found Turbo Homes. Now, I look forward to my next home.</p>
            </div>
        </div>
        <!--Grid column-->

        <!--Grid column-->
        <div class="col-md-6 mb-r">
            <div class="testimonial">
                <!--Avatar-->
                <div class="avatar">
                    <img src="img/defaultprofilepicture.png" class="rounded-circle z-depth-1 img-fluid">
                </div>

                <!--Content-->
                <h4 class="mb-3">Unknown</h4>
                <h6 class="mb-3 font-bold grey-text">Seller</h6>
                <p><i class="fa fa-quote-left"></i> One reason why I was hesitant to sell my house was because of the agent fees. I feel cheated. That is why I chose Turbo Homes, with their commission-free policy.</p>
            </div>
        </div>
        <!--Grid column--> 
    </div>
    <!--Grid row-->
<hr>
</section>
	                            </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>      
</div>
</div>


	<!--  Content -->
    @include('frontend.includes.footer')
    </div>
</body>
</html>