@extends('frontend.layout.app')

@section('title', 'How to Sell with Turbo Homes') 

@section('page-js')

    <script type="text/javascript" src="https://jstest.authorize.net/v1/Accept.js" charset="utf-8"></script>

    <script src="/ng-fe/controllers/aboutyourhome.js" type="text/javascript"></script>
    <script src="/ng-fe/factory/abouthome.js" type="text/javascript"></script>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    document.getElementById('buyernoagentpic').style.backgroundImage="url("+ e.target.result +")";
                    // $('#profilepic')
                    //     // .attr('background-image', e.target.result)

                    //     .width(150)
                    //     .height(150);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }


        function readURL2(input) {
            if (input.files && input.files[0]) {
                var reader2 = new FileReader();

                reader2.onload = function (e) {
                    document.getElementById('buyerwithagentpic').style.backgroundImage="url("+ e.target.result +")";
                    // $('#profilepic')
                    //     // .attr('background-image', e.target.result)

                    //     .width(150)
                    //     .height(150);
                };

                reader2.readAsDataURL(input.files[0]);
            }
        }

        function validateQty(event) {
            var key = window.event ? event.keyCode : event.which;
            if (event.keyCode == 8 || event.keyCode == 46
               || event.keyCode == 37 || event.keyCode == 39) {
                return true;
            }
            else if ( key < 48 || key > 57 ) {
                return false;
            }
            else return true;
        };
    </script>
@endsection

@section('content')
    <div id="list-your-home" class="content-container ng-cloak">
        <div class="row" ng-controller="aboutyourhomeCtrl as abyhctrl">
            <div class="row bg-white">
                <div class="container mb-40 mt-40">

                    <h4 class="t-center mb-40 font-weight700">Tell us about your home. We use this info to market it!</h4>

                    <div class="stepwizard col-xs-12 col-md-8 col-lg-8 col-xs-offset-0 col-md-offset-2 col-lg-offset-2">
                        <div class="stepwizard-row setup-panel">
                            <div class="stepwizard-step">
                                <a href="#step-1" type="button" class="btn btn-default btn-circle" ng-class="{'bgorange': abyhctrl.form.data.step1 == true}" ng-disabled="abyhctrl.form.data.step1 == false">1</a>
                                <p>Step 1</p>
                            </div>
                            <div class="stepwizard-step">
                                <a href="#step-2" type="button" class="btn btn-default btn-circle" ng-class="{'bgorange': abyhctrl.form.data.step2 == true}" ng-disabled="abyhctrl.form.data.step2 == false">2</a>
                                <p>Step 2</p>
                            </div>
                            <div class="stepwizard-step">
                                <a href="#step-3" type="button" class="btn btn-default btn-circle" ng-class="{'bgorange': abyhctrl.form.data.step3 == true}" ng-disabled="abyhctrl.form.data.step3 == false">3</a>
                                <p>Step 3</p>
                            </div>
                            <div class="stepwizard-step">
                                <a href="#step-4" type="button" class="btn btn-default btn-circle" ng-class="{'bgorange': abyhctrl.form.data.step4 == true}" ng-disabled="abyhctrl.form.data.step4 == false">4</a>
                                <p>Step 4</p>
                            </div>
                            <div class="stepwizard-step">
                                <a href="#step-5" type="button" class="btn btn-default btn-circle" ng-class="{'bgorange': abyhctrl.form.data.step5 == true}" ng-disabled="abyhctrl.form.data.step5 == false">5</a>
                                <p>Step 5</p>
                            </div>
                        </div>
                    </div>

                    <div class="row setup-content" id="step-1" ng-if="abyhctrl.form.data.step1">

                        <div class="col-xs-12 col-md-8 col-md-offset-2">

                            <div class="col-md-12">

                                <h3> Step 1 </h3>
                                <br>


                                <div class="col-md-12 mb-20">
                                    <label for="homeaddress">Your home address</label>
                                    <div class="input-group">
                                    
                                        <span class="input-group-addon" id="homeaddress"><i class="fa fa-home" aria-hidden="true"></i></span>
                                        <input type="text" g-places-autocomplete placeholder="Your Home Address" ng-model="abyhctrl.form.data.homeaddress" class="form-control" aria-describedby="homeaddress"/>
                                    </div>
                                    <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.homeaddress">Home Address is required.</small>
                                </div>

                                
                                <div class="col-md-12 mb-20">
                                    <label for="features">Features You Want to Highlight</label>
                                    <p>This will help us write the description of your home in the listing and other marketing materials.</p>
                                    <textarea class="form-control" id="features" rows="5" ng-model="abyhctrl.form.data.features"></textarea>
                                </div>


                                <div class="col-md-12 mb-20">
                                    <div class="form-check abc-checkbox abc-checkbox-warning">
                                        <input class="form-check-input" id="mls" type="checkbox" ng-model="abyhctrl.form.data.mls" ng-true-value="1" ng-false-value="0">
                                        <label class="form-check-label" for="mls" style="display: block !important;"> &nbsp; List my home on the MLS </label>
                                    </div>

                                    <p>Listing your home on the MLS is included in our price. It is provided by one of our valued partners. Some Turbo clients choose not to list their home on the MLS right away to try to increase their chances to sell to a buyer that doesn’t have an agent so they can avoid paying the buyer’s agent a big commission. Either way we will list and market your property on all the other major real estate sites. However, listing on the MLS does still increase visibility to your property and we recommend it.</p>
                                </div>


                                <div class="col-md-12 mb-20 ng-cloak" ng-if="abyhctrl.form.data.mls == 1">

                                    <label for="features">Percent of sales price you will pay to the buyer’s agent (typically 3%)</label>
                                    
                                    <div class="col-md-12" ng-init="abyhctrl.form.data.percentpay = 3">
                                        <div class="form-check abc-radio abc-radio-warning">
                                            <input class="form-check-input" id="percentpay2" type="radio" name="radio1" value="2" ng-model="abyhctrl.form.data.percentpay">
                                            <label class="form-check-label" for="percentpay2" style="display: block !important;"> &nbsp; 2% </label>
                                        </div>

                                        <div class="form-check abc-radio abc-radio-warning">
                                            <input class="form-check-input" id="percentpay25" type="radio" name="radio1" value="2.5" ng-model="abyhctrl.form.data.percentpay">
                                            <label class="form-check-label" for="percentpay25" style="display: block !important;"> &nbsp; 2.5% </label>
                                        </div>

                                        <div class="form-check abc-radio abc-radio-warning">
                                            <input class="form-check-input" id="percentpay3" type="radio" name="radio1" value="3" ng-model="abyhctrl.form.data.percentpay">
                                            <label class="form-check-label" for="percentpay3" style="display: block !important;"> &nbsp; 3% </label>
                                        </div>

                                        <div class="form-check abc-radio abc-radio-warning">
                                            <input class="form-check-input" id="percentpay" type="radio" name="radio1" value="other" ng-model="abyhctrl.form.data.percentpay">
                                            <label class="form-check-label" for="percentpay" style="display: block !important;"> &nbsp; Other <small>Enter a percent or a dollar amount</small> </label>
                                        </div>

                                        <div class="input-group" style="margin-left: -15px;width: 40%" ng-if="abyhctrl.form.data.percentpay == 'other'">
                                            <input type="text" placeholder="% of Price or Fixed $ Amount " ng-model="abyhctrl.form.data.percentpayother" class="form-control" aria-describedby="otherpercent"/>
                                        </div>
                                    </div>
                                </div>

                                

                                <div class="col-md-12">
                                    <button class="btn btn-primary nextBtn btn-lg pull-right bgorange" type="button" ng-click="abyhctrl.form.gotostep2()">Next</button>
                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="row setup-content ng-cloak" id="step-2" ng-if="abyhctrl.form.data.step2">

                        <div class="col-xs-12 col-md-8 col-md-offset-2">

                            <div class="col-md-12 agent-profile">
                                <h3> Step 2</h3>

                                <h4>Let’s set the price.</h4>
                               
                                <div class="row mb-20 mt-20">
                                    

                                    <div class="col-lg-6 col-md-12 p-20">

                                        
                                        <!-- <img src="img/defaultprofilepicture.png" alt="" width="100%" height="100%"> -->

                                        <div class="profilecenter">
                                            <div class="profilepiccontainer">
                                                <div class="profilepic" id="buyernoagentpic"></div>​
                                            </div>
                                        </div>

                                        <h5 class="t-center">Buyer with No Agent</h5>

                                        <div class="input-group">
                                            <span class="input-group-addon" id="buyernoagent"><i class="fa fa-dollar" aria-hidden="true"></i></span>
                                            <input type="text" placeholder="Price" ng-model="abyhctrl.form.data.buyernoagent" class="form-control" aria-describedby="buyernoagent" ng-keyup="abyhctrl.form.buyernoagentchange($event)" mask-money no-prefix with-decimal/>
                                        </div>
                                        <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.buyernoagent">Buyer with no agent price is required!</small>
                                        <p style="text-align: center; margin-top: 10px!important;"><small>In some of our marketing we can advertise a lower price for those that don’t have an agent. What do you want that price to be? It should be lower than the price for a Buyer with an Agent.</small></p>
                                    </div>


                                    <div class="col-lg-6 col-md-12 p-20">

                                        <div class="profilecenter">
                                            <div class="profilepiccontainer">
                                                <div class="profilepic" id="buyerwithagentpic"></div>​
                                            </div>
                                        </div>

                                        <h5 class="t-center">Buyer with an Agent</h5>

                                        <div class="input-group">
                                            <span class="input-group-addon" id="buyerwithagent"><i class="fa fa-dollar" aria-hidden="true"></i></span>
                                            <input type="text" placeholder="Price"  ng-model="abyhctrl.form.data.buyerwithagent" class="form-control" aria-describedby="buyerwithagent" mask-money no-prefix with-decimal/>
                                        </div>
                                        <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.buyerwithagent">Buyer with an agent price is required!</small>
                                        <p style="text-align: center; margin-top: 10px!important;"><small>This price should be higher since you will have to pay a commission to the buyer’s agent. We have calculated the price based on the commission you said you would pay but you can type in the price you want.</small></p>
                                    </div>
                                </div>
                                <button class="btn btn-default prevBtn btn-lg pull-left" type="button" ng-click="abyhctrl.form.data.step2 = false; abyhctrl.form.data.step1= true">Previous</button>
                                <button class="btn btn-primary nextBtn btn-lg pull-right bgorange" type="button" ng-click="abyhctrl.form.gotostep3()">Next</button>
                            </div>
                        </div>
                    </div>

                    <div class="row setup-content ng-cloak" id="step-3" ng-if="abyhctrl.form.data.step3">
                        <div class="col-xs-12 col-md-8 col-md-offset-2">
                            <div class="col-md-12">
                                <h3> Step 3</h3>

                                <h4>Tell us about yourself.</h4>

                                <p>Please include all owners of the property. Use the names you would like used in the contract.</p>

                            <div class="row mb-20">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="sc_other_seller_name">Name:</label>                                     
                                        <input type="text" ng-init="abyhctrl.form.data.fullname = loutctrl.fullname" ng-model="abyhctrl.form.data.fullname" name="fullname" id="fullname" class="form-control"  />
                                        <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.fullname">Owner name is required.</small>
                                    </div>
                                </div>
                                
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="sc_other_seller_address">Email:</label>                                     
                                        <input type="text" ng-init="abyhctrl.form.data.owneremail = loutctrl.email" ng-model="abyhctrl.form.data.owneremail" name="owneremail" id="owneremail" class="form-control" />
                                        <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.owneremail">Email is invalid.</small>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="sc_other_seller_address">Phone:</label>
                                        <input type="text" ng-init="abyhctrl.form.data.ownerphone = loutctrl.phone" ng-model="abyhctrl.form.data.ownerphone" name="ownerphone" id="ownerphone" class="form-control" ui-mask="(999) 999-9999"  ui-mask-placeholder ui-mask-placeholder-char="_" model-view-value="true"/>
                                    </div>
                                </div>
                            </div>


                            <div class="row mb-40">

                                <div class="col-md-4">
                                    <label for="">Other Owner</label>
                                </div>

                                <div class="col-sm-12 mb-20" ng-repeat="other in abyhctrl.form.data.otherowner">
                                    <h5>Owner {[{$index + 1}]}</h5>
                                    <a class="btn-danger btn btn-sm" ng-click="abyhctrl.form.removeotherowner(other)">Remove this Owner</a>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="sc_other_seller_name">Name:</label>                                     
                                                <input type="text" ng-model="other.name" name="" id="" class="form-control"  />
                                                <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.otherownername[$index]">Owner name is required.</small>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="sc_other_seller_address">Email:</label>                                     
                                                <input type="text" ng-model="other.email" name="" id="" class="form-control" />
                                                 <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.otherowneremail[$index]">Email is invalid.</small>
                                            </div>
                                        </div>

                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="sc_other_seller_address">Phone:</label>                                     
                                                <input type="text" ng-model="other.phone" name="" id="" class="form-control" ui-mask="(999) 999-9999"  ui-mask-placeholder ui-mask-placeholder-char="_" model-view-value="true"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <a class="btn-success btn btn-sm" ng-click="abyhctrl.form.addotherowner()"> Add another Owner </a>
                                </div>
                             </div>

                                

                                <button class="btn btn-default prevBtn btn-lg pull-left" type="button" ng-click="abyhctrl.form.data.step3 = false; abyhctrl.form.data.step2= true">Previous</button>
                                <button class="btn btn-primary nextBtn btn-lg pull-right bgorange" type="button" ng-click="abyhctrl.form.gotostep4()">Next</button>
                            </div>
                        </div>
                    </div>

                    <div class="row setup-content ng-cloak" id="step-4" ng-if="abyhctrl.form.data.step4">
                        <!--test step 4-->
                        <div  class="col-xs-12 col-md-8 col-md-offset-2">
                            <h3> Step 4</h3>
                            <!-- <h4>{[{abyhctrl.form.msg}]}</h4> -->
                            <h4>Turbo Pricing</h4>
                            <div id="resize1" class="col-md-12">
                                
                                <p>We get paid in two low and easy installments.</p>

                                <div class="summery-container" style="margin-top: 25px;">
                                    <h3 id="resizeh3" style="margin: 0; padding: 10px 0; border-top: 1px solid #ccc; border-bottom: 1px solid #ccc;">Installment 1 - Paid at Listing<span class="pull-right">$199</span></h3>
                                    <h3 id="resizeh3" style="margin: 0; padding: 10px 0; border-bottom: 1px solid #ccc;">Installment 2 - Paid Upon Closing<span class="pull-right">$699</span></h3>
                                </div>
                                <div style="margin-top: 25px; font-size: 14px; letter-spacing: 2px; line-height: 24px;">
                                    <div id="resize1" class="col-md-12">
                                        <p>
                                            I have read and understand that by proceeding I am entering into a legally binding contract with Turbo Homes to perform these services contemplated in the Listing Agreement accessible at, <a href="files/TurboHomes Terms of Service-1_19_18.pdf" target="_blank"><strong style="letter-spacing: 1px;">www.turbohomes.com</strong></a> (the “Listing Agreement”). By selecting “Agree” below, I agree to the terms of the Listing Agreement. 
                                        </p>
                                        <p>
                                            <input style="display: inline-block; margin-right: 8px;" class="form-check-input" id="mls" type="checkbox" ng-model="abyhctrl.form.data.term_agreement">
                                            <label style="display: inline-block; vertical-align: middle; letter-spacing: 1px;" class="form-check-label" for="mls"> I agree to the Listing Agreement Terms </label>
                                        </p>    

                                        <div class="m-b-20" ng-if="abyhctrl.form.data.term_agreement">
                                            <form name="paymentForm" id="paymentForm" action="{{ env('PAYPAL_FORM_ACTION') }}" method="post">                                    
                                                <input type="hidden" name="cmd" value="_xclick">
                                                <input type="hidden" name="business" value="{{ env('PAYPAL_ACCOUNT') }}">
                                                <input type="hidden" name="item_name" value="Turbohomes - Installment 1">
                                                <!-- <input type="hidden" name="amount" value="199.00"> -->
                                                <input type="hidden" name="amount" value="199.00">
                                                <input type="hidden" name="currency_code" value="USD">
                                                <input type="hidden" name="return" id="return" value="{{ env('APP_URL') }}/sell-form?custom={[{ paypalcustom }]}">
                                                <input type="hidden" name="custom" id="paypalcustom" ng-model="paypalcustom" value="">
                                            </form>

                                            <div class="m-b-20">
                                                <div vc-recaptcha key="'{{ env('RECAPTCHA_SITEKEY') }}'" ng-model="abyhctrl.form.data.recaptcha"></div>
                                                
                                                <small class="haserror ng-cloak m-t-10" ng-if="abyhctrl.form.data.invalidRecaptcha">reCaptcha is required.</small>
                                            </div>
                                            
                                            <a href="#" ng-click="submitpayment(abyhctrl.form.data, abyhctrl.form.data.paymentmethod)">
                                                <img src="https://www.paypalobjects.com/webstatic/en_US/i/buttons/checkout-logo-large.png" alt="Check out with PayPal" />
                                            </a>

                                             <p style="margin-top:10px">Note: You can also pay via Visa, Mastercard through this paypal link.</p>

                                        </div>
                                        <button class="btn btn-default prevBtn btn-lg pull-left" type="button" ng-click="abyhctrl.form.data.step4 = false; abyhctrl.form.data.step3 = true">Previous</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row setup-content ng-cloak" id="step-5" ng-if="abyhctrl.form.data.step5">
                        <!--test step 4-->
                        <div  class="col-xs-12 col-md-8 col-md-offset-2">
                            <h3> Step 5</h3>
                            <!-- <h4>{[{abyhctrl.form.msg}]}</h4> -->
                            <h4 ng-if="abyhctrl.form.data.step5a">Just a few more questions</h4>
                            <div>
                                <div ng-if="abyhctrl.form.data.step5a">
                                    <!-- <div class="col-xs-8 col-md-offset-2">
                                        <h2 style="text-align: center; font-weight: bold">Installment 1 - Paid Now $199</h2>
                                    </div> -->
                                    <div class="row">
                                        <div class="col-md-2"></div>
                                        <div class="col-md-8">
                                            <h5>How would you like to be contacted to setup showing appointments?</h5>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select  class="col-md-8 form-control" ng-model="abyhctrl.form.data.contactvia" ng-change="abyhctrl.form.addMask(abyhctrl.form.data.contactvia)">
                                                        <option value="">--Select--</option>
                                                        <option value="phone">By phone</option>
                                                        <option value="text">By text</option>
                                                        <option value="email">By email</option>
                                                    </select>
                                                    <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.contactvia">Please select one.</small>
                                                </div>
                                            </div>
                                            <div class="row" ng-show="abyhctrl.form.data.contactvia">
                                                <div class="col-md-12 margin-t-5">
                                                    <label>Please enter your {[{ abyhctrl.form.via_placeholder }]} below.</label>
                                                    <input type="text"  class="form-control" ng-model="abyhctrl.form.data.contact" ui-mask="{[{abyhctrl.form.mask}]}"  ui-mask-placeholder="" ui-mask-placeholder-char="{[{abyhctrl.form.char}]}" model-view-value="true">
                                                    
                                                <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.contact">Please enter your contact.</small>
                                                </div>
                                            </div>

                                            <h5>Select those that apply:</h5>
                                            <div class="p-l-20">
                                                <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.apply">Please select atleast one.</small>
                                                <div class="checkbox">
                                                    <label class="control-label"><input type="checkbox" ng-model="abyhctrl.form.data.homevacant">Home is vacant</label>
                                                </div>
                                                <div class="checkbox">
                                                    <label class="control-label"><input type="checkbox" ng-model="abyhctrl.form.data.tenantoccupied">Tenant occupied</label>
                                                </div>
                                                <div class="checkbox">
                                                    <label class="control-label"><input type="checkbox" ng-model="abyhctrl.form.data.permrequired">Permission required for showings</label>
                                                </div>
                                                <div class="checkbox">
                                                    <label class="control-label"><input type="checkbox" ng-model="abyhctrl.form.data.gatecode">Gate Code</label>
                                                </div>

                                                <!--Gate Code-->
                                                <div class="p-l-20" ng-show="abyhctrl.form.data.gatecode">
                                                    <div class="row">
                                                        <div class="col-md-12 margin-t-5">
                                                            <select class="col-md-8 form-control" ng-model="abyhctrl.form.data.gatecodeprov">
                                                                <option value="">--Select--</option>
                                                                <option value="w_provide">Will provide code when showing appointment is made.</option>
                                                                <option value="provide_gcode">Provide gate code</option>
                                                            </select>
                                                            <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.gatecodeprov">Please select one.</small>
                                                        </div>
                                                        <div class="col-md-12 m-t-20" ng-show="abyhctrl.form.data.gatecodeprov=='provide_gcode'">
                                                            <input type="text" placeholder="Provide gate code here" class="form-control" ng-model="abyhctrl.form.data.providedcode" required>
                                                            <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.providedcode">Please enter your gate code.</small>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="checkbox">
                                                    <label class="control-label"><input type="checkbox" ng-model="abyhctrl.form.data.alarmactivated">Alarm activated</label>
                                                </div>

                                                <!--Alarm Activated-->
                                                <div class="p-l-20" ng-show="abyhctrl.form.data.alarmactivated">
                                                    <div class="row">
                                                        <div class="col-md-12 margin-t-5">
                                                            <select class="col-md-8 form-control" ng-model="abyhctrl.form.data.willalarm">
                                                                <option value="">--Select--</option>
                                                                <option value="w_provide">Will provide instructions to deactivate alarm.</option>
                                                                <option value="provide_alarm_ins">Provide alarm deactivation instructions</option>
                                                            </select>
                                                             <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.willalarm">Please select one.</small>
                                                        </div>
                                                        <div class="col-md-12 m-t-20" ng-show="abyhctrl.form.data.willalarm=='provide_alarm_ins'">
                                                            <input type="text" placeholder="Provide gate code here" class="form-control" ng-model="abyhctrl.form.data.alarmcode" required>
                                                            <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.alarmcode">Please enter your gate code.</small>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <h5>How would you like potential buyers to gain access to enter the house?</h5>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <select class="col-md-8 form-control" ng-model="abyhctrl.form.data.buyeraccess">
                                                        <option value="">--Select--</option>
                                                        <option value="combination_lbox">Combination Lockbox</option>
                                                        <option value="unlocked">Personally let them in</option>
                                                    </select>
                                                    <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.buyeraccess">Please select one.</small>
                                                </div>
                                            </div>
                                            <div class="row" ng-show="abyhctrl.form.data.buyeraccess=='combination_lbox'">
                                                <div class="col-md-12 margin-t-5">
                                                    <select class="col-md-8 form-control" ng-model="abyhctrl.form.data.lockbox">
                                                        <option value="">--Select--</option>
                                                        <option value="will_provide_lbox">Provide lockbox code now for buyer agents view only</option>
                                                        <option value="provide_lbox">Provide lockbox</option>
                                                    </select>
                                                    <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.lockbox">Please select one.</small>
                                                </div>
                                                <!--Provide lockbox Code-->
                                                <div class="col-md-12 margin-t-5" ng-show="abyhctrl.form.data.lockbox=='provide_lbox'">
                                                    <input type="text" placeholder="Provide lockbox code here" class="form-control" ng-model="abyhctrl.form.data.lockboxdesc">
                                                     <small class="haserror ng-cloak" ng-if="abyhctrl.form.validation.lockboxdesc">Please enter your lockbox.</small>
                                                </div>
                                                <!-- <p class="col-md-12">Lockbox code location. Typically: Front door or indicate the side of the house</p> -->
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12" style="margin-bottom: 25px;">
                                            <h4>Special instructions</h4>
                                            <textarea type="text" rows="5" placeholder="Special instructions" class="form-control" ng-model="abyhctrl.form.data.specialinstruction"></textarea>
                                        </div>
                                        <div class="col-md-12">
                                             <!-- ng-click="abyhctrl.form.submit()" -->
                                            <button class="btn btn-default prevBtn btn-lg pull-left" type="button" ng-click="abyhctrl.form.data.step5 = false; abyhctrl.form.data.step3 = true">Previous</button>
                                            <button class="btn btn-primary nextBtn btn-lg pull-right bgorange" type="button" ng-click="abyhctrl.form.submit()">Next</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" ng-if="abyhctrl.form.data.step5b">
                                    <div class="col-md-12" style="margin-top: 25px;">
                                        <h4 style="font-size: 20px;">
                                            Congratulations! You have taken the first step to save thousands. 
                                        </h4>
                                    </div>
                                    <div class="col-md-12">
                                        <h4>
                                            So what's next? 
                                        </h4>
                                    </div>
                                    <div class="col-md-12">
                                        <!-- <h5>
                                            Within 24 hours <br/>  
                                              Turbo will gather the rest of the info needed to list your home and email the info for you to verify. 
                                              <br/>
                                              Turbo Photographer will contact you to schedule a photoshoot.
                                        </h5> -->
                                        <h4>Within 24 hours:</h4>
                                        <p style="margin: 0; font-size: 18px"><i class="fa fa-home" style="font-size: 30px; display: inline-block; vertical-align: middle; margin-right: 12px;"></i>Turbo will gather the rest of the info needed to list your home and email the info for you to verify.</p>
                                        <p style="font-size: 18px"><i class="fa fa-camera-retro" style="font-size: 26px; display: inline-block; vertical-align: middle; margin-right: 12px;"></i>Turbo Photographer will contact you to schedule a photoshoot.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection