<!DOCTYPE html>
<html data-ng-app="app"  lang="{{ app()->getLocale() }}">
    <head>
        <title>Turbo Homes | @yield('title')</title>

        @include('frontend.includes.head')
        
        @yield('page-css')
    </head>
    <body ng-controller="layoutCtrl as loutctrl">
       
        <!-- Google Tag Manager (noscript) -->
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5SVH8PX"
        height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
        <!-- End Google Tag Manager (noscript) -->

        @include('frontend.includes.header')
        
        @yield('content')

        <toaster-container toaster-options="{ 'time-out': 5000, 'position-class': 'toast-bottom-right' }"></toaster-container>
        
        @include('frontend.includes.footer')
        
        @yield('page-js')
    </body>
</html>