<section class="section-detail-content default-page">
	<div class="container">
		<div class="row dashboardcontainer">
			<div class="col-md-12">
				<div class="page-main">
					<div class="article-detail sc_dashboard_container">
						
						<div id="dashboard_utilities" class="">
							<div class="row">
								<div class="col-md-6 pull-left">
									<p class="display_name">Hello <a href="#" class="view_profile ng-cloak">{[{ firstname }]}</a></p>
								</div>

								<div class="col-md-6 text-right pull-right">
									<a class="sc_logout" href="" ng-click="logout()">Log Out</a>
								</div>
							</div>

							<div class="row">
								<div id="sc_dashboard_menu_container" class="m-b-20">
									<ul class="navoffer nav-tabs sc_dashboard_menu">
										<li class="dropdown">
											<a class="dropdown-toggle" data-toggle="dropdown" href="#">Buyer <span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="#buyer_dashboard_menu" data-toggle="pill">Buying Process</a></li>						
												<li><a href="#make_an_offer" data-toggle="pill">Make an Offer Tool</a></li>						
												<li><a href="#buyer_contract_dashboard" data-toggle="pill">Contract Dashboard</a></li>
												<li>
													<a href="/provider-market">
													   Provider Market 
												    </a>
												</li>
											</ul>
										</li>
										<li class="dropdown">
											<a class="dropdown-toggle" data-toggle="dropdown" href="#">Seller <span class="caret"></span></a>
											<ul class="dropdown-menu">
												<li><a href="#seller_dashboard_menu" data-toggle="tab">Selling Process</a></li>											
												<li><a href="/sell-form">List Property</a></li>
												<li><a href="#update_listing" data-toggle="tab">Update Listing</a></li>	
												<li><a href="#offer_review" data-toggle="tab">Offer Review</a></li>
												<li><a href="#seller_contract_dashboard" data-toggle="tab">Contract Dashboard</a></li>
												<li>
													<a href="/provider-market">
													   Provider Market 
												    </a>
												</li>
											</ul>
										</li>
										<li><a data-toggle="pill" href="#property_value_dashboard_menu" class="property_zillow_menu">Property Value</a></li> 
									</ul>
								</div>
							</div>
						</div>
						
						<div class="sc_dashboard">
							
							<div class="tab-content">								
								<!-- Buyer Link -->
								<div id="buyer_dashboard_menu" class="tab-pane fade">		
									@include('frontend.dashboard.tabs.buyer_dashboard')
								</div>
								
								<div id="make_an_offer" class="tab-pane fade">		
									@include('frontend.dashboard.tabs.make_offer')
								</div>
								
								<div id="buyer_contract_dashboard" class="tab-pane fade">		
									@include('frontend.dashboard.tabs.buyer_contract_dashboard')
								</div>
								
								<div id="buyer_reminders" class="tab-pane fade">		
									<!-- include('buyer_reminders.php'); ?> -->
								</div>
								
								<!-- Seller Link -->
								<div id="seller_dashboard_menu" class="tab-pane fade">	
									@include('frontend.dashboard.tabs.seller_dashboard')
								</div>
								
								<div id="update_listing" class="tab-pane fade">		
									@include('frontend.dashboard.tabs.update_listing')
								</div>
								
								<div id="offer_review" class="tab-pane fade">	
									@include('frontend.dashboard.tabs.offer_review')
								</div>
								
								<div id="seller_contract_dashboard" class="tab-pane fade">		
									@include('frontend.dashboard.tabs.seller_contract_dashboard')
								</div>
								
								<div id="seller_reminders" class="tab-pane fade">		
									<!-- include('seller_reminders.php'); ?> -->
								</div>
								
								<!-- Zillow Link -->
								<div id="property_value_dashboard_menu" class="tab-pane fade">		
									<!-- include('property_dashboard.php'); ?> -->
									@include('frontend.dashboard.tabs.property_dashboard')
								</div>
							</div>
						</div>	
						
						<br class="clear" />
						
					</div>					
				</div>
			</div>
		</div>
	</div>
</section>

<!-- Profile Modal -->


<!-- Modal -->
<div id="sc_welcome_modal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header" style="padding-bottom: 0px!important;border-bottom: 0px!important;">		
				<img src="/img/turbohomes-cloud-logo.png" alt="" style="width: 75px;display: block;margin: auto;">		
				<h4 class="modal-title text-center primary-color f-bold">Welcome to the Turbo Hub</h4>
			</div>
			<div class="modal-body" style="padding: 15px 35px;">
				<p>You can access the Turbo Hub at any time by logging in. The Hub walks you through the buying and selling process and provides the other resources you will need. It’s designed so you can easily access the most important information, even the information you didn’t know you needed. Just click on the Buyer or Seller Menu tabs to access the tools. It includes:</p>
				
				<ul>
					<li>Step by step instructions to buy and sell a home</li>	
					<li>The Make an Offer Tool (Buyers)</li>
					<li>List Your Property Tool (Sellers)</li>
					<li>Property Value Reports</li>
					<li>Your Dashboard so you can check off important steps and deadlines</li>
				</ul>
				
				<p>Take a look around.</p>
				
				<div class="text-center m-b-10">
					<a href="" class="btn btn-default btn-sm buttonlink" data-dismiss="modal" style="display:initial; width:auto; height:auto;">Go to the Hub</a>				
				</div>
			</div>
		</div>
		
	</div>
</div>


	