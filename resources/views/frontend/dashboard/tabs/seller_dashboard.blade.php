<div data-ng-controller="dashboardCtrl">
	<h2 class="text-center m-t-5 m-b-20">Selling Process</h2>
	
	<ul id="main_seller_menu" class="nav nav-tabs seller_menu">
		<li class="active"><a data-toggle="tab" href="#seller_menu1" data-id="1">1</a> <span>Overview</span></li>
		<li><a data-toggle="tab" href="#seller_menu2" data-id="2">2</a> <span>Prepare to Sell</span></li>
		<li><a data-toggle="tab" href="#seller_menu3" data-id="3">3</a> <span>Marketing</span></li> 
		<li><a data-toggle="tab" href="#seller_menu4" data-id="4">4</a> <span>Showings</span></li>
		<li><a data-toggle="tab" href="#seller_menu5" data-id="5">5</a> <span>Offers and Negotiation</span></li>
		<li><a data-toggle="tab" href="#seller_menu6" data-id="6">6</a> <span>Under Contract</span></li>
		<li><a data-toggle="tab" href="#seller_menu7" data-id="7">7</a> <span>Closing</span></li>
	</ul>

	<div class="tab-content" id="seller-tab-content">
		<div id="seller_menu1" class="tab-pane fade in active">		
			
			<div class="menu_header">
				<h3>Overview</h3>
			</div>
			
			<div class="banner_dashboard">
				<img src="libs/houzez-child/images/Prequalify.jpg" class="img-responsive" />
			</div>
			
			<img src="img/1-05.png" class="w-100" />	
		</div>
		
		<div id="seller_menu2" class="tab-pane fade">
			<div class="menu_header">
				<h3>Prepare to Sell</h3>
			</div>			
			
			<div class="banner_dashboard">
				<img src="libs/houzez-child/images/Find.jpg" class="img-responsive" />			
			</div>
			
			<ul class="nav nav-tabs seller_menu_new1">
				<li class="active">
					<a data-toggle="tab" href="#inner1_seller_new1">
						Sign Up with Turbo
					</a>
				</li>
				<li>
					<a data-toggle="tab" href="#inner1_seller_new2">
						Property Value
					</a>
				</li>
				<li>
					<a data-toggle="tab" href="#inner1_seller_new3">
						Paperwork
					</a>
				</li>
				<li>
					<a data-toggle="tab" href="#inner1_seller_new4">
						House Staging
					</a>
				</li>
			</ul>	
			
			<div class="tab-content">			
				<div id="inner1_seller_new1" class="tab-pane fade in active">
					<p class="text-center f-18">Congratulations! <br> You are on your way to saving lots of mullah.</p>

					<p class="text-center f-18">You have signed up with Turbo <br> but now it's time to list your home for sale.</p>

					<button type="button" class="btn-primary btn btn-lg property_section_btn bntorange m-auto show m-t-10" ng-click="goTo('/sell-form')">List Home</buttton>
				</div>
				
				<div id="inner1_seller_new2" class="tab-pane fade">
					<h3 class="fc-7">Property value is driven by comparable properties.</h3>

					<div class="row">
						<div class="col-sm-6">
							<img src="img/1-04.png" class="w-100" />		
						</div>
						<div class="col-sm-6 m-tb-40">
							<h4>What should be my price?</h4>
							<p>
								Your listing price should be justified by comparable properties. You want to price it
								competitively with similar homes currently for sale and, in a rising market, at or a little above
								the price of similar recently sold houses.
							</p>

							<button type="button" class="btn-primary btn btn-lg property_section_btn bntorange m-auto show m-t-20" data-toggle="pill" data-target="#property_value_dashboard_menu"  ng-click="setActivePills($event)">Property Value Report</button>
							
						</div>
					</div>				
				</div>

				<div id="inner1_seller_new3" class="tab-pane fade">
					<table class='m-b-20'>
						<tbody>
							<tr>
								<td>
									<img src="img/circle-01.png" class="w-100 bullet" />
								</td>
								<td>
									<p class="f-18 v-align">Go to <a href="/sell-form" class="primary-link">List Your Home ></a> to provide some basic info. It just takes a few minutes.</p>
								</td>
							</tr>


							<tr>
								<td>
									<img src="img/circle-02.png" class="w-100 bullet" />
								</td>
								<td>
									<p class="f-18 v-align">Turbo will pull the rest of the info needed from country records and other public sources.</p>
								</td>
							</tr>


							<tr>
								<td>
									<img src="img/circle-03.png" class="w-100 bullet" />
								</td>
								<td>
									<p class="f-18 v-align">The required paperwork will then be sent to you for review and e-signature.</p>
								</td>
							</tr>
						</tbody>
					</table>

					<div class="w-100 bg-primary p-15">
						<p class="text-center f-18">Turbo attorneys handle the paperwork <br> and answer your questions. It's all included</p>
					</div>
				</div>

				<div id="inner1_seller_new4" class="tab-pane fade p-0">
					<img src="img/graph.jpg" class="w-100 m-tb-20">
				</div>
			</div>
		</div>
		
		<div id="seller_menu3" class="tab-pane fade">
			
			<div class="menu_header">
				<h3>Marketing</h3>
			</div>
			
			<div class="banner_dashboard">
				<img src="libs/houzez-child/images/MakeOffer1.jpg" class="img-responsive" />
			</div>
			
			<ul class="nav nav-tabs seller_menu_inner3">
				<li class="active">
					<a data-toggle="tab" href="#inner3_seller_menu1">	Listing
					</a>
				</li>
				<li>
					<a data-toggle="tab" href="#inner3_seller_menu2">	Photos
					</a>
				</li>
				<li>
					<a data-toggle="tab" href="#inner3_seller_menu3">	Signs and Flyers
					</a>
				</li>
				<li>
					<a data-toggle="tab" href="#inner3_seller_menu4">	Property Site
					</a>
				</li>
			</ul>
			
			<div class="tab-content">
				
				<div id="inner3_seller_menu1" class="tab-pane fade in active">
					<p class="f-18">
						We list your home for sale on all the leading real estate sites.
					</p>
					
					<div class="row f-18">
						<div class="col-sm-6" style="padding-left: 0">
							<p class="f-18">
								It’s up to you if you want us to list your home for sale on the MLS (Multiple Listing Service). It is
								all included in our price. However, we recommend that you have us list your home on the MLS
								because it still will get you much more exposure to your home.
							</p>
							<p class="f-18">
								Here are some of the pros and cons of using the MLS.
							</p>
						</div>
						<div class="col-sm-6">
							<div class="row">
								<div class="col-sm-4">
									<button class="btn-primary btn btn-lg property_section_btn bntorange m-auto show h-70 w-100" style="cursor: default;">MLS</button>
								</div>
								<div class="col-sm-8">
									<p>(+) A lot more exposure to potential buyers</p>
									<p>(-) You will likely pay a buyer’s agent a commission</p>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-4">
									<button class="btn-primary btn btn-lg property_section_btn bntorange m-auto show h-70 w-100"  style="cursor: default;">No MLS</button>
								</div>
								<div class="col-sm-8">
									<p>(+) Less likely to have to pay thousands to a buyer’s agent</p>
									<p>(-) Much less exposure to potential buyers</p>
								</div>
							</div>

							<div class="row">
								<div class="col-sm-12">
									<p class="text-center">
										Already listed? <a href="#update_listing" data-target="#update_listing" data-toggle="tab" ng-click="setActiveTabs($event)" class="primary-link">Add MLS ></a>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div id="inner3_seller_menu2" class="tab-pane fade">
					<p class="f-18">Potential buyers share your photos with friends and family.<br> Let's make them look greate.</p>
					
					<div class="row">
						<div class="col-sm-6">
							<img src="img/seller_photos_1.jpg" alt="" class="w-100" height="250">
						</div>
						<div class="col-sm-6">
							<p class="f-18">Once you sign up to list your home with Turbo a professional photographer will schedule an appointment with you.</p>

							<p class="f-18">Prepare your home for the photos. Clean, declutter...follow the same recommendations as <a href="#" class="primary-link">House Staging ></a></p>
						</div>
					</div>
				</div>		

				<div id="inner3_seller_menu3" class="tab-pane fade">
					<div class="row">
						<div class="col-sm-6">
							<p class="f-18">The basics. As soon as you signup to list:</p>

							<ul class="bulleted d-table f-18">
								<li class="text-left">A Turbo sign will go up in your front yard</li>
								<li class="text-left">Turbo will send you flyers to distribute</li>
								<li class="text-left">Turbo will provide online flyers for you to share on social media</li>
							</ul>
						</div>
						<div class="col-sm-6">
							<img src="img/seller_photos_2.jpg" alt="" class="w-100" height="300">
						</div>
					</div>
				</div>	

				<div id="inner3_seller_menu4" class="tab-pane fade">
					<div class="row">
						<div class="col-sm-6">
							<p class="f-18 primary-color text-center m-tb-100">
								You get a site dedicated to showcasing your house.
							</p>
						</div>
						<div class="col-sm-6">
							<img src="img/seller_photos_32.jpg" alt="" class="w-100" height="300">
						</div>
					</div>
				</div>	
			</div>
		</div>
		
		<div id="seller_menu4" class="tab-pane fade">
			
			<div class="menu_header">
				<h3>Showings</h3>
			</div>
			
			<div class="banner_dashboard">
				<img src="libs/houzez-child/images/SubmitLoan.jpg" class="img-responsive" />
			</div>
			
			<ul class="nav nav-tabs seller_menu_inner4">
				<li class="active"><a data-toggle="tab" href="#inner4_seller_menu1">Appointments</a></li>
				<li><a data-toggle="tab" href="#inner4_seller_menu2">Home Staging</a></li>		
			</ul>
			
			<div class="tab-content">
				
				<div id="inner4_seller_menu1" class="tab-pane fade in active">
					<ul class="bulleted d-table f-18 m-tb-20 m-auto">
						<li class="text-left">A Turbo sign will go up in your front yard</li>
						<li class="text-left">Turbo will send you flyers to distribute</li>
						<li class="text-left">Turbo will provide online flyers for you to share on social media</li>
					</ul>

					<button class="btn-primary btn btn-lg property_section_btn bntorange m-auto show">Need to determine what to do here</button>
				</div>
				
				<div id="inner4_seller_menu2" class="tab-pane fade p-0">
					<img src="img/graph.jpg" class="w-100 m-tb-20">
				</div>				
			</div>
		</div>
		
		<div id="seller_menu5" class="tab-pane fade">
			
			<div class="menu_header">
				<h3>Offers and Negotiation</h3>
			</div>
			
			<div class="banner_dashboard">
				<img src="libs/houzez-child/images/Inspect.jpg" class="img-responsive" />
			</div>
			
			<ul class="nav nav-tabs seller_menu_inner5">
				<!-- <li class="active"><a data-toggle="tab" href="#inner5_buyer_menu1">Contract Dates</a></li>
				<li><a data-toggle="tab" href="#inner5_buyer_menu2">Disclosures</a></li>
				<li><a data-toggle="tab" href="#inner5_buyer_menu3">Restrictions</a></li>
				<li><a data-toggle="tab" href="#inner5_buyer_menu4">Inspection</a></li>
				<li><a data-toggle="tab" href="#inner5_buyer_menu5">Insurance</a></li> -->
			</ul>
			
			<div class="tab-content">
				<div id="inner5_buyer_menu1" class="tab-pane fade in active">
					<p class="f-18 m-tb-15">When you receive offers everything is time sensitive.</p>

					<p class="f-18 m-tb-15">Forward offers to <a href="mailto:offers@turbohomes.com" class="primary-color">offers@turbohomes.com</a>.</p>

					<p>Turbo attorneys will discuss the offer with you and negotiate for you. It's all included!</p>
				</div>
			</div>
		</div>
		
		<div id="seller_menu6" class="tab-pane fade">		
			<div class="menu_header">
				<h3>Under Contract</h3>
			</div>
		
			<div class="banner_dashboard">
				<img src="libs/houzez-child/images/Closing.jpg" class="img-responsive" />
			</div>
			
			<ul class="nav nav-tabs seller_menu_inner6">
				<li class="active">
					<a data-toggle="tab" href="#inner6_seller_menu1">	
						Earnest Money
					</a>
				</li>										
				<li>
					<a data-toggle="tab" href="#inner6_seller_menu2">
						Seller Disclosure
					</a>
				</li>								
				<li>
					<a data-toggle="tab" href="#inner6_seller_menu3">
						Contingencies
					</a>
				</li>								
				<li>
					<a data-toggle="tab" href="#inner6_seller_menu4">
						Due Diligence
					</a>
				</li>								
				<li>
					<a data-toggle="tab" href="#inner6_seller_menu5">
						Pack the House!
					</a>
				</li>									
			</ul>
			
			<div class="tab-content">
				
				<div id="inner6_seller_menu1" class="tab-pane fade in active">
					<p class="f-18 m-tb-15">Confirm earnest money is deposited into an escrow account. Your Title/Escrow Company can confirm this for you.</p>

					<p class="f-18 m-tb-15">Earnest money makes sure that the buyer has skin in the game.</p>
				</div>			
				
				<div id="inner6_seller_menu2" class="tab-pane fade">
					<p class="f-18 m-tb-15">As soon as you have a signed contract to buy your house you need to provide the buyer a form in which you disclose any known issues with the home. Turbo provides the form and can help answer questions.</p>

					<h1 class="text-center primary-color">Disclose.Disclose.Disclose</h1>
				</div>
				
				<div id="inner6_seller_menu3" class="tab-pane fade">
					<p class="f-18 m-tb-15">After you get a signed contract the deal is not done. The contract can allow for several contingencies. Here are some of the big ones. Turbo attorneys can help you understand those that apply to you.</p>

					<ul class="bulleted d-table f-18 m-auto">
						<li class="text-left">Ability to obtain a loan</li>
						<li class="text-left">Buyer's inspection of the property</li>
						<li class="text-left">Property appraisal</li>
					</ul>
				</div>

				<div id="inner6_seller_menu4" class="tab-pane fade">
					<div class="row">
						<div class="col-sm-6" style="border-right:1px solid #DDD;">
							<img src="img/buyer-icon.png" class="w-100-px show m-auto">
							<h4 class="text-center font-bold">
								Buyer Due Diligence
							</h4>
							<p class="f-18 m-tb-15">Be ready to allow buyers the ability to do contractually allowed due diligence. This typically includes:</p>
							<ul class="bulleted d-table f-18">
								<li class="text-left">Home inspection</li>
								<li class="text-left">Appraisal</li>
								<li class="text-left">Final walkthrough</li>
							</ul>
						</div>
						<div class="col-sm-6">
							<img src="img/seller-icon.png" class="w-100-px show m-auto">
							<h4 class="text-center font-bold">
								Seller Due Diligence
							</h4>
							<p class="f-18 m-tb-15">Buyers have most of the due diligence but sellers still have some important things to do:</p>
							<ul class="bulleted d-table f-18 text-left">
								<li class="text-left">Review Loan Status Updateds to ensure the buyer's loan will go thru</li>
								<li class="text-left">Keep the home in same condition</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div id="inner6_seller_menu5" class="tab-pane fade">
					<h1 class="text-center primary-color font-bold">Don't forget to pack!</h1>

					<p class="text-center f-18">It's not fun but it feels good when it's done.</p>
				</div>
			</div>
		</div>
		
		<div id="seller_menu7" class="tab-pane fade">		
			<div class="menu_header">
				<h3>Closing</h3>
			</div>
		
			<div class="banner_dashboard">
				<img src="libs/houzez-child/images/Closing.jpg" class="img-responsive" />
			</div>
			
			<ul class="nav nav-tabs seller_menu_inner7">
				<li class="active">
					<a data-toggle="tab" href="#inner7_seller_menu1">	
						Final Closing
					</a>
				</li>										
				<li>
					<a data-toggle="tab" href="#inner7_seller_menu2">
						House is SOLD!
					</a>
				</li>										
			</ul>
			
			<div class="tab-content">
				<div id="inner7_seller_menu1" class="tab-pane fade in active">

					<div class="row m-tb-10">
						<div class="col-sm-8">
							<p class="f-18 m-tb-15">Earnest money makes sure that the buyer has skin in the game.</p>

							<p class="f-18 m-tb-15">The escrow company will provide you a ALTA Statement before you sign the final closing documents.</p>

							<p class="f-18 m-tb-15">Don't forget to schedule time with the escrow company to sign the many papers. They can help you understand the paperwork but Turbo is here to help also.</p>
						</div>
						<div class="col-sm-4">
							<div class="bordered p-10" style="background-color: #DDD">
								<p class="f-18 m-tb-15 text-center m-b-0">
									<span class="primary-color font-bold">ALTA Statement</span><br>
									Report that shows where all expenses and money distribution
								</p>
							</div>
						</div>
					</div>
				</div>			
				
				<div id="inner7_seller_menu2" class="tab-pane fade">

					<h1 class="text-center primary-color">House is SOLD!</h1>

					<p class="f-18 m-tb-15 text-center">When you sign papers the sale is not complet.<br>
						You must wait until the sale funds.<br>
					The escrow company will let you know when it's done.</p>
				</div>
			</div>
		</div>
	</div>
</div>					