<div id="contract_dashboard" class="sc_dashboard menu_page_container main_dashboard" data-ng-controller="contractDashboardCtrl">
	<div class="col-md-12">
        <h4 class="dashboard_main_title">Buyer Dashboard</h4>
        <strong>Check off items as they are completed and see what's coming up next.</strong>
        <p class="p_title">Once the seller has accepted your offer to buy their home then you are under contract. This is a legally binding contract. You need to be aware of important dates in the contract and take appropriate action. This table summarizes some of the key dates. Turbo will also send you reminders for these.</p>

		<a role="button" data-toggle="collapse" href="#get-ready" aria-expanded="false" class="link-collapsible">Get Ready</a>
        <ul class="collapse in" id="get-ready">
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="get_prequalified" type="checkbox" value="get_prequalified" ng-click="set($event)">
                    <label class="pull-left" for="get_prequalified"></label>
        			<div class="m-t-5"><span class="or_highlight bolder_text">Get prequalified.</span> You will look like a more serious buyer. Prequal form is attached. <a href="/provider-market#lender" target="_blank" class="or_highlight">Find a Lender ></a></div>
        		</div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="get_searching" type="checkbox" value="get_searching" ng-click="set($event)">
                    <label class="pull-left" for="get_searching"></label>
        			<div class="m-t-5"><span class="or_highlight bolder_text">Get searching.</span> You can find your house any way you wish and then we help with the rest.</div>
        		</div>
        	</li>
        </ul>

        <a role="button" data-toggle="collapse" href="#showing-offers" aria-expanded="false" class="link-collapsible">Showing & Offers</a>
        <ul class="collapse" id="showing-offers">
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="tour_a_house" type="checkbox" value="tour_a_house" ng-click="set($event)">
                    <label class="pull-left" for="tour_a_house"></label>
        			<div class="m-t-5">To <span class="or_highlight bolder_text">tour a house</span> you're interested in contact the selling agent or owner directly and they should be happy to show it.</div>
        		</div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="makeanoffer" type="checkbox" value="makeanoffer" ng-click="set($event)">
                    <label class="pull-left" for="makeanoffer"></label>
        			<div class="m-t-5">When ready to make an offer navigate to the <a href="/dashboard#!#make_an_offer" class="or_highlight bolder_text" target="_blank">Make an Offer Tool ></a> and Turbo will help you through it.</div>
        		</div>
        	</li>
        </ul>

        <a role="button" data-toggle="collapse" href="#under-contract-inspection-period" aria-expanded="false" class="link-collapsible">Under Contract <br/> Inspection Period</a>
        <ul class="collapse" id="under-contract-inspection-period">
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="deliver_earnest_money" type="checkbox" value="deliver_earnest_money" ng-click="set($event)">
                    <label class="pull-left" for="deliver_earnest_money"></label>
        			<div class="m-t-5">Deliver <span class="or_highlight bolder_text">Earnest Money</span> to your Escrow Company immediately and ask for a receipt.</div>
        		</div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="home_loan" type="checkbox" value="home_loan" ng-click="set($event)">
                    <label class="pull-left" for="home_loan"></label>
        			<div class="m-t-5">&nbsp;Apply for <span class="or_highlight bolder_text">home loan</span> within 3 days. Get them all the info right away.</div>
        		</div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="provide_loan_status_update" type="checkbox" value="provide_loan_status_update" ng-click="set($event)">
                    <label class="pull-left" for="provide_loan_status_update"></label>
        			<div class="m-t-5">Ensure your lender provides a <span class="or_highlight bolder_text">Loan Status Update</span> to the seller within 10 days of the contract.</div>
        		</div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="buyer_inspect_the_home" type="checkbox" value="buyer_inspect_the_home" ng-click="set($event)">
                    <label class="pull-left" for="buyer_inspect_the_home"></label>
        			<div class="m-t-5"><span class="or_highlight bolder_text">Inspect the home</span> then request repairs or back out. We recommend hiring a professional inspector right away. <span class="or_highlight">See Provicer Market</span></div>
        		</div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="insurance" type="checkbox" value="insurance" ng-click="set($event)">
                    <label class="pull-left" for="insurance"></label>
        			<div class="m-t-5">Confirm ability to obtain <span class="or_highlight bolder_text">insurance</span> on the property. Seller to deliver a 5-year insurance claims history within 5 days of the contract.</div>
        		</div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="leased_items" type="checkbox" value="leased_items" ng-click="set($event)">
                    <label class="pull-left" for="leased_items"></label>
        			<div class="m-t-5">Seller to deliver notice of <span class="or_highlight bolder_text">leased items</span> 
        			(e.g. solar panels) within 3 days of the contract. Decide if you are okay with the lease terms.</div>
        		</div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="seller_disclosures" type="checkbox" value="seller_disclosures" ng-click="set($event)">
                    <label class="pull-left" for="seller_disclosures"></label>
        			<div class="m-t-5">Seller to provide <span class="or_highlight bolder_text">Seller Disclosures</span> 
        			within 5 days of contract. Review these in detail. Ask seller questions if needed.</div>
        		</div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="title_commitment" type="checkbox" value="title_commitment" ng-click="set($event)">
                    <label class="pull-left" for="title_commitment"></label>
        			<div class="m-t-5">Review <span class="or_highlight bolder_text">Title commitment, CC&R's and other governing documents(HOA)</span> provided by Escrow Company and cancel contract in writing within 5 days of receipt if needed.</div>
        		</div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="appraised_value" type="checkbox" value="appraised_value" ng-click="set($event)">
                    <label class="pull-left" for="appraised_value"></label>
        			<div class="m-t-5">You have 5 days after notice of <span class="or_highlight bolder_text">appraised value</span> to cancel the Contract and receive refund of Earnest Money.</div>
        		</div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="decision_point" type="checkbox" value="decision_point" ng-click="set($event)">
                    <label class="pull-left" for="decision_point"></label>
        	    	<div class="m-t-5"><span class="or_highlight bolder_text">Decision Point!</span>
        	    	If you want repairs done or to cancel the contract due to your inspections we must give the seller written notice within the inspection period to get your Earnest Money back.</div>
        	    </div>
        	</li>
        </ul>

        <a role="button" data-toggle="collapse" href="#under-contract-other-items" aria-expanded="false" class="link-collapsible">Under Contract <br/> Other Items</a>
        <ul class="collapse" id="under-contract-other-items">
            <li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="home_warranty_plan" type="checkbox" value="home_warranty_plan" ng-click="set($event)">
                    <label class="pull-left" for="home_warranty_plan"></label>
        	    	<div class="m-t-5">If you want a <span class="or_highlight bolder_text">Home Warranty Plan</span> set it up now. Give the invoice to the Escrow Company if the seller is to pay for it.</div> 
        	    </div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="cant_get_the_loan" type="checkbox" value="cant_get_the_loan" ng-click="set($event)">
                    <label class="pull-left" for="cant_get_the_loan"></label>
        	    	<div class="m-t-5">If you <span class="or_highlight bolder_text">can't get the loan</span> you must deliver notice to Seller at least 3 days before closing to get the Earnest Money back.</div> 
        	    </div>
        	</li>
         </ul>

        <a role="button" data-toggle="collapse" href="#closing" aria-expanded="false" class="link-collapsible">Closing</a>
        <ul class="collapse" id="closing">
            <li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="buyer_final_walkthrough" type="checkbox" value="buyer_final_walkthrough" ng-click="set($event)">
                    <label class="pull-left" for="buyer_final_walkthrough"></label>
        	    	<div class="m-t-5">Schedule a <span class="or_highlight bolder_text">final walkthrough</span> before closing. Confirm items are in good working order.</div>
        	    </div>
        	</li>
        	<li>
        		<div class="col-xs-12">
        			<input class="styled-checkbox" id="hud_1" type="checkbox" value="hud_1" ng-click="set($event)">
                    <label class="pull-left" for="hud_1"></label>
        	    	<div class="m-t-5">Escrow company will provide a <span class="or_highlight bolder_text">HUD 1</span> that shows who pays what and who gets what. Review this in detail.</div>
        	    </div>
        	</li>
        	<li>
        		<div class="col-xs-12">
                    <input class="styled-checkbox" id="buyer_sign_final_docs" type="checkbox" value="buyer_sign_final_docs" ng-click="set($event)">
                    <label class="pull-left" for="buyer_sign_final_docs"></label>
        	    	<div class="m-t-5">Schedule a time for your closing and to <span class="or_highlight bolder_text">sign final docs</span> with the Escrow Company. Loan docs must be signed at least 3 days before closing. <span class="or_highlight bolder_text">Get the KEYS!</span></div>
        	    </div>
        	</li>
         </ul>
	</div>
	
	{{-- <div class="col-md-4">
		<h3>Contact Repository</h3>
		<div id="contact_repository">
		</div>
	</div> --}}
	
</div>	

