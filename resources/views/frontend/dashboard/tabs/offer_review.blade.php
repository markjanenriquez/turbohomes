<div class="sc_dashboard menu_page_container" ng-controller="offerreviewCtrl as offrevCtrl">	
	<div class="sc_dashboard menu_page_container">
		
		<p>Congratulations on getting an offer! Now it is time to evaluate the offer and decide whether to accept, reject or counteroffer.</p>
		
		<p>Typically you will receive the offer in an email. Here is what to do:</p>
		<ol>
			<li>Forward the offer to offer@turbohomes.com</li>
			<li>Turbo will contact you by phone shortly (during business hours).</li>
		</ol>							
		
	</div>
</div>			
