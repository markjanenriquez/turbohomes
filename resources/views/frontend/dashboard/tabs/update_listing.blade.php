<div class="sc_dashboard menu_page_container" ng-controller="updateListingCtrl as ulCtrl">
	<div ng-if="!success">
		<p>Need to update your home listing or have a question about your listing? Fill out the applicable details below and we will quickly respond.</p>
		
		<div class="alert alert-{[{ alert.type }]} ng-cloak" ng-repeat="alert in alerts">{[{ alert.msg }]}</div>

		<form id="update_listing_form" name="update_listing_form" novalidate ng-submit="update_listing(update_listing_form, listing)">
			<div class="col-sm-12">
				<div class="form-group" ng-class="{true: 'has-error'}[update_listing_form.$submitted && update_listing_form.address.$invalid]">
					<label for="sc_new_price">Property:</label>										
	                <select  class="form-control" name="address" ng-model="listing.address" required>
	                    <option ng-repeat="item in listedHomes" value="{[{ item.homeaddress }]}">{[{ item.homeaddress }]}</option>
	                </select>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group" ng-class="{true: 'has-error'}[update_listing_form.$submitted && update_listing_form.sc_new_price.$invalid]">
					<label for="sc_new_price">New Price:</label>										
					<input type="text" name="sc_new_price" id="sc_new_price" class="form-control" ng-model="listing.price" required>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="form-group">
					<label for="sc_changes">Other Changes:</label>										
					<textarea name="sc_changes" id="sc_changes" class="form-control" ng-model="listing.other" required></textarea>
				</div>
			</div>

	        <div class="col-xs-12 col-sm-12 col-md-12">
	            <div id="sc_spinner_container" class="ng-cloak" ng-if="onprogress">
	                <div class="spinner">
	                    <div class="bounce1"></div>
	                    <div class="bounce2"></div>
	                    <div class="bounce3"></div>
	                </div>
	            </div>
	            <button type="submit" class="btn btn-primary btn_submit btn-block btn-lg login-btn" ng-if="onprogress == false">Submit</button>
	        </div>

		</form>
	</div>
	
	<div class="col-md-12 ng-cloak" ng-if="success">
        <div class="signupsuccess" style="height: auto!important; padding-bottom: 25px;">
            <h3>Success!</h3>
            <p>
                We received your update request.<br>
                A turbo attorney will be contacting you shortly for your change requests.
            </p>
			<p class="text-left" style="padding: 0 25px;">
                <small class="text-left">
                	Property: {[{ success.address }]} <br>
        			Changes to be made: <br>
        			Price: {[{ success.price }]} <br>
        			Other Changes: {[{ success.other }]}
                </small>
			</p>
        </div>
    </div>
</div>	