<div class="sc_dashboard menu_page_container make_offer" ng-controller="makeofferCtrl as moctrl">
	<ul id="make_offer_menu_container" class="navoffer nav-tabs flex-container desktop-view">
		<li ng-class="{ 'active' : section.tpl === activeSection }" ng-repeat="section in sections">
			<a href="#" class="property" ng-class="{ 'sc_done' : section.done, 'disabled': section.disabled }" ng-click="selectSection(section)">{[{ section.name }]}</a>
		</li>	
	</ul>
	
	<div class="row" id="make_offer_menu_container-mobile">
		<div class="col-sm-12">
			<ul class="navoffer nav-tabs sc_dashboard_menu">
				<li class="dropdown active open">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">{[{ activeTab }]} <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li ng-repeat="section in sections" ng-class="{ 'active open': activeTab === section.name }">
							<a href="#" ng-class="{ 'sc_done' : section.done, 'disabled': section.disabled }" ng-click="selectSection(section)">
								{[{ section.name }]}
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</div>
	</div>

	<div class="tab-content" ng-switch="activeSection">

		<div class="animate-switch" ng-switch-when="property" ng-controller="propertyCtrl as propCtrl">	
			<div ng-switch="step">
				<form name="property1" class="animate-switch" ng-switch-when="1" novalidate ng-submit="nextStep(property1)">
					<div id="property_section_one" class="col-sm-12">	
						<h3>Step 1</h3>
						<div class="form-group" ng-class="{true: 'has-error'}[property1.$submitted && property1.sc_property_address.$invalid]">
							<label class="control-label" for="sc_property_address">Property's Address:</label>
							<input type="text" ng-model="form.data.scpropertyaddress" name="sc_property_address" id="sc_property_address" class="form-control" placeholder="Enter the address of the home you want to buy" required g-places-autocomplete autocomplete="off" />
							<span class="text-red m-t-5 display-b" ng-if="property1.$submitted && property1.sc_property_address.$invalid">Property's Address is required</span>
						</div>
					
						<div class="text-right">
							<button class="btn-primary btn btn-md property_section_btn bntorange" type="submit"> Next </button>
						</div>
					</div>
				</form>
				
				<form name="property2" class="animate-switch" ng-switch-when="2" novalidate ng-submit="nextSection(property2)">
					<div id="property_section_two" class="row">	
						<h3>Step 2</h3>
						<div class="form-group">
							<label class="control-label">Is the house listed by:</label>
							<div class="row">
								<ul id="listed_menu" class="navoffer nav-tabs">
									<li ng-class="{ 'active': form.data.listedmenuasnwer == 'turbo' }">
										<a href="#" ng-click="listedBy('turbo', $event);">
											Turbo Homes
										</a>
									</li>
									<li ng-class="{ 'active': form.data.listedmenuasnwer == 'realtor' }">
										<a href="#" ng-click="listedBy('realtor', $event);">
											Realtor
										</a>
									</li>
									<li ng-class="{ 'active': form.data.listedmenuasnwer == 'other' }">
										<a href="#" ng-click="listedBy('other', $event);">
											Other
										</a>
									</li>
									<li ng-class="{ 'active': form.data.listedmenuasnwer == 'notsure' }">
										<a href="#" ng-click="listedBy('notsure', $event);">
											Not Sure Request Attorney Help
										</a>
									</li> 	
								</ul>
							</div>
							
							<div class="row">
								<div class="tab-content" ng-switch="form.data.listedmenuasnwer">
									<div class="animate-switch" ng-switch-when="turbo">	
										<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_seller_name.$invalid]">
											<label class="control-label" for="sc_seller_name">Seller's Name:</label>										
											<input type="text" ng-model="form.data.scsellername" name="sc_seller_name" id="sc_seller_name" class="form-control" required />
											<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_seller_name.$invalid">Seller's Name is required</span>
										</div>
										
										<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_seller_address.$invalid]">
											<label class="control-label" for="sc_seller_address">Seller's Email:</label>										
											<input type="text" ng-model="form.data.scselleraddress" name="sc_seller_address" id="sc_seller_address" class="form-control" required />
											<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_seller_address.$invalid">Seller's Email is required</span>
										</div>
										
										<div class="text-right">
											<button type="submit" class="btn-primary btn btn-md listed_menu1_btn bntorange"> Next </button>
										</div>
									</div>
									
									<div class="animate-switch" ng-switch-when="realtor">	
										<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_realtor_name.$invalid]">
											<label class="control-label" for="sc_realtor_name">Realtor's Name:</label>										
											<input type="text" ng-model="form.data.screaltorname" name="sc_realtor_name" id="sc_realtor_name" class="form-control" required />
											<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_realtor_name.$invalid">Realtor's Name is required</span>
										</div>
										
										<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_realtor_address.$invalid]">
											<label class="control-label" for="sc_realtor_address">Realtor's Email:</label>										
											<input type="text" ng-model="form.data.screaltoraddress" name="sc_realtor_address" id="sc_realtor_address" class="form-control" required />
											<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_realtor_address.$invalid">Realtor's Email is required</span>
										</div>
										
										<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_brokerage_address.$invalid]">
											<label class="control-label" for="sc_brokerage_address">Brokerage Name:</label>										
											<input type="text" ng-model="form.data.scbrokerageaddress" name="sc_brokerage_address" id="sc_brokerage_address" class="form-control" required />
											<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_brokerage_address.$invalid">Brokerage Name is required</span>
										</div>
										
										<div class="text-right">
											<button type="submit" class="btn-primary btn btn-md listed_menu2_btn bntorange"> Next </button>
										</div>
									</div>
									
									<div class="animate-switch" ng-switch-when="other">	
										<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_other_seller_name.$invalid]">
											<label class="control-label" for="sc_other_seller_name">Seller's Name:</label>										
											<input type="text" ng-model="form.data.scothersellername" name="sc_other_seller_name" id="sc_other_seller_name" class="form-control" required />
											<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_other_seller_name.$invalid">Seller's Name is required</span>
										</div>
										
										<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_other_seller_address.$invalid]">
											<label class="control-label" for="sc_other_seller_address">Seller's Email:</label>										
											<input type="text" ng-model="form.data.scotherselleraddress" name="sc_other_seller_address" id="sc_other_seller_address" class="form-control" required />
											<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_other_seller_address.$invalid">Seller's Email is required</span>
										</div>
										
										<div class="text-right">
											<button type="submit" class="btn-primary btn btn-md listed_menu3_btn bntorange"> Next </button>
										</div>
									</div>

									<div class="animate-switch" ng-switch-when="notsure">

										{{-- <div class="form-group">
											<label class="control-label" for="sc_other_seller_name">You may request Attorney's Help for this process</label>
										</div>	 --}}

										<div class="text-right">
											<button type="submit" class="btn-primary btn btn-md listed_menu4_btn bntorange"> Next </button>
										</div>
									</div>
								</div>
							</div>
							
							<div class="row">
								<div class="text-right">
									<br>
									<button type="button" class="btn-default btn btn-md back_to_one_btn" ng-click="goBack()"> Back </button>
								</div>
							</div>
							
						</div>
					</div>
				</form>	
			</div>
		</div>
		
		<div class="animate-switch" ng-switch-when="buyers-info" ng-controller="buyersinfoCtrl as buyinfCtrl">
			<div ng-switch="step">	
				<form name="buyersinfo1" class="animate-switch" ng-switch-when="1" novalidate ng-submit="nextStep(buyersinfo1)">
					<div class="col-sm-12">
						<h3>Step 1</h3>
						<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo1.$submitted && buyersinfo1.buyersname.$invalid]">
							<label class="control-label" for="sc_other_seller_name">Buyer's Name:</label>										
							<input type="text" ng-init="form.data.buyersname = fullname" ng-model="form.data.buyersname" name="buyersname" id="buyersname" class="form-control" required />

							<span class="text-red m-t-5 display-b" ng-if="buyersinfo1.$submitted && buyersinfo1.buyersname.$invalid">Buyer's Name is required</span>
						</div>					
						<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo1.$submitted && buyersinfo1.buyersemail.$invalid]">
							<label class="control-label" for="sc_other_seller_address">Buyer's Email:</label>										
							<input type="text" ng-init="form.data.buyersemail = email" ng-model="form.data.buyersemail" name="buyersemail" id="buyersemail" class="form-control" required />
							
							<span class="text-red m-t-5 display-b" ng-if="buyersinfo1.$submitted && buyersinfo1.buyersemail.$invalid">Buyer's Email is required</span>
						</div>		
						
						<div class="form-group">
							<label class="control-label">Are you buying the house alone or with someone else?</label>
							<div class="row">
								<ul id="listed_menu" class="navoffer nav-tabs">
									<li ng-class="{ 'active': form.data.sectionbuyingmenuasnwer == 'alone' }">
										<a href="#" ng-click="selectBuyingMenu('alone', $event);">
											Alone
										</a>
									</li>
									<li ng-class="{ 'active': form.data.sectionbuyingmenuasnwer == 'someone' }">
										<a href="#" ng-click="selectBuyingMenu('someone', $event);">
											With Someone
										</a>
									</li>
									<li ng-class="{ 'active': form.data.sectionbuyingmenuasnwer == 'notsure' }">
										<a href="#" ng-click="selectBuyingMenu('notsure', $event);">
											Not Sure Request Attorney Help
										</a>
									</li> 	
								</ul>
							</div>

							<div class="row">
								<div class="tab-content" ng-switch="form.data.sectionbuyingmenuasnwer">
									<div class="animate-switch" ng-switch-when="alone">	
										<div class="text-right">
											<button class="btn-primary btn btn-md bntorange" type="submit"> Next </button>
										</div>
									</div>
									<div class="animate-switch" ng-switch-when="someone">	
										<div class="row">

											<div class="row">
												<button type="button" class="btn-success btn btn-sm" ng-click="buyer.add()"> Add Buyer </button>
											</div>

											<div class="row mb-20" ng-repeat="other in form.data.otherbuyer">
												<h5>Buyer {[{ $index + 1 }]}</h5>
												<button type="button" class="btn-danger btn btn-sm" ng-click="buyer.remove($index)" ng-if="!$first">Remove this Buyer</button>
												<div class="form-group">
													<label class="control-label" for="sc_other_seller_name">Buyer's Name:</label>				
													<input type="text" ng-model="other.name" class="form-control"  />
												</div>

												<div class="form-group">
													<label class="control-label" for="sc_other_seller_address">Buyer's Email:</label>					
													<input type="text" ng-model="other.email" class="form-control" />
												</div>
											</div>
										</div>

										<div class="text-right">
											<button class="btn-primary btn btn-md bntorange" type="submit"> Next </button>
										</div>
									</div>
									<div class="animate-switch" ng-switch-when="notsure">	
										{{-- <div class="form-group">
											<label class="control-label" for="sc_other_seller_name">You may request Attorney's Help for this process</label>
										</div> --}}
										<div class="text-right">
											<button class="btn-primary btn btn-md bntorange" type="submit"> Next </button>
										</div>
									</div>
								</div>
							</div>
						</div>
						
							
						<!-- <div class="text-right">
							<br>
							<button type="button" class="btn-default btn btn-md" ng-click="goBack()"> Back </button>
						</div> -->

					</div>
				</form>
				
				<form name="buyersinfo2" class="animate-switch" ng-switch-when="2" novalidate ng-submit="nextSection(buyersinfo2)">
					<div class="col-sm-12">
						<h3>Step 2</h3>

						<div class="form-group">
							<label class="control-label">Do you need to sell your current home?<br><small class="font-w-normal">By selecting "yes" we will make the offer to buy contingent on the sale of your current home. This means if you do not accept an offer by a specified date and close the sale of your current home by a specified date than this offer to buy can be cancelled.</small></label>
							<ul id="listed_menu" class="navoffer nav-tabs">
								<li ng-class="{ 'active': form.data.sectionbuyingmenu2asnwer == 'yes' }">
									<a href="#" ng-click="selectBuyingMenu2('yes', $event);">
										Yes
									</a>
								</li>
								<li ng-class="{ 'active': form.data.sectionbuyingmenu2asnwer == 'no' }">
									<a href="#" ng-click="selectBuyingMenu2('no', $event);">
										No
									</a>
								</li>
								<li ng-class="{ 'active': form.data.sectionbuyingmenu2asnwer == 'notsure' }">
									<a href="#" ng-click="selectBuyingMenu2('notsure', $event);">
										Not Sure Request Attorney Help
									</a>
								</li> 	
							</ul>
						</div>
						

						<div class="tab-content" ng-switch="form.data.sectionbuyingmenu2asnwer">
							<div class="animate-switch" ng-switch-when="yes">
								<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo2.$submitted && buyersinfo2.sc_current_address.$invalid]">
									<label class="control-label" for="sc_current_address">What is the address of your current home?</label>
									<input type="text"  ng-init="form.data.sccurrentaddress = address" ng-model="form.data.sccurrentaddress" name="sc_current_address" id="sc_current_address" class="form-control" placeholder="Enter the address of your current home" required g-places-autocomplete autocomplete="off" />
							
									<span class="text-red m-t-5 display-b" ng-if="buyersinfo2.$submitted && buyersinfo2.sc_current_address.$invalid">This field is required</span>
								</div>
								
								<div class="form-group">
									<label class="control-label">Do you have a current accepted offer to sell your current house?</label>
									<ul id="listed_menu" class="navoffer nav-tabs">
										<li ng-class="{ 'active': form.data.sectionbuyingmenu3asnwer == 'yes' }">
											<a href="#" ng-click="selectBuyingMenu3('yes', $event);">
												Yes
											</a>
										</li>
										<li ng-class="{ 'active': form.data.sectionbuyingmenu3asnwer == 'no' }">
											<a href="#" ng-click="selectBuyingMenu3('no', $event);">
												No
											</a>
										</li>	
									</ul>
								</div>
								
								<div class="tab-content" ng-switch="form.data.sectionbuyingmenu3asnwer">
									<div class="animate-switch" ng-switch-when="yes">
										<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo2.$submitted && buyersinfo2.contract_date.$invalid]">
											<label class="control-label" for="sc_current_address">The date you want in the contract that you must sell your house by. <br><small class="font-w-normal">If you are unable to sell your home by this date then you will either need to get an extension from the seller or cancel the contract to buy the house. Otherwise you will be at risk to forfeit your earnest money.</small></label>
											<input type="text" ng-model="form.data.contract_date" name="contract_date" id="contract_date" class="form-control" date-picker required readonly />	
							
											<span class="text-red m-t-5 display-b" ng-if="buyersinfo2.$submitted && buyersinfo2.contract_date.$invalid">This field is required</span>
										</div>

										<div id="section_buying_no">		
											<div class="text-right">
												<button type="submit" class="btn-primary btn btn-md buying_no_btn bntorange"> Next </button>
											</div>
										</div>
									</div>
									<div class="animate-switch" ng-switch-when="no">
										<div class="form-group">
											<label class="control-label" for="sc_current_address">Have you already listed your home for sale?</label>	
											<ul id="listed_menu" class="navoffer nav-tabs">
												<li ng-class="{ 'active': form.data.sectionlistedanswer == 'yes' }">
													<a href="#" ng-click="selectBuyingMenu4('yes', $event);">
														Yes
													</a>
												</li>
												<li ng-class="{ 'active': form.data.sectionlistedanswer == 'no' }">
													<a href="#" ng-click="selectBuyingMenu4('no', $event);">
														No
													</a>
												</li>	
											</ul>
										</div>

										<div class="tab-content" ng-switch="form.data.sectionlistedanswer">
											<div class="animate-switch" ng-switch-when="yes">
												<div class="form-group">
													<label class="control-label" for="sc_current_address">With whom?</label>		
													<ul id="listed_menu" class="navoffer nav-tabs">
														<li ng-class="{ 'active': form.data.sectionwhomanswer == 'turbohomes' }">
															<a href="#" ng-click="selectBuyingMenu6('turbohomes', $event);">
																Turbo Homes
															</a>
														</li>
														<li ng-class="{ 'active': form.data.sectionwhomanswer == 'realtor' }">
															<a href="#" ng-click="selectBuyingMenu6('realtor', $event);">
																Realtor
															</a>
														</li>
														<li ng-class="{ 'active': form.data.sectionwhomanswer == 'self' }">
															<a href="#" ng-click="selectBuyingMenu6('self', $event);">
																Self
															</a>
														</li>	
														<li ng-class="{ 'active': form.data.sectionwhomanswer == 'other' }">
															<a href="#" ng-click="selectBuyingMenu6('other', $event);">
																Other
															</a>
														</li>		
													</ul>
												</div>

												<div class="tab-content" ng-switch="form.data.sectionwhomanswer">
													<div class="animate-switch" ng-switch-when="realtor">
														<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo4.$submitted && buyersinfo4.whom_realtor.$invalid]">
															<label class="control-label" for="sc_realtor_name">Realtor's Name:</label>							
															<input type="text" ng-model="form.data.whom_realtor" name="whom_realtor" id="whom_realtor" class="form-control" required />	
							
															<span class="text-red m-t-5 display-b" ng-if="buyersinfo4.$submitted && buyersinfo4.whom_realtor.$invalid">Realtor's Name is required</span>
														</div>
														
														<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo4.$submitted && buyersinfo4.whom_realstate.$invalid]">
															<label class="control-label" for="sc_brokerage_address">Real Estate Brokerage:</label>				
															<input type="text" ng-model="form.data.whom_realstate" name="whom_realstate" id="whom_realstate" class="form-control" required />	
							
															<span class="text-red m-t-5 display-b" ng-if="buyersinfo4.$submitted && buyersinfo4.whom_realstate.$invalid">Real Estate Brokerage is required</span>
														</div>
													</div>
													<div class="animate-switch" ng-switch-when="other">
														<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo4.$submitted && buyersinfo4.whom_other.$invalid]">
															<label class="control-label" for="whom_other">Other Description</label>
															<input type="text" name="whom_other" ng-model="form.data.whom_other" class="form-control" required />	
							
															<span class="text-red m-t-5 display-b" ng-if="buyersinfo4.$submitted && buyersinfo4.whom_other.$invalid">Other Description is required</span>
														</div>
													</div>
												</div>

												<div id="section_sell_current_house_accept_offer">
													<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo4.$submitted && buyersinfo4.accept_offer_date.$invalid]">
														<label class="control-label" for="sc_current_address">The date you want in the contract that you must accept an offer to sell your house. 
															<!-- <br><small>If you are unable to to accept an offer by this date then you will either need to get an extension from the seller or cancel the contract to buy the house. Otherwise you will be at risk to forfeit your earnest money.</small> -->
														</label>
														<input type="text" ng-model="form.data.accept_offer_date" name="accept_offer_date" id="accept_offer_date" class="form-control" date-picker required readonly />	
							
														<span class="text-red m-t-5 display-b" ng-if="buyersinfo4.$submitted && buyersinfo4.accept_offer_date.$invalid">This field is required</span>	
													</div>
												</div>

												<div id="section_must_sell_current_house_contract">
													<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo4.$submitted && buyersinfo4.must_sell_current_house_contract.$invalid]">
														<label class="control-label" for="sc_current_address">The date you want in the contract that you must sell your house by. 
															<!-- <br><small>If you are unable to to accept an offer by this date then you will either need to get an extension from the seller or cancel the contract to buy the house. Otherwise you will be at risk to forfeit your earnest money.</small> -->
														</label>
														<input type="text" ng-model="form.data.must_sell_current_house_contract" name="must_sell_current_house_contract" id="must_sell_current_house_contract" class="form-control" date-picker required readonly />	
							
														<span class="text-red m-t-5 display-b" ng-if="buyersinfo4.$submitted && buyersinfo4.must_sell_current_house_contract.$invalid">This field is required</span>	
													</div>
												</div>

												<div id="section_buying_no">		
													<div class="text-right">
														<button type="submit" class="btn-primary btn btn-md buying_no_btn bntorange"> Next </button>
													</div>
												</div>
											</div>
											<div class="animate-switch" ng-switch-when="no">
												<label class="control-label">Do you want to sell your home with Turbo and save thousands?</label>
												<ul id="listed_menu" class="navoffer nav-tabs">
													<li ng-class="{ 'active': form.data.sell_home_to_turbohomes == 'yes' }">
														<a href="#" ng-click="selectBuyingMenu5('yes', $event);">
															Yes
														</a>
													</li>
													<li ng-class="{ 'active': form.data.sell_home_to_turbohomes == 'no' }">
														<a href="#" ng-click="selectBuyingMenu5('no', $event);">
															No
														</a>
													</li>	
												</ul>

												<div id="section_sell_current_house_accept_offer">
													<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo2.$submitted && buyersinfo2.accept_offer_date.$invalid]">
														<label class="control-label" for="sc_current_address">The date you want in the contract that you must accept an offer to sell your house. 
															<!-- <br><small>If you are unable to to accept an offer by this date then you will either need to get an extension from the seller or cancel the contract to buy the house. Otherwise you will be at risk to forfeit your earnest money.</small> -->
														</label>
														<input type="text" ng-model="form.data.accept_offer_date" name="accept_offer_date" id="accept_offer_date" class="form-control" date-picker required readonly />	
							
														<span class="text-red m-t-5 display-b" ng-if="buyersinfo2.$submitted && buyersinfo2.accept_offer_date.$invalid">This field is required</span>		
													</div>
												</div>

												<div id="section_must_sell_current_house_contract">
													<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo2.$submitted && buyersinfo2.must_sell_current_house_contract.$invalid]">
														<label class="control-label" for="sc_current_address">The date you want in the contract that you must sell your house by. 
														<!-- 	<br><small>If you are unable to to accept an offer by this date then you will either need to get an extension from the seller or cancel the contract to buy the house. Otherwise you will be at risk to forfeit your earnest money.</small> -->
														</label>
														<input type="text" ng-model="form.data.must_sell_current_house_contract" name="must_sell_current_house_contract" id="must_sell_current_house_contract" class="form-control" date-picker required readonly />	
							
														<span class="text-red m-t-5 display-b" ng-if="buyersinfo2.$submitted && buyersinfo2.must_sell_current_house_contract.$invalid">This field is required</span>	
													</div>
												</div>

												<div id="section_buying_no">		
													<div class="text-right">
														<button type="submit" class="btn-primary btn btn-md buying_no_btn bntorange"> Next </button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="animate-switch" ng-switch-when="no">	
								<div class="text-right">
									<button class="btn-primary btn btn-md bntorange" type="submit"> Next </button>
								</div>
							</div>
							<div class="animate-switch" ng-switch-when="notsure">	
								{{-- <div class="form-group">
									<label class="control-label" for="sc_other_seller_name">You may request Attorney's Help for this process</label>
								</div> --}}
								<div class="text-right">
									<button class="btn-primary btn btn-md bntorange" type="submit"> Next </button>
								</div>
							</div>
						</div>
							
						<div class="text-right">
							<br>
							<button type="button" class="btn-default btn btn-md" ng-click="goBack()"> Back </button>
						</div>
					</div>
				</form>
			</div>
		</div>
		
		<div class="animate-switch" ng-switch-when="financing" ng-controller="financingCtrl as finCtrl">
			<div ng-switch="step">
				<form name="financing1" class="animate-switch" ng-switch-when="1" novalidate ng-submit="nextStep(financing1)">
					<div class="col-sm-12">
						<h3>Step 1</h3>
						
						<div class="form-group" ng-class="{true: 'has-error'}[financing1.$submitted && financing1.sc_financing_amt.$invalid]">
							<label class="control-label" for="sc_financing_amt">How much do you want to offer?</label>										
							<input type="text" ng-model="form.data.scfinancingamt" name="sc_financing_amt" id="sc_financing_amt" class="form-control" placeholder="Amount" ng-init="form.data.scfinancingamt = 0" required mask-money/>	
							
							<span class="text-red m-t-5 display-b" ng-if="financing1.$submitted && financing1.sc_financing_amt.$invalid">This field is required</span>	
						</div>	
						
						<div class="text-right">
							<button type="submit" class="btn-primary btn btn-md financing_one_next_btn bntorange"> Next </button>
						</div>	
					</div>
				</form>

				<form name="financing2" class="animate-switch" ng-switch-when="2" novalidate ng-submit="nextStep(financing2)">
					<div class="col-sm-12">
						<h3>Step 2</h3>

						<div class="form-group" ng-class="{true: 'has-error'}[financing2.$submitted && financing2.sc_financing_earnest_amt.$invalid]">
							<label class="control-label" for="sc_financing_earnest_amt">How much money do you want to offer in earnest? <small class="font-w-normal">Typically 1%</small></label>

							<input type="text" ng-model="form.data.scfinancingearnestamt" name="sc_financing_earnest_amt" id="sc_financing_earnest_amt" class="form-control" placeholder="Amount" required mask-money with-decimal />	
							
							<span class="text-red m-t-5 display-b" ng-if="financing2.$submitted && financing2.sc_financing_earnest_amt.$invalid">This field is required</span>	
						</div>	
						
						<div class="form-group">
							<label class="control-label">Earnest money is deposited in escrow account with a title/escrow company.<br> Do you have a title company you want to work with?</label>	
							<ul id="listed_menu" class="navoffer nav-tabs">
								<li ng-class="{ 'active': form.data.financingmenulistthreeanswer == 'yes' }">
									<a href="#" ng-click="financingmenu('yes', $event);">
										Yes
									</a>
								</li>
								<li ng-class="{ 'active': form.data.financingmenulistthreeanswer == 'no' }">
									<a href="#" ng-click="financingmenu('no', $event);">
										no
									</a>
								</li>	
							</ul>
						</div>

						<div class="tab-content" ng-switch="form.data.financingmenulistthreeanswer">
							<div class="animate-switch" ng-switch-when="yes">	
								<div class="form-group" ng-class="{true: 'has-error'}[financing2.$submitted && financing2.sc_financing_escrow_name.$invalid]">
									<label class="control-label" for="sc_financing_escrow_name">Company Name:</label>
									<input type="text" ng-model="form.data.scfinancingescrowname" name="sc_financing_escrow_name" id="sc_financing_escrow_name" class="form-control" required />	
							
									<span class="text-red m-t-5 display-b" ng-if="financing2.$submitted && financing2.sc_financing_escrow_name.$invalid">Company Name is required</span>
								</div>	
								
								<div class="form-group" ng-class="{true: 'has-error'}[financing2.$submitted && financing2.sc_financing_escrow_officer_name.$invalid]">
									<label class="control-label" for="sc_financing_escrow_officer_name">Officer Name:</label>
									<input type="text" ng-model="form.data.scfinancingescrowofficername" name="sc_financing_escrow_officer_name" id="sc_financing_escrow_officer_name" required class="form-control" />	
							
									<span class="text-red m-t-5 display-b" ng-if="financing2.$submitted && financing2.sc_financing_escrow_officer_name.$invalid">Officer Name is required</span>
								</div>
								
								<div class="form-group" ng-class="{true: 'has-error'}[financing2.$submitted && financing2.sc_financing_escrow_tel.$invalid]">
									<label class="control-label" for="sc_financing_escrow_tel">Telephone:</label>
									<input type="text" ng-model="form.data.scfinancingescrowtel" name="sc_financing_escrow_tel" id="sc_financing_escrow_tel" class="form-control" required />	
							
									<span class="text-red m-t-5 display-b" ng-if="financing2.$submitted && financing2.sc_financing_escrow_tel.$invalid">Telephone is required</span>
								</div>
							
								<div class="text-right">
									<button type="submit" class="btn-primary btn btn-md financing_three_next2_btn bntorange"> Next </button>
								</div>
							</div>
							<div class="animate-switch" ng-switch-when="no">
								<div class="text-right">
									<button type="submit" class="btn-primary btn btn-md financing_three_next2_btn bntorange"> Next </button>
								</div>	
							</div>
						</div>
							
						<div class="text-right">
							<br>
							<button type="button" class="btn-default btn btn-md" ng-click="goBack()"> Back </button>
						</div>
				
					</div>
				</form>
				
				<form name="financing3" class="animate-switch" ng-switch-when="3" novalidate ng-submit="nextStep(financing3)">
					<div class="col-sm-12">
						<h3>Step 3</h3>

						<div class="form-group">
							<label class="control-label">How do you plan on paying for the house?</label>	

							<div class="form-group" ng-class="{true: 'has-error'}[financing3.$submitted && financing3.scfinancingearnestamt.$invalid]">
								<label class="control-label" for="sc_property_address">Earnest Money</label>										
								<input type="text" ng-model="form.data.scfinancingearnestamt" name="scfinancingearnestamt" id="scfinancingearnestamt" class="form-control" required mask-money with-decimal />	
							
								<span class="text-red m-t-5 display-b" ng-if="financing3.$submitted && financing3.scfinancingearnestamt.$invalid">Earnest Money is required</span>
							</div>
			
							<div class="form-group" ng-class="{true: 'has-error'}[financing3.$submitted && financing3.down_payment.$invalid]">
								<label class="control-label" for="sc_property_address" ng-init="form.data.down_payment = (form.data.scfinancingamt * 0.02)">Cash/Down Payment</label>										
								<input type="text" ng-model="form.data.down_payment" name="down_payment" id="down_payment" class="form-control" required mask-money />	
							
								<span class="text-red m-t-5 display-b" ng-if="financing3.$submitted && financing3.down_payment.$invalid">Cash/Down Payment is required</span>
							</div>

							<div class="form-group">
								<label class="control-label" for="sc_property_address">Mortgage/Loan</label>										
								<input type="text" ng-model="form.data.mortgage" name="mortgage" id="mortgage" ng-init="form.data.mortgage = (form.data.scfinancingamt * 0.97)" class="form-control" mask-money />
							</div>

							<div class="form-group">
								<label class="control-label" for="sc_property_address">Other</label>										
								<input type="text" ng-model="form.data.planforpayingother" name="planforpayingother" id="planforpayingother" class="form-control" mask-money ng-init="form.data.planforpayingother = 0"/>
							</div>
						</div>	
						
						<div class="form-group" ng-if="form.data.mortgage > 0">
							<label class="control-label">Have you been prequalified?</label>	
							<ul id="listed_menu" class="navoffer nav-tabs">
								<li ng-class="{ 'active': form.data.financingmenulisttwoanswer == 'yes' }">
									<a href="#" ng-click="financingmenu4('yes', $event);">
										Yes
									</a>
								</li>
								<li ng-class="{ 'active': form.data.financingmenulisttwoanswer == 'no' }">
									<a href="#" ng-click="financingmenu4('no', $event);">
										No
									</a>
								</li>	
							</ul>
						</div>

						<div class="animate-switch">
							<div class="text-right">
								<button type="submit" class="btn-primary btn btn-md financing_three_next2_btn bntorange"> Next </button>
							</div>	
						</div>

						<div class="text-right">
							<br>
							<button type="button" class="btn-default btn btn-md" ng-click="goBack()"> Back </button>
						</div>
					</div>
				</form>
				
				<form name="financing4" class="animate-switch" ng-switch-when="4" novalidate ng-submit="nextStep(financing4)">
					<div class="col-sm-12">
						<h3>Step 4</h3>

						<div class="form-group o-hidden">
							<label class="control-label">Type of Financing <br><small class="font-w-normal">Most are conventional but a lender can help you determine the best kind for you</small></label>	
							<ul class="navoffer nav-tabs type-of-financing clearfix">
								<li ng-class="{ 'active': form.data.type_of_financing == 'conventional' }">
									<a href="#" ng-click="financingmenu5('conventional', $event)">
										Conventional
									</a>
								</li>
								<li ng-class="{ 'active': form.data.type_of_financing == 'fha' }">
									<a href="#" ng-click="financingmenu5('fha', $event)">
										FHA
									</a>
								</li>
								<li ng-class="{ 'active': form.data.type_of_financing == 'va' }">
									<a href="#" ng-click="financingmenu5('va', $event)">
										VA
									</a>
								</li>
								<li ng-class="{ 'active': form.data.type_of_financing == 'usda' }">
									<a href="#" ng-click="financingmenu5('usda', $event)">
										USDA
									</a>
								</li>
								<li ng-class="{ 'active': form.data.type_of_financing == 'assumption' }">
									<a href="#" ng-click="financingmenu5('assumption', $event)">
										Assumption
									</a>
								</li>
								<li ng-class="{ 'active': form.data.type_of_financing == 'seller_carryback' }">
									<a href="#" ng-click="financingmenu5('seller_carryback', $event)">
										Seller Carryback
									</a>
								</li>
								<li ng-class="{ 'active': form.data.type_of_financing == 'other' }">
									<a href="#" ng-click="financingmenu5('other', $event)">
										Other
									</a>
								</li>
								<li ng-class="{ 'active': form.data.type_of_financing == 'notsure' }" class="w-66-p">
									<a href="#" ng-click="financingmenu5('notsure', $event)">
										Not Sure Request Attorney Help
									</a>
								</li>
							</ul>
						</div>	
						
						<div class="form-group" ng-if="form.data.type_of_financing == 'other'" ng-class="{true: 'has-error'}[financing4.$submitted && financing4.type_of_financing_other.$invalid]">									
							<input type="text" ng-model="form.data.type_of_financing_other" id="type_of_financing_other" name="type_of_financing_other" class="form-control" placeholder="Specify" required />	
							
							<span class="text-red m-t-5 display-b" ng-if="financing4.$submitted && financing4.type_of_financing_other.$invalid">This field is required</span>
						</div>	

						<div class="animate-switch">
							<div class="text-right">
								<button type="submit" class="btn-primary btn btn-md financing_three_next2_btn bntorange"> Next </button>
							</div>	
						</div>

						<div class="text-right">
							<br>
							<button type="button" class="btn-default btn btn-md" ng-click="goBack()"> Back </button>
						</div>
					</div>
				</form>
				
				<form name="financing5" class="animate-switch" ng-switch-when="5" novalidate ng-submit="nextStep(financing5)">
					<div class="col-sm-12">
						<h3>Step 5</h3>

						<p>Closing Costs</p>

						<div class="form-group">
							<label class="control-label">Do you want the seller to help cover closing costs?</label>	
							<ul class="navoffer nav-tabs">
								<li ng-class="{ 'active': form.data.financingmenulistfouranswer == 'yes' }">
									<a href="#" ng-click="financingmenu6('yes', $event)">
										Yes
									</a>
								</li>
								<li ng-class="{ 'active': form.data.financingmenulistfouranswer == 'no' }">
									<a href="#" ng-click="financingmenu6('no', $event)">
										No
									</a>
								</li>
							</ul>
						</div>

						<div class="tab-content" ng-switch="form.data.financingmenulistfouranswer">
							<div class="animate-switch" ng-switch-when="yes">	
								<div class="form-group" ng-class="{true: 'has-error'}[financing5.$submitted && financing5.closingcostamount.$invalid]">
									<label class="control-label" for="closingcostamount">Amount:</label>										
									<input type="text" ng-model="form.data.closingcostamount" name="closingcostamount" id="closingcostamount" class="form-control" placeholder="Amount" required  />	
							
									<span class="text-red m-t-5 display-b" ng-if="financing5.$submitted && financing5.closingcostamount.$invalid">Amount is required</span>
								</div>

								<div class="text-right">
									<button type="submit" class="btn-primary btn btn-md financing_one_next_btn bntorange"> Next </button>
								</div>	
							</div>
							<div class="animate-switch" ng-switch-when="no">
								<div class="text-right">
									<button type="submit" class="btn-primary btn btn-md financing_one_next_btn bntorange"> Next </button>
								</div>		
							</div>
						</div>

						<div class="text-right">
							<br>
							<button type="button" class="btn-default btn btn-md" ng-click="goBack()"> Back </button>
						</div>
					</div>
				</form>
				
				<form name="financing6" class="animate-switch" ng-switch-when="6" novalidate ng-submit="nextSection(financing6)">
					<div class="col-sm-12">
						<h3>Step 6</h3>
						
						<p>Home Warranty</p>
						
						<div class="form-group">
							<label class="control-label">Do you want a home warranty included?  <br><small class="font-w-normal">It is not uncommon to have the seller pay for this.</small></label>
							<ul id="listed_menu" class="navoffer nav-tabs">
								<li ng-class="{ 'active': form.data.financingmenulistsixanswer == 'yes' }">
									<a href="#" ng-click="financingmenu7('yes', $event);">
										Yes
									</a>
								</li>
								<li ng-class="{ 'active': form.data.financingmenulistsixanswer == 'no' }">
									<a href="#" ng-click="financingmenu7('no', $event);">
										No
									</a>
								</li>
							</ul>
						</div>

						<div class="tab-content" ng-switch="form.data.financingmenulistsixanswer">
							<div class="animate-switch" ng-switch-when="yes">	
								<div class="form-group">
									<label class="control-label">Who will you have pay for the home warranty? <br><small class="font-w-normal">Although it is not uncommon for the seller to pay for this it is one more thing that will make the offer less attractive to the seller.</small></label>
									<ul id="listed_menu" class="navoffer nav-tabs">
										<li ng-class="{ 'active': form.data.financingmenulistsevenanswer == 'buyer' }">
											<a href="#" ng-click="financingmenu8('buyer', $event);">
												Buyer
											</a>
										</li>
										<li ng-class="{ 'active': form.data.financingmenulistsevenanswer == 'seller' }">
											<a href="#" ng-click="financingmenu8('seller', $event);">
												Seller
											</a>
										</li>
										<li ng-class="{ 'active': form.data.financingmenulistsevenanswer == 'split' }">
											<a href="#" ng-click="financingmenu8('split', $event);">
												Split evenly
											</a>
										</li>
									</ul>	
								</div>
								
								<div class="form-group" ng-class="{true: 'has-error'}[financing6.$submitted && financing6.costnotexceed.$invalid]">
									<label class="control-label" for="costnotexceed">Cost to not exceed <br><small class="font-w-normal">Typically $400-$500 depending on the size of house and coverage desired. You could quickly call a home warranty company in the Provider Marketplace to learn more.</small></label>										
									<input type="text" ng-model="form.data.costnotexceed" name="costnotexceed" id="costnotexceed" class="form-control" placeholder="Amount" required  />	
							
									<span class="text-red m-t-5 display-b" ng-if="financing6.$submitted && financing6.costnotexceed.$invalid">This field is required</span>
								</div>

								<div class="text-right" ng-if="form.data.financingmenulistsevenanswer">
									<button type="submit" class="btn-primary btn btn-md financing_three_next2_btn bntorange"> Next </button>
								</div>	
							</div>
							<div class="animate-switch" ng-switch-when="no">	
								<div class="text-right">
									<button type="submit" class="btn-primary btn btn-md financing_three_next2_btn bntorange"> Next </button>
								</div>	
							</div>
						</div>

						<div class="text-right">
							<br>
							<button type="button" class="btn-default btn btn-md" ng-click="goBack()"> Back </button>
						</div>
					</div>
				</form>

			</div>
		</div>

		<div class="animate-switch" ng-switch-when="import-dates" ng-controller="importDatesCtrl as impdCtrl">
			<div ng-switch="step">
				<form name="importdates1" class="animate-switch" ng-switch-when="1" novalidate ng-submit="nextStep(importdates1)">
					<div class="col-sm-12">

						<p>These dates create the timeline leading to the closing of your house.</p>
						<h3>Step 1</h3>

						<div class="form-group o-hidden">
							<label class="control-label">When do you want to hear back from the seller regarding this offer?</label>
							<ul class="navoffer nav-tabs type-of-financing clearfix">
								<li ng-class="{ 'active': form.data.importlistoneanswer == '24' }">
									<a href="#" ng-click="importdatesmenu('24', $event)">
										24 Hours
									</a>
								</li>
								<li ng-class="{ 'active': form.data.importlistoneanswer == '48' }">
									<a href="#" ng-click="importdatesmenu('48', $event)">
										48 Hours
									</a>
								</li>
								<li ng-class="{ 'active': form.data.importlistoneanswer == '72' }">
									<a href="#" ng-click="importdatesmenu('72', $event)">
										72 Hours
									</a>
								</li>
								<li ng-class="{ 'active': form.data.importlistoneanswer == 'other' }">
									<a href="#" ng-click="importdatesmenu('other', $event)">
										Other
									</a>
								</li>
							</ul>
						</div>

						<div class="form-group" ng-class="{true: 'has-error'}[importdates1.$submitted && importdates1.importlistoneanswerother.$invalid]" ng-if="form.data.importlistoneanswer == 'other'">				
							<input type="text" ng-model="form.data.importlistoneanswerother" name="importlistoneanswerother" id="importlistoneanswerother" class="form-control" placeholder="Other Specify" required date-picker />	
							
							<span class="text-red m-t-5 display-b" ng-if="importdates1.$submitted && importdates1.importlistoneanswerother.$invalid">This field is required</span>
						</div>

						<div class="text-right" ng-if="form.data.importlistoneanswer">
							<button type="submit" class="btn-primary btn btn-md financing_three_next2_btn bntorange"> Next </button>
						</div>
					</div>
				</form>

				<form name="importdates2" class="animate-switch" ng-switch-when="2" novalidate ng-submit="nextStep(importdates2)">
					<div class="col-sm-12">
						<h3>Step 2</h3>
						
						<div class="form-group" ng-class="{true: 'has-error'}[importdates2.$submitted && importdates2.inspectiondeadline.$invalid]">
							<label class="control-label">Due Diligence/Inspections Deadline <small class="font-w-normal">Typically 10 days</small></label>
							<input type="text" ng-model="form.data.inspectiondeadline" name="inspectiondeadline" id="inspectiondeadline" class="form-control" required  />	
							
							<span class="text-red m-t-5 display-b" ng-if="importdates2.$submitted && importdates2.inspectiondeadline.$invalid">This field is required</span>
						</div>

						<div class="text-right">
							<button type="submit" class="btn-primary btn btn-md financing_three_next2_btn bntorange"> Next </button>
						</div>

						<div class="text-right">
							<br>
							<button type="button" class="btn-default btn btn-md" ng-click="goBack()"> Back </button>
						</div>
					</div>
				</form>

				<form name="importdates3" class="animate-switch" ng-switch-when="3" novalidate ng-submit="nextStep(importdates3)">
					<div class="col-sm-12">
						<h3>Step 3</h3>

						<div class="form-group">
							<label class="control-label">Do you have an inspector selected? <br><small class="font-w-normal">Having the home professionally inspected is not required but we always recommend it. We have some great recommendations in the Provider Marketplace.</small></label>
							<ul id="listed_menu" class="navoffer nav-tabs">
								<li ng-class="{ 'active': form.data.inspectorselected == 'yes' }">
									<a href="#" ng-click="importdatesmenu2('yes', $event);">
										Yes
									</a>
								</li>
								<li ng-class="{ 'active': form.data.inspectorselected == 'no' }">
									<a href="#" ng-click="importdatesmenu2('no', $event);">
										No
									</a>
								</li>
							</ul>
						</div>

						<div class="text-right" ng-if="form.data.inspectorselected">
							<button type="submit" class="btn-primary btn btn-md financing_three_next2_btn bntorange"> Next </button>
						</div>

						<div class="text-right">
							<br>
							<button type="button" class="btn-default btn btn-md" ng-click="goBack()"> Back </button>
						</div>
					</div>
				</form>

				<form name="importdates4" class="animate-switch" ng-switch-when="4" novalidate ng-submit="nextSection(importdates4)">
					<div class="col-sm-12">
						<h3>Step 4</h3>
						<div class="form-group o-hidden" ng-init="form.data.closingdate = (form.data.mortgage > 0 ? '45' : '')">
							<label class="control-label">Closing Date <br><small class="font-w-normal">If you require a mortgage than this must be at least 45 days from this offer. Unless you put a specific date we will round to the nearest Monday through Thursday. Trying to close on a Friday can often not fund in time making you wait the weekend to get the keys. We don't want that.</small></label>
							<ul class="navoffer nav-tabs type-of-financing clearfix">
								<li ng-class="{ 'active': form.data.closingdate == '30' }" ng-if="!form.data.mortgage || form.data.mortgage == 0">
									<a href="#" ng-click="importdatesmenu3('30', $event)">
										30 Days
									</a>
								</li>
								<li ng-class="{ 'active': form.data.closingdate == '45' }">
									<a href="#" ng-click="importdatesmenu3('45', $event)">
										45 Days
									</a>
								</li>
								<li ng-class="{ 'active': form.data.closingdate == '60' }">
									<a href="#" ng-click="importdatesmenu3('60', $event)">
										60 Days
									</a>
								</li>
								<li ng-class="{ 'active': form.data.closingdate == '90' }" ng-if="form.data.mortgage > 0">
									<a href="#" ng-click="importdatesmenu3('90', $event)">
										90 Days
									</a>
								</li>
								<li ng-class="{ 'active': form.data.closingdate == 'other' }">
									<a href="#" ng-click="importdatesmenu3('other', $event)">
										Other
									</a>
								</li>
							</ul>
						</div>

						<div class="form-group" ng-class="{true: 'has-error'}[importdates4.$submitted && importdates4.closingdateother.$invalid]" ng-if="form.data.closingdate == 'other'">				
							<input type="text" ng-model="form.data.closingdateother" name="closingdateother" id="closingdateother" class="form-control" placeholder="Other Specify" required date-picker />	
							
							<span class="text-red m-t-5 display-b" ng-if="importdates4.$submitted && importdates4.closingdateother.$invalid">This field is required</span>
						</div>

						<div class="text-right" ng-if="form.data.closingdate">
							<button type="submit" class="btn-primary btn btn-md financing_three_next2_btn bntorange"> Next </button>
						</div>

						<div class="text-right">
							<br>
							<button type="button" class="btn-default btn btn-md" ng-click="goBack()"> Back </button>
						</div>
					</div>
				</form>
			</div>
		</div>
		
		<div class="animate-switch" ng-switch-when="inclusions" ng-controller="inclusionsCtrl as incCtrl">
			<div ng-switch="step">
				<form name="inclusions1" class="animate-switch" ng-switch-when="1" novalidate ng-submit="nextStep(inclusions1)">
					<div class="col-sm-12">
						<div class="form-group o-hidden">
							<label class="control-label">What "personal property" items do you want included in the sale? <br><small class="font-w-normal">Include a description of the items so they don't swap them out for something worse, e.g., make, model, color. Remeber that "fixtures" are included in the sale unless specifically excluded. Fixtures are property attached/affixed to the premises.</small></label>
						</div>

						<div class="checkbox">
							<label class="control-label"><input type="checkbox" ng-model="form.data.inclusion.refrigerator">Refrigerator</label>
						</div>
						<div ng-if="form.data.inclusion.refrigerator == true">
							<div class="form-group">
								<label class="control-label" for="sc_financing_escrow_name">Refrigerator Description (i.e. Make, Model, Color)</label>
								<input type="text" ng-model="form.data.inclusion.refrigeratordesc" class="form-control" />
							</div>
						</div>

						<div class="checkbox">
							<label class="control-label"><input type="checkbox" ng-model="form.data.inclusion.washer">Washer</label>
						</div>
						<div ng-if="form.data.inclusion.washer == true">
							<div class="form-group">
								<label class="control-label" for="sc_financing_escrow_name">Washer Description (i.e. Make, Model, Color)</label>
								<input type="text" ng-model="form.data.inclusion.washerdesc" class="form-control" />
							</div>
						</div>

						<div class="checkbox">
							<label class="control-label"><input type="checkbox" ng-model="form.data.inclusion.dryer">Dryer</label>
						</div>
						<div ng-if="form.data.inclusion.dryer == true">
							<div class="form-group">
								<label class="control-label" for="sc_financing_escrow_name">Dryer Description (i.e. Make, Model, Color)</label>
								<input type="text" ng-model="form.data.inclusion.dryerdesc" class="form-control" />
							</div>
						</div>

						<div class="checkbox">
							<label class="control-label"><input type="checkbox" ng-model="form.data.inclusion.aboveground">Above-ground spa/hot tub</label>
						</div>
						<div ng-if="form.data.inclusion.aboveground == true">
							<div class="form-group">
								<label class="control-label" for="sc_financing_escrow_name">spa/hot tub Description (i.e. Make, Model, Color)</label>
								<input type="text" ng-model="form.data.inclusion.abovegrounddesc" class="form-control" />
							</div>
						</div>

						<div class="checkbox">
							<label class="control-label"><input type="checkbox" ng-model="form.data.inclusion.other">Other</label>
						</div>
						<div ng-if="form.data.inclusion.other == true">
							<div class="form-group">
								<label class="control-label" for="sc_financing_escrow_name">Other Description (i.e. Make, Model, Color)</label>
								<input type="text" ng-model="form.data.inclusion.otherdesc" class="form-control" />
							</div>
						</div>

						<div class="text-right">
							<button type="submit" class="btn-primary btn btn-md financing_three_next2_btn bntorange m-b-20"> Next </button>
							<br>
							<button type="button" class="btn-default btn btn-md" ng-click="goBack()"> Back </button>
						</div>
					</div>
				</form>

				<form name="inclusions2" class="animate-switch" ng-switch-when="2" novalidate ng-submit="saveOffer(inclusions2)">
					<div class="col-sm-12">
						
						<p>You are almost done. Let us know if you would like to discuss this offer with a Turbo Attorney before moving forward.</p>

						<div class="checkbox">
							<label class="control-label"><input type="checkbox" ng-model="form.data.discusswithattorny">Discuss with a Turbo Attorney</label>
						</div>

						<div class="form-group">
							<label class="control-label" for="sc_financing_escrow_name">Any additional terms and conditions that you want to include</label>
							<textarea class="form-control" rows="8" ng-model="form.data.additionalcondition"></textarea>
						</div>
						
                        <div id="sc_spinner_container" class="ng-cloak" ng-if="onProgress">
                            <div class="spinner">
                                <div class="bounce1"></div>
                                <div class="bounce2"></div>
                                <div class="bounce3"></div>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary btn_submit btn-block btn-lg login-btn" ng-if="onProgress == false">Submit</button>

						<div class="text-right">
							<br>
							<button type="button" class="btn-default btn btn-md" ng-click="goBack()"> Back </button>
						</div>
					</div>
				</form>

				<form name="inclusions3" class="animate-switch" ng-switch-when="3" novalidate>
					<div class="col-sm-12">
						<h4 class="text-center">Your offer has been submitted.</h4>

						<p ng-if="!form.data.discusswithattorny">Congratulations! You have completed the initial information needed to create the offer. The Turbo Attorneys will complete the rest and email you a copy for review and signature.</p>

						<p ng-if="form.data.discusswithattorny">Congratulations! You have completed the initial information needed to create the offer. A Turbo Attorney will contact you to discuss your questions and then the Turbo Attorneys will complete the rest and email you a copy for review and signature.</p>
						
						<div class="text-center m-t-20">
							<a href="#" ng-click="reload($event);" class="btn btn-primary">
								Buyer Dashboard
							</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>	
</div>	

