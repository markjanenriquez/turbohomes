<div ng-controller="propertyvalueCtrl as proctrl" style="overflow: hidden;">
	
	<div class="col-sm-12">
		
		<div class="property_search_container" ng-init="address = undefined">
			
			<h3 class="text-center">Enter the address of the property you want to value.</h3>
			<div class="form-group">
				<!-- <input id="autocomplete" placeholder="Enter your address" onFocus="geolocate()" ng-model="myScopeVar" type="text" class="form-control"  ng-change="proctrl.property.submit(myScopeVar)"/>	 -->
				<input type="text" g-places-autocomplete ng-model="address" class="form-control" />
				<span class="text-danger m-t-10 display-b text-center" ng-if="$validation.address.invalid">
					Invalid Address
				</span>
			</div>
			<div class="text-center">
				<!-- <button type="button" class="btn btn-primary sc_get_property_report_btn" ng-click="proctrl.property.submit(myScopeVar)">Get Report</button> -->
				<button type="button" class="btn btn-primary bntorange" ng-click="save(address)" ng-if="!$variable.onprogress">Get Report</button>
			</div>
			
			<div id="sc_spinner_container" ng-if="$variable.onprogress">
				<div class="spinner">
					<div class="bounce1"></div>
					<div class="bounce2"></div>
					<div class="bounce3"></div>
				</div>
			</div>
		</div>
	</div>
	<br class="clear" />
	<form id="sc_zillow_form">
		<input type="hidden" name="action" value="sc_get_deep_comps" ng-model="proctrl.property.data.sc_get_deep_comps"/>
		<input type="hidden" id="street_number" name="street_number" ng-model="proctrl.property.data.street_number"/>
		<input type="hidden" id="route" name="route" ng-model="proctrl.property.data.route"/>
		<input type="hidden" id="locality" name="locality" ng-model="proctrl.property.data.locality"/>
		<input type="hidden" id="administrative_area_level_1" name="administrative_area_level_1" ng-model="proctrl.property.data.administrative_area_level_1"/>
		<input type="hidden" id="postal_code" name="postal_code" ng-model="proctrl.property.data.postal_code"/>
		<input type="hidden" id="country" name="country" ng-model="proctrl.property.data.country"/>
	</form>
	<br />
	<!-- <button ng-click="export()">Export</button> -->
	<div class="col-sm-12">
		<div id="display_data" class="panel panel-default" ng-if="$variable.submitted">
			<div class="panel-body ng-cloak" ng-if="!$validation.no_address_found && !$variable.onprogress">
				<div class="row property_report_container" id="report-lockup">
					<img src="/img/logo-01.png" height="65" alt="Turbo homes" class="thomes_img" />
					<img src="/img/Zillowlogo_200x50.gif" width="200" height="50" alt="Zillow Real Estate Search" class="zillow_img" />
					<h3 class="property_header">Property Value Report</h3>
					<p class="text-center" ng-bind="$variable.property_details.title"></p>
					<div class="col-md-6" ng-if="$variable.property_details.images">
						<div style="height : 405px">
							<div uib-carousel active="$variable.active" interval="$variable.myInterval" no-wrap="$variable.noWrapSlides">
								<div uib-slide ng-repeat="image in $variable.property_details.images track by $index" index="$index">
									<img ng-src="{[{image.url}]}" style="margin:auto;width:100%;height:405px">
									<div class="carousel-caption">
										<h4>Photo {[{ $index + 1 }]}</h4>
									</div>
								</div>
							</div>

							<div class="home-img-lockup" style="display: flex;align-items: center;height: 100%;" ng-if="!$variable.property_details.images.length">
								<img ng-src="img/600px-No_image_available.svg.png" style="margin:auto;width: 300px;">
							</div>
						</div>
					</div>
					<div ng-class="{'col-md-6' : $variable.property_details.images, 'col-md-12' : !$variable.property_details.images}">
						<table class="table table-bordered">
							<th colspan="2" class="no-padding">
								<h3 class="property_header no-margin">Property Details</h3>
							</th>
							<tr>
								<td>Finished Sq Ft</td>
								<td ng-bind="$variable.property_details.finishedSqFt"></td>
							</tr>
							<tr>
								<td>Bedrooms</td>
								<td ng-bind="$variable.property_details.bedrooms"></td>
							</tr>
							<tr>
								<td>Bathrooms</td>
								<td ng-bind="$variable.property_details.bathrooms"></td>
							</tr>
							<tr>
								<td>Year Built</td>
								<td ng-bind="$variable.property_details.yearBuilt"></td>
							</tr>
							<tr>
								<td>Lot Size SqFt</td>
								<td ng-bind="$variable.property_details.lotSizeSqFt"></td>
							</tr>
							<tr>
								<td>Last Sold Date</td>
								<td ng-bind="$variable.property_details.lastSoldDate |
								amDateFormat:'ddd, MMM. Do YYYY'"></td>
							</tr>
							<tr>
								<td>Last Sold Price</td>
								<td ng-bind="$variable.property_details.lastSoldPrice | currency"></td>
							</tr>
							<tr>
								<td>Tax Assessment Year</td>
								<td ng-bind="$variable.property_details.taxAssessmentYear"></td>
							</tr>
							<tr>
								<td>Tax Assessment</td>
								<td ng-bind="$variable.property_details.taxAssessment | currency"></td>
							</tr>
						</table>
					</div>
					<div id="zestimate_container">
						<div class="row">
							<div class="col-md-6">
								<table class="table table-bordered zestimate">
									<th colspan="3" class="no-padding">
										<h3 class="property_header no-margin">Zestimate Valuation</h3>
									</th>
									<tr>
										<td ng-bind="$variable.property_details.zestimate.valuationRange.low | currency"></td><td ng-bind="$variable.property_details.zestimate.amount | currency"></td>
										<td ng-bind="$variable.property_details.zestimate.valuationRange.high | currency"></td>
									</tr>
									<tr>
										<td>Low</td>
										<td>Zestimate</td>
										<td>High</td>
									</tr>
								</table>
								<p>Last Updated: {[{ $variable.property_details.zestimate['last-updated'] }]} </p>
							</div>
							<div class="col-md-6">
								<table class="table table-bordered zestimate">
									<th colspan="3" class="no-padding">
										<h3 class="property_header no-margin">Valuation Estimate Based on $/SqFt</h3>
									</th>
									<tr class="">
										<td ng-bind="::$variable.property_details.comps_valuation_low | currency"></td>
										<td ng-bind="::$variable.property_details.comps_valuation_average | currency"></td>
										<td ng-bind="::$variable.property_details.comps_valuation_high | currency"></td>
									</tr>
									<tr>
										<td>Low</td>
										<td>Average</td>
										<td>High</td>
									</tr>
								</table>
								<p>Calculation based on comparable properties</p>
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<h3 class="property_header no-margin">Change in Zestimate Values</h3>
							<div id="zillow_chart">
								<img ng-src="{[{ $variable.property_details.chart }]}" class="img-responsive" id="chart-img" style="width: 100%; visibility: hidden" />
								<canvas width="494" height="266" id="chart" style="display: none;"></canvas>
							</div>
						</div>
					</div>

					<div id="comparable_container" class="row">
						<h3 class="property_header">Comparable Properties</h3>
						<div class="table-responsive">
							<table class="table table-bordered table-striped comparables_table">
								<thead>
									<th>#</th>
									<th>Comp Score</th>
									<th>Street Address</th>
									<th>SqFt</th>
									<th>Bedroom</th>
									<th>Bathroom</th>
									<th>No. Of Rooms</th>
									<th>Year Built</th>
									<th>Lot Size</th>
									<th>Last Sold Date</th>
									<th>Last Sold Price</th>
									<th>$/SqFt</th>
								</thead>
								<tbody>
									<tr class="subject_property_row">
										<td> </td>
										<td> </td>
										<td class="nowrap" ng-bind-html="::$variable.property_details.main_propery_address | trustHtml"></td>
										<td ng-bind="::$variable.property_details.finishedSqFt"></td>
										<td ng-bind="::$variable.property_details.bedrooms"></td>
										<td ng-bind="::$variable.property_details.bathrooms"></td>
										<td ng-bind="::$variable.property_details.totalRooms"></td>
										<td ng-bind="::$variable.property_details.yearBuilt"></td>
										<td ng-bind="::$variable.property_details.lotSizeSqFt"></td>
										<td ng-bind="::$variable.property_details.lastSoldDate | amDateFormat:'ddd, MMM. Do YYYY'"></td>
										<td ng-bind="::$variable.property_details.lastSoldPrice | currency"></td>
										<td ng-bind="::$variable.property_details.main_propery_price_sqft | currency"></td>
									</tr>
									<tr ng-repeat="comp in $variable.property_details.comparables | orderBy : 'score': false">
										<td >{[{ $index + 1 }]}</td>
										<td ng-bind="::comp['@attributes']['score']"></td>
										<td class="nowrap" ng-bind-html="::(comp.address.street + ', <br/>' +  comp.address.city + ' '+ comp.address.state + ' ' + comp.address.zipcode) | trustHtml"></td>
										<td ng-bind="::comp.finishedSqFt"></td>
										<td ng-bind="::comp.bedrooms"></td>
										<td ng-bind="::comp.bathrooms"></td>
										<td ng-bind="::comp.totalRooms"></td>
										<td ng-bind="::comp.yearBuilt"></td>
										<td ng-bind="::comp.lotSizeSqFt"></td>
										<td ng-bind="::comp.lastSoldDate | amDateFormat:'ddd, MMM. Do YYYY'"></td>
										<td ng-bind="::comp.lastSoldPrice | currency"></td>
										<td ng-bind="::comp.pricesqft | currency"></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
					<div id="zillow_link_container" class="row">
						<div class="col-md-12">
							<h3 class="property_header">Learn More About These Properties</h3>
							<ul>
								<!-- <li><a href="{[{  $variable.property_details.links.graphsanddata }]}" target="_blank"> Graphs And Data </a></li> -->
								<li><a href="{[{  ::$variable.property_details.links.mapthishome }]}" target="_blank"> Listing Information </a></li>
								<li><a href="{[{  ::$variable.property_details.links.comparables }]}" target="_blank"> Comparables </a></li>
							</ul>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		<div id="sc_zillow_msg_container" ng-if="$validation.no_address_found">										<div class="alert alert-danger no-margin ng-cloak" >No Result Found on this address!</div>
	</div>
	
</div>
</div>