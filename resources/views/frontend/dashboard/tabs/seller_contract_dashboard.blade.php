<div id="contract_dashboard" class="sc_dashboard menu_page_container main_dashboard" data-ng-controller="contractDashboardCtrl">
	
	<h4 class="dashboard_main_title">Seller Dashboard</h4>
	<strong>Check off items as they are completed and see what's coming up next.</strong>
	<p class="p_title">Once you have accepted an offer to buy your home then you under contract. This is a legally binding document. You need to be aware of important dates in the contract and take appropriate action. This table summarizes some of the key dates. Turbo will also send automated reminders for these.</p>
	
    <a role="button" data-toggle="collapse" href="#seller-get-ready" aria-expanded="false" class="link-collapsible">Get Ready</a>
    <ul class="collapse in" id="seller-get-ready">
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="initial_paperwork" type="checkbox" value="initial_paperwork" ng-click="set($event)">
                <label class="pull-left" for="initial_paperwork"></label>
    			<div class="m-t-5">Complete <span class="or_highlight bolder_text">initial paperwork</span> and e-sign.</div>
    		</div>
    	</li>
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="seller_disclosure_form" type="checkbox" value="seller_disclosure_form" ng-click="set($event)">
                <label class="pull-left" for="seller_disclosure_form"></label>
    			<div class="m-t-5">Complete <span class="or_highlight bolder_text">Seller Disclosure form</span> and e-sign.</div>
    		</div>
    	</li>
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="temp_no" type="checkbox" value="temp_no" ng-click="set($event)">
                <label class="pull-left" for="temp_no"></label>
    			<div class="m-t-5">Turbo provides temporary <span class="or_highlight bolder_text">phone # and email</span>.</div>
    		</div>
    	</li>
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="for_sale_sign" type="checkbox" value="for_sale_sign" ng-click="set($event)">
                <label class="pull-left" for="for_sale_sign"></label>
    			<div class="m-t-5">Turbo puts up <span class="or_highlight bolder_text">'For Sale' sign</span> in your yard.</div>
    		</div>
    	</li>
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="photographs" type="checkbox" value="photographs" ng-click="set($event)">
                <label class="pull-left" for="photographs"></label>
    			<div class="m-t-5">Professional <span class="or_highlight bolder_text">photographs</span> taken by Turbo.</div>
    		</div>
    	</li>
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="key_lockbox" type="checkbox" value="key_lockbox" ng-click="set($event)">
                <label class="pull-left" for="key_lockbox"></label>
    			<div class="m-t-5">If needed, setup a <span class="or_highlight bolder_text">key lockbox</span> so potential buyers can access a key to see your house.</div>
    		</div>
    	</li>
    </ul>

    <a role="button" data-toggle="collapse" href="#seller-showing-offer" aria-expanded="false" class="link-collapsible">Showing & Offers</a>
    <ul class="collapse" id="seller-showing-offer">
    	<li class="reminder">
           <h4 class="or_highlight bolder_text">Reminder!</h4>
           <ul>
           	  <li><i class="fa fa-dot-circle-o"></i> Stage your house for every showing.</li>
           	  <li><i class="fa fa-dot-circle-o"></i> Make it easy to see. Try to accommodate times.</li>
           	  <li><i class="fa fa-dot-circle-o"></i> Be friendly. Make them want to work with you.</li>
           </ul>
        <li>
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="receive_an_offer" type="checkbox" value="receive_an_offer" ng-click="set($event)">
                <label class="pull-left" for="receive_an_offer"></label>
    	    	<div class="m-t-5"><span class="or_highlight bolder_text">Receive an offer:</span> If an offer goes directly to you please forward immediately.</div>
    		</div>
    	</li>
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="negotiate" type="checkbox" value="negotiate" ng-click="set($event)">
                <label class="pull-left" for="negotiate"></label>
    			<div class="m-t-5"><span class="or_highlight bolder_text">Turbo Attorneys</span> will help you <span class="or_highlight bolder_text">negotiate</span> the deal and complete the paperwork.</div>
    		</div>
    	</li>
    </ul>

    <a role="button" data-toggle="collapse" href="#seller-under-contract" aria-expanded="false" class="link-collapsible">Under Contract</a>
    <ul class="collapse" id="seller-under-contract">
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="confirm_earnest_money" type="checkbox" value="confirm_earnest_money" ng-click="set($event)">
                <label class="pull-left" for="confirm_earnest_money"></label>
    	    	<div class="m-t-5">Confirm receipt of <span class="or_highlight bolder_text">Earnest Money</span>. Ask Escrow Company for this.</div>
    		</div>
    	</li>
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="review_loan_status_update" type="checkbox" value="review_loan_status_update"  ng-click="set($event)">
                <label class="pull-left" for="review_loan_status_update"></label>
    			<div class="m-t-5">Review the buyer's <span class="or_highlight bolder_text">Loan Status Update</span> to ensure loan is on track. Buyer to send within 10 days of contract.</div>
    		</div>
    	</li>
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="seller_inspect_the_home" type="checkbox" value="seller_inspect_the_home"  ng-click="set($event)">
                <label class="pull-left" for="seller_inspect_the_home"></label>
    			<div class="m-t-5">Buyer has a set time period to <span class="or_highlight bolder_text">inspect the home</span> and request repairs or back out (typically 10 days). Your response to any request for repairs is time sensitive.</div>
    		</div>
    	</li>
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="appraisal" type="checkbox" value="appraisal" ng-click="set($event)">
                <label class="pull-left" for="appraisal"></label>
    			<div class="m-t-5">Buyer has a set time period after the <span class="or_highlight bolder_text">appraisal</span> to cancel the contract (typically 5 days).</div>
    		</div>
    	</li>
    </ul>

    <a role="button" data-toggle="collapse" href="#seller-closing" aria-expanded="false" class="link-collapsible">Closing</a>
    <ul class="collapse" id="seller-closing">
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="sign_all_loan_docs" type="checkbox" value="sign_all_loan_docs" ng-click="set($event)">
                <label class="pull-left" for="sign_all_loan_docs"></label>
    	    	<div class="m-t-5">3 days before close, <span class="or_highlight bolder_text">buyer must sign all loan docs</span> or cancel.</div>
    		</div>
    	</li>
    	<li>
    		<div class="col-xs-12">
    			<input class="styled-checkbox" id="seller_final_walkthrough" type="checkbox" value="seller_final_walkthrough" ng-click="set($event)">
                <label class="pull-left" for="seller_final_walkthrough"></label>
    			<div class="m-t-5"><span class="or_highlight bolder_text">Buyer final walkthrough</span> if they want.</div>
    		</div>
    	</li>
    	<li>
    		<div class="col-xs-12">
    			 <input class="styled-checkbox" id="seller_sign_final_docs" type="checkbox" value="seller_sign_final_docs" ng-click="set($event)">
                <label class="pull-left" for="seller_sign_final_docs"></label>
    			<div class="m-t-5">Schedule with Escrow company to  <span class="or_highlight bolder_text">sign final docs</span> and get the <span class="or_highlight bolder_text">KEYS!</span></div>
    		</div>
    	</li>
    </ul>
	
	<!-- <div class="col-md-4">
		<h3>Contact Repository</h3>
		<div id="contact_repository">
		</div>
	</div> -->
	
</div>	

