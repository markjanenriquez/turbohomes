@extends('frontend.layout.app')

@section('title', 'How to Sell with Turbo Homes') 

@section('page-css') 
    <link href="/libs/houzez-child/lib/assets/stylesheets/vendors64e9.css?v=1-0-6400-31987" rel="stylesheet" />
    <link href="/libs/houzez-child/lib/assets/stylesheets/app64e9.css?v=1-0-6400-31987" rel="stylesheet" />
    <link href="/libs/houzez-child/static-style.css" rel="stylesheet" /> 
@endsection

@section('page-js')
    <script type="text/javascript" src="/libs/houzez-child/lib/assets/javascripts/vendor/foundation.min.js"></script>

    <script>
        $(document).ready(function() {
            $(document).foundation();

            function nFormatter(num) {
                if (num >= 1000000) {
                    return (num / 1000000).toFixed(1).replace(/\.0$/, '') + 'M';
                }
                if (num >= 1000) {
                    return (num / 1000).toFixed(0).replace(/\.0$/, '') + 'K';
                }
                return num;
            }
            
            function addCommas(num) {
                num += '';
                x = num.split('.');
                x1 = x[0];
                x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
            }
            
            $('#moneySlider').on('moved.zf.slider', function () {
                var dollarAmount = Math.round($('#moneySliderHandle').attr('aria-valuenow'));
                var agentFee = Math.round(dollarAmount * .03);
                var seller = agentFee - 798; 
                $('#sliderValue').text(nFormatter(dollarAmount));
                $('#savingsAmount').text(addCommas(seller));
            });
        });
    </script>
@endsection

@section('content')
    <div id="sell-with-turbo" class="content-container main-page">
        <!--Video Section-->
        <section class="content-section video-section">
        	<div class="imgbanner" style="background-image: url('img/house.jpg');">
        		<div class="container">
        			<div class="row">
        				<div class="col-lg-12 m-t-20 mt-115">
        					<!-- <p class="featured-header">The new way to sell your home. <br> <span class="kicker">sell different. save more. the end.</span></p> -->
                            <p class="featured-header">sell different. save more. the end.</p>
                        </div>
        			</div>
                    <div class="row">
                    <br>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="col-sm-6">
                                <a href="#" class="buttonlink buttonlink-left" ng-click="scrollTo('#a-better-way');">HOW IT WORKS <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>

                             <div class="col-sm-6">
                                <a class="buttonlink buttonlink-right" href="/signup" >SIGN UP <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
        		</div>

        	</div>
        </section>
        <!--Video Section Ends Here-->

        <div class="row maincolor kicker-section">
            <div class="container">            
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <p class="kicker f-bold featured-header">Why use Turbo?</p>
                            <p class="kicker">It doesn’t cost what it once did to market a home. 
                            <br>We do it for less. Much less.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row container-gray-light" id="home-slider">
            <div class="container">            
                <div class="col-sm-12 mpc-column">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            @include('frontend.includes.seller-slider')
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row maincolor kicker-section">
            <div class="container" style="padding-top: 40px !important;padding-bottom: 40px !important;">
                <div class="wpb_wrapper">
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper">
                            <p class="kicker">Compare what we offer to what a real estate agent provides.
                            <br>Trust us...you won't go back.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="featured-section" style="padding-bottom: 0!important">
            <div class="row margin-top-large" id="a-better-way">
                <div class="column text-center">
                    <h4>How Turbo Works</h4>
                    <p class="margin-bottom-medium m-b-0">Turbo provides you all the tools you need to sell your own home. Our technology and real people support makes it easy and saves you thousands. Here's some of the support we provide.</p>
                </div>
            </div>


            <div class="row toggle">
                <div class="col-sm-12 p-40" style="text-align: center;" ng-init="acdn.showservice = 0">
                    <a class="ng-cloak" ng-if="acdn.showservice == 1" ng-click="acdn.showservice = 0" href="" data-toggle="collapse" data-target="#demo">Show Turbo Powered Service <i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
                    <a class="ng-cloak" ng-if="acdn.showservice == 0" ng-click="acdn.showservice = 1" href="" data-toggle="collapse" data-target="#demo">Hide Turbo Powered Service <i class="fa fa-angle-double-down" aria-hidden="true"></i></a>
                </div>
            </div>
            
     
            <div id="demo" class="collapse featured-section collapse in">
                
                <div class="featured-container">
                    <div class="bulletcons-inline margin-bottom-medium">
                        
                        <div class="gray-bg">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p class="m-b-30"><strong>Step 1:</strong> Sign up with Turbo to list your home for sale.</p>
                                </div>
                            </div>

                            <div class="row m-b-80">
                                <div class="col-sm-4">
                                    <div class="icon">
                                        <img src="img/sell-icons/Icons-01.png" />
                                    </div>
                                    <div class="text">
                                        <h5 class="bulletcon-inline-heading">Home Selling Guide for Every Step</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="icon">
                                        <img src="img/sell-icons/Icons-02.png" />
                                    </div>
                                    <div class="text">
                                        <h5 class="bulletcon-inline-heading">Home Value Reports</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="icon">
                                        <img src="img/sell-icons/Icons-11.png" />
                                    </div>
                                    <div class="text">
                                        <h5 class="bulletcon-inline-heading">For Sale Sign Posted</h5>
                                    </div>
                                </div>
                            </div>

                            <div class="row m-b-80">
                                <div class="col-sm-4">
                                    <div class="icon">
                                        <img src="img/sell-icons/Icons-03.png" />
                                    </div>
                                    <div class="text">
                                        <h5 class="bulletcon-inline-heading">Professional Photography</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="icon">
                                        <img src="img/sell-icons/Icons-04.png" />
                                    </div>
                                    <div class="text">
                                        <h5 class="bulletcon-inline-heading">Home Tour Video and Property Website</h5>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <div class="icon">
                                        <img src="img/sell-icons/Icons-05.png" />
                                    </div>
                                    <div class="text">
                                        <h5 class="bulletcon-inline-heading">Easy Tour Scheduling Tool</h5>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row m-b-80">
                                <div class="col-sm-offset-4 col-sm-4">
                                    <div class="icon">
                                        <img src="img/sell-icons/Icons-06.png" />
                                    </div>
                                    <div class="text">
                                        <h5 class="bulletcon-inline-heading">Listed on All Major Sites (MLS listing included thru partner)</h5>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <p class="m-b-30"><strong>Step 2:</strong> Review, negotiate and accept offers.</p>
                            </div>
                        </div>

                        <div class="row m-b-80">
                            <div class="col-sm-6">
                                <div class="icon">
                                    <img src="img/sell-icons/Icons-07.png" />
                                </div>
                                <div class="text">
                                    <h5 class="bulletcon-inline-heading">Turbo Real Estate Attorneys Review, Negotiate and Handle All Paperwork</h5>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="icon">
                                    <img src="img/sell-icons/Icons-08.png" />
                                </div>
                                <div class="text">
                                    <h5 class="bulletcon-inline-heading">Electronic Signatures (Makes it easy)</h5>
                                </div>
                            </div>
                        </div>

                        
                        <div class="gray-bg">
                            <div class="row">
                                <div class="col-sm-12">
                                    <p class="m-b-30"><strong>Step 3:</strong> Once you accept an offer you are under contract.</p>
                                </div>
                            </div>

                            <div class="row m-b-80">
                                <div class="col-sm-6">
                                    <div class="icon">
                                        <img src="img/sell-icons/Icons-09.png" />
                                    </div>
                                    <div class="text">
                                        <h5 class="bulletcon-inline-heading">Automated Contract Alerts</h5>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <div class="icon">
                                        <img src="img/sell-icons/Icons-10.png" />
                                    </div>
                                    <div class="text">
                                        <h5 class="bulletcon-inline-heading">Trusted Service Providers</h5>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-12">
                                <p class="m-b-0 f-35"><strong>Step 4:</strong> Close on the sale and collect the money!</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="column text-center margin-bottom-large margin-top-small" style="margin-bottom: 50px!important">           
                        <h4 class="margin-bottom-small text-gray-light">Have questions? Call (602) 845-9990<span class="nowrap"></span></h4>
                        <a href="/signup" target="_parent" class="button large" onclick="trackClick('Sign Up')">Get Started <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
        
       <!--  <div class="row container-gray-light" id="home-slider">
            <div class="container">            
                <div class="col-sm-12 mpc-column">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element ">
                                <div class="wpb_wrapper">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
        
    </div>
@endsection
