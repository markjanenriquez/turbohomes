@extends('frontend.layout.app')

@section('title', 'Privacy Policy')

@section('page-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/css/mdb.min.css" />
<style>

.title{
  font: bold 44px 'Helvetica'; 
  color: #6fb536;
}

.a-link{
  text-decoration: underline;
  color: #767171;
}

.numbered-title{
   font : bold 19px 'Corbel';
   color : #767171;
}

.ulList{
  padding-left: 35px;
  text-align: justify;
}

.ulList li{
   list-style-type: disc;
   color : #767171;
   font : 16px 'Corbel';
   line-height : 20px;
   margin-bottom: 5px;
}

.bolder{
   font-weight: bold;
}

.content{
   margin-top: 50px;
   margin-bottom: 20px;
   font: 16px 'Corbel';
   color: #636363;
   line-height: 19px;
}

@media only screen and (min-width: 320px) and (max-width: 374px)
{

  .title{
     font-size: 20px;
  }

  .section-container{
      padding-left: 0px;
      padding-right: 0px;
  }

}

@media only screen and (min-width: 375px) and (max-width: 424px)
{

  .title{
     font-size: 27px;
  }

  .section-container{
      padding-right: 0px;
  }

}

.navbar-nav .open .dropdown-menu {
    position: static!important;
}

</style>
@endsection

@section('page-js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/js/mdb.min.js"></script>
<script src="/ng-fe/controllers/contactus.js" type="text/javascript"></script>
@endsection

@section('content')
<div class="content-container" ng-controller="contactusCtrl"> 
    <div class="row" style="background-color:#ffffff">
        <div class="container">            
            <div class="col-sm-12 col-xs-12 mpc-column">                            
                <div class="container section-container">
                  
                  <div class="col-md-10 col-md-offset-1 content">
                       <p class="title">
                          TurboHomes Privacy Policy
                        </p>
                        <p>Last Updated: January 1, 2018</p>
                        <p>Thank you for using TurboHomes!</p>
                        <p class="text-justify">This Privacy Policy explains how we collect, use, process, and disclose your information across the TurboHomes Platform.</p>
                        <p class="text-justify">If you see an undefined term in this Privacy Policy (such as “Listing” or “TurboHomes Platform”), it has the same definition as outlined in our <a href="/termsandcondition" target="_blank" class="a-link">Terms of Service</a>.</p>
                        <p class="text-justify">
                          When this policy mentions “TurboHomes” or “we” or “us” or “our” it refers to TurboHomes LLC, an Arizona limited liability company that is responsible for your information under this Privacy Policy (the “<strong>Data Controller</strong>”).
                        </p>
                        <p>
                          This Privacy Policy also applies to the Payment Services provided to you by TurboHomes Payments pursuant to the <a href="#" target="_blank" class="a-link">Payments Terms of Service </a> (“<strong>Payments Terms</strong>”).
                        </p>
                        <p class="numbered-title">
                          1. Information we collect
                        </p>
                        <p class="text-justify">
                          There are three general categories of information we collect.
                        </p>
                        <p>
                          <ul class="ulList bolder">
                            <li>Information you give us.</li>
                            <li>Information we automatically collect from your use of the turbohomes platform.</li>
                            <li>Information we collect from third parties.</li>
                          </ul>
                        </p>
                        <p class="numbered-title">
                          1.1 Information you give us
                        </p>
                        <p class="text-justify">
                          We collect information you share with us when you use the TurboHomes Platform.
                        </p>
                        <p>
                          <ul class="ulList">
                           <li><strong>Account Information.</strong> When you sign up for an TurboHomes Account, we require certain information such as your name, email address, and date of birth.</li>

                           <li><strong>Profile Information.</strong> To use certain features within the TurboHomes Platform, we may also ask you to complete a profile, which may include your address, phone number, and gender. Certain parts of your profile (like your profile picture, first name, and description) are a part of your public profile page, and will be publicly visible to others.</li>

                           <li><strong>Other Authentication-Related Information.</strong> To help create and maintain a trusted environment, we may collect identification (like a photo of your government-issued ID) or another authentication information.</li>

                           <li><strong>Payment Information.</strong> We collect your financial information (like your bank account or credit card information) when you use the Payment Services to process payments.</li>

                           <li><strong>Communications with TurboHomes and other Members.</strong> When you communicate with TurboHomes or use the TurboHomes Platform to communicate with other Members, Guests, or Buyers we collect information about your communication and any information you choose to provide.</li>

                           <li><strong>Address Book Contact Information.</strong> You may choose to import your address book contacts or enter your contacts’ information manually to access certain features of the TurboHomes Platform, like inviting them to use TurboHomes.</li>

                           <li><strong>Other Information.</strong> You may otherwise choose to provide us information when you fill in a form, conduct a search, update or add information to your TurboHomes Account, respond to surveys, post to community forums, participate in promotions, or use other features of the TurboHomes Platform.</li>
                          </ul>
                        </p>
                        <p class="numbered-title">
                          1.2 Information We Automatically Collect from Your Use of the turbohomes Platform.
                        </p>
                        <p class="text-justify">
                          When you use the TurboHomes Platform, we collect information about the services you use and how you use them.
                        </p>
                        <p>
                          <ul class="ulList">
                            <li><strong>Usage Information.</strong> We collect information about your interactions with the TurboHomes Platform, such as the pages or other content you view, your searches for Listings, showings you have made, and other actions on the TurboHomes Platform.</li>

                            <li><strong>Location Information.</strong> When you use certain features of the TurboHomes Platform, we may collect different types of information about your general location (e.g. IP address, zip code) or more specific location information (e.g. precise location from your mobile GPS). Most mobile devices allow you to control or disable the use of location services for applications in the device’s settings menu.</li>

                            <li><strong>Log Data.</strong> We automatically collect log information when you use the TurboHomes Platform, even if you have not created an TurboHomes Account or logged in. That information includes, among other things: details about how you’ve used the TurboHomes Platform (including links to third party applications), IP address, access times, hardware and software information, device information, device event information (e.g., crashes, browser type), and the page you’ve viewed or engaged with before or after using the TurboHomes Platform.

                            <li><strong>Transaction Information.</strong> We collect information related to your transactions on the TurboHomes Platform, including the date and time, amounts charged, and other related transaction details.</li>

                            <li><strong>Cookies and Similar Technologies.</strong> We use cookies and other similar technologies, such as web beacons, pixels, and mobile identifiers. We may also allow our business partners to use these tracking technologies on the TurboHomes Platform, or engage others to track your behavior on our behalf. While you may disable the usage of cookies through your browser settings, we do not change our practices in response to a “Do Not Track” signal in the HTTP header from your browser or mobile application. For more information on our use of these technologies, see our <a href="#" class="a-link">Cookie Policy.</a></li>
                          </ul>
                        </p>
                        <p class="numbered-title">
                          1.3 Information We Collect from Third Parties.
                        </p>
                        <p class="text-justify">
                          We may collect information that others provide about you when they use the TurboHomes Platform, or obtain information from other sources and combine that with information we collect through the TurboHomes Platform.
                        </p>
                        <p>
                          <ul class="ulList">
                            <li><strong>Third Party Services.</strong> If you link, connect, or login to your TurboHomes Account with a third-party service (e.g., Facebook, Instagram), the third-party service may send us information such as your registration and profile information from that service. This information varies and is controlled by that service or as authorized by you via your privacy settings at that service.</li>

                            <li><strong>Background Information.</strong> For Members, Sellers, Guests, or Buyers in the United States, to the extent permitted by applicable laws, TurboHomes may obtain reports from public records of criminal convictions or sex offender registrations. We may use your information, including your full name and date of birth, to obtain such reports.</li>

                            <li><strong>Other Sources.</strong> To the extent permitted by applicable law, we may receive additional information about you, such as demographic data or fraud detection information, from third-party service providers and/or partners, and combine it with information we have about you. For example, we may receive background check results or fraud warnings from service providers like identity verification services for our fraud prevention and risk assessment efforts. We may receive information about you and your activities on and off the TurboHomes Platform through partnerships, or about your experiences and interactions from our partner ad networks.</li>
                          </ul>
                        </p>
                        <p class="numbered-title">
                           2. How we use information we collect
                        </p>
                        <p class="text-justify">
                           We use, store, and process information about you to provide, understand, improve, and develop the TurboHomes Platform, and to create and maintain a trusted and safer environment.
                        </p>
                         <p class="numbered-title">
                          2.1 Provide, Improve, and Develop the turbohomes Platform.
                        </p>
                        <p>
                          <ul class="ulList">
                            <li>Enable you to access and use the TurboHomes Platform.</li>
                            <li>Enable you to communicate with other Members, Guests, or Buyers.</li>
                            <li>Operate, protect, improve and optimize the TurboHomes Platform and experience, such as by performing analytics and conducting research.</li>
                            <li>Personalize or otherwise customize your experience by, among other things, ranking search results or showing ads based on your search, showing history, and preferences.</li>
                            <li>Enable you to access and use the Payment Services.</li>
                            <li>Provide customer service.</li>
                            <li>Send you service or support messages, such as updates, security alerts, and account notifications.</li>
                            <li>If you provide us with your contacts’ information, we may use and store this information: (i) to facilitate your referral invitations, (ii) send your requests for references, (iii) for fraud detection and prevention, and (iv) for any purpose you authorize at the time of collection.</li>
                          </ul>
                        </p>
                        <p class="numbered-title">
                          2.2 Create and Maintain a Trusted and Safer Environment.
                        </p>
                        <p>
                          <ul class="ulList">
                            <li>Detect and prevent fraud, spam, abuse, security incidents, and other harmful activity.</li>
                            <li>Conduct investigations and risk assessments.</li>
                            <li>Verify or authenticate information or identifications provided by you (such as to verify your Accommodation address or compare your identification photo to another photo you provide).</li>
                            <li>Conduct checks against databases and other information sources.</li>
                            <li>Comply with our legal obligations.</li>
                            <li>Resolve any disputes with any of our users and enforce our agreements with third parties.</li>
                            <li>Enforce our <a href="/termsandcondition" target="_blank" class="a-link">Terms of Service</a>, <a href="#" target="_blank" class="a-link">Payments Terms</a>, and other policies.</li>
                          </ul>
                        </p>
                        <p class="numbered-title">
                          2.3 Provide, Personalize, Measure, and Improve our Advertising and Marketing.
                        </p>
                        <p>
                          <ul class="ulList">
                            <li>Send you promotional messages, marketing, advertising, and other information that may be of interest to you based on your communication preferences (including information about TurboHomes or partner campaigns and services).</li>
                            <li>Personalize, measure, and improve advertising.</li>
                            <li>Administer referral programs, rewards, surveys, sweepstakes, contests, or other promotional activities or events sponsored or managed by TurboHomes or its third-party business partners.</li>
                          </ul>
                        </p>
                        <p class="numbered-title">
                          3. Sharing & disclosure
                        </p>
                        <p class="numbered-title">
                          3.1 With your consent.
                        </p>
                        <p class="text-justify">
                          We may share your information at your direction or as described at the time of sharing, such as when you authorize a third-party application or website to access your TurboHomes Account.
                        </p>
                        <p class="numbered-title">
                          3.2 Sharing between Sellers, Guests and Co-Sellers.
                        </p>
                        <p class="text-justify">
                          To help facilitate tours, we may share your information with other Members as follows:
                        </p>
                        <p>
                          <ul class="ulList">
                            <li>When you as a Guest submit a showing request, certain information about you is shared with the Seller (and Co-Seller, if applicable), including your full name, the full name of any guests, your cancellation history, and other information you agree to share. When your showing is confirmed, we will disclose additional information to assist with coordinating the tour, like your phone number.</li>
                            <li>When you as a Seller (or Co-Seller, if applicable) have a confirmed showing, certain information is shared with the guest to coordinate the tour, such as your full name, phone number, and Seller’s Home address.</li>
                            <li>When you as a Seller invite another Member to become a Co-Seller, you authorize the Co-Seller to access your information and Member Content, including but not limited to certain information like your full name, phone number, Seller’s Home address, and email address.</li>
                          </ul>
                        </p>
                        <p class="text-justify">We don’t share your billing information with other Members.</p>
                        <p class="numbered-title">
                            3.3 Profiles, Listings, and other Public Information.
                        </p>
                        <p class="text-justify">The TurboHomes Platform lets you publish information that is visible to the general public. For example:</p>
                        <p>
                          <ul class="ulList">
                            <li>Listing pages are publicly visible and include information such as the Seller’s Home approximate location (neighborhood and city), Listing description, calendar availability, aggregated demand information (like page views over a period of time), and any additional information the Seller chooses to share.</li>
                            <li>After completing a showing, Guests may write reviews. Reviews are a part of your public profile page.</li>
                            <li>If you submit content in a public forum or social media post, or use a similar feature on the TurboHomes Platform, that content is publicly visible.</li>
                          </ul>
                        </p>
                        <p class="text-justify">Information you share publicly on the TurboHomes Platform may be indexed through third-party search engines. In some cases, you may opt-out of this feature in your Account settings. If you change your settings or your public-facing content, these search engines may not update their databases. We do not control the practices of third party search engines, and they may use caches containing your outdated information.</p>
                        <p class="numbered-title">
                          3.4 Service providers.
                        </p>
                        <p class="text-justify">We use a variety of third-party service providers to help us provide services related to the TurboHomes Platform. For example, service providers may help us: (i) verify or authenticate your identification, (ii) check information against public databases, (iii) assist us with background checks, fraud prevention, and risk assessment, or (iv) provide customer service, advertising, or payments services. These providers have limited access to your information to perform these tasks on our behalf, and are contractually obligated to use it consistent with this Privacy Policy.</p>
                        <p class="numbered-title">
                          3.5 Additional services.
                        </p>
                        <p class="text-justify">
                          Seller may use third-party services available through the TurboHomes Platform to assist with managing their Listing or providing additional services, such as cleaning services, appraisals, inspections, mortgages, etc.
                        </p>
                        <p class="numbered-title">
                          3.6 Turbohomes payments.
                        </p>
                        <p class="text-justify">
                          TurboHomes may share your personal information to: (i) process your payment, (ii) facilitate your use of the TurboHomes Platform, and (iii) jointly market products or services to you with other financial third-parties with whom TurboHomes Payments has a formal agreement. You can unsubscribe or opt-out from receiving such marketing communications in your Account settings.
                        </p>
                        <p class="numbered-title">
                          3.7 Safety and compliance with law.
                        </p>
                        <p class="text-justify">
                          TurboHomes may disclose your information to courts, law enforcement, or governmental authorities, or authorized third-parties, if and to the extent we are required to do so by law or if such disclosure is reasonably necessary: (i) to comply with legal process and to respond to claims asserted against TurboHomes, (ii) to respond to verified requests relating to a criminal investigation or alleged or suspected illegal activity or any other activity that may expose us, you, or any other of our users to legal liability, (iii) to enforce and administer our Terms of Service1 or other agreements with Members, Buyers, or third-parties (iv) for fraud investigation and prevention, risk assessment, customer support, product development and debugging purposes, or (v) to protect the rights, property or personal safety of TurboHomes, its employees, its Members, or members of the public.
                        </p>
                        <p class="text-justify">
                          We will attempt to notify Members about these requests unless: (i) providing notice is prohibited by the legal process itself, by court order we receive, or by applicable law, or (ii) we believe that providing notice would be futile, ineffective, create a risk of injury or bodily harm to an individual or group, or create or increase a risk of fraud upon TurboHomes’s property, its Members and the Platform (collectively, “<strong>Risk Scenarios</strong>”). In instances where we comply with legal requests without notice for these reasons, we will attempt to notify that Member about the request after the fact if we determine in good faith that we are no longer legally prohibited from doing so and that no Risk Scenarios apply.
                        </p>
                        <p class="numbered-title">
                          3.8 Corporate affiliates.
                        </p>
                        <p class="text-justify">
                          To help us provide the TurboHomes Platform, we may share your information within our corporate family of companies (both financial and non-financial entities) that are related by common ownership or control. These related entities are covered under this Privacy Policy, and when services have additional terms specific to them, we tell you through the TurboHomes Platform.
                        </p>
                        <p class="numbered-title">
                          3.9 Widgets.
                        </p>
                        <p class="text-justify">
                          We may display parts of the TurboHomes Platform (e.g., your Listing page) on sites operated by TurboHomes’s business partners, using technologies such as widgets or APIs. If your Listings are displayed on a partner’s site, information from your public profile page may also be displayed.
                        </p>
                        <p class="numbered-title">
                          3.10 Aggregated data.
                        </p>
                        <p class="text-justify">
                          We may also share aggregated information (information about our users that we combine together so that it no longer identifies or references an individual user) and non-personally identifiable information for industry and market analysis, demographic profiling, marketing and advertising, and other business purposes.
                        </p>
                        <p class="numbered-title">
                          4. Other important information
                        </p>
                        <p class="numbered-title">
                          4.1 Analyzing your communications.
                        </p>
                        <p class="text-justify">
                          We may review, scan, or analyze your communications on the TurboHomes Platform for fraud prevention, risk assessment, regulatory compliance, investigation, product development, research, and customer support purposes. For example, as part of our fraud prevention efforts, we scan and analyze messages to mask contact information and references to other websites. In some cases, we may also scan, review, or analyze messages to debug, improve, and expand product offerings. We use automated methods where reasonably possible. However, occasionally we may need to manually review some communications, such as for fraud investigations and customer support, or to assess and improve the functionality of these automated tools. We will not review, scan, or analyze your communications to send third party marketing messages to you, and we will not sell reviews or analyses of these communications.
                        </p>
                        <p class="numbered-title">
                          4.2 Linking third-party accounts.
                        </p>
                        <p class="text-justify">
                          You may link your TurboHomes Account with your account at a third party social networking service. Your contacts on these third-party services are referred to as “Friends.” When you create this link:
                        </p>
                        <p>
                          <ul class="ulList">
                            <li>some of the information you provide to us from the linking of your accounts may be published on your TurboHomes Account profile;</li>
                            <li>your activities on the TurboHomes Platform may be displayed to your Friends on the TurboHomes Platform and/or that third party site;</li>
                            <li>a link to your public profile on that third party social networking service may be included in your TurboHomes public profile;</li>
                            <li>other TurboHomes users may be able to see any common Friends that you may have with them, or that you are a Friend of their Friend if applicable;</li>
                            <li>other TurboHomes users may be able to see any schools, hometowns or other groups you have in common with them as listed on your linked social networking service;</li>
                            <li>the information you provide to us from the linking of your accounts may be stored, processed and transmitted for fraud prevention and risk assessment purposes; and</li>
                            <li>the publication and display of information that you provide to TurboHomes through this linkage is subject to your settings and authorizations on the TurboHomes Platform and the third-party site.</li>
                          </ul>
                        </p>
                        <p class="numbered-title">
                           4.3 Google Maps/Earth.
                        </p>
                        <p class="text-justify">
                          Parts of the TurboHomes Platform use Google Maps/Earth services, including the Google Maps API(s). Use of Google Maps/Earth is subject to <a href="http://www.google.com/intl/en_us/help/terms_maps.html" target="_blank" class="a-link">Google Maps/Earth Additional Terms of Use</a> and the <a href="http://www.google.com/privacy.html" target="_blank" class="a-link">Google Privacy Policy</a>.
                        </p>
                        <p class="numbered-title">
                          5. THIRD-party partners & integrations
                        </p>
                        <p class="text-justify">
                          The TurboHomes Platform may contain links to third-party websites or services, such as third-party integrations, co-branded services, or third party-branded services (“<strong>Third Party Partners</strong>”). TurboHomes doesn’t own or control these Third-Party Partners and when you interact with them, you may be providing information directly to the Third-Party Partner, TurboHomes, or both. These Third-Party Partners will have their own rules about the collection, use, and disclosure of information. We encourage you to review the privacy policies of the other websites you visit.
                        </p>
                        <p class="numbered-title">
                          6. Your choices
                        </p>
                        <p class="numbered-title">
                          6.1 Access and update.
                        </p>
                        <p class="text-justify">
                          You may review, update, or delete the information in your TurboHomes Account by logging into your TurboHomes Account and reviewing your Account settings and profile.
                        </p>
                        <p class="numbered-title">
                          6.2 Account cancellation.
                        </p>
                        <p class="text-justify">
                          For information on how to cancel your TurboHomes Account, visit www.TurboHomes.com/help. Note that information that you have shared with others (like Reviews) may continue to be publicly visible on the TurboHomes Platform in association with your first name, even after your TurboHomes Account is cancelled.
                        </p>
                        <p class="numbered-title">
                          6.3 California & Vermont residents.
                        </p>
                        <p class="text-justify">
                          TurboHomes Payments will not share information it collects about you with their affiliates or third parties (both financial and non-financial), except as required or permitted by your state’s law.
                        </p>
                        <p class="numbered-title">
                          7. Security
                        </p>
                        <p class="text-justify">
                          We are continuously implementing and updating administrative, technical, and physical security measures to help protect your information against unauthorized access, loss, destruction, or alteration. However, the Internet is not a 100% secure environment so we can’t guarantee the security of the transmission or storage of your information.
                        </p>
                        <p class="numbered-title">
                          8. Changes to this privacy policy
                        </p>
                        <p class="text-justify">
                          TurboHomes reserves the right to modify this Privacy Policy at any time in accordance with this provision. If we make changes to this Privacy Policy, we will post the revised Privacy Policy on the TurboHomes Platform and update the “Last Updated” date at the top of this Privacy Policy. We will also provide you with notice of the modification by email at least thirty (30) days before the date they become effective. If you disagree with the revised Privacy Policy, you may cancel your Account. If you do not cancel your Account before the date the revised Privacy Policy becomes effective, your continued access or use of the TurboHomes Platform will constitute acceptance of the revised Privacy Policy.
                        </p>
                        <p class="numbered-title">
                          9. Contact
                        </p>
                        <p class="text-justify">
                           If you have any questions or complaints about this Privacy Policy or TurboHomes’s information handling practices, you may contact us at:
                        </p>
                        <p>
                          <ul class="ulList">
                            <li>TurboHomes LLC’s Legal Department: <a class="a-link" href="#">legal@turbohomes.com</a></li>
                          </ul>
                        </p>
                  </div>
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection