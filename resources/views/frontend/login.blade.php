@extends('frontend.layout.app')

@section('title', 'Login') 

@section('page-css')
@endsection

@section('page-js')
    <script src="/ng-be/factory/forgotpassword.js" type="text/javascript"></script>
@endsection

@section('content')
    <div id="login" class="content-container ng-cloak">
        <div class="row min-height-700" ng-controller="loginCtrl as lctrl">

            <div class="logincontainer" ng-if="loginView">
                <h2 class="t-center">Login</h2>

                <form id="sc_login_form" ng-submit="lctrl.login.submit()">                              
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <div class="input-group-addon loginicon">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </div>
                                    <input type="text" ng-model="lctrl.login.username" placeholder="Email Address" name="uname" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <div class="input-group-addon loginicon">
                                        <span class="glyphicon glyphicon-lock"></span>
                                    </div>

                                    <input type="password" ng-model="lctrl.login.password" placeholder="Password" name="pass" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12 text-left">                                         
                            <div class="form-group">
                                No account? <a href="/signup"> Sign Up </a> | <a href="#" ng-click="forgotpasswordToggle()">Forgot Password?</a>
                            </div>
                        </div>
                    </div>


                    <div class="row m-b-20">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <input type="hidden" name="action" value="sc_login_action">
                            <div id="sc_spinner_container" class="ng-cloak" ng-if="lctrl.login.onprogress">
                                <div class="spinner">
                                    <div class="bounce1"></div>
                                    <div class="bounce2"></div>
                                    <div class="bounce3"></div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn_submit btn-block btn-lg login-btn" ng-if="lctrl.login.onprogress == false">Login</button>
                        </div>
                    </div>
                </form>
            </div>

            <div class="logincontainer" ng-if="!loginView">
                <h2 class="t-center">Forgot Password</h2>
                <p class="text-center p-l-20 p-r-20 m-b-20">We will send you a reset password link. Please enter your email address below</p>
                <form id="sc_forgot_password_form" ng-submit="forgotpassword.submit(forgotpassword.data)">                              
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <div class="input-group input-group-lg">
                                    <div class="input-group-addon loginicon">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </div>
                                    <input type="text" ng-model="forgotpassword.data.email" placeholder="Email Address" name="uname" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row m-b-20">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <input type="hidden" name="action" value="sc_login_action">
                            <div id="sc_spinner_container" class="ng-cloak" ng-if="forgotpassword.onprogress">
                                <div class="spinner">
                                    <div class="bounce1"></div>
                                    <div class="bounce2"></div>
                                    <div class="bounce3"></div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn_submit btn-block btn-lg login-btn" ng-if="forgotpassword.onprogress == false">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

            
