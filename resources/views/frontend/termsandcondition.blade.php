@extends('frontend.layout.app')

@section('title', 'Terms and Conditions')

@section('page-css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/css/mdb.min.css" />
<style>

.title{
  font: bold 44px 'Helvetica'; 
  color: #6fb536;
}

.sub-title{
	color: #6fb536;
    font-weight: bold;
    font-size: 25px;
    margin-top: 18px;
}

.m-top-50{
	 margin-top: 50px;
}

.a-link{
  text-decoration: underline;
  color: #767171;
}

.content{
   margin-top: 50px;
   margin-bottom: 20px;
   font: 16px 'Corbel';
   color: #636363;
   line-height: 19px;
}

ol { counter-reset: item }
ol li{ display: block; text-align: justify; margin-bottom: 10px }
ol li:before { content: counters(item, ".") " "; counter-increment: item }

.bulletedList{
	 margin: 10px 0px 10px 30px;
}

.bulletedList p:before{
	content: "• ";
}

@media only screen and (min-width: 320px) and (max-width: 374px)
{

  .title{
     font-size: 20px;
  }

  .section-container{
      padding-left: 0px;
      padding-right: 0px;
  }

}

@media only screen and (min-width: 375px) and (max-width: 424px)
{

  .title{
     font-size: 27px;
  }

  .section-container{
      padding-right: 0px;
  }

}


.navbar-nav .open .dropdown-menu {
    position: static!important;
}

</style>
@endsection

@section('page-js')
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/js/mdb.min.js"></script>
<script src="/ng-fe/controllers/contactus.js" type="text/javascript"></script>
@endsection

@section('content')
<div  class="content-container" ng-controller="contactusCtrl"> 
    <div class="row" style="background-color:#ffffff">
        <div class="container">            
            <div class="col-sm-12 mpc-column">                            
                <div class="container section-container">
                	
                	<div class="col-md-10 col-md-offset-1 content">
                		<p class="title">
                           Terms of Service
                        </p>
                        <p class="text-justify">
                         	These Terms of Service were last updated: January 1, 2018.</p>
                        <p class="text-justify">Welcome to TurboHomes.com!</p>
                        <p class="text-justify">
                         	Please read these Terms of Service carefully as they contain important information about your legal rights, remedies, and obligations. By accessing or using the TurboHomes Platform, you agree to comply with and be bound by these Terms of Service.
                        </p>
                        <p class="text-justify">
                          	Important: Section 17 of these Terms of Service contains an arbitration clause and class action waiver that applies to all TurboHomes Users. It affects how disputes with TurboHomes are resolved.
                        </p>
                        <p class="text-justify">
                        	By accepting these Terms of Service, you agree to be bound by the terms, including the arbitration clause and class action waiver, any applicable supplemental terms—such as the <a class="a-link" href="#">Listing/Marketing Terms</a>, and Policies. Please read it carefully.
                        </p>
                        <p class="text-justify">
                        	These Terms of Service ("<strong>Terms</strong>") constitute a legally binding agreement (an "<strong>Agreement</strong>") between you and TurboHomes (as defined below) governing your access to and use of the TurboHomes website, including any subdomains thereof, and any other websites through which TurboHomes makes the TurboHomes Services available (collectively, "<strong>Site</strong>"), our mobile, tablet, and other smart device applications, and application program interfaces (collectively, "<strong>Application</strong>") and all associated services (collectively, "<strong>TurboHomes Services</strong>"). The Site, Application, and TurboHomes Services together are hereinafter collectively referred to as the TurboHomes Platform . Our other policies, such as our <a href="/privacyandpolicy" class="a-link" target="_blank">Privacy Policy</a>, are applicable to your use of the TurboHomes Platform are incorporated by reference into this Agreement.
                        </p>
                        <p class="text-justify">
                        	When these Terms mention <strong>TurboHomes</strong> or <strong>we</strong> or <strong>us</strong> or <strong>our</strong> , it refers to TurboHomes LLC, an Arizona limited liability company. TurboHomes is NOT a real estate brokerage. And TurboHomes is NOT acting as an agent in any capacity.
                        </p>
                        <p class="text-justify">
                        	Our collection and use of personal information in connection with your access to and use of the TurboHomes Platform is described in our <a href="/privacyandpolicy" class="a-link" target="_blank">Privacy Policy</a>.
                        </p>
                        <p class="text-justify">
                        	Any payment processing for TurboHome Services through or in connection with your use of the TurboHomes Platform ("<strong>Payment Services</strong>") are governed by our <a href="#" class="a-link" target="_blank">Payment Terms of Service</a> ("<strong>Payments Terms</strong>").	
                        </p>
                        <p class="text-justify">
                        	Additional supplemental terms may apply to sellers, including the Payment Terms. You understand you are under no obligation to use the TurboHomes Services or TurboHomes Platform, but if you do, additional applicable terms will also apply to your use of the TurboHomes Services and TurboHomes Platform.
                         </p>
                         <p class="text-justify">
							TurboHomes makes no representations that the TurboHomes Services are appropriate for you or that the TurboHome Services are available for use in your area. If you choose to access or use the TurboHomes Services or the TurboHomes Platform, you do so at your own risk. And you are responsible for identifying, understanding, and complying with all laws, rules, and regulations that apply to advertising, listing, selling, or buying a home. For example, some states may have laws that restrict a seller’s ability to list, offer, sale, or advertise their home. Some states may require a license to list or sale a private residence. Certain types of Seller Listings may be prohibited altogether. Penalties may include fines or other enforcement. We provide some information in our Help Center to help you identify some of the obligations that apply to you. If you have questions about how local laws apply to your use of TurboHomes Services, you should always seek legal guidance.
                        </p>
                        <p>
                        	<ol>
                        		<li>
                        			Scope of TurboHomes Services
                        			<ol>
                        				<li>
                        					The TurboHomes Platform is an online marketplace that enables registered users (each registered user, a <strong>Member</strong> ) to 1) advertise and offer to sale his/her/their home (a Member
											who advertises and offers to sale his/her/their home are referred to as <strong>Seller</strong> and the home advertised and offered for sale is the <strong>Seller’s Home</strong> ), 2) publish Seller’s Home on

											the TurboHomes Platform (a <strong>Listing</strong> ), 3) communicate and transact directly with individuals who (i) request information about the Listing, (ii) schedule a showing of Seller’s Home, or (iii) view Seller’s Home (individuals who perform at least one action described in 3)(i)-(iii) is a <strong>Guest</strong> ), and 4) offer to purchase or purchase Seller’s Home (a Guest who offer to purchase or actually purchase Seller’s Home is a <strong>Buyer</strong> ).
                        				</li>
                        				<li>
                        					As the provider of the TurboHomes Platform, TurboHomes does not own, create, sell, resell, provide, control, manage, offer, deliver, or supply any Listing. A Seller alone are responsible for their Listing. When a Buyer makes an offer to purchase or a Seller accepts an offer to purchase, Buyer and Seller are entering into a contract directly with each other. <i>TurboHomes is not and does not become a party to or other participant in any contractual relationship between a Buyer and a Seller. Again, TurboHomes is NOT a real estate broker or insurer. And TurboHomes is NOT acting as an agent in any capacity for any Buyer or Seller, except as specified in the <a href="#" class="a-link">Payments Terms</a>.</i>
                        				</li>
                        				<li>
                        					While we may help facilitate the resolution of disputes, TurboHomes has no control over and does not guarantee
                        					<ol>
                        						<li>
                        						 	the existence, quality, safety, suitability, or legality of any Listing,
                        					    </li>
                        					    <li>
                        					    	the truth or accuracy of any Listing descriptions or other Member Content (as defined below), or
                        					    </li>
                        					    <li>
                        					    	the performance or conduct of any Member or third-party.
                        					    </li>
                        					</ol>
                        				</li>
                        				<li>
                        					TurboHomes does not endorse any Buyer, Seller, or Listing. Any references to a Listing being "verified" (or similar language) only indicates that the Seller or Buyer has completed a relevant verification or identification process and nothing else. Any such description is not an endorsement, certification, or guarantee by TurboHomes about any Guest, Buyer, or Seller, including the Guest’s, Buyer’s, or Seller’s identity or background or whether the Guest, Buyer, or Seller is trustworthy, safe, or suitable. You should always exercise due diligence and care when deciding whether to make an offer to purchase or to accept an offer to purchase, or communicate and interact with Guest, Buyer, or Seller, whether online or in person. Verified Images (as defined below) are intended only to indicate a photographic representation of a Listing at the time the photograph was taken, and are therefore not an endorsement by TurboHomes of any Seller or Listing.
                        				</li>
                        				<li>
                        					If you choose to use the TurboHomes Platform, your relationship with TurboHomes is limited to being an independent, third-party contractor, and not an employee, agent, joint venture, or partner of TurboHomes for any reason, and you act exclusively on your own behalf and for your own benefit, and not on behalf, or for the benefit, of TurboHomes.
                        				</li>
                        				<li>
                        					To promote the TurboHomes Platform and to increase the exposure of Listings to potential Guests, Listings and other Member Content may be displayed on other websites, in applications, within emails, and in online and offline advertisements. To assist Sellers, Buyers, and Guests who speak different languages, Listings and other Member Content may be translated, in whole or in part, into other languages. TurboHomes cannot guarantee the accuracy or quality of such translations and Members are responsible for reviewing and verifying the accuracy of such translations. The TurboHomes Platform may contain translations powered by Google. Google disclaims all warranties related to the translations, express or implied, including any warranties of accuracy, reliability, and any implied warranties for merchantability, fitness for a particular purpose, and noninfringement.
                        				</li>
                        				<li>
                        					The TurboHomes Platform may contain links to third-party websites or resources ( <strong>Third-Party Services</strong> ). Such Third-Party Services may be subject to different terms and conditions and privacy practices. TurboHomes is not responsible or liable for the availability or accuracy of such Third-Party Services, or the content, products, or services available from such Third-Party Services. Links to such Third-Party Services are not an endorsement by TurboHomes of such Third-Party Services.
                        				</li>
                        				<li>
                        					Due to the nature of the Internet, TurboHomes cannot guarantee the continuous and uninterrupted availability and accessibility of the TurboHomes Platform. TurboHomes may restrict the availability of the TurboHomes Platform or certain areas or features thereof, if this is necessary in view of capacity limits, the security or integrity of our servers, or to carry out maintenance measures that ensure the proper or improved functioning of the TurboHomes Platform. TurboHomes may improve, enhance, and modify the TurboHomes Platform and introduce new TurboHomes Services from time to time.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Eligibility, Using the TurboHome Platform, Member Verification
                        			<ol>
                        				<li>
                        					You must be at least 18 years old and able to enter into legally binding contracts to access and use the TurboHomes Platform or register a TurboHomes Account. By accessing or using the TurboHomes Platform you represent and warrant that you are 18 years old (or older) and have the legal capacity and authority to enter into a contract.
                        				</li>
                        				<li>
                        					TurboHomes may make the access to and use of the TurboHomes Platform, or certain areas or features of the TurboHomes Platform, subject to certain conditions or requirements, such as completing a verification process, meeting specific quality or eligibility criteria, showing, and cancellation history.
                        				</li>
                        				<li>
                        					User verification on the Internet is difficult and we do not assume any responsibility for the confirmation of any Guest’s, Buyer’s, or Seller’s identity. Notwithstanding the above, for transparency and fraud prevention purposes, and as permitted by applicable laws, we may, but have no obligation to (i) ask Guests, Buyers, and Sellers, to provide a form of government identification or other information or undertake additional checks designed to help verify the identities or backgrounds of Guests, Buyers, or Sellers, (ii) screen Buyers or Sellers against third-party databases or other sources and request reports from service providers, and (iii) where we have sufficient information to identify a Guest, Buyer, or Seller, obtain reports from public records of criminal convictions or sex offender registrations or an equivalent version of background or registered sex offender checks in your local jurisdiction (if available).
                        				</li>
                        				<li>
                        					The access to or use of certain areas and features of the TurboHomes Platform may be subject to separate policies, standards or guidelines, or may require that you accept additional supplemental terms and conditions. If there is a conflict between these Terms and terms and conditions applicable to a specific area or feature of the TurboHomes Platform, the supplemental terms and conditions will indicate which terms will take precedence with respect to your access to or use of that area or feature.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Modification of these Terms
                        			<ol>
                        				<li>
                        					TurboHomes reserves the right to modify these Terms at any time in accordance with this provision. If we make changes to these Terms, we will post the revised Terms on the TurboHomes Platform and update the Last Updated date at the top of these Terms. We will also provide you with notice of the modifications by email at least thirty (30) days before the date they become effective. If you disagree with the revised Terms, you may terminate this Agreement with immediate effect. We will inform you about your right to terminate the Agreement in the notification email. If you terminate this Agreement, a refund, if any, will be awarded in accordance with our refund policy. If you do not terminate your Agreement before the date the revised Terms become effective, your continued access to or use of the TurboHomes Platform will constitute acceptance of the revised Terms.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Account Registration
                        			<ol>
                        				<li>
                        					You must register an account ("<strong>TurboHomes Account</strong>") to access and use certain features of the TurboHomes Platform, such as publishing a Listing. To establish a TurboHomes Account, you will provide TurboHomes with both personal, such as first and last name, email address, home address, telephone number, etc., and nonpersonal information. Personal and nonpersonal information will be used in accordance with our <a href="/privacyandpolicy" target="_blank" class="a-link">Privacy Policy</a>.
                        				</li>
                        				<li>
                        					You can register an TurboHomes Account using an email address and creating a password, or through your account with certain third-party social networking services, such as Facebook or Google ("<strong>SNS Account</strong>"). You have the ability to disable the connection between your TurboHomes Account and your SNS Account at any time, by accessing the "Settings" section of the TurboHomes Platform.
                        				</li>
                        				<li>
                        					If you are registering an TurboHomes Account for a company or other legal entity, you represent and warrant that you have the authority to legally bind that entity and grant us all permissions and licenses provided in these Terms.
                        				</li>
                        				<li>
                        					You must provide accurate, current, and complete information during the registration process and keep your TurboHomes Account and public TurboHomes Account profile page information up-to-date at all times.
                        				</li>
                        				<li>
                        					You may not register more than one (1) TurboHomes Account unless TurboHomes authorizes you to do so. You may not assign or otherwise transfer your TurboHomes Account to another party.
                        				</li>
                        				<li>
                        					You are responsible for maintaining the confidentiality and security of your TurboHomes Account credentials and may not disclose your credentials to any third party. You must immediately notify TurboHomes if you know or have any reason to suspect that your credentials have been lost, stolen, misappropriated, or otherwise compromised or in case of any actual or suspected unauthorized use of your TurboHomes Account. You are liable for any and all activities conducted through your TurboHomes Account, unless such activities are not authorized by you and you are not otherwise negligent (such as failing to report the unauthorized use or loss of your credentials).
                        				</li>
                        				<li>
                        					TurboHomes may enable features that allow you to authorize other Members or certain third parties to take certain actions that affect your TurboHomes Account. For example, we may allow Members associated with an Enterprise (as defined in our Privacy Policy) or who are designated as Co-Sellers (as defined below) to update, maintain, and help manage a Member’s Listing. These features do not require that you share your credentials with any other person. No third-party is authorized by TurboHomes to ask for your credentials, and you shall not request the credentials of another Member.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Content
                        			<ol>
                        				<li>
                        					TurboHomes may, at its sole discretion, enable Members to (i) create, upload, post, send, receive and store content, such as text, photos, audio, video, or other materials and information on or through the TurboHomes Platform ("<strong>Member Content</strong>"); and (ii) access and view Member Content and any content that TurboHomes itself makes available on or through the TurboHomes Platform, including proprietary TurboHomes content and any content licensed or authorized for use by or through TurboHomes from a third party ("<strong>TurboHomes Content</strong>" and together with Member Content, "<strong>Collective Content</strong>").
                        				</li>
                        				<li>
                        					The TurboHomes Platform, TurboHomes Content, and Member Content may in its entirety or in part be protected by copyright, trademark, and/or other laws of the United States and other countries. You acknowledge and agree that the TurboHomes Platform and TurboHomes Content, including all associated intellectual property rights, are the exclusive property of TurboHomes and/or its licensors or authorizing third-parties. You will not remove, alter, or obscure any copyright, trademark, service mark or other proprietary rights notices incorporated in or accompanying the TurboHomes Platform, TurboHomes Content, or Member Content. All trademarks, service marks, logos, trade names, and any other source identifiers of TurboHomes used on or in connection with the TurboHomes Platform and TurboHomes Content are trademarks or registered trademarks of TurboHomes. Trademarks, service marks, logos, trade names and any other proprietary designations of third-parties used on or in connection with the TurboHomes Platform, TurboHomes Content, and/or Collective Content are used for identification purposes only and may be the property of their respective owners.
                        				</li>
                        				<li>
                        					You will not use, copy, adapt, modify, prepare derivative works of, distribute, license, sell, transfer, publicly display, publicly perform, transmit, broadcast or otherwise exploit the TurboHomes Platform or Collective Content, except to the extent you are the legal owner of certain Member Content or as expressly permitted in these Terms. No licenses or rights are granted to you by implication or otherwise under any intellectual property rights owned or controlled by TurboHomes or its licensors, except for the licenses and rights expressly granted in these Terms.
                        				</li>
                        				<li>
                        					Subject to your compliance with these Terms, TurboHomes grants you a limited, non-exclusive, non-sublicensable, revocable, non-transferable license to (i) download and use the Application on your personal device(s); and (ii) access and view any Collective Content made available on or through the TurboHomes Platform and accessible to you, solely for your personal and non-commercial use.
                        				</li>
                        				<li>
                        					By creating, uploading, posting, sending, receiving, storing, or otherwise making available any Member Content on or through the TurboHomes Platform, you grant to TurboHomes a non-exclusive, worldwide, royalty-free, irrevocable, perpetual (or for the term of the protection), sub-licensable and transferable license to such Member Content to access, use, store, copy, modify, prepare derivative works of, distribute, publish, transmit, stream, broadcast, and otherwise exploit in any manner such Member Content to provide and/or promote the TurboHomes Platform, in any media or platform. Unless you provide specific consent, TurboHomes does not claim any ownership rights in any Member Content and nothing in these Terms will be deemed to restrict any rights that you may have to use or exploit your Member Content.
                        				</li>
                        				<li>
                        					TurboHomes may offer Seller the option of having professional photographers take photographs of their home, which are made available by the photographer to Seller to include in their Listing with or without a watermark or tag bearing the words "TurboHomes.com Verified Photo" or similar wording ("<strong>Verified Images</strong>"). You are responsible for ensuring that your Listing is accurately represented in the Verified Images and you will stop using the Verified Images on or through the TurboHomes Platform if they no longer accurately represent your Listing, if you stop advertising or offering your home for sale, or if your TurboHomes Account is terminated or suspended for any reason. You acknowledge and agree that TurboHomes shall have the right to use any Verified Images in advertising, marketing, and/or any other business purposes in any media or platform, whether in relation to your Listing or otherwise, without further notice or compensation to you. Where TurboHomes is not the exclusive owner of Verified Images, by using such Verified Images on or through the TurboHomes Platform, you grant to TurboHomes an exclusive, worldwide, royalty-free, irrevocable, perpetual (or for the term of the protection), sub-licensable, and transferable license to use such Verified Images for advertising, marketing, and/or any other business purposes in any media or platform, whether in relation to your Listing or otherwise, without further notice or compensation to you. TurboHomes in turn grants you a limited, non-exclusive, non-sublicensable, revocable, non-transferable license to use Verified Images outside of the TurboHomes Platform solely for your personal and non-commercial use.
                        				</li>
                        				<li>
                        					You are solely responsible for all Member Content that you make available on or through the TurboHomes Platform. Accordingly, you represent and warrant that
                        					<ol>
                        						<li>
                        							you are the sole and exclusive owner of all Member Content that you make available on or through the TurboHomes Platform; or
                        						</li>
                        						<li>
                        							you have all rights, licenses, consents and releases that are necessary to grant to TurboHomes the rights in and to such Member Content, as contemplated under these Terms; and
                        						</li>
                        						<li>
                        							neither the Member Content nor your posting, uploading, publication, submission or transmittal of the Member Content or TurboHomes's use of the Member Content (or any portion thereof) will infringe, misappropriate or violate a third party's patent, copyright, trademark, trade secret, moral rights or other proprietary or intellectual property rights, or rights of publicity or privacy, or result in the violation of any applicable law or regulation.
                        						</li>
                        					</ol>
                        				</li>
                        				<li>
                        					You will not post, upload, publish, submit or transmit any Member Content that
                        					<ol>
                        						<li>
                        							is fraudulent, false, misleading (directly or by omission or failure to update information) or deceptive;
                        						</li>
                        						<li>
                        							is defamatory, libelous, obscene, pornographic, vulgar or offensive;
                        						</li>
                        						<li>
                        							promotes discrimination, bigotry, racism, hatred, harassment or harm against any individual or group;
                        						</li>
                        						<li>
                        							is violent or threatening or promotes violence or actions that are threatening to any other person;
                        						</li>
                        						<li>
                        							promotes illegal or harmful activities or substances; or
                        						</li>
                        						<li>
                        							violates any other TurboHomes policy.
                        						</li>
                        					</ol>
                        				</li>
                        				<li>
                        					TurboHomes may, without prior notice, remove or disable access to any Member Content that TurboHomes finds to be in violation of these Terms or TurboHomes’s then-current policies or standards, or otherwise may be harmful or objectionable to TurboHomes, its Members, Guests, Buyers, third-parties, or property.
                        				</li>
                        				<li>
                        					TurboHomes respects copyright law and expects its Members to do the same. If you believe that any content on the TurboHomes Platform infringes copyrights you own, please notify us in accordance with our <a href="#" class="a-link">Copyright Policy</a>.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Service Fees
                        			<ol>
                        				<li>
                        					TurboHomes may charge fees to Seller ("<strong>Seller’s Fees</strong>") and/or Buyer ("<strong>Buyer’s Fees</strong>") (collectively, "<strong>Service Fees</strong>") in consideration for the use of the TurboHomes Platform. More information about Service Fees can be found in our <a href="#" class="a-link">Payment Terms</a>.
                        				</li>
                        				<li>
                        					Any applicable Service Fees (including any applicable Taxes) will be displayed to Seller prior to publishing a Listing and to Buyer prior to beginning the offer documents. TurboHomes reserves the right to change the Service Fees at any time, and we will provide Seller and Buyer adequate notice of any fee changes before they become effective.
                        				</li>
                        				<li>
                        					You are responsible for paying any Service Fees that you owe to TurboHomes. The applicable Service Fees are due and payable and collected by TurboHomes pursuant to the <a href="#" class="a-link">Payment Terms</a>. Except as otherwise provided on the TurboHomes Platform, Service Fees are non-refundable.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Terms specific for Sellers
                        			<ol>
                        				<li>
                        					Terms applicable to all Listings
                        					<ol>
                        						<li>
                        							When creating a Listing through the TurboHomes Platform you must
                        							<ol>
                        								<li>
                        									provide complete and accurate information about your Home (such as square footage, location, and other pertinent information),
                        								</li>
                        								<li>
                        									disclose any deficiencies, restrictions (such as HOA rules) and requirements that apply (such as any minimum age), and
                        								</li>
                        								<li>
                        									provide any other pertinent information requested by TurboHomes.
                        								</li>
                        							</ol>
                        						</li>
                        						<li>
                        							You are responsible for keeping your Listing information including viewing availability up-to-date at all times.
                        						</li>
                        						<li>
                        							You are responsible for promptly responding to any Guest inquiries and requests to view or show your home, including accepting, denying, and rescheduling requests to view or show your home received via text message.D
                        						</li>
                        						<li>
                        							You are solely responsible for setting a purchase price (including any closing costs and represented party fees, if applicable) for your Listing ( <strong>Offer Price</strong> ). Once a Buyer makes an offer to purchase your home, the terms of the purchase contract ( Purchase Contract Terms ) will set forth the procedures and processes governing acceptance, rejection, counter-offers, and exchanges ordinary and common in the sale and purchase of residential real estate in the jurisdiction in which the home is located.
                        						</li>
                        						<li>
                        							Any terms and conditions included in your Listing, in particular in relation to showings, must not conflict with these Terms or the showing policy you have selected for your Listing.
                        						</li>
                        						<li>
                        							Pictures, animations, or videos (collectively, "<strong>Images</strong>") used in your Listing must accurately reflect the quality and condition of Seller’s Home. TurboHomes reserves the right to require that Listings have a minimum number of Images of a certain format, size, and resolution.
                        						</li>
                        						<li>
                        							If TurboHomes offers Guests the ability to search Listing on the TurboHomes Platform, placement and ranking of Listing in search results on the TurboHomes Platform may vary and depend on a variety of factors, such as Guest search parameters and preferences, Seller requirements, price and availability, number and quality of Images, customer service and showing history, type of home, and/or lot size, etc.
                        						</li>
                        						<li>
                        							When Seller accept an offer to purchase by a Buyer, you are entering into a legally binding agreement with the Buyer and are required to sell your home to the Buyer as described in your Listing when the offer was made. You also agree to pay the applicable Seller’s Fee and any applicable closing costs and represented party fees, which will be collected pursuant to the <a href="#" class="a-link">Payments Terms</a>.
                        						</li>
                        						<li>
                        							TurboHomes recommends that a Seller obtain appropriate insurance for their showings and open houses, if applicable. Please review any respective insurance policy carefully, and in particular make sure that you are familiar with and understand any exclusions to, and any deductibles that may apply for, such insurance policy, including, but not limited to, whether or not your insurance policy will cover the actions or inactions of Guests (and the individuals the Guest may bring with them, if applicable) while looking through Seller’s Home.
                        						</li>
                        					</ol>
                        				</li>
                        				<li>
                        					Listing Information
                        					<ol>
                        						<li>
                        							You may only list one home per Listing.
                        						</li>
                        						<li>
                        							You represent and warrant that any Listing you publish and any contract you enter into with Guest or Buyer will
                        							<ol>
                        								<li>
                        									not breach any agreements you have entered into with any third parties, such as homeowner’s association, condominium, or other agreements, and
                        								</li>
                        								<li>
                        									comply with all applicable laws (such as zoning laws), ordnances, CC&Rs, rules, and regulations (including having all required permits, licenses, and registrations).
                        								</li>
                        							</ol>
                        						</li>
                        						<li>
                        							As Seller, you are responsible for your own acts and omissions and are also responsible for the acts and omissions of any individuals, including Guests and any individuals a Guest may invite to Seller’s Home, who reside at or are otherwise present at Seller’s Home at your request or invitation.
                        						</li>
                        					</ol>
                        				</li>
                        				<li>
                        					Co-Sellers
                        					<ol>
                        						<li>
                        							TurboHomes may enable Seller to authorize other Members ( <strong>Co-Sellers</strong> ) to administer the Seller’s Listing(s), and to bind the Seller and take certain actions in relation to the Listing(s) as permitted by the Seller, such as accepting showing requests, messaging, and welcoming Guests, and updating, managing, and maintaining the Listing and calendar availability (collectively, <strong>Co-Sellers Services</strong> ). Any agreement formed between Seller and Co-Seller may not conflict with these Terms, local laws, and the <a href="#" class="a-link">Payments Terms</a>. Co-Sellers may only act in an individual capacity and not on behalf of a company or other organization, unless expressly authorized by TurboHomes. TurboHomes reserves the right, in our sole discretion, to limit the number of Co-Sellers Seller may invite for each Listing and to limit the number of Listings Co-Sellers may manage.
                        						</li>
                        					</ol>
                        				</li>
                        				<li>
                        					Terms applicable to offer acceptance
                        					<ol>
                        						<li>
                        							Seller or Buyer may make an offer to purchase Seller’s Home using an agreement with the minimum statutory requirements required by the jurisdiction in which the Seller’s Home is located. Seller or Buyer may use TurboHome’s Purchase Contract. Acceptance of Buyer’s offer to purchase will be determined and are governed by the terms of acceptance contained in the purchase agreement agreed upon by the parties.
                        						</li>
                        					</ol>
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Terms specific for Buyers
                        			<ol>
                        				<li>
                        					Terms applicable to submitting an offer
                        					<ol>
                        						<li>
                        							Buyer may use Home Purchase Agreement and TurboHome Services to prepare and submit a Purchase Offer to Seller. Terms and conditions of acceptance and purchase including purchase price, earnest money, closing, and matters usual and ordinary to the sale and purchase of residential real estate are governed by the terms and conditions set forth in the Purchase Offer.
                        						</li>
                        					</ol>
                        				</li>
                        				<li>
                        					Terms applicable to Co-Buyers
                        					<ol>
                        						<li>
                        							All terms applicable to a Buyer also apply to any other person or entity with which the Buyer intends to or should be included in the Purchase Offer (a Co- Buyer ). Buyer and Co-Buyer individually and jointly understand and acknowledge that each is bound by these Terms and any additional applicable terms, and terms found in a purchase offer agreement govern the interactions between Guest, Buyer, and Seller.
                        						</li>
                        					</ol>
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Listing, Listing Modifications, Showings, Rescheduling, Resolution Center
                        			<ol>
                        				<li>
                        					Sellers are responsible for any modifications to a Listing that they publish via the TurboHomes Platform or otherwise direct a Co-Seller or TurboHomes customer service to make ("<strong>Listing Modifications</strong>"), and agree to pay any additional Listing Fees, Seller’s Fees, or Third-Party Service Fees associated with such Listing Modifications.
                        				</li>
                        				<li>
                        					Sellers can cancel a Listing at any time subject to the Listing’s cancellation policy, and TurboHomes Payments will provide any refund to Sellers in accordance with such cancellation policy.
                        				</li>
                        				<li>
                        					Sellers must input dates and times into the showing calendar located in the Seller’s Dashboard. Seller is responsible for maintain and updating availability and accept, reject, or propose an alternative showing time request you receive from a Guest. If must cancel an accepted showing appointment, Seller must notify Guest of cancelled showing. Seller is solely responsible for notifying Guest of any cancelled showing and for rescheduling any showing, if desired.
                        				</li>
                        				<li>
                        					TurboHomes may keep a record of showing availability, unavailability, accepted, rejected, cancelled, or rescheduled times and dates for showings. TurboHomes may cancel, suspend, or restrict a Seller’s TurboHome Account or Listing for unusual or unordinary, as determined solely by TurboHome’s, acceptance, rejection, or cancellation practices by Seller, unless the Sellers has a valid reason for cancelling the show or has legitimate concerns about the Guest’s behavior.
                        				</li>
                        				<li>
                        					In certain circumstances, TurboHomes may decide, in its sole discretion, that it is necessary to cancel a Listing, a TurboHomes Account, or a showing. Such instance may arise where TurboHomes believes in good faith, while taking the legitimate interests of both parties into account, this is necessary to avoid significant harm to TurboHomes, other Members, Guests, Buyer, Seller, third parties, or property or for any of the reasons set out in these Terms.
                        				</li>
                        				<li>
                        					Members may use the Resolution Center to send or request money for refunds, additional Seller’s Services, Co-Sellers Services, or Third-Party Services. You agree to pay all amounts sent through the Resolution Center in connection with your TurboHomes Account, and TurboHomes will handle all such payments pursuant to the <a href="#" class="a-link">Payments Terms</a>.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Damage to Home, Disputes between Members
                        			<ol>
                        				<li>
                        					Guest or Buyer are responsible for leaving the Seller’s Home (including any personal or other property located at the Seller’s Home) in the condition it was in when you arrived. Guests or Buyer are responsible for your own acts and omissions and are also responsible for the acts and omissions of any individuals whom you invite to, or otherwise provide access to, the Seller’s Home, excluding the Seller’s (and the individuals the Seller’s invites to the Seller’s Home, if applicable). Seller and Guest or Buyer are responsible for resolving any issue or dispute about damage to Seller’s Home or damage to or personal property missing from Seller’s Home.
                        				</li>
                        				<li>
                        					If you are a Guest, Buyer, Seller, or Co-Sellers, you understand and agree that TurboHomes may make a claim under your homeowner's, renter's, umbrella, or other insurance policy related to any damage or loss that you may have caused, or been responsible for, to Seller’s Home or any personal or other property located at Seller’s Home. You agree to cooperate with and assist TurboHomes in good faith, and to provide TurboHomes with such information as may be reasonably requested by TurboHomes, to make a claim under your homeowner's, renter's, umbrella, or other insurance policy, including, but not limited to, executing documents and taking such further acts as TurboHomes may reasonably request to assist TurboHomes in accomplishing the foregoing.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Purchase Price, Earnest Money, Closing Costs, Represented Buyer Costs
                        			<ol>
                        				<li>
                        					As Seller, you are solely responsible for determining the Purchase Price, Earnest Money, Closing Costs, and Represented Buyer Costs (in any), any tax implications and obligations, and lien or mortgage payoff obligations.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Prohibited Activities
                        			<ol>
                        				<li>
                        					You are solely responsible for compliance with any and all laws, rules, regulations, and obligations that may apply to your use of the TurboHomes Platform. In connection with your use of the TurboHomes Platform, you will not and will not assist or enable others to:
                        					<ol>
                        						<li>
                        							breach or circumvent any applicable laws or regulations, agreements with third- parties, third-party rights, or our Terms, Policies or Standards;
                        						</li>
                        						<li>
                        							use the TurboHomes Platform or Collective Content for any commercial or other purposes that are not expressly permitted by these Terms or in a manner that falsely implies TurboHomes endorsement, partnership or otherwise misleads others as to your affiliation with TurboHomes;
                        						</li>
                        						<li>
                        							copy, store, or otherwise access or use any information, including personally identifiable information about any other Member, Guest, or Buyer, contained on the TurboHomes Platform in any way that is inconsistent with TurboHomes’s <a href="/privacyandpolicy" target="_blank" class="a-link">Privacy Policy</a> or these Terms or that otherwise violates the privacy rights of Members, Guests, Buyers, or third parties;
                        						</li>
                        						<li>
                        							use the TurboHomes Platform in connection with the distribution of unsolicited commercial messages ("spam");
                        						</li>
                        						<li>
                        							offer, as Seller, any Seller’s Home that you do not yourself own or have permission to make available as a residential or other property through the TurboHomes Platform;
                        						</li>
                        						<li>
                        							unless TurboHomes explicitly permits otherwise, publish any Listing or schedule any tour if you will not actually be using the Seller Services yourself;
                        						</li>
                        					</ol>
                        				</li>
                        				<li>
                        					contact another Member for any purpose other than asking a question related to your showing, Listing, or a Member or Guests use of the TurboHomes Platform, including, but not limited to, recruiting or otherwise soliciting any Member, Guest, or Buyer to join third-party services, applications, or websites, without our prior written approval;
                        					<ol>
                        						<li>
                        							use the TurboHomes Platform to request, make or accept a tour independent of the TurboHomes Platform, to circumvent any Service Fees or for any other reason;
                        						</li>
                        						<li>
                        							request, accept, or make any payment for the Offer Price outside of the TurboHomes Platform or TurboHomes Payments. If you do so, you acknowledge and agree that you: (i) would be in breach of these Terms; (ii) accept all risks and responsibility for such payment, and (iii) hold TurboHomes harmless from any liability for such payment;
                        						</li>
                        						<li>
                        							discriminate against or harass anyone on the basis of race, national origin, religion, gender, gender identity, physical or mental disability, medical condition, marital status, age or sexual orientation, or otherwise engage in any abusive or disruptive behavior;
                        						</li>
                        						<li>
                        							use, display, mirror, or frame the TurboHomes Platform or Collective Content, or any individual element within the TurboHomes Platform, TurboHomes's name, any TurboHomes trademark, logo or other proprietary information, or the layout and design of any page or form contained on a page in the TurboHomes Platform, without TurboHomes's express written consent;
                        						</li>
                        						<li>
                        							dilute, tarnish, or otherwise harm the TurboHomes brand in any way, including through unauthorized use of Collective Content, registering, and/or using TurboHomes or derivative terms in domain names, trade names, trademarks, or other source identifiers, or registering and/or using domains names, trade names, trademarks, or other source identifiers that closely imitate or are confusingly similar to TurboHomes domains, trademarks, taglines, promotional campaigns, or Collective Content;
                        						</li>
                        						<li>
                        							use any robots, spider, crawler, scraper, or other automated means or processes to access, collect data, or other content from or otherwise interact with the TurboHomes Platform for any purpose;
                        						</li>
                        						<li>
                        							avoid, bypass, remove, deactivate, impair, descramble, or otherwise attempt to circumvent any technological measure implemented by TurboHomes or any of TurboHomes's providers or any other third party to protect the TurboHomes Platform;
                        						</li>
                        						<li>
                        							attempt to decipher, decompile, disassemble, or reverse engineer any of the software used to provide the TurboHomes Platform;
                        						</li>
                        						<li>
                        							take any action that damages or adversely affects, or could damage or adversely affect the performance or proper functioning of the TurboHomes Platform;
                        						</li>
                        						<li>
                        							export, re-export, import, or transfer the Application except as authorized by United States law, the export control laws of your jurisdiction, and any other applicable laws; or
                        						</li>
                        						<li>
                        							violate or infringe anyone else’s rights or otherwise cause harm to anyone.
                        						</li>
                        					</ol>
                        				</li>
                        				<li>
                        					You acknowledge that TurboHomes has no obligation to monitor the access to or use of the TurboHomes Platform by any Member or Guest or to review, disable access to, or edit any Member Content, but has the right to do so to (i) operate, secure and improve the TurboHomes Platform (including without limitation for fraud prevention, risk assessment, investigation, and customer support purposes); (ii) ensure Members’ or Guest’s compliance with these Terms; (iii) comply with applicable law or the order or requirement of a court, law enforcement, or other administrative agency or governmental body; (iv) respond to Member Content that it determines is harmful or objectionable; or (v) as otherwise set forth in these Terms. Members agree to cooperate with and assist TurboHomes in good faith, and to provide TurboHomes with such information and take such actions as may be reasonably requested by TurboHomes with respect to any investigation undertaken by TurboHomes or a representative of TurboHomes regarding the use or abuse of the TurboHomes Platform.
                        				</li>
                        				<li>
                        					If you feel that any Member or Guest you interact with, whether online or in person, is acting or has acted inappropriately, including but not limited to anyone who (i) engages in offensive, violent, or sexually inappropriate behavior, (ii) you suspect of stealing from you, or (iii) engages in any other disturbing conduct, you should immediately report such person to the appropriate authorities and then to TurboHomes by contacting us with your police station and report number (if available); provided that your report will not obligate us to take any action beyond that required by law (if any) or cause us to incur any liability to you.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Term and Termination, Suspension, and other Measures
                        			<ol>
                        				<li>
                        					This Agreement shall be effective for a 30-day term, at the end of which it will automatically and continuously renew for subsequent 30-day terms until such time when you or TurboHomes terminate the Agreement in accordance with this provision.
                        				</li>
                        				<li>
                        					You may terminate this Agreement at any time via the "Cancel Account" feature on the TurboHomes Platform or by sending us an email. If you cancel your TurboHomes Account as Seller, any confirmed showing(s) will be automatically cancelled and your Guest will receive a notice of cancellation. If Seller Cancel Account while negotiating a purchase of Seller’s Home with Buyer, any Earnest Money or other money held in earnest or escrow will depend upon the terms of the agreement of the parties.
                        				</li>
                        				<li>
                        					Without limiting our rights specified below, TurboHomes may terminate this Agreement for convenience at any time by giving you thirty (30) days' notice via email to your registered email address.
                        				</li>
                        				<li>
                        					TurboHomes may immediately, without notice terminate this Agreement if (i) you have materially breached your obligations under these Terms, <a class="a-link" href="#">Payments Terms</a>, or our Policies, (ii) you have violated applicable laws, regulations, or third party rights, or (iii) TurboHomes believes in good faith that such action is reasonably necessary to protect the personal safety or property of TurboHomes, its Members, Guests, or third parties (for example in the case of fraudulent behavior of a Member or Guest).
                        				</li>
                        				<li>
                        					In addition, TurboHomes may take any of the following measures (i) to comply with applicable law, or the order or request of a court, law enforcement or other administrative agency or governmental body, or if (ii) you have breached these Terms, the Payments Terms, or our Policies, applicable laws, regulations, or third party rights, (iii) you have provided inaccurate, fraudulent, outdated or incomplete information during the TurboHomes Account registration, Listing process or thereafter, (iv) you and/or your Listings or Seller Services at any time fail to meet any applicable quality or eligibility criteria, (v) TurboHomes otherwise becomes aware of or has received complaints about your performance or conduct, (vi) you have repeatedly cancelled confirmed showings or failed to respond to showing requests without a valid reason, or (vii) TurboHomes believes in good faith that such action is reasonably necessary to protect the personal safety or property of TurboHomes, its Members, Guests, or third parties, or to prevent fraud or other illegal activity:
                        					<div class="bulletedList">
                        						<p class="text-justify">
                        						   refuse to publish, delete, or delay any Listings or other Member Content;
                        					    </p>
                        					    <p class="text-justify">
                        					        cancel any pending or confirmed showing;
                        					    </p>
                        					    <p class="text-justify">
                        					    	limit your access to or use of the TurboHomes Platform;
                        					    </p>
                        					    <p class="text-justify">
                        					    	temporarily or permanently revoke any special status associated with your TurboHomes Account; or
                        					    </p>
                        					    <p class="text-justify">
                        					    	temporarily or in case of severe or repeated offenses permanently suspend your TurboHomes Account.
                        					    </p>
                        					</div>
                        					<p class="text-justify">
                        						In case of non-material breaches and where appropriate, you will be given notice of any intended measure by TurboHomes and an opportunity to resolve the issue to TurboHomes's reasonable satisfaction.
                        					</p>
                        				</li>
                        				<li>
                        					If we take any of the measures described above we may cancel your Listing and you will not be entitled to any compensation for pending or confirmed Listings that were cancelled.
                        				</li>
                        				<li>
                        					When this Agreement has been terminated, you are not entitled to a restoration of your TurboHomes Account or any of your Member Content. If your access to or use of the TurboHomes Platform has been limited or your TurboHomes Account has been suspended or this Agreement has been terminated by us, you may not register a new TurboHomes Account or access and use the TurboHomes Platform through an TurboHomes Account of another Member.
                        				</li>
                        				<li>
                        					If you or we terminate this Agreement, the clauses of these Terms that reasonably should survive termination of the Agreement will remain in effect.
                        				</li>
                        			</ol>
                        		</li>
                        		<i>
                        			<li>
                        				Disclaimers
                        				<ol>
                        					<li>
                        						If you choose to use the TurboHomes Platform or Collective Content, you do so voluntarily and at your sole risk. The TurboHomes Platform and Collective Content is provided as is , without warranty of any kind, either express or implied.
                        					</li>
                        					<li>
                        						You agree that you have had whatever opportunity you deem necessary to investigate the TurboHomes Services, laws, rules, or regulations that may be applicable to your Listings and/or Sellers Services you are receiving and that you are not relying upon any statement of law or fact made by TurboHomes relating to a Listing.
                        					</li>
                        					<li>
                        						If we choose to conduct identity verification or background checks on any Member, to the extent permitted by applicable law, we disclaim warranties of any kind, either express or implied, that such checks will identify prior misconduct by a Member, Guest, or Buyer or guarantee that a Member, Guest, or Buyer will not engage in misconduct in the future.
                        					</li>
                        					<li>
                        						You assume full responsibility for the choices you make before, during and after your participation in a showing of Seller’s Home. If you are bringing a minor as an additional guest, you are solely responsible for the supervision of that minor throughout the duration of your showing of Seller’s Home and to the maximum extent permitted by law, you agree to release and hold harmless TurboHomes from all liabilities and claims that arise in any way from any injury, death, loss, or harm that occurs to that minor during the showing of Seller’s Home or in any way related to your Seller Service.
                        					</li>
                        					<li>
                        						The foregoing disclaimers apply to the maximum extent permitted by law. You may have other statutory rights. However, the duration of statutorily required warranties, if any, shall be limited to the maximum extent permitted by law.
                        					</li>
                        				</ol>
                        			</li>
                        			<li>
                        				Liability
                        				<ol>
                        					<li>
                        						You acknowledge and agree that, to the maximum extent permitted by law, the entire risk arising out of your access to and use of the TurboHomes Platform and Collective Content, your publishing any Listing or scheduling any showing via the TurboHomes Platform, your showing of any Seller’s Home, or use of any other Seller Service or Third-Party Service, or any other interaction you have with other Members whether in person or online remains with you. Neither TurboHomes nor any other party involved in creating, producing, or delivering the TurboHomes Platform or Collective Content will be liable for any incidental, special, exemplary, or consequential damages, including lost profits, loss of data, or loss of goodwill, service interruption, computer damage, or system failure, or the cost of substitute products or services, or for any damages for personal or bodily injury or emotional distress arising out of or in connection with (i) these Terms, (ii) from the use of or inability to use the TurboHomes Platform or Collective Content, (iii) from any communications, interactions, or meetings with other Members, Guests, Buyers or other persons with whom you communicate, interact, or meet with as a result of your use of the TurboHomes Platform, or (iv) from your publishing of a Listing or scheduling a showing, including the provision or use of a Listing, Seller Services, whether based on warranty, contract, tort (including negligence), product liability, or any other legal theory, and whether or not TurboHomes has been informed of the possibility of such damage, even if a limited remedy set forth herein is found to have failed of its essential purpose. In no event will TurboHomes’s aggregate liability arising out of or in connection with these Terms and your use of the TurboHomes Platform including, but not limited to, from your publishing of any Listing or scheduling of any showing via the TurboHomes Platform, or from the use of or inability to use the TurboHomes Platform or Collective Content and in connection with any Seller’s Home, Seller Service, Third-Party Service or interactions with any other Members, Guests, or Buyer exceed the amounts you have paid or owe for viewing Listings via the TurboHomes Platform as a Guest in the twelve (12) month period prior to the event giving rise to the liability, or if you are Seller, the amounts paid by TurboHomes to you in the twelve (12) month period prior to the event giving rise to the liability, or one hundred U.S. dollars (US$100), if no such payments have been made, as applicable. The limitations of damages set forth above are fundamental elements of the basis of the bargain between TurboHomes and you. Some jurisdictions do not allow the exclusion or limitation of liability for consequential or incidental damages, so the above limitation may not apply to you. If you reside outside of the U.S., this does not affect TurboHomes’s liability for death or personal injury arising from its negligence, nor for fraudulent misrepresentation, misrepresentation as to a fundamental matter or any other liability which cannot be excluded or limited under applicable law.
                        					</li>
                        				</ol>
                        			</li>
                        		</i>
                        		<li>
                        		   Indemnification
                        		   <ol>
                        		   		<li>
                        		   			You agree to release, defend (at TurboHomes’s option), indemnify, and hold TurboHomes and its affiliates and subsidiaries, and their officers, directors, employees and agents, harmless from and against any claims, liabilities, damages, losses, and expenses, including, without limitation, reasonable legal and accounting fees, arising out of or in any way connected with (i) your breach of these Terms or our Policies, (ii) your improper use of the TurboHomes Platform or any TurboHomes Services, (iii) your interaction with any Member, Guest, or Buyer, showing at Seller’s Home, use of Third-Party Service, including without limitation any injuries, losses, or damages (whether compensatory, direct, incidental, consequential, or otherwise) of any kind arising in connection with or as a result of such interaction, showing, or use, or (iv) your breach of any laws, regulations, or third- party rights.
                        		   		</li>
                        		   </ol>
                        		</li>
                        		<li>
                        			Dispute Resolution and Arbitration Agreement
                        			<ol>
                        				<li>
                        					This Dispute Resolution and Arbitration Agreement shall apply if you (i) reside in the United States; or (ii) do not reside in the United States, but bring any claim against TurboHomes in the United States (to the extent not in conflict with Section 19).
                        				</li>
                        				<li>
                        					<i>Overview of Dispute Resolution Process</i>. TurboHomes is committed to participating in a consumer-friendly dispute resolution process. To that end, these Terms provide for a two-part process for individuals to whom Section 17.1 applies: (1) an informal negotiation directly with TurboHomes’s customer service team, and (2) a binding arbitration administered by the American Arbitration Association ( <strong>AAA</strong> ) using its specially designed Consumer Arbitration Rules (as modified by this Section 17). Specifically, the process provides:
                        					<div class="bulletedList">
                        						<p class="text-justify">
                        							Claims can be filed with AAA online (<a href="www.adr.org" class="a-link" target="_blank">www.adr.org</a>);
                        						</p>
                        						<p class="text-justify">
                        							Arbitrators must be neutral and no party may unilaterally select an arbitrator;
                        						</p>
                        						<p class="text-justify">
                        							Arbitrators must disclose any bias, interest in the result of the arbitration, or relationship with any party;
                        						</p>
                        						<p class="text-justify">
                        							Parties retain the right to seek relief in small claims court for certain claims, at their option;
                        						</p>
                        						<p class="text-justify">
                        							he initial filing fee for the consumer is capped at $200;
                        						</p>
                        						<p class="text-justify">
                        							The consumer gets to elect the hearing location and can elect to participate live, by phone, video conference, or, for claims under $25,000, by the submission of documents;
                        						</p>
                        						<p class="text-justify">
                        							The arbitrator can grant any remedy that the parties could have received in court to resolve the party’s individual claim.
                        						</p>
                        					</div>
                        				</li>
                        				<li>
                        					<i>Pre-Arbitration Dispute Resolution and Notification</i>. Prior to initiating an arbitration, you and TurboHomes each agree to notify the other party of the dispute and attempt to negotiate an informal resolution to it first. We will contact you at the email address you have provided to us; you can contact TurboHomes’s customer service team by <a href="#" class="a-link">emailing us</a>. If after a good faith effort to negotiate one of us feels the dispute has not and cannot be resolved informally, the party intending to pursue arbitration agrees to notify the other party via email prior to initiating the arbitration. In order to initiate arbitration, a claim must be filed with the AAA and the written Demand for Arbitration (available at <a href="www.adr.org" class="a-link" target="_blank">www.adr.org</a>) provided to the other party, as specified in the AAA Rules.
                        				</li>
                        				<li>
                        					<i>Agreement to Arbitrate</i>. You and TurboHomes mutually agree that any dispute, claim or controversy arising out of or relating to these Terms or the breach, termination, enforcement or interpretation thereof, or to the use of the TurboHomes Platform, the Host Services, or the Collective Content (collectively, Disputes ) will be settled by binding arbitration (the Arbitration Agreement ). If there is a dispute about whether this Arbitration Agreement can be enforced or applies to our Dispute, you and TurboHomes agree that the arbitrator will decide that issue.
                        				</li>
                        				<li>
                        					<i>Exceptions to Arbitration Agreement</i>. You and TurboHomes each agree that the following claims are exceptions to the Arbitration Agreement and will be brought in a judicial proceeding in a court of competent jurisdiction: (i) Any claim related to actual or threatened infringement, misappropriation or violation of a party’s copyrights, trademarks, trade secrets, patents, or other intellectual property rights; (ii) Any claim seeking emergency injunctive relief based on exigent circumstances (e.g., imminent danger or commission of a crime, hacking, cyber-attack).
                        				</li>
                        				<li>
                        					<i>Arbitration Rules and Governing Law</i>. This Arbitration Agreement evidences a transaction in interstate commerce and thus the Federal Arbitration Act governs the interpretation and enforcement of this provision. The arbitration will be administered by AAA in accordance with the Consumer Arbitration Rules (the <strong>AAA Rules</strong> ) then in effect, except as modified here. The AAA Rules are available at <a href="www.adr.org" class="a-link" target="_blank">www.adr.org</a> or by calling the AAA at 1–800–778–7879.
                        				</li>
                        				<li>
                        					<i>Modification to AAA Rules - Arbitration Hearing/Location</i>. In order to make the arbitration most convenient to you, TurboHomes agrees that any required arbitration hearing may be conducted, at your option, (a) in the county where you reside; (b) in Maricopa County; (c) in any other location to which you and TurboHomes both agree; (d) via phone or video conference; or (e) for any claim or counterclaim under $25,000, by solely the submission of documents to the arbitrator.
                        				</li>
                        				<li>
                        					<i>Modification of AAA Rules - Attorney’s Fees and Costs</i>. You may be entitled to seek an award of attorney fees and expenses if you prevail in arbitration, to the extent provided under applicable law and the AAA rules. Unless the arbitrator determines that your claim was frivolous or filed for the purpose of harassment, TurboHomes agrees it will not seek, and hereby waives all rights it may have under applicable law or the AAA rules, to recover attorneys’ fees and expenses if it prevails in arbitration.
                        				</li>
                        				<li>
                        					<i>Arbitrator’s Decision</i>. The arbitrator’s decision will include the essential findings and conclusions upon which the arbitrator based the award. Judgment on the arbitration award may be entered in any court with proper jurisdiction. The arbitrator may award declaratory or injunctive relief only on an individual basis and only to the extent necessary to provide relief warranted by the claimant’s individual claim.
                        				</li>
                        				<li>
                        					<i>Jury Trial Waiver</i>. You and TurboHomes acknowledge and agree that we are each waiving the right to a trial by jury as to all arbitrable Disputes.
                        				</li>
                        				<li>
                        					<i>No Class Actions or Representative Proceedings</i>. You and TurboHomes acknowledge and agree that we are each waiving the right to participate as a plaintiff or class member in any purported class action lawsuit, class-wide arbitration, private attorney-general action, or any other representative proceeding as to all Disputes. Further, unless you and TurboHomes both otherwise agree in writing, the arbitrator may not consolidate more than one party’s claims and may not otherwise preside over any form of any class or representative proceeding. If this paragraph is held unenforceable with respect to any Dispute, then the entirety of the Arbitration Agreement will be deemed void with respect to such Dispute.
                        				</li>
                        				<li>
                        					<i>Severability</i>. Except as provided in Section 17.11, in the event that any portion of this Arbitration Agreement is deemed illegal or unenforceable, such provision shall be severed and the remainder of the Arbitration Agreement shall be given full force and effect.
                        				</li>
                        				<li>
                        					<i>Changes</i>. Notwithstanding the provisions of Section 3 ( Modification of these Terms ), if TurboHomes changes this Section 17 ( Dispute Resolution and Arbitration Agreement ) after the date you last accepted these Terms (or accepted any subsequent changes to these Terms), you may reject any such change by sending us written notice (including by email) within thirty (30) days of the date such change became effective, as indicated in the Last Updated date above or in the date of TurboHomes’s email to you notifying you of such change. By rejecting any change, you are agreeing that you will arbitrate any Dispute between you and TurboHomes in accordance with the provisions of the Dispute Resolution and Arbitration Agreement section as of the date you last accepted these Terms (or accepted any subsequent changes to these Terms).
                        				</li>
                        				<li>
                        					<i>Survival</i>. Except as provided in Section 17.12 and subject to Section 13.8, this Section 17 will survive any termination of these Terms and will continue to apply even if you stop using the TurboHomes Platform or terminate your TurboHomes Account.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Feedback
                        			<ol>
                        				<li>
                        					We welcome and encourage you to provide feedback, comments and suggestions for improvements to the TurboHomes Platform ( <strong>Feedback</strong> ). You may submit Feedback by emailing us, through the Contact section of the TurboHomes Platform, or by other means of communication. Any Feedback you submit to us will be considered non-confidential and non-proprietary to you. By submitting Feedback to us, you grant us a non-exclusive, worldwide, royalty-free, irrevocable, sub-licensable, perpetual license to use and publish those ideas and materials for any purpose, without compensation to you.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			Applicable Law and Jurisdiction
                        			<ol>
                        				<li>
                        					If you reside in the United States, these Terms will be interpreted in accordance with the laws of the State of Arizona and the United States of America, without regard to conflict-of-law provisions. Judicial proceedings (other than small claims actions) that are excluded from the Arbitration Agreement in Section 17 must be brought in state or federal court in Gilbert, Arizona, unless we both agree to some other location. You and we both consent to venue and personal jurisdiction in Gilbert, Arizona.
                        				</li>
                        			</ol>
                        		</li>
                        		<li>
                        			General Provisions
                        			<ol>
                        				<li>
                        					Except as they may be supplemented by additional terms and conditions, policies, guidelines or standards, these Terms constitute the entire Agreement between TurboHomes and you pertaining to the subject matter hereof, and supersede any and all prior oral or written understandings or agreements between TurboHomes and you in relation to the access to and use of the TurboHomes Platform.
                        				</li>
                        				<li>
                        					No joint venture, partnership, employment, or agency relationship exists between you and TurboHomes as a result of this Agreement or your use of the TurboHomes Platform.
                        				</li>
                        				<li>
                        					These Terms do not and are not intended to confer any rights or remedies upon any person other than the parties.
                        				</li>
                        				<li>
                        					If any provision of these Terms is held to be invalid or unenforceable, such provision will be struck and will not affect the validity and enforceability of the remaining provisions.
                        				</li>
                        				<li>
                        					TurboHomes’s failure to enforce any right or provision in these Terms will not constitute a waiver of such right or provision unless acknowledged and agreed to by us in writing. Except as expressly set forth in these Terms, the exercise by either party of any of its remedies under these Terms will be without prejudice to its other remedies under these Terms or otherwise permitted under law.
                        				</li>
                        				<li>
                        					You may not assign, transfer or delegate this Agreement and your rights and obligations hereunder without TurboHomes's prior written consent. TurboHomes may without restriction assign, transfer, or delegate this Agreement and any rights and obligations hereunder, at its sole discretion, with 30 days prior notice. Your right to terminate this Agreement at any time remains unaffected.
                        				</li>
                        				<li>
                        					Unless specified otherwise, any notices or other communications to Members permitted or required under this Agreement, will be in writing and given by TurboHomes via email, TurboHomes Platform notification, or messaging service (including SMS). For notices made to Members, the date of receipt will be deemed the date on which TurboHomes transmits the notice.
                        				</li>
                        				<li>
                        					If you have any questions about these Terms please <a href="#" class="a-link">email us</a>.
                        				</li>
                        			</ol>
                        		</li>
                        	</ol>
                        </p>
                        <p class="title m-top-50">
                           Payment Terms of Service
                        </p>
                        <p class="text-justify">
                        	Last updated: January 1, 2018
                        </p>
                        <p class="text-justify">
                        	Please read these Payment Terms of Service ( Payment Terms ) carefully as they contain important information about your legal rights, remedies, and obligations. By using Payment Services (defined below), you agree to comply with and be bound by these Payment Terms.
                        </p>
                        <p class="text-justify">
                        	These Payment Terms constitute a legally binding agreement ( Agreement ) between you and TurboHomes governing the Payment Services conducted through or in the TurboHomes Platform. Mentions of TurboHomes or we or us or our refers to TurboHomes LLC, an Arizona limited liability company.
                        </p>
                        <p class="text-justify">
                        	The TurboHomes Terms of Service ( TurboHomes Terms ) and any supplemental terms applicable to you separately govern your use of the TurboHomes Platform. All capitalized terms used have the meaning set forth in the <a href="#" class="a-link">TurboHomes Terms</a> or applicable supplemental terms, unless usage indicates otherwise or the capitalized term is defined in these Payment Terms. If a contradiction between these Payment Terms and the <a href="#" class="a-link">TurboHomes Terms</a> or applicable supplemental terms exists, Payment Terms prevail.
                        </p>
                        <p class="text-justify">
                        	Our collection and use of personal information for your access to and use of the Payment Services is described in TurboHomes <a href="/privacyandpolicy" class="a-link" target="_blank">Privacy Policy</a>.
                        </p>
                        <p class="sub-title">
                        	Scope and Use
                        </p>
                        <p class="text-justify">
                        	TurboHomes collects payment for TurboHomes Services provided to Members in connection with and through the TurboHomes Platform ( Payment Services ). TurboHomes may now or at a future date collect payments for other services provided to, offered, or marketed to Members on the TurboHomes Platform by TurboHomes or a third-party affiliate.
                        </p>
                        <p class="text-justify">
                        	Payment availability or certain features of the Payment Services may become unavailable to carry out maintenance or perform updates that ensure the proper or improved functioning of the Payment Services. TurboHomes may improve, enhance, and modify the Payment Services portion of the TurboHomes Platform and introduce new payment service functions occasionally.
                        </p>
                        <p class="sub-title">
                        	Account Registration
                        </p>
                        <p class="text-justify">
                        	To pay for TurboHomes Services, you must register for a TurboHomes Account on the TurboHomes Platform. Besides having a TurboHomes Account, your TurboHomes Account must be in good standing. If you or TurboHomes closes your TurboHomes Account you can no longer use, change, or access Payment Services.
                        </p>
                        <p class="text-justify">
                        	You may authorize a third-party to use your TurboHomes Account under the <a href="#" class="a-link">TurboHomesTerms</a>. If you authorize a third-party to use your TurboHomes Account, you acknowledge and understand that anyone you authorize to use your TurboHomes Account may also use the Payment Services portion on your behalf and that you will be responsible for any payments made by such person.
                        </p>
                        <p class="sub-title">
                        	Payment Methods
                        </p>
                        <p class="text-justify">
                        	When you register for a TurboHomes Account as Seller, we ask you to provide a Payment Method to pay for the TurboHomes Services. Payment Method information is typically collected at checkout. You will be asked customary billing information such as name, billing address, and financial instrument information either to TurboHomes or its third-party processor(s). You must provide accurate, current, and complete information when inputting your Payment Method, and it is your obligation to keep your Payment Method up-to-date .
                        </p>
                        <p class="text-justify">
                        	You are solely responsible for the accuracy and completeness of your Payment Method information. TurboHomes is not responsible for any loss suffered by you because of incorrect Payment Method information provided by you.
                        </p>
                        <p class="sub-title">
                        	Payment Method Verification
                        </p>
                        <p class="text-justify">
                        	TurboHomes may verify the Payment Method by authorizing a nominal amount, not to exceed $1.00.
                        </p>
                        <p class="sub-title">
                        	Third-Party Payment Service Providers
                        </p>
                        <p class="text-justify">
                        	Payment Methods may involve the use of third-party payment service providers. These service providers may charge you additional fees when processing payments for the TurboHomes Services, and TurboHomes is not responsible for any such fees and disclaims all liability in this regard. Your Payment Method may also be subject to additional terms and conditions imposed by the applicable third-party payment service provider; please review these terms and conditions before using your Payment Method.
                        </p>
                        <p class="sub-title">
                        	Payments
                        </p>
                        <p class="text-justify">
                        	By entering your Payment Method information, you authorize TurboHomes to charge your Payment Method the TurboHomes Services Fees for the TurboHomes Services. TurboHomes will collect the TurboHomes Services Fees in the manner agreed between you and TurboHomes.
                        </p>
                        <p class="text-justify">
                        	You authorize TurboHomes to store your Payment Method information and charge your Payment Method as outlined in these Payments Terms. If your Payment Method’s account information changes (e.g., account number, routing number, expiration date) because of reissuance or otherwise, we may acquire that information from our financial services partner or your bank and update your Payment Method on file in your TurboHomesAccount.
                        </p>
                        <p class="text-justify">
                        	While TurboHomes Services Fees are due at checkout, TurboHomes will generally collect the TurboHomes Services Fees in two installments: the first installment ($199) will be collected immediately at or shortly after checkout; and the second installment ($699) will be collected at closing.
                        </p>
						<p class="text-justify">
							You are responsible for paying TurboHomes from funds distributed at closing. TurboHomes may, at its sole discretion, offer alternative options for the timing and manner of collecting TurboHomes Services Fees; any additional fees for using offered payment options will be displayed via the TurboHomes Platform and included in the TurboHomes Services Fees, and you agree to pay such fees by selecting the payment option. If TurboHomes cannot collect the TurboHomes Services Fees as scheduled, TurboHomes will collect the TurboHomes Services Fees at a later point.
                        </p>
                        <p class="text-justify">
                        	You authorize TurboHomes to perform any Payment Method verifications described above, and to charge your Payment Method for any TurboHome Services made in connection with your TurboHomes Account. You hereby authorize TurboHomes to collect TurboHomesServices Fees or any amounts due or outstanding by charging the Payment Method provided at checkout, either directly by TurboHomes or indirectly, via a third-party online payment processor. TurboHomes is not responsible for any fees that a third-party payment service provider may impose when TurboHomes charges your Payment Method, and TurboHomes disclaims all liability in this regard.
                        </p>
                        <p class="sub-title">
                        	Payment Processing Errors
                        </p>
                        <p class="text-justify">
                        	If a payment processing error occurs, we will try to rectify the payment processing error we learn of. These steps may include crediting or debiting (as appropriate) the same Payment Method used for the original payment by you, so you pay the correct amount.
                        </p>
                        <p class="sub-title">
                        	Collections
                        </p>
                        <p class="text-justify">
                        	Because we are optimists, we hope we never have to implement this portion of these terms. Our lawyers, however, are not and say we need to include this section. Let’s hope this situation never occurs, but if it does, and TurboHomes cannot collect any amounts you owe under these Payment Terms, TurboHomes may engage in collection efforts to recover such amounts from you.
                        </p>
                        <p class="text-justify">
                        	TurboHomes will deem any owed amount overdue when (a) 10 days have elapsed after you enter into a purchase agreement or (b) 120 days have elapsed after engaging TurboHomes Services and no purchase agreement has been entered into. Any overdue amounts not collected within the time specified in (a) or within 10 days after the occurrence of (b) will be deemed in default.
                        </p>
                        <p class="text-justify">
                        	You hereby explicitly agree that all communication in relation to amounts owed will be made by electronic mail or by phone, as provided to TurboHomes and by you. Such communication may be made by TurboHomes, or anyone on their behalf, including but not limited to a third-party collection agent. Also, you will be responsible for all costs, including attorneys’ fees, associated with collecting of amounts owed.
                        </p>
                        <p class="sub-title">
                        	Modification of these Payment Terms
                        </p>
                        <p class="text-justify">
                        	TurboHomes reserves the right to modify these Payments Terms at any time under this provision. If we change these Payments Terms, we will post the revised Payments Terms on the TurboHomes Platform and update the Last Updated date at the top of these Payments Terms. We will also provide you with notice by email of the modification at least thirty (30) days before the date they become effective. If you disagree with the revised Payments Terms, you may terminate this Agreement with immediate effect. We will inform you about your right of refusal and your right to terminate this Agreement in the notification email. If you do not terminate your Agreement before the date the revised Terms become effective, your continued use of the Payment Services will constitute acceptance of the revised Payments Terms.
                        </p>
                        <p class="sub-title">
                        	Definitions
                        </p>
                        <p class="text-justify">
                        	<i>Payment Method</i> means a financial instrument you use at checkout to pay for TurboHomes Services such as a credit card, debit card, or PayPal account.
                        </p>
                       <p class="text-justify">
                       	  <i>TurboHomes Services Fees means the $898 charged by TurboHomes to use the TurboHomes Services and TurboHomes Platform.</i>
                       </p>
                	</div>
              </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection