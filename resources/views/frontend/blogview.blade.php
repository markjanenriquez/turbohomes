@extends('frontend.layout.app')

@section('title', 'Blogs')

@section('page-js')
	<script src="/ng-fe/controllers/blogview.js" type="text/javascript"></script>
	<script src="/ng-fe/factory/blog.js" type="text/javascript"></script>
@endsection

@section('content')
	<div class="content-container main-page" id="blogs" ng-controller="viewBlogCtrl">
	    <div class="row">
	    	<div class="col-md-7 col-md-offset-1 no-pad-left m-top-20">
	    		<ul class="breadcrumb">
    				<li><a href="/">Home</a></li>
    				<li><a href="/blogs">Blogs</a></li>
    				<li ng-if="tag">
    					<a href="#" ng-click="viewBlogListByTag(tag.id, tag.slug)" ng-bind="tag.name"> </a>
    				</li>
   					<li class="active" ng-bind="blog.main.title"></li>
				</ul>
	    	</div>
			<div class="col-md-12 no-pad-left">
				<div class="col-md-7 col-md-offset-1 blogList view">
					<div class="card">
						<div class="card-block">
							<h1 class="card-title mt-3 f-4em" ng-bind="blog.main.title"></h1>
							<div class="meta text-muted view">
								<small><i class="fa fa-calendar fa-fw"></i> <span ng-bind="blog.main.created_at |
                          amDateFormat:'ddd, MMM. Do YYYY'"></span></small>
	                            <small><i class="fa fa-clock-o fa-fw"></i> Last updated <span am-time-ago="blog.main.updated_at"></span></small>
	                        </div>
					    </div>
	                    <!-- <img class="card-img-top" ng-src="/img/{[{ blog.main.featured_image }]}"> -->
	                    <div class="blog-cover" style="background-image: url('/img/{[{ blog.main.featured_image }]}')"></div>
	                    <div class="card-block">
	                         <div class="card-text view"  ng-bind-html="blog.main.content | trustHtml">
                             </div>
	                    </div>
	                    <div class="card-footer clearfix">
	                        <p><small>Tags:</small></p>
	                        <a href="#" class="tags" ng-repeat="tag in blog.main.tags" ng-bind="tag.name" ng-click="viewBlogListByTag(tag.id, tag.slug)"></a>	
	                    </div>
	                </div>
			    </div>
			</div>
			<div class="col-md-7 col-md-offset-1 no-pad-left m-top-15">
				<div class="text-center">
					<button class="pull-left btn btn-default navButton" ng-disabled="!blog.secondary['prev']" title="Prev Post - {[{ blog.secondary['prev'].title }]}" ng-click="viewPost(blog.secondary['prev'].slug, blog.secondary['prev'].id)">« Previous Post</button>
					<button class="pull-right btn btn-default navButton" ng-disabled="!blog.secondary['next']" title="Next Post - {[{ blog.secondary['next'].title }]}" ng-click="viewPost(blog.secondary['next'].slug, blog.secondary['next'].id)">Next Post »</button>
				</div>
			</div>
		</div>
	</div>
@endsection