@extends('frontend.layout.app')

@section('title', 'About Us')

@section('page-css')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/css/mdb.min.css" />
    <style>
        .navbar-nav .open .dropdown-menu {
            position: static!important;
        }
    </style>
@endsection

@section('page-js')
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.3.2/js/mdb.min.js"></script>
    <script src="/ng-fe/controllers/contactus.js" type="text/javascript"></script>
@endsection

@section('content')
    <div id="contact-us" class="content-container" ng-controller="contactusCtrl"> 
        <div class="row" style="background-color:#ffffff">
            <div class="container">            
                <div class="col-sm-12 mpc-column">                            
                        <div class="container">
                            <section class="section">
                                <h1>Get in Touch with us</h1>
                                <div class="row">
                                    <div class="col-md-8 col-xl-9">
                                        <form name="contactusForm" ng-submit="submit(contactusForm, form)" novalidate>
                                            <div class="row m-b-25">
                                                <div class="col-sm-12 col-md-6">
                                                    <div class="md-form">
                                                        <div class="md-form" ng-class="{ 'has-error': contactusForm.$submitted && contactusForm.name.$invalid }">
                                                            <input type="text" id="contact-name" class="form-control" ng-model="form.name" name="name" required>
                                                            <label for="contact-name" class="" ng-if="!(contactusForm.$submitted && contactusForm.name.$invalid)">Your name</label>
                                                            <p class="text-red ng-cloak" ng-if="contactusForm.$submitted && contactusForm.name.$invalid">Name is required</p>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-md-6">
                                                    <div class="md-form" ng-class="{ 'has-error': contactusForm.$submitted && contactusForm.email.$invalid }">
                                                        <div class="md-form">
                                                            <input type="text" id="contact-email" class="form-control" ng-model="form.email" name="email" required>
                                                            <label for="contact-email" class="" ng-if="!(contactusForm.$submitted && contactusForm.email.$invalid)">Your email</label>
                                                            <p class="text-red ng-cloak" ng-if="contactusForm.$submitted && contactusForm.email.$invalid">Email is required</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row m-b-25">
                                                <div class="col-md-12" ng-class="{ 'has-error': contactusForm.$submitted && contactusForm.subject.$invalid }">
                                                    <div class="md-form">
                                                        <input type="text" id="contact-Subject" class="form-control" ng-model="form.subject" name="subject" required>
                                                        <label for="contact-Subject" class="" ng-if="!(contactusForm.$submitted && contactusForm.subject.$invalid)">Subject</label>
                                                        <p class="text-red ng-cloak" ng-if="contactusForm.$submitted && contactusForm.subject.$invalid">Subject is required</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row m-b-25">
                                                <div class="col-md-12" ng-class="{ 'has-error': contactusForm.$submitted && contactusForm.message.$invalid }">
                                                    <div class="md-form">
                                                        <textarea type="text" id="contact-message" class="md-textarea" ng-model="form.message" name="message" required></textarea>
                                                        <label for="contact-message" ng-if="!(contactusForm.$submitted && contactusForm.message.$invalid)">Your message</label>
                                                        <p class="text-red ng-cloak" ng-if="contactusForm.$submitted && contactusForm.message.$invalid">Message is required</p>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-12 btn-lockup">
                                                    <button type="submit" class="btn btn-primary">Send</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="col-md-4 col-xl-3">
                                        <ul class="contact-icons">
                                            <li><i class="fa fa-phone fa-2x"></i>
                                                <p>(602) 845-9990</p>
                                            </li>

                                            <li><i class="fa fa-envelope fa-2x"></i>
                                                <p>hello@turbohomes.com</p>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </section> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection