@extends('frontend.layout.app')

@section('title', 'Account Activated')

@section('page-js')
@endsection

@section('content')
            
    <div class="content-container" ng-controller="loginCtrl as lctrl">
        <div class="row min-height-700 mt-20">

            <div class="col-md-12">
                <div class="signupsuccess" style="padding: 40px 30px!important; height: auto!important;">
                    <h3>Congratulations!</h3>
                    <p class="m-b-20">Your Turbo Homes Account has been successfully activated.</p>
                    
                    <div class="row mt-10">
                        <div class="col-sm-12">
                            <div class="alert alert-danger ng-cloak" ng-if="lctrl.login.errorlogin == true">{[{lctrl.login.errormsg}]}</div>
                        </div>
                    </div>

                    <form id="sc_login_form" ng-submit="lctrl.login.submit()">                               

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <div class="input-group-addon loginicon">
                                            <span class="glyphicon glyphicon-user"></span>
                                        </div>
                                        <input type="text" ng-model="lctrl.login.username" placeholder="User Name" name="uname" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <div class="input-group input-group-lg">
                                        <div class="input-group-addon loginicon">
                                            <span class="glyphicon glyphicon-lock"></span>
                                        </div>

                                        <input type="password" ng-model="lctrl.login.password" placeholder="Password" name="pass" class="form-control" required>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <input type="hidden" name="action" value="sc_login_action">
                                <div id="sc_spinner_container" class="ng-cloak" ng-if="lctrl.login.onprogress">
                                    <div class="spinner">
                                        <div class="bounce1"></div>
                                        <div class="bounce2"></div>
                                        <div class="bounce3"></div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary btn_submit btn-block btn-lg login-btn" ng-if="lctrl.login.onprogress == false">Login</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
    </div>

@endsection
