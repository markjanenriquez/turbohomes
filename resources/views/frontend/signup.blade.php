@extends('frontend.layout.app')

@section('title', 'Sign up') 

@section('page-css')
@endsection

@section('page-js')

@endsection

@section('content')
    <div id="signup" class="row min-height-700 mt-20 content-container" ng-controller="signupCtrl as sctrl" id="signup">
        <div class="col-md-12 ng-cloak" ng-if="signup.success == true && emailVerification == false">
            <div class="signupsuccess">
                <h3>Success!</h3>
                <p>
                    Thank you for signing up! Please check your email and verify your email address to activate your account.
                    <br>
                    <small>If you don't see a message in your inbox, check your spam or junk mail folder.</small>
                </p>
                <button class="btn btn-primary m-t-20" ng-click="resendEmailVerification()" ng-disabled="verificationOnProgress">Resend Email verification</button>
            </div>
        </div>

        <div class="col-md-12 ng-cloak" ng-if="signup.success == false && emailVerification == false">
            <div class="page-main">
                <div class="article-detail">
                    <div id="sc_form_container">
                        <div class="panel with-nav-tabs panel-default">
                            <div class="panel-body">
                                <div class="container-fluid">
                                    <div class="row">                                                   
                                        <form id="sc_registration_form" name="signupForm" ng-submit="signup.submit(signupForm, signup)">
                                            <div id="registration_part_one">    
                                                <h3>Welcome to the Turbo family!</h3>
                                                <p>Did not recieve an email verification? <a href="#" class="a-primary" ng-click="resendVerificationToggle()">Resend verification</a></p>
                                                <p>Please fill out the information below so we can better serve you.</p>
                                                <div class="col-sm-12">
                                                    <style type="text/css">
                                                        .abc-radio {
                                                             display: inline-block;
                                                             margin-right: 30px;
                                                        }
                                                    </style>
                                                                    
                                                    <div class="col-sm-12" style="padding-left:0px;" ng-init="signup.data.role = 1">
                                                        <label class="control-label">Are you interested in:</label>
                                                        <div class="rd-btn-lockup">
                                                            <div class="form-check abc-radio abc-radio-warning" style="margin-left:30px">
                                                                <input class="form-check-input" id="radBuyer" type="radio" name="radChoices" value="1" ng-model="signup.data.role">
                                                                <label class="form-check-label" for="radBuyer" style="display: block !important;">Buying </label>
                                                            </div>

                                                            <div class="form-check abc-radio abc-radio-warning">
                                                                <input class="form-check-input" id="radSeller" type="radio" name="radChoices" value="2" ng-model="signup.data.role">
                                                                <label class="form-check-label" for="radSeller" style="display: block !important;">Selling </label>
                                                            </div>

                                                            <div class="form-check abc-radio abc-radio-warning">
                                                                <input class="form-check-input" id="radBoth" type="radio" name="radChoices" value="3" ng-model="signup.data.role">
                                                                <label class="form-check-label" for="radBoth" style="display: block !important;">Both </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="b_firstname">First Name:</label>                                        
                                                        <input type="text" name="b_firstname" id="b_firstname" ng-model="signup.data.firstname" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="b_lastname">Last Name:</label>                                        
                                                        <input type="text" name="b_lastname" id="b_lastname" ng-model="signup.data.lastname" class="form-control" required>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group" ng-class="{ 'has-error' : signup.validEmail === false }">
                                                        <label for="b_email">Email Address:</label>                                        
                                                        <input type="email" name="b_email" id="b_email" ng-model="signup.data.email" class="form-control" ng-change="signup.validateEmail(signup.data.email)" ng-model-options="{ debounce: 500 }" required>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group {[{passwordclasstype}]}">
                                                        <label class="control-label" for="password">Password <em>(Minimum of 6 characters)</em> <span ng-bind="passwordmsg"></span><i class="{[{passwordicontype}]}"></i></label>
                                                        <input type="password" class="form-control" id="password" placeholder="Enter Password" ng-model="signup.data.password" ng-change="passwordchange(signup.data.password)" required>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group" ng-show="value != 0">
                                                        <label class="control-label" >Password Strength <em ng-if="value <= 0"></em><em class="text-light-blue" ng-if="value < 50 && value > 0">(Weak)</em><em class="text-yellow" ng-if="value < 83 && value >= 50">(Medium)</em><em class="text-green" ng-if="value >= 83">(Strong)</em></label>
                                                        <div ng-password-strength="signup.data.password" strength="value" strength="passStrength" inner-class="progress-bar" inner-class-prefix="progress-bar-" class="ng-isolate-scope"></div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group {[{repasswordclasstype}]}">
                                                        <label class="control-label" for="repassword">Re-enter Password <em>(Minimum of 6 characters)</em> <span ng-bind="repasswordmsg"></span><i class="{[{repasswordicontype}]}"></i></label>
                                                        <input type="password" class="form-control" id="repassword" placeholder="Re-enter Password" ng-model="signup.data.repassword" ng-change="repasswordchange(signup.data.password, signup.data.repassword)" required>
                                                    </div>
                                                </div>                

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label for="bmobilenumber">Mobile Number:</label>                                       
                                                        <input type="text" name="bmobilenumber" id="bmobilenumber" ng-model="signup.data.mobilenumber" class="form-control" required  ui-mask="(999) 999-9999"  ui-mask-placeholder ui-mask-placeholder-char="_" model-view-value="true">
                                                    </div>
                                                </div>  

                                                <div class="col-sm-12" ng-if="signup.data.role ==1 || signup.data.role == 3">
                                                    <div class="form-group">
                                                        <label for="b_buydate">When do you plan to buy?</label>                                       
                                                        <select id="b_buydate" name="b_buydate" class="form-control" ng-model="signup.data.plantobuy" required>
                                                            <option value="1">Within 3 months</option>
                                                            <option value="2">Within 6 months</option>
                                                            <option value="3">Within 1 year</option>
                                                        </select>
                                                    </div>
                                                </div>  

                                                <div class="col-sm-12" ng-if="signup.data.role ==1 || signup.data.role == 3">
                                                    <div class="form-group">
                                                        <label for="b_state">State you plan to buy in:</label>                                     
                                                        <select id="b_state" name="b_state" class="form-control" ng-model="signup.data.state" ng-change="changeState(signup.data.state)" required>
                                                            <option value="" selected="selected">Select a State</option>
                                                            <option value="AZ">Arizona</option>
                                                            <option value="CA">California</option>
                                                            <option value="CO">Colorado</option>
    
                                                            <option value="ID">Idaho</option>
                                                            <option value="NV">Nevada</option>
                                                            <option value="NM">New Mexico</option>
                                                            <option value="OR">Oregon</option>
                                                            <option value="TX">Texas</option>
                                                            <option value="UT">Utah</option>
                                                            <option value="WA">Washington</option>
                                                        </select>
                                                    </div>
                                                </div>  
                                                
                                                <div class="col-sm-12" ng-if="signup.data.role ==2 || signup.data.role == 3">
                                                    <div class="form-group">
                                                        <label for="sc_address_property">Address of the property you want to sell:</label>                                       
                                                        <input type="text" name="sc_address_property" id="sc_address_property" ng-model="signup.data.propertyaddress" class="form-control" autocomplete="off" required g-places-autocomplete ng-change="addressChange(signup.data.propertyaddress)">
                                                    </div>
                                                </div> 

                                                <div class="col-sm-12 col-xs-12 text-right" ng-if="signup.regpart2 == false">
                                                    <a class="btn-primary btn btn-md property_section_btn bntorange" id="registration_part_one_continue_btn" ng-click="signup.regpart2 = true">Continue</a>
                                                    <a class="btn btn-default" id="registration_part_one_cancel_btn" href="/">Cancel</a>
                                                </div>
                                            </div>

                                            <div id="registration_part_two" class="ng-cloack fade active in" ng-if="signup.regpart2 == true">
                                                <h3>Just a few more questions.</h3>

                                                 <div class="col-sm-12" ng-if="signup.data.role == 1 || signup.data.role == 3">
                                                    <div class="form-group">
                                                        <label>Have you been preapproved for a home loan?</label>                                               
                                                        <div>
                                                            <div class="btn-group" data-toggle="buttons">                                               
                                                                <label class="btn btn-default sc_home_loan_answer" ng-click="signup.data.homeloan = 1">
                                                                    <input type="radio" name="b_homeloan" value="1" autocomplete="off" required> Yes
                                                                </label>
                                                                <label class="btn btn-default sc_home_loan_answer" ng-click="signup.data.homeloan = 0">
                                                                    <input type="radio" name="b_homeloan" value="0" autocomplete="off" required> No
                                                                </label>
                                                            </div>                                          
                                                        </div>                                          
                                                    </div>
                                                </div>

                                                <div class="col-sm-12" ng-if="signup.data.role == 1">
                                                    <div class="form-group">
                                                        <label>Do you also need to sell your house?</label>                                             
                                                        <div>
                                                            <div class="btn-group" data-toggle="buttons">                                               
                                                                <label class="btn btn-default sc_home_sell_answer" ng-click="signup.data.homesell = 1">
                                                                    <input type="radio" name="b_homesell" value="1"  autocomplete="off" required> Yes
                                                                </label>
                                                                <label class="btn btn-default sc_home_sell_answer" ng-click="signup.data.homesell = 0">
                                                                    <input type="radio" name="b_homesell" value="0"  autocomplete="off" required> No
                                                                </label>
                                                            </div>                                          
                                                        </div>                                          
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Have you already found a home you want to buy?</label>                                               
                                                        <div>
                                                            <div class="btn-group" data-toggle="buttons">                                               
                                                                <label class="btn btn-default sc_home_found_answer" ng-click="signup.data.homefound = 1">
                                                                    <input type="radio" name="b_homefound" value="1"  autocomplete="off" required> Yes
                                                                </label>
                                                                <label class="btn btn-default sc_home_found_answer" ng-click="signup.data.homefound = 0">
                                                                    <input type="radio" name="b_homefound" value="0"  autocomplete="off" required> No
                                                                </label>
                                                            </div>                                          
                                                        </div>                                          
                                                    </div>
                                                </div>

                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                        <label>Are you a real estate agent?</label>                                             
                                                        <div class="form-group">
                                                            <div class="btn-group" data-toggle="buttons" >                                               
                                                                <label class="btn btn-default sc_real_state_answer"  ng-click="signup.data.realstateagent = 1">
                                                                    <input type="radio" name="b_reakstateagent" value="1"> Yes
                                                                </label>
                                                                <label class="btn btn-default sc_real_state_answer" ng-click="signup.data.realstateagent = 0">
                                                                    <input type="radio" name="b_reakstateagent" value="0" ng-click="signup.data.realstateagent = 0"> No
                                                                </label>
                                                            </div>                                          
                                                        </div> 
                                                        <div id="sc_real_agent_container" ng-if="signup.data.realstateagent == 1">
                                                            <div class="form-group">                                                                        
                                                                <input type="text" name="b_agentnumber" id="b_agentnumber" class="form-control" placeholder="License #" ng-required="signup.data.realstateagent == 1" ng-model="signup.data.agentnumber">
                                                            </div>

                                                            <div class="form-group">                                                                        
                                                                <input type="email" name="sc_agent_email" id="sc_agent_email" class="form-control" placeholder="Email Address" ng-required="signup.data.realstateagent == 1" ng-model="signup.data.agentemail" g-places-autocomplete>
                                                            </div>
                                                        </div>

                                                    </div>
                                                </div>
                                                               
                                                <div id="" ng-show="signup.issaving == true">
                                                    <div class="spinner">
                                                        <div class="bounce1"></div>
                                                        <div class="bounce2"></div>
                                                        <div class="bounce3"></div>
                                                    </div>
                                                </div>

                                                <div class="col-sm-12 col-xs-12 text-right final_btn_container" ng-hide="signup.issaving == true">        
                                                    <input type="hidden" name="account_type" value="houzez_buyer"> 
                                                    <input type="hidden" name="action" value="sc_registration_form_submit"> 
                                                    <button class="btn btn-primary" id="registration_part_two_continue_btn buttonlink">Get Started</button>
                                                    <button class="btn btn-default" id="registration_part_two_cancel_btn">Back</button>
                                                </div>

                                            </div>

                                            <div id="sc_registration_msg_container" ng-if="signup.alerts.length > 0">
                                                <div class="alert alert-danger">{[{signup.alerts[0].msg}]}</div>
                                            </div>

                                        </form>     

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>                  
            </div>
        </div>

        <div class="col-md-12 ng-cloak" ng-if="emailVerification == true">
            <div class="signupsuccess" style="padding: 40px 30px!important; height: auto!important;">

                <h3 class="m-b-20">Resend Email Verification!</h3>
                {{-- <p class="m-b-20">Your Turbo Homes Account has been successfully activated.</p> --}}
                
                <div class="row mt-10">
                    <div class="col-sm-12">
                        <div class="alert alert-{[{ alert.type }]} ng-cloak" ng-repeat="alert in alerts">{[{ alert.msg }]}</div>
                    </div>
                </div>

                <form id="sc_login_form" name="verificationForm" ng-submit="verifyEmail(verificationForm)" novalidate>                               

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group" ng-class="{ 'has-error' : verificationForm.$submitted && verificationForm.email.$invalid }">
                                <div class="input-group input-group-lg">
                                    <div class="input-group-addon loginicon">
                                        <span class="glyphicon glyphicon-user"></span>
                                    </div>
                                    <input type="text" ng-model="verification.email" placeholder="Email Address" name="email" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12" style="margin-bottom: 15px!important">
                            <input type="hidden" name="action" value="sc_login_action">
                            <div id="sc_spinner_container" class="ng-cloak" ng-if="verificationOnProgress">
                                <div class="spinner">
                                    <div class="bounce1"></div>
                                    <div class="bounce2"></div>
                                    <div class="bounce3"></div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary btn_submit btn-block btn-lg login-btn" ng-if="verificationOnProgress == false">Submit</button>
                        </div>
                    </div>
                
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12" style="font-size: 20px!important">
                            <a href="#" class="a-primary" ng-click="resendVerificationToggle()">
                                <i class="fa fa-chevron-left fa-2" aria-hidden="true"></i> Back
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection