<!doctype html>
<html lang="{{ app()->getLocale() }}" >
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>Event Listener</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }
        </style>
    </head>
    <body ng-app="app">
        <div class="flex-center position-ref full-height" ng-controller="myCtrl">

            <div class="content">
                <li ng-repeat="datum in myCtrl.data">{[{ $index }]}</li>
               
            </div>
        </div>
    </body>
    <!-- Components -->
    <script src="/components/angular/angular.min.js" type="text/javascript"></script>
    <!-- Angular app & config -->
    <script src="/ng-fe/ng-app.js" type="text/javascript"></script>
    <script src="/ng-fe/ng-config.js" type="text/javascript"></script>
</html>


