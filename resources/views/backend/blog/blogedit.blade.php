
<section class="content-header" id="gototop">
	<h1>
		Edit Blog
		<!-- <small>Add New User</small> -->
	</h1>
	<ol class="breadcrumb">
		<li><a href="#/dashboard"><i class="fa fa-dashboard"></i> Main</a></li>
		<li class="active">Admin</li>
		<li class="active">Edit Blog</li>
	</ol>
</section>

<section class="content">
	<!-- SELECT2 EXAMPLE -->
	<div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">{[{alert.msg}]}</div>

	<div class="box {[{boxtype}]}">
		<form name="form" ng-submit="save(form)" novalidate="">
			<div class="box-header with-border">
				<h3 class="box-title"></h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-8">
						<h4 class="page-header">Blog Information</h4>
						<div class="form-group">
							<label class="control-label">Title</label>
							<span class="text-danger pull-right" ng-messages="form.title.$error" ng-if="setError(form,'title')">
								<span ng-message="required">Title is required</span>
							</span>
							<input type="text" name="title" class="form-control" ng-model="field.title" required=""  ng-keyup="slugify(field.title)">
						</div>
						<div class="form-group">
							<label class="control-label">Slug</label>
							<span class="text-danger pull-right" ng-messages="form.slug.$error" ng-if="setError(form,'slug')">
								<span ng-message="required">Slug is required</span>
								<span ng-message="existed">Slug is already taken</span>
							</span>
							<input type="text" name="slug" class="form-control" ng-model="field.slug" required="">
						</div>
						<div class="form-group">
							<label class="control-label">Content</label>
							<span class="text-danger pull-right" ng-messages="form.content.$error" ng-if="setError(form,'content')">
								<span ng-message="required">Content is required</span>
							</span>
							<div ckeditor="options"  name="content" ng-model="field.content" required contenteditable></div>
						</div>
						<div class="form-group">
							<label class="control-label">Tags</label>
							<style type="text/css">
								.select2-container{
									width : 100% !important;
								}
								.select2-selection__choice{
									color: #fff !important;
								    background: #00c0ef !important;
								    border: 1px solid #00acd6 !important;
								}
								.select2-selection__choice__remove{
									color: #fff !important;
								}
							</style>
							<span class="text-danger pull-right" ng-messages="form.tags.$error" ng-if="setError(form,'tags')">
								<span ng-message="required">Tags is required</span>
							</span>
							<select ui-select2 ng-model="field.tags" name="tags" class="form-control" data-placeholder="Select Tags" ng-options="tag.id as tag.name for tag in tags" multiple="" required="">
							</select>
						</div>
					</div>

					<div class="col-md-4">
						<h4 class="page-header">Featured Image
							
						</h4>
						<div class="profilecenter">
							<div class="profilepiccontainer">
								<div class="profilepic" id="profilepic" style="background-image: url(/img/icon-no-image.png);"></div>​
								<label for="profile_pic" class="label_profile_pic fadeinup animated">Change Image</label>
							</div>
							<!-- <img src="/img/defaultprofilepicture.png" id="profilepic" class="profilepic"  alt="your image" width="150px" height="150px"> -->
							<input type="file" nv-file-select="" id="profile_pic" class="filestyle" data-buttonName="btn-primary" uploader="uploader" onchange="readURL(this);" accept="image/x-png, image/gif, image/jpeg"/>
							
							<span class="text-danger" ng-messages="form.featured_image.$error" ng-if="setError(form,'featured_image')">
								<span ng-message="required">Featured Image is required</span>
							</span>
							<input type="hidden" ng-model="field.featured_image" name="featured_image" required="">
						</div>
					</div>

				</div><!-- /.row -->
			</div><!-- /.box-body -->
			<div class="box-footer">
				<a class="btn btn-default" ui-sref="blogmanage">Cancel</a>
				<button type="submit" class="btn btn-info">Save</button>
			</div>
		</form>

		<div class="overlay" ng-show="saveloading">
			<i class="fa fa-refresh fa-spin" style="font-size:50px;"></i>
		</div>

	</div><!-- /.box -->
</section>

<script type="text/javascript">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
            	document.getElementById('profilepic').style.backgroundImage="url("+ e.target.result +")";
                // $('#profilepic')
                //     // .attr('background-image', e.target.result)

                //     .width(150)
                //     .height(150);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
