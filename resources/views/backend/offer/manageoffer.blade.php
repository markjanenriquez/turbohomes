<script type="text/ng-template" id="useredit">
	<div ng-include="'/bemodal/useredit.html'"></div>
</script>

<script type="text/ng-template" id="offerdelete">
	<div ng-include="'/bemodal/userdelete.html'"></div>
</script>

<section class="content-header" id="gototop">
	<h1>
		Manage Offers
		<!-- <small>Add New User</small> -->
	</h1>
	<ol class="breadcrumb">
		<li><a href="#/dashboard"><i class="fa fa-dashboard"></i> Main</a></li>
		<li class="active">Admin</li>
		<li class="active">Manage Offer</li>
	</ol>
</section>

<section class="content">
	<div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">{[{alert.msg}]}</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">Offer List</h3>
				</div>

				<div class="row">
					<div class="box-header">
					<div class="col-sm-6">
						<div id="example1_filter" class="dataTables_filter">
							<label>Search:</label>
							<div class="input-group">
								<input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search" ng-model="keyword" ng-enter="search(keyword)">
								<div class="input-group-btn">
									<button class="btn btn-sm btn-default" ng-click="search(keyword)"><i class="fa fa-search"></i></button>
									<button class="btn btn-sm btn-default" ng-click="reset()"><i class="fa fa-refresh"></i></button>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>

				<div class="box-body table-responsive no-padding">

				    <div class="col-md-12">
						<div class="form-group pull-right">
						<a hef="#" ui-sref="createoffer" class="btn btn-default">Create Offer</a>
						<button ng-json-export-excel data="dataList" report-fields='{id : "ID", userid : "User ID", scpropertyaddress : "Property Address", listedmenuasnwer : "Who Listed", buyersname : "Name", buyersemail : "Email", sectionbuyingmenuasnwer : "Are with Someone?", scfinancingamt : "Offer", sc_financing_earnest_amt : "Earnest Money Offer", down_payment : "Downpayment", inspectiondeadline : "Inspection Deadline(no.of day/s)", closingdate :"Closing Date(no. of days)", created_at : "Date Listed"}' filename =" 'offer_list' " separator="," class="btn btn-primary">Export To Excel</button>
					    </div>
					</div>
					<table class="table table-hover">
						<thead>
							<tr>
								<th style="width:5%;border:solid 1px #f4f4f4;border-left:none"></th>
								<th style="width:15%;border:solid 1px #f4f4f4;" ng-repeat="col in columns" ng-click="sort(sorting[col.value])">
									{[{ col.col }]}
									<span class="pull-right" ng-if="sortBy == sorting[col.value]['sortBy']"><i ng-class="{'fa fa-caret-up': sorting[col.value]['order'] == 'asc','fa fa-caret-down': sorting[col.value]['order'] == 'desc'}"></i></span>
								</th>
								<th style="width:20%;border:solid 1px #f4f4f4;border-right:none">Action</th>
							</tr>
						</thead>
						<tbody>
							<tr ng-repeat="list in listofoffer">
								<td>{[{ index + $index }]}</td>
								<td>{[{list.scpropertyaddress}]}</td>
								<td>{[{list.buyersname}]}</td>
								<td>{[{list.buyersemail}]}</td>
								<td>{[{list.buyersemail}]}</td>
								<td>{[{list.created_at  |
                              amDateFormat:'ddd, MMM. Do YYYY' }]}</td>
                              <td align="center">
									<button class="btn btn-warning btn-sm" title="View" ui-sref="viewoffer({id: list.id})">
										<i class="fa fa-align-justify"></i>
									</button>
									<button class="btn btn-primary btn-sm" ng-click="editoffer(list.id)">
											<i class="glyphicon glyphicon-pencil"></i>
									</button>
									<button class="btn btn-danger btn-sm" title="Delete" ng-click="deleteoffer(list.id)">
										<i class="glyphicon glyphicon-trash"></i>
									</button>
								</td>
							</tr>
						</tbody>
					</table>
				</div><!-- /.box-body -->
				<div class="box-footer text-center clearfix">
                <ul uib-pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize" class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></ul>
                </div>
                <div class="overlay" ng-show="listloading">
                	<i class="fa fa-refresh fa-spin" style="font-size:50px;"></i>
                </div>
			</div><!-- /.box -->
		</div>
	</div>
</section> <!-- end of content -->
