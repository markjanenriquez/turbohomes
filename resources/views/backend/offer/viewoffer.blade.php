<!-- <script type="text/ng-template" id="neworlist">
	<div ng-include="'/bemodal/neworlist.html'"></div>
</script>
<script type="text/ng-template" id="addsubject">
	<div ng-include="'/bemodal/addsubject.html'"></div>
</script>
<script type="text/ng-template" id="addfile">
	<div ng-include="'/bemodal/addfile.html'"></div>
</script>
<script type="text/ng-template" id="updatefile">
	<div ng-include="'/bemodal/updatefile.html'"></div>
</script>
<script type="text/ng-template" id="deletefile">
	<div ng-include="'/bemodal/deletefile.html'"></div>
</script> -->
<section class="content-header">
	<h1>
	Offers Details
	</h1>
	<ol class="breadcrumb">
		<li><a href="#/dashboard"><i class="fa fa-dashboard"></i> Main</a></li>
		<li class="active"><a href="">Offers Details</a></li>
	</ol>
</section>
<section class="content offer-details">
	<div class="row m-b-10">
		<div class="col-md-12">
			<button type="button" class="btn btn-primary" ng-if="offer.viewdata.status === constant.pending" ng-click="setContract()">Set Under Contract</button>
			<button type="button" class="btn btn-primary" ng-if="offer.viewdata.status !== constant.pending" ng-click="viewContract()">View Contract</button>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Property</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div><strong>Property's Address:</strong> {[{ offer.viewdata.scpropertyaddress }]}</div>
					<div>
						<strong>The house is listed by:</strong> {[{ constant[offer.viewdata.listedmenuasnwer] }]}
						<div ng-switch="offer.viewdata.listedmenuasnwer" class="m-l-15">
							<div ng-switch-when="turbo">
								<p class="m-0"><strong>Seller's Name:</strong> {[{ offer.viewdata.scsellername }]}</p>
								<p class="m-0"><strong>Seller's Email:</strong> {[{ offer.viewdata.scselleraddress }]}</p>
							</div>
							<div ng-switch-when="realtor">
								<p class="m-0"><strong>Name:</strong> {[{ offer.viewdata.screaltorname }]}</p>
								<p class="m-0"><strong>Email:</strong> {[{ offer.viewdata.screaltoraddress }]}</p>
								<p class="m-0"><strong>Brokerage Name:</strong> {[{ offer.viewdata.scbrokerageaddress }]}</p>
							</div>
							<div ng-switch-when="other">
								<p class="m-0"><strong>Seller's Name:</strong> {[{ offer.viewdata.scothersellername }]}</p>
								<p class="m-0"><strong>Seller's Email:</strong> {[{ offer.viewdata.scotherselleraddress }]}</p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Financing</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div><strong>Offer:</strong> ${[{ offer.viewdata.scfinancingamt }]}</div>
					<div><strong>Earnest money offer:</strong> ${[{ offer.viewdata.scfinancingearnestamt }]}</div>
					<div><strong>Title company to work with:</strong> {[{ constant[offer.viewdata.financingmenulistthreeanswer] }]}</div>
					<div ng-if="offer.viewdata.financingmenulistthreeanswer === 'yes'" class="m-l-15">
						<div><strong>Name:</strong> {[{ offer.viewdata.scfinancingescrowname }]}</div>
						<div><strong>Telephone:</strong> {[{ offer.viewdata.scfinancingescrowtel }]}</div>
						<div><strong>Officer Name:</strong> {[{ offer.viewdata.scfinancingescrowofficername }]}</div>
					</div>
					<div><strong>Plan on paying the house:</strong></div>
					<div class="m-l-15">
						<div><strong>Earnest Money:</strong> $ {[{ offer.viewdata.scfinancingearnestamt }]}</div>
						<div><strong>Cash/Down Payment:</strong> $ {[{ offer.viewdata.down_payment }]}</div>
						<div ng-if="offer.viewdata.mortgage"><strong>Mortgage/Loan:</strong> $ {[{ offer.viewdata.mortgage }]}</div>
						<div ng-if="offer.viewdata.planforpayingother"><strong>Other:</strong> $ {[{ offer.viewdata.planforpayingother }]}</div>
					</div>
					<div><strong>Type of Financing:</strong> {[{ (offer.viewdata.type_of_financing !== 'other' ? offer.viewdata.type_of_financing : offer.viewdata.type_of_financing_other) }]}</div>
					<div><strong>Helps the seller to help cover closing costs:</strong> ${[{ constant[offer.viewdata.financingmenulistfouranswer] }]}</div>
					<div class="m-l-15" ng-if="offer.viewdata.financingmenulistfouranswer === 'yes'">
						<div><strong>Amount:</strong> $ {[{ offer.viewdata.closingcostamount }]}</div>
					</div>
					<div><strong>Home warranty included:</strong> {[{ constant[offer.viewdata.financingmenulistsixanswer] }]}</div>
					<div class="m-l-15" ng-if="offer.viewdata.financingmenulistsixanswer === 'yes'">
						<div><strong>Home warranty is payed by:</strong> {[{ constant[offer.viewdata.financingmenulistsevenanswer] }]}</div>
						<div><strong>Cost to not exceed:</strong> ${[{ offer.viewdata.costnotexceed }]}</div>
					</div>
				</div>
			</div>
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Inclusions</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div ng-if="offer.viewdata.refrigerator == 'true' || offer.viewdata.washer == 'true' || offer.viewdata.dryer == 'true' || offer.viewdata.aboveground == 'true' || offer.viewdata.other == 'true'">
						<div><strong>Included "Personal property" for sale:</strong></div>
						
						<div class="m-l-15">
							<div ng-if="offer.viewdata.refrigerator == 'true'"><strong>Refrigerator:</strong> {[{ offer.viewdata.refrigeratordesc }]}</div>
							<div ng-if="offer.viewdata.washer == 'true'"><strong>Washer:</strong> {[{ offer.viewdata.washerdesc }]}</div>
							<div ng-if="offer.viewdata.dryer == 'true'"><strong>Dryer:</strong> {[{ offer.viewdata.dryerdesc }]}</div>
							<div ng-if="offer.viewdata.other == 'true'"><strong>Other:</strong> {[{ offer.viewdata.otherdesc }]}</div>
						</div>
					</div>
					<div><strong>Discuss with a Turbo Attorney:</strong> {[{ constant[offer.viewdata.discusswithattorny] }]}</div>
					<div><strong>Additional terms and conditions:</strong> {[{ offer.viewdata.additionalcondition }]}</div>
				</div>
			</div>
		</div> <!-- /.col-md-4 -->
		<div class="col-md-6">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Buyer's Information</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div><strong>Name:</strong> {[{ offer.viewdata.buyersname }]}</div>
					<div><strong>Email:</strong> {[{ offer.viewdata.buyersemail }]}</div>
					<div><strong>Buying the house:</strong> {[{ constant[offer.viewdata.sectionbuyingmenuasnwer] }]}</div>
					<div ng-if="offer.viewdata.sectionbuyingmenuasnwer === 'someone'">
						<div class="m-l-15 m-b-15" ng-repeat="buyer in offer.viewdata.otherbuyer">
							<p class="m-0"><strong>Buyer {[{ $index + 1 }]}</strong></p>
							<p class="m-0"><strong>Name:</strong> {[{ buyer.buyersname }]}</p>
							<p class="m-0"><strong>Email:</strong> {[{ buyer.buyersemail }]}</p>
						</div>
					</div>
					<div><strong>Needs to sell his/her current home:</strong> {[{ constant[offer.viewdata.sectionbuyingmenu2asnwer] }]}</div>
					<div ng-if="offer.viewdata.sectionbuyingmenu2asnwer === 'yes'">
						<div><strong>Current Home Address:</strong> {[{ offer.viewdata.sccurrentaddress }]}</div>
						<div><strong>Has a current accepted offer to sell the house:</strong> {[{ constant[offer.viewdata.sectionbuyingmenu3asnwer] }]}</div>
						<div ng-switch="offer.viewdata.sectionbuyingmenu3asnwer">
							<div ng-switch-when="yes">
								<div><strong>Contract date that the house must be sold:</strong> {[{ offer.viewdata.contract_date }]}</div>
							</div>
							<div ng-switch-when="no">
								<div><strong>Has already listed the house for sale:</strong> {[{ constant[offer.viewdata.sectionlistedanswer] }]}</div>
								<div ng-switch="offer.viewdata.sectionlistedanswer">
									<div ng-switch-when="yes">
										<div><strong>With:</strong> {[{ constant[offer.viewdata.sectionwhomanswer] }]}</div>
										<div ng-switch="offer.viewdata.sectionwhomanswer" class="m-l-15">
											<div ng-switch-when="realtor">
												<div><strong>Name:</strong> {[{ offer.viewdata.whom_realtor }]}</div>
												<div><strong>Real Estate Brokerage:</strong> {[{ offer.viewdata.whom_realstate }]}</div>
											</div>
											<div ng-switch-when="other">
												<div>{[{ offer.viewdata.whom_other }]}</div>
											</div>
										</div>
									</div>
									<div ng-switch-when="no">
										<div><strong>Wants to sell the house with Turbo homes:</strong> {[{ constant[offer.viewdata.sell_home_to_turbohomes] }]}</div>
									</div>
								</div>
								<div><strong>Contract date that an offer to sell the house must be accepted:</strong> {[{ offer.viewdata.accept_offer_date }]}</div>
								<div><strong>Contract date that the house must be sold:</strong> {[{ offer.viewdata.must_sell_current_house_contract }]}</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Important Dates</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div>
						<strong>Hear back from the seller in:</strong> {[{ offer.viewdata.importlistoneanswer != 'other' ? offer.viewdata.importlistoneanswer : offer.viewdata.importlistoneanswerother }]}
					</div>
					<div>
						<strong>Due Diligence/Inspections Deadline:</strong> {[{ offer.viewdata.inspectiondeadline }]}
					</div>
					<div>
						<strong>Inspector selected:</strong> {[{ constant[offer.viewdata.inspectorselected] }]}
					</div>
					<div>
						<strong>Closing Date:</strong> {[{ offer.viewdata.closingdate != 'other' ? offer.viewdata.closingdate + " Days" : offer.viewdata.closingdateother }]}
					</div>
				</div>
			</div>
		</div> <!-- /.col-md-4 -->
		<div class="col-md-6" ng-if="offer.viewdata.status !== constant.pending">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Sent Mail Status</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
                   <div id="buyer" class="tab-pane fade in active">
                   	    <div class="table-responsive">
                   	    	<table class="table table-hover">
                   	    		<thead>
                   	    			<tr>
                   	    			    <th style="border:solid 1px #f4f4f4;">Type</th>
                   	    				<th style="border:solid 1px #f4f4f4;">Status</th>
                   	    				<th style="border:solid 1px #f4f4f4;">Date Sent</th>
                   	    				<th style="border:solid 1px #f4f4f4;">Action</th>
                   	    			</tr>
                   	    		</thead>
                   	    		<tbody>
                   	    			<tr ng-repeat="email in offer.emailstatuslist">
                   	    				<td>{[{ email.type }]}</td>
                   	    				<td>{[{ email.status }]}</td>
                   	    				<td>{[{ email.date_sent }]}</td>
                   	    				<th><button type="button" class="btn btn-primary" ng-click="sendEmail(email.model)">Send Email</button></th>
                   	    			</tr>
                   	    		</tbody>
                   	    	</table>
                   	    </div>
                   	</div>
				</div>
			</div>
		</div><!-- /.col-md-6 -->
	</div> <!-- /.row -->
</section>

<script type="text/ng-template" id="setContract.html">
	<form name="contractForm" novalidate ng-submit="setContract(contractForm)">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="icon-info"></i> Set Contract Info</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-offset-2 col-sm-8">
						<div class="form-group">
							<label for="contractdate">Contract Date</label>
							<input type="text" class="form-control" name="contractdate" ng-model="data.contractdate" date-picker required readonly>
						</div>
						<div class="form-group">
							<label for="inspectiondate">Inspection Date</label>
							<input type="text" class="form-control" name="inspectiondate" ng-model="data.inspectiondate" date-picker required readonly>
						</div>
						<div class="form-group">
							<label for="closingdate">Closing Date</label>
							<input type="text" class="form-control" name="closingdate" ng-model="data.closingdate" date-picker required readonly>
						</div>
						<!-- <div class="form-group">
							<label for="closingdate">Home # <small>(optional: can be found at the listed home details)</small></label>
							<input type="text" class="form-control" name="homeid" ng-model="data.homeid">
						</div> -->
					</div>
				</div>
			</div>
			<footer class="panel-footer">
				<div class="pull-right">
					<button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				<div style="clear:both;"></div>
			</footer>
		</div>
	</form>
</script>

<script type="text/ng-template" id="viewContract.html">
	<form name="viewContractForm" novalidate ng-submit="updateContract(viewContractForm)">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="icon-info"></i> View Contract Info</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-offset-2 col-sm-8">
						<div class="form-group">
							<label for="contractdate">Contract Date</label>
							<input type="text" class="form-control" name="contractdate" ng-model="contract.contractdate" date-picker required readonly min-date="minDate">
						</div>
						<div class="form-group">
							<label for="inspectiondate">Inspection Date</label>
							<input type="text" class="form-control" name="inspectiondate" ng-model="contract.inspectiondate" date-picker required readonly min-date="minDate">
						</div>
						<div class="form-group">
							<label for="closingdate">Closing Date</label>
							<input type="text" class="form-control" name="closingdate" ng-model="contract.closingdate" date-picker required readonly min-date="minDate">
						</div>
						<!-- <div class="form-group">
							<label for="closingdate">Home #</label>
							<input type="text" class="form-control" name="homeid" ng-model="contract.homeid" disabled>
						</div> -->
					</div>
				</div>
			</div>
			<footer class="panel-footer">
				<div class="pull-right">
					<button class="btn btn-default" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
				<div style="clear:both;"></div>
			</footer>
		</div>
	</form>
</script>