<section class="content-header" id="gototop">
	<h1>
	Edit Offer
	</h1>
	<ol class="breadcrumb">
		<li><a href="#/dashboard"><i class="fa fa-dashboard"></i> Main</a></li>
		<li class="active"><a href="">Edit Offer</a></li>
	</ol>
</section>
<section class="content offer-details">
	
	<div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">{[{alert.msg}]}</div>

    <form name="form" ng-submit="save(form)" novalidate="">
	<div class="row">
		<div class="col-md-6">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Property</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label>Property's Address</label>
						<span class="text-danger pull-right" ng-messages="form.scpropertyaddress.$error" ng-if="setError('scpropertyaddress')">
							<span ng-message="required">Property Address Required</span>
						</span>
						<input type="text" name="scpropertyaddress" ng-model="field.scpropertyaddress"  class="form-control" placeholder="Enter the address of the home you want to buy" required g-places-autocomplete autocomplete="off">
					</div>
					<br/>
					<div class="form-group">
						<label>House Listed By</label>
						<span class="text-danger pull-right" ng-messages="form.listedmenuasnwer.$error" ng-if="setError('listedmenuasnwer')">
							<span ng-message="required">Please Select One</span>
						</span>
						<div class="form-group">
							<label>
							<input type="radio" name="listedmenuasnwer" class="flat-blue" value="turbo" ng-model="field.listedmenuasnwer" icheck required>
							&nbsp;Turbo Homes
							</label>
							&nbsp;
							<label>
								<input type="radio" name="listedmenuasnwer" class="flat-blue" value="realtor" ng-model="field.listedmenuasnwer" icheck required>
								&nbsp;Realtor
							</label>
							&nbsp;
							<label>
								<input type="radio" name="listedmenuasnwer" class="flat-blue" value="other" ng-model="field.listedmenuasnwer" icheck required>
								&nbsp;Other
							</label>
							&nbsp;
							<label>
								<input type="radio" name="listedmenuasnwer" class="flat-blue" value="notsure" ng-model="field.listedmenuasnwer" icheck required>
								&nbsp;Not Sure Request Attorney Help
							</label>	
						</div>
						<div class="form-group" ng-if="field.listedmenuasnwer === 'turbo'">
							<div class="form-group">
								<label>Seller's Name</label>
								<span class="text-danger pull-right" ng-messages="form.scsellername.$error" ng-if="setError('scsellername')">
									<span ng-message="required">
										Name is required
									</span>
								</span>
								<input type="text" class="form-control" name="scsellername" ng-model="field.scsellername" required="">
							</div>
							<div class="form-group">
								<label>Seller's Email</label>
								<span class="text-danger pull-right" ng-messages="form.scselleraddress.$error" ng-if="setError('scselleraddress')">
									<span ng-message="required">
										Email is required
									</span>
									<span ng-message="email">
										Invalid Email
									</span>
								</span>
								<input type="text" class="form-control" name="scselleraddress" ng-model="field.scselleraddress" required="" ng-change="validateEmail(field.scselleraddress, 'scselleraddress')">
							</div>
						</div>
						<div class="form-group" ng-if="field.listedmenuasnwer === 'realtor'">
							<div class="form-group">
								<label>Realtor's Name</label>
								<span class="text-danger pull-right" ng-messages="form.screaltorname.$error" ng-if="setError('screaltorname')">
									<span ng-message="required">
										Name is required
									</span>
								</span>
								<input type="text" class="form-control" name="screaltorname" ng-model="field.screaltorname" required="">
							</div>
							<div class="form-group">
								<label>Realtor's Email</label>
								<span class="text-danger pull-right" ng-messages="form.screaltoraddress.$error" ng-if="setError('screaltoraddress')">
									<span ng-message="required">
										Email is required
									</span>
									<span ng-message="email">
										Invalid Email
									</span>
								</span>
								<input type="text" class="form-control" name="screaltoraddress" ng-model="field.screaltoraddress" required="" ng-change="validateEmail(field.screaltoraddress, 'screaltoraddress')">
							</div>
							<div class="form-group">
								<label>Brokerage Name</label>
								<span class="text-danger pull-right" ng-messages="form.scbrokerageaddress.$error" ng-if="setError('scbrokerageaddress')">
									<span ng-message="required">
										Name is required
									</span>
								</span>
								<input type="text" class="form-control" name="scbrokerageaddress" ng-model="field.scbrokerageaddress" required="">
							</div>
						</div>
						<div class="form-group" ng-if="field.listedmenuasnwer === 'other'">
							<div class="form-group">
								<label>Seller's Name</label>
								<span class="text-danger pull-right" ng-messages="form.scothersellername.$error" ng-if="setError('scothersellername')">
									<span ng-message="required">
										Name is required
									</span>
								</span>
								<input type="text" class="form-control" name="scothersellername" ng-model="field.scothersellername" required="">
							</div>
							<div class="form-group">
								<label>Seller's Email</label>
								<span class="text-danger pull-right" ng-messages="form.scotherselleraddress.$error" ng-if="setError('scotherselleraddress')">
									<span ng-message="required">
										Email is required
									</span>
									<span ng-message="email">
										Invalid Email
									</span>
								</span>
								<input type="text" class="form-control" name="scotherselleraddress" ng-model="field.scotherselleraddress" required="" ng-change="validateEmail(field.scotherselleraddress, 'scotherselleraddress')">
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Financing</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label>Offer</label>
						<span class="text-danger pull-right" ng-messages="form.scfinancingamt.$error" ng-if="setError('scfinancingamt')">
							<span ng-message="required">Offer is required</span>							
						</span>
						<input type="text" class="form-control" name="scfinancingamt" ng-model="field.scfinancingamt" ng-blur="checkIfEmpty('scfinancingamt');" ng-keyup="setEarnestMoney($event);checkIfEmpty('scfinancingamt', $event);" required mask-money>
					</div>
					<div class="form-group">
						<label>Earnest money offer</label>
						<span class="text-danger pull-right" ng-messages="form.sc_financing_earnest_amt.$error" ng-if="setError('sc_financing_earnest_amt')">
							<span ng-message="required">Earnest money offer
							 is required</span>							
						</span>
						<input type="text" class="form-control" name="sc_financing_earnest_amt" ng-model="field.sc_financing_earnest_amt" ng-blur="checkIfEmpty('sc_financing_earnest_amt');" ng-keyup="setOtherEarnestMoney($event);checkIfEmpty('sc_financing_earnest_amt', $event);" required mask-money with-decimal>
					</div>
					<div class="form-group">
						<label>Have a title company want to work with?</label>
						<span class="text-danger pull-right" ng-messages="form.financingmenulistthreeanswer.$error" ng-if="setError('financingmenulistthreeanswer')">
							<span ng-message="required">
								Please Select One
							</span>
						</span>
						<div class="form-group">
							<label>
							<input type="radio" name="financingmenulistthreeanswer" class="flat-blue" value="yes" ng-model="field.financingmenulistthreeanswer" icheck required>
							&nbsp;Yes
							</label>
							&nbsp;
							<label>
								<input type="radio" name="financingmenulistthreeanswer" class="flat-blue" value="no" ng-model="field.financingmenulistthreeanswer" icheck required>
								&nbsp;No
							</label>
							<div class="form-group" ng-if="field.financingmenulistthreeanswer === 'yes'">
								<div class="form-group">
									<label>Company Name</label>
									<span class="text-danger pull-right" ng-messages="form.scfinancingescrowname.$error" ng-if="setError('scfinancingescrowname')">
										<span ng-message="required">
											Company name required
										</span>
									</span>
									<input type="text" name="scfinancingescrowname" ng-model="field.scfinancingescrowname" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>Officer Name</label>
									<span class="text-danger pull-right" ng-messages="form.scfinancingescrowofficername.$error" ng-if="setError('scfinancingescrowofficername')">
										<span ng-message="required">
											Officer name required
										</span>
									</span>
									<input type="text" name="scfinancingescrowofficername" ng-model="field.scfinancingescrowofficername" class="form-control" required="">
								</div>
								<div class="form-group">
									<label>Telephone</label>
									<span class="text-danger pull-right" ng-messages="form.scfinancingescrowtel.$error" ng-if="setError('scfinancingescrowtel')">
										<span ng-message="required">
											Telephone is required
										</span>
									</span>
									<input type="text" name="scfinancingescrowtel" ng-model="field.scfinancingescrowtel" class="form-control" required="">
								</div>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Plan on paying the house</label>
						<div class="form-group">
							<label>Earnest Money</label>
							<span class="text-danger pull-right" ng-messages="form.scfinancingearnestamt.$error" ng-if="setError('scfinancingearnestamt')">
								<span ng-message="required">Earnest Money required</span>								
							</span>
							<input class="form-control" type="text" name="scfinancingearnestamt" ng-model="field.scfinancingearnestamt" ng-blur="checkIfEmpty('scfinancingearnestamt');" ng-keyup="checkIfEmpty('scfinancingearnestamt', $event);"  required="" mask-money with-decimal>
						</div>
						<div class="form-group">
							<label>Cash/Down Payment</label>
							<span class="text-danger pull-right" ng-messages="form.down_payment.$error" ng-if="setError('down_payment')">
								<span ng-message="required">Cash/Down Payment required</span>								
							</span>
							<input class="form-control" type="text" name="down_payment" ng-model="field.down_payment" ng-blur="checkIfEmpty('down_payment');" ng-keyup="checkIfEmpty('down_payment', $event);"  required="" mask-money>
						</div>
						<div class="form-group">
							<label>Mortgage/Loan</label>
							<span class="text-danger pull-right" ng-messages="form.mortgage.$error" ng-if="setError('mortgage')">
								<span ng-message="required">Mortgage/Loan required</span>								
							</span>
							<input class="form-control" type="text" name="mortgage" ng-model="field.mortgage" ng-blur="checkIfEmpty('mortgage');" ng-keyup="checkIfEmpty('mortgage', $event);"  required="" mask-money>
						</div>
						<div class="form-group">
							<label>Other</label>
							<input class="form-control" type="text" name="planforpayingother" ng-model="field.planforpayingother" mask-money>
						</div>
						<div class="form-group">
							<label>Have been prequalified?</label>
							<span class="text-danger pull-right" ng-messages="form.financingmenulisttwoanswer.$error" ng-if="setError('financingmenulisttwoanswer')">
								<span ng-message="required">Please Select One</span>
							</span>
							<div class="form-group">
								<label>
									<input type="radio" name="financingmenulisttwoanswer" class="flat-blue" value="yes" ng-model="field.financingmenulisttwoanswer" icheck required>
									&nbsp;Yes
								</label>
								&nbsp;
								<label>
									<input type="radio" name="financingmenulisttwoanswer" class="flat-blue" value="no" ng-model="field.financingmenulisttwoanswer" icheck required>
									&nbsp;No
								</label>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Type of Financing</label>
						<span class="text-danger pull-right" ng-messages="form.type_of_financing.$error" ng-if="setError('type_of_financing')">
							<span ng-message="required">Please Select One</span>
						</span>
						<div class="form-group">
							<label>
								<input type="radio" name="type_of_financing" class="flat-blue" value="conventional" ng-model="field.type_of_financing" icheck required>
								&nbsp;Conventional
							</label>
							<br/>
							<label>
								<input type="radio" name="type_of_financing" class="flat-blue" value="fha" ng-model="field.type_of_financing" icheck required>
								&nbsp;FHA
							</label>
							<br/>
							<label>
								<input type="radio" name="type_of_financing" class="flat-blue" value="va" ng-model="field.type_of_financing" icheck required>
								&nbsp;VA
							</label>
							<br/>
							<label>
								<input type="radio" name="type_of_financing" class="flat-blue" value="usda" ng-model="field.type_of_financing" icheck required>
								&nbsp;USDA
							</label>
							<br/>
							<label>
								<input type="radio" name="type_of_financing" class="flat-blue" value="assumption" ng-model="field.type_of_financing" icheck required>
								&nbsp;Assumption
							</label>
							<br/>
							<label>
								<input type="radio" name="type_of_financing" class="flat-blue" value="seller_carryback" ng-model="field.type_of_financing" icheck required>
								&nbsp;Seller Carryback
							</label>
							<br/>
							<label>
								<input type="radio" name="type_of_financing" class="flat-blue" value="other" ng-model="field.type_of_financing" icheck required>
								&nbsp;Other
							</label>
							<br/>
							<div class="form-group" ng-if="field.type_of_financing === 'other'">
								<input class="form-control" placeholde="Please Specify this field" type="text" name="type_of_financing_other" ng-model="field.type_of_financing_other" required="">
								<small class="pull-right text-danger" ng-if="setError('type_of_financing_other')" ng-messages="form.type_of_financing_other.$error">
									<span ng-message="required">This field is required.</span>
								</small>
							</div>
							<label>
								<input type="radio" name="type_of_financing" class="flat-blue" value="notsure" ng-model="field.type_of_financing" icheck required>
								&nbsp;Not Sure Request Attorney Help
							</label>
						</div>
					</div>
					<div class="form-group">
						<label>Helps the seller to help cover closing costs?</label>
						<span class="text-danger pull-right" ng-messages="form.financingmenulistfouranswer.$error" ng-if="setError('financingmenulistfouranswer')">
							<span ng-message="required">Please Select One</span>
						</span>
						<div class="form-group">
							<label>
								<input type="radio" name="financingmenulistfouranswer" class="flat-blue" value="yes" ng-model="field.financingmenulistfouranswer" icheck required>
								&nbsp;Yes
							</label>
							&nbsp;
							<label>
								<input type="radio" name="financingmenulistfouranswer" class="flat-blue" value="no" ng-model="field.financingmenulistfouranswer" icheck required>
								&nbsp;No
							</label>
						</div>
						<div class="form-group" ng-if="field.financingmenulistfouranswer === 'yes'">
							<label>Amount</label>
							<span class="text-danger pull-right" ng-messages="form.closingcostamount.$error" ng-if="setError('closingcostamount')">
								<span ng-message="required">
									This field is required
								</span>
							</span>
							<input type="text" name="closingcostamount" class="form-control" ng-model="field.closingcostamount" ng-blur="checkIfEmpty('closingcostamount')" ng-keyup="checkIfEmpty('closingcostamount', $event)" required="" mask-money>
						</div>
					</div>
					<div class="form-group">
						<label>Home warranty included?</label>
						<span class="text-danger pull-right" ng-messages="form.financingmenulistsixanswer.$error" ng-if="setError('financingmenulistsixanswer')">
							<span ng-message="required">Please Select One</span>
						</span>
						<div class="form-group">
							<label>
								<input type="radio" name="financingmenulistsixanswer" class="flat-blue" value="yes" ng-model="field.financingmenulistsixanswer" icheck required>
								&nbsp;Yes
							</label>
							&nbsp;
							<label>
								<input type="radio" name="financingmenulistsixanswer" class="flat-blue" value="no" ng-model="field.financingmenulistsixanswer" icheck required>
								&nbsp;No
							</label>
						</div>
						<div class="form-group" ng-if="field.financingmenulistsixanswer === 'yes'">
							<div class="form-group">	
							    <label>Home warranty is payed by?</label>
								<span class="text-danger pull-right" ng-messages="form.financingmenulistsevenanswer.$error" ng-if="setError('financingmenulistsevenanswer')">
									<span ng-message="required">Please Select One</span>
								</span>
								<div class="form-group">
									<label>
										<input type="radio" name="financingmenulistsevenanswer" class="flat-blue" value="buyer" ng-model="field.financingmenulistsevenanswer" icheck required>
										&nbsp;Buyer
									</label>
									&nbsp;
									<label>
										<input type="radio" name="financingmenulistsevenanswer" class="flat-blue" value="seller" ng-model="field.financingmenulistsevenanswer" icheck required>
										&nbsp;Seller
									</label>
									&nbsp;
									<label>
										<input type="radio" name="financingmenulistsevenanswer" class="flat-blue" value="split" ng-model="field.financingmenulistsevenanswer" icheck required>
										&nbsp;Split evenly
									</label>
								</div>
							</div>
							<div class="form-group">
								<label>Cost to not exceed</label>
								<span class="text-danger pull-right" ng-messages="form.costnotexceed.$error" ng-if="setError('costnotexceed')">
									<span ng-message="required">
										This field is required
									</span>
							    </span>
								<input type="text" name="costnotexceed" class="form-control" ng-model="field.costnotexceed" ng-blur="checkIfEmpty('costnotexceed')" ng-keyup="checkIfEmpty('costnotexceed', $event)" required="" mask-money>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> <!-- /.col-md-4 -->
		<div class="col-md-6">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Buyer's Information</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label>Name</label>
						<span class="text-danger pull-right" ng-messages="form.buyersname.$error" ng-if="setError('buyersname')">
							<span ng-message="required">Name is required</span>
						</span>
						<input type="text" name="buyersname" ng-model="field.buyersname" required="" class="form-control">
					</div>
					<div class="form-group">
						<label>Email</label>
						<span class="text-danger pull-right" ng-messages="form.buyersemail.$error" ng-if="setError('buyersemail')">
							<span ng-message="required">Email is required</span>
							<span ng-message="email">Invalid Email</span>
						</span>
						<input type="text" name="buyersemail" ng-model="field.buyersemail" required="" class="form-control" ng-change="validateEmail(field.buyersemail, 'buyersemail')">
					</div>
					<div class="form-group">
						<label>
							Buying the house :
						</label>
						<span class="pull-right text-danger" ng-messages="form.sectionbuyingmenuasnwer.$error" ng-if="setError('sectionbuyingmenuasnwer')">
							<span ng-message="required">
								Please Select One
							</span>
						</span>
						<div class="form-group">
							<label>
								<input type="radio" name="sectionbuyingmenuasnwer" class="flat-blue" value="alone" ng-model="field.sectionbuyingmenuasnwer" icheck required>
								&nbsp;Alone
							</label>
							&nbsp;
							<label>
								<input type="radio" name="sectionbuyingmenuasnwer" class="flat-blue" value="someone" ng-model="field.sectionbuyingmenuasnwer" icheck required>
								&nbsp;With Someone
							</label>
							&nbsp;
							<label>
								<input type="radio" name="sectionbuyingmenuasnwer" class="flat-blue" value="notsure" ng-model="field.sectionbuyingmenuasnwer" icheck required>
								&nbsp;Not Sure Request Attorney Help
							</label>
							<div class="form-group" ng-if="field.sectionbuyingmenuasnwer === 'someone'">
								<div class="form-group" ng-repeat="other in field.otherbuyer">
									<label>Buyer {[{ $index + 1 }]}</label>
									<button type="button" class="btn btn-danger pull-right" ng-click="removeAnotherBuyer($index)" ng-if="$index > 0">
										<i class="fa fa-times"></i>
									</button>
									<div class="form-group">
										<label>Name</label>
										<span class="pull-right text-danger" ng-if="setError('name' + $index)" ng-messages="form['name' + $index].$error" >
											<span ng-message="required">
												Name is required
											</span>
										</span>
										<input type="text" name="name{[{ $index }]}" ng-model="other.name" class="form-control" required>
									</div>
									<div class="form-group">
										<label>Email</label>
										<span class="pull-right text-danger" ng-if="setError('email' + $index)" ng-messages="form['email' + $index].$error" >
											<span ng-message="required">
												Email is required
											</span>
											<span ng-message="email">
												Invalid Email
											</span>
										</span>
										<input type="text" name="email{[{ $index }]}" ng-model="other.email" class="form-control" ng-change="validateEmail(other.email, 'email' + $index)" required>
									</div>
								</div>
								<button type="button" class="btn btn-primary pull-right" ng-click="addAnotherBuyer();">
									<i class="fa fa-plus"></i>
								</button>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>Needs to sell his/her current home:</label>
						<span class="text-danger pull-right" ng-messages="form.sectionbuyingmenu2asnwer.$error" ng-if="setError('sectionbuyingmenu2asnwer')">
							<span ng-message="required">
							Please Select One</span>
						</span>
						<div class="form-group">
							<label>
								<input type="radio" name="sectionbuyingmenu2asnwer" class="flat-blue" value="yes" ng-model="field.sectionbuyingmenu2asnwer" icheck required>
								&nbsp;Yes
							</label>
							&emsp;
							<label>
								<input type="radio" name="sectionbuyingmenu2asnwer" class="flat-blue" value="no" ng-model="field.sectionbuyingmenu2asnwer" icheck required>
								&nbsp;No
							</label>
							&emsp;
							<label>
								<input type="radio" name="sectionbuyingmenu2asnwer" class="flat-blue" value="notsure" ng-model="field.sectionbuyingmenu2asnwer" icheck required>
								&nbsp;Not Sure Request Attorney Help
							</label>
							<div class="form-group" ng-if="field.sectionbuyingmenu2asnwer === 'yes'">
								<div class="form-group">
									<label>Address of current home</label>
									<span class="text-danger pull-right" ng-messages="form.sccurrentaddress.$error" ng-if="setError('sccurrentaddress')">
										<span ng-message="required">This field is required</span>
									</span>
									<input type="text" name="sccurrentaddress" ng-model="field.sccurrentaddress"  class="form-control" placeholder="Enter the address of current home" required g-places-autocomplete autocomplete="off">
								</div>
								<div class="form-group">
									<label>Have a current accepted offer to sell the current house?</label>
									<span class="text-danger pull-right" ng-messages="form.sectionbuyingmenu3asnwer.$error" ng-if="setError('sectionbuyingmenu3asnwer')">
										<span ng-message="required">
										Please Select One</span>
									</span>
									<div class="form-group">
										<label>
											<input type="radio" name="sectionbuyingmenu3asnwer" class="flat-blue" value="yes" ng-model="field.sectionbuyingmenu3asnwer" icheck required>
											&nbsp;Yes
										</label>
										&emsp;
										<label>
											<input type="radio" name="sectionbuyingmenu3asnwer" class="flat-blue" value="no" ng-model="field.sectionbuyingmenu3asnwer" icheck required>
											&nbsp;No
										</label>
									</div>
									<div class="form-group" ng-if="field.sectionbuyingmenu3asnwer === 'yes'">
										<div class="form-group">
											<label>The date want in the contract that must sell the house by.</label>
											<span class="text-danger pull-right" ng-messages="form.contract_date.$error" ng-if="setError('contract_date') && isDatePickerLoaded.contractDate">
												<span ng-message="required">This field is required. 
												</span>
											</span>
											<input type="text" name="contract_date" class="form-control" ng-model="field.contract_date"
											ng-blur="isDatePickerLoaded.contractDate = true" date-picker required readonly>		
										</div>
									</div>
									<div class="form-group" ng-if="field.sectionbuyingmenu3asnwer === 'no'">
										<div class="form-group">
											<label>Had already listed the home for sale?</label>
											<span class="text-danger pull-right" ng-messages="form.sectionlistedanswer.$error" ng-if="setError('sectionlistedanswer')">
												<span ng-message="required">
												Please Select One</span>
											</span>
											<div class="form-group">
												<label>
													<input type="radio" name="sectionlistedanswer" class="flat-blue" value="yes" ng-model="field.sectionlistedanswer" icheck required>
													&nbsp;Yes
												</label>
												&emsp;
												<label>
													<input type="radio" name="sectionlistedanswer" class="flat-blue" value="no" ng-model="field.sectionlistedanswer" icheck required>
													&nbsp;No
												</label>
											</div>
											<div class="form-group" ng-if="field.sectionlistedanswer === 'yes'">
												<label>With whom?</label>
												<span class="text-danger pull-right" ng-messages="form.sectionwhomanswer.$error" ng-if="setError('sectionwhomanswer')">
													<span ng-message="required">
													Please Select One</span>
												</span>
												<div class="form-group">
													<label>
														<input type="radio" name="sectionwhomanswer" class="flat-blue" value="turbohomes" ng-model="field.sectionwhomanswer" icheck required>
														&nbsp;Turbo Homes
													</label>
													&emsp;
													<label>
														<input type="radio" name="sectionwhomanswer" class="flat-blue" value="realtor" ng-model="field.sectionwhomanswer" icheck required>
														&nbsp;Realtor
													</label>
													&emsp;
													<label>
														<input type="radio" name="sectionwhomanswer" class="flat-blue" value="self" ng-model="field.sectionwhomanswer" icheck required>
														&nbsp;Self
													</label>
													&emsp;
													<label>
														<input type="radio" name="sectionwhomanswer" class="flat-blue" value="other" ng-model="field.sectionwhomanswer" icheck required>
														&nbsp;Other
													</label>
												</div>
												<div class="form-group" ng-if="field.sectionwhomanswer === 'realtor'">
												    <div class="form-group">
												    	<label>Realtor's Name</label>
												    	<span class="text-danger pull-right" ng-messages="form.whom_realtor.$error" ng-if="setError('whom_realtor')">
															<span ng-message="required">Realtor Name required</span>												    		
												    	</span>
												    	<input type="text" name="whom_realtor" class="form-control" ng-model="field.whom_realtor" required="">
												    </div>
												    <div class="form-group">
												    	<label>Real Estate Brokerage</label>
												    	<span class="text-danger pull-right" ng-messages="form.whom_realstate.$error" ng-if="setError('whom_realstate')">
															<span ng-message="required">Real Estate Brokerage required</span>												    		
												    	</span>
												    	<input type="text" name="whom_realstate" class="form-control" ng-model="field.whom_realstate" required="">
												    </div>	
												</div>
												<div class="form-group" ng-if="field.sectionwhomanswer === 'other'">
												    <div class="form-group">
												    	<label>Other Description</label>
												    	<span class="text-danger pull-right" ng-messages="form.whom_other.$error" ng-if="setError('whom_other')">
															<span ng-message="required">Other Description is required</span>												    		
												    	</span>
												    	<input type="text" name="whom_other" class="form-control" ng-model="field.whom_other" required="">
												    </div>
												</div>
											</div>
											<div class="form-group" ng-if="field.sectionlistedanswer === 'no'">
												<label>Want to sell the home with Turbo and save thousands?</label>
												<span class="text-danger pull-right" ng-messages="form.sell_home_to_turbohomes.$error" ng-if="setError('sell_home_to_turbohomes')">
													<span ng-message="required">
													Please Select One</span>
												</span>
												<div class="form-group">
													<label>
														<input type="radio" name="sell_home_to_turbohomes" class="flat-blue" value="yes" ng-model="field.sell_home_to_turbohomes" icheck required>
														&nbsp;Yes
													</label>
													&emsp;
													<label>
														<input type="radio" name="sell_home_to_turbohomes" class="flat-blue" value="no" ng-model="field.sell_home_to_turbohomes" icheck required>
														&nbsp;No
													</label>
												</div>
											</div>
											<div class="form-group" ng-if="field.sectionlistedanswer">
												<div class="form-group">
													<label>The date want in the contract that must accept an offer to sell the house.</label>
													<span class="text-danger pull-right" ng-messages="form.accept_offer_date.$error" ng-if="setError('accept_offer_date') && isDatePickerLoaded.acceptOfferDate">
														<span ng-message="required">This field is required. 
														</span>
													</span>
													<input type="text" name="accept_offer_date" class="form-control" ng-model="field.accept_offer_date"
													ng-blur="isDatePickerLoaded.acceptOfferDate = true" date-picker required readonly>
												</div>
												<div class="form-group">
													<label>The date want in the contract that must sell the house by.</label>
													<span class="text-danger pull-right" ng-messages="form.must_sell_current_house_contract.$error" ng-if="setError('must_sell_current_house_contract') && isDatePickerLoaded.mustSellDate">
														<span ng-message="required">This field is required. 
														</span>
													</span>
													<input type="text" name="must_sell_current_house_contract" class="form-control" ng-model="field.must_sell_current_house_contract"
													ng-blur="isDatePickerLoaded.mustSellDate = true" date-picker required readonly>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Important Dates</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label>Hear back from the seller in</label>
						<span class="text-danger pull-right" ng-messages="form.importlistoneanswer.$error" ng-if="setError('importlistoneanswer')">
							<span ng-message="required">Please Select One</span>
						</span>
						<div class="form-group">
							<label>
								<input type="radio" name="importlistoneanswer" class="flat-blue" value="24" ng-model="field.importlistoneanswer" icheck required>
								&nbsp;24 Hours
							</label>
							&emsp;
							<label>
								<input type="radio" name="importlistoneanswer" class="flat-blue" value="48" ng-model="field.importlistoneanswer" icheck required>
								&nbsp;48 Hours
							</label>
							&emsp;
							<label>
								<input type="radio" name="importlistoneanswer" class="flat-blue" value="72" ng-model="field.importlistoneanswer" icheck required>
								&nbsp;72 Hours
							</label>
							&emsp;
							<label>
								<input type="radio" name="importlistoneanswer" class="flat-blue" value="other" ng-model="field.importlistoneanswer" icheck required>
								&nbsp;Other
							</label>
							&emsp;
						</div>
						<div class="form-group" ng-if="field.importlistoneanswer === 'other'">
							<input type="text" name="importlistoneanswerother" placeholder="Please specify" class="form-control" ng-model="field.importlistoneanswerother"
							ng-blur="isDatePickerLoaded.listOneOtherDate = true" date-picker required readonly>
							<small class="text-danger pull-right" ng-messages="form.importlistoneanswerother.$error" ng-if="setError('importlistoneanswerother') && isDatePickerLoaded.listOneOtherDate">
								<span ng-message="required">This field is required. 
								</span>
							</small>
						</div>
					</div>
					<div class="form-group">
						<label>Due Diligence/Inspections Deadline</label>
						<span class="text-danger pull-right" ng-messages="form.inspectiondeadline.$error" ng-if="setError('inspectiondeadline')">
							<span ng-message="required">Due Diligence/Inspections Deadline required</span>
						</span>
						<input type="text" name="inspectiondeadline" ng-model="field.inspectiondeadline" ng-blur="checkIfEmpty('inspectiondeadline');" ng-keyup="checkIfEmpty('inspectiondeadline', $event)" class="form-control" mask-money no-prefix required="">
					</div>
					<div class="form-group">
						<label>Have an inspector selected?</label>
						<span class="pull-right text-danger" ng-if="setError('inspectorselected')" ng-messages="form.inspectorselected.$error">
							<span ng-message="required">Please Select One</span>
						</span>
						<div class="form-group">
							<label>
								<input type="radio" name="inspectorselected" class="flat-blue" value="yes" ng-model="field.inspectorselected" icheck required>
								&nbsp;Yes
							</label>
							&emsp;
							<label>
								<input type="radio" name="inspectorselected" class="flat-blue" value="no" ng-model="field.inspectorselected" icheck required>
								&nbsp;No
							</label>
						</div>
					</div>
					<div class="form-group">
						<label>Closing date</label>
						<span class="pull-right text-danger" ng-if="setError('closingdate')" ng-messages="form.closingdate.$error">
							<span ng-message="required">Please Select One</span>
						</span>
						<div class="form-group">
							<!-- <label ng-if="field.mortgage == 0 || !field.mortgage">
								<input type="radio" name="closingdate" class="flat-blue" value="30" ng-model="field.closingdate" icheck required>
								&nbsp;30 Days
							</label>
							<br/ ng-if="field.mortgage == 0 || !field.mortgage"> -->
							<label>
								<input type="radio" name="closingdate" class="flat-blue" value="45" ng-model="field.closingdate" icheck required>
								&nbsp;45 Days
							</label>
							<br/>
							<label>
								<input type="radio" name="closingdate" class="flat-blue" value="60" ng-model="field.closingdate" icheck required>
								&nbsp;60 Days
							</label>
							<br/>
							<label>
								<input type="radio" name="closingdate" class="flat-blue" value="90" ng-model="field.closingdate" icheck required>
								&nbsp;90 Days
							</label>
							<br/>
							<label>
								<input type="radio" name="closingdate" class="flat-blue" value="other" ng-model="field.closingdate" icheck required>
								&nbsp;Other
							</label>
						</div>
						<div class="form-group" ng-if="field.closingdate === 'other'">
							<input type="text" name="closingdateother" placeholder="Please specify" class="form-control" ng-model="field.closingdateother"
							ng-blur="isDatePickerLoaded.closingDateOther = true" date-picker required readonly>
							<small class="text-danger pull-right" ng-messages="form.closingdateother.$error" ng-if="setError('closingdateother') && isDatePickerLoaded.closingDateOther">
								<span ng-message="required">This field is required. 
								</span>
							</small>
						</div>
					</div>
				</div>
			</div>
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Inclusions</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label>Included "Personal property" for sale</label>
						<span class="pull-right text-danger" ng-if="setError('closingdate')" ng-messages="form.closingdate.$error">
							<span ng-message="required">Please Select One</span>
						</span>
						<div class="form-group">
							<label>
								<input type="checkbox" name="refrigerator" class="flat-blue" ng-model="field.inclusion.refrigerator" ng-true-value="true" icheck>
								&nbsp;Refrigerator
							</label>
							<br/>
							<div class="form-group" ng-if="field.inclusion.refrigerator">
								<input type="text" name="refrigeratordesc" placeholder="Description (i.e. Make, Model, Color)" ng-model="field.inclusion.refrigeratordesc" class="form-control" required="">
								<small class="text-danger pull-right" ng-messages="form.refrigeratordesc.$error" ng-if="setError('refrigeratordesc')">
									<span ng-message="required">This field is required</span>
								</small>
							</div>
							<label>
								<input type="checkbox" name="washer" class="flat-blue" ng-model="field.inclusion.washer" ng-true-value="true" icheck>
								&nbsp;Washer
							</label>
							<br/>
							<div class="form-group" ng-if="field.inclusion.washer">
								<input type="text" name="washerdesc" placeholder="Description (i.e. Make, Model, Color)" ng-model="field.inclusion.washerdesc" class="form-control" required="">
								<small class="text-danger pull-right" ng-messages="form.washerdesc.$error" ng-if="setError('washerdesc')">
									<span ng-message="required">This field is required</span>
								</small>
							</div>
							<label>
								<input type="checkbox" name="dryer" class="flat-blue" ng-model="field.inclusion.dryer" ng-true-value="true" icheck>
								&nbsp;Dryer
							</label>
							<br/>
							<div class="form-group" ng-if="field.inclusion.dryer">
								<input type="text" name="dryerdesc" placeholder="Description (i.e. Make, Model, Color)" ng-model="field.inclusion.dryerdesc" class="form-control" required="">
								<small class="text-danger pull-right" ng-messages="form.dryerdesc.$error" ng-if="setError('dryerdesc')">
									<span ng-message="required">This field is required</span>
								</small>
							</div>
							<label>
								<input type="checkbox" name="aboveground" class="flat-blue" ng-model="field.inclusion.aboveground" ng-true-value="true" icheck>
								&nbsp;Above-ground spa/hot tub
							</label>
							<br/>
							<div class="form-group" ng-if="field.inclusion.aboveground">
								<input type="text" name="abovegrounddesc" placeholder="Description (i.e. Make, Model, Color)" ng-model="field.inclusion.abovegrounddesc" class="form-control" required="">
								<small class="text-danger pull-right" ng-messages="form.abovegrounddesc.$error" ng-if="setError('abovegrounddesc')">
									<span ng-message="required">This field is required</span>
								</small>
							</div>
							<label>
								<input type="checkbox" name="other" class="flat-blue" ng-model="field.inclusion.other" ng-true-value="true" icheck>
								&nbsp;Other
							</label>
							<br/>
							<div class="form-group" ng-if="field.inclusion.other">
								<input type="text" name="otherdesc" placeholder="Description (i.e. Make, Model, Color)" ng-model="field.inclusion.otherdesc" class="form-control" required="">
								<small class="text-danger pull-right" ng-messages="form.otherdesc.$error" ng-if="setError('otherdesc')">
									<span ng-message="required">This field is required</span>
								</small>
							</div>
					    </div>
					</div> 
					<div class="form-group">
						<label>
							<input type="checkbox" name="discusswithattorny" class="flat-blue" ng-model="field.discusswithattorny" ng-true-value="true" icheck>
						    &nbsp;Discuss with a Turbo Attorney
						</label>
					</div>
					<div class="form-group">
						<label>Additional terms and conditions</label>
						<textarea rows="8" ng-model="field.additionalcondition" class="form-control">
							
						</textarea>
					</div> 
				</div>
			</div>
		</div> <!-- /.col-md-4 -->
		<div class="col-md-12">
			 <div class="box box-default">
	             <div class="box-footer clearfix">
             	    <button class="btn btn-primary pull-right" style="margin-left:  5px">
                    	Save
                	</button>
             	    <a href="#" ui-sref="manageoffer" class="btn btn-default pull-right m-l-15">
             	    	Cancel
             	    </a>
                 </div>
			 </div>
		</div>
	</div> <!-- /.row -->
	</form>

</section>
