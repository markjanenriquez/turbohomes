<section class="content-header" id="gototop">
	<h1>
		Make An Offer Tool
		<!-- <small>Add New User</small> -->
	</h1>
	<ol class="breadcrumb">
		<li><a href="#/dashboard"><i class="fa fa-dashboard"></i> Main</a></li>
		<li class="active">Admin</li>
		<li class="active">Make An Offer Tool</li>
	</ol>
</section>

<section class="content">
	<!-- <div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">{[{alert.msg}]}</div> -->
	<div class="box {[{boxtype}]}">
		<form name="form" ng-submit="save(form)" novalidate="">
			<div class="box-header with-border">
				<h3 class="box-title"></h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<!-- Property -->			
					<div class="col-md-12">
						<h4 class="page-header" ng-model="PropertyMain">Property</h4>
					</div>		
					<div class="col-md-8">
						<h4 class="page-header">Property - Step 1</h4>
						<div class="form-group">
							<label class="control-label" for="sc_property_address">Property's Address</label>
							<input type="text" ng-model="form.data.scpropertyaddress" name="sc_property_address" id="sc_property_address" class="form-control" placeholder="Enter the address of the home you want to buy" required g-places-autocomplete autocomplete="off" />
							<span class="text-red m-t-5 display-b" ng-if="property1.$submitted && property1.sc_property_address.$invalid">Property's Address is required</span>
						</div>
					</div>
					<div class="col-md-8">
						<h4 class="page-header">Property - Step 2</h4>
						<div   ng-init="propertyvar='Porperty'">

							<input type="radio" ng-model="PropertyVar" value="turbohomes" >Turbo Homes

							<input type="radio" ng-model="PropertyVar" value="realtor">Realtor

							<input type="radio" ng-model="PropertyVar" value="other">Other

							<input type="radio" ng-model="PropertyVar" value="notsure">Not Sure Request Attorney Help

							<div ng-show="PropertyVar === 'turbohomes'">
								<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_other_seller_name.$invalid]">
									<label class="control-label" for="sc_other_seller_name">Seller's Name:</label>										
									<input type="text" ng-model="form.data.scothersellername" name="sc_other_seller_name" id="sc_other_seller_name" class="form-control" required />
									<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_other_seller_name.$invalid">Seller's Name is required</span>
								</div>

								<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_other_seller_address.$invalid]">
									<label class="control-label" for="sc_other_seller_address">Seller's Email:</label>									
									<input type="text" ng-model="form.data.scotherselleraddress" name="sc_other_seller_address" id="sc_other_seller_address" class="form-control" required />
									<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_other_seller_address.$invalid">Seller's Email is required</span>
								</div>								
							</div>

							<div ng-show="PropertyVar === 'realtor'">
								<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_realtor_name.$invalid]">
									<label class="control-label" for="sc_realtor_name">Realtor's Name:</label>										
									<input type="text" ng-model="form.data.screaltorname" name="sc_realtor_name" id="sc_realtor_name" class="form-control" required />
									<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_realtor_name.$invalid">Realtor's Name is required</span>
								</div>

								<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_realtor_address.$invalid]">
									<label class="control-label" for="sc_realtor_address">Realtor's Email:</label>										
									<input type="text" ng-model="form.data.screaltoraddress" name="sc_realtor_address" id="sc_realtor_address" class="form-control" required />
									<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_realtor_address.$invalid">Realtor's Email is required</span>
								</div>

								<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_brokerage_address.$invalid]">
									<label class="control-label" for="sc_brokerage_address">Brokerage Name:</label>										
									<input type="text" ng-model="form.data.scbrokerageaddress" name="sc_brokerage_address" id="sc_brokerage_address" class="form-control" required />
									<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_brokerage_address.$invalid">Brokerage Name is required</span>
								</div>


							</div>

							<div ng-show="PropertyVar === 'other'">
								<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_other_seller_name.$invalid]">
									<label class="control-label" for="sc_other_seller_name">Seller's Name:</label>										
									<input type="text" ng-model="form.data.scothersellername" name="sc_other_seller_name" id="sc_other_seller_name" class="form-control" required />
									<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_other_seller_name.$invalid">Seller's Name is required</span>
								</div>

								<div class="form-group" ng-class="{true: 'has-error'}[property2.$submitted && property2.sc_other_seller_address.$invalid]">
									<label class="control-label" for="sc_other_seller_address">Seller's Email:</label>									
									<input type="text" ng-model="form.data.scotherselleraddress" name="sc_other_seller_address" id="sc_other_seller_address" class="form-control" required />
									<span class="text-red m-t-5 display-b" ng-if="property2.$submitted && property2.sc_other_seller_address.$invalid">Seller's Email is required</span>
								</div>	
							</div>
						</div>
					</div>
					<!-- Buyer info -->

					<div class="col-md-12">
						<h4 class="page-header">Buyer's Info</h4>
					</div>
					<div class="col-md-8">
						<h4 class="page-header">Buyer's Info - Step 1</h4>		

						<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo1.$submitted && buyersinfo1.buyersname.$invalid]">
							<label class="control-label" for="sc_other_seller_name">Buyer's Name:</label>										
							<input type="text" ng-init="form.data.buyersname = fullname" ng-model="form.data.buyersname" name="buyersname" id="buyersname" class="form-control" required />

							<span class="text-red m-t-5 display-b" ng-if="buyersinfo1.$submitted && buyersinfo1.buyersname.$invalid">Buyer's Name is required</span>
						</div>					
						<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo1.$submitted && buyersinfo1.buyersemail.$invalid]">
							<label class="control-label" for="sc_other_seller_address">Buyer's Email:</label>										
							<input type="text" ng-init="form.data.buyersemail = email" ng-model="form.data.buyersemail" name="buyersemail" id="buyersemail" class="form-control" required />
							
							<span class="text-red m-t-5 display-b" ng-if="buyersinfo1.$submitted && buyersinfo1.buyersemail.$invalid">Buyer's Email is required</span>
						</div>	
						<label class="control-label">Are you buying the house alone or with someone else?</label>

						<div   ng-init="BuyerVar='Buyer'" ng-switch="form.data.sectionbuyingmenuasnwer">
							<input type="radio" ng-model="BuyerVar" value="alone" >Alone

							<input type="radio" ng-model="BuyerVar" value="withsomeone">With Someone

							<input type="radio" ng-model="BuyerVar" value="notsureattorneyhelp">Not Sure Request Attorney Help

							<div ng-show="BuyerVar === 'alone'">

							</div>
							<div class="animate-switch" ng-show="BuyerVar === 'withsomeone'">
								<!-- Need paganahin yung pag add ng button -->
												<button type="button" class="btn-success btn btn-sm" ng-click="buyer.add()"> Add Buyer </button>
										
								<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo1.$submitted && buyersinfo1.buyersname.$invalid]">
									<label class="control-label" for="sc_other_seller_name">Buyer's Name:</label>										
									<input type="text" ng-init="form.data.buyersname = fullname" ng-model="form.data.buyersname" name="buyersname" id="buyersname" class="form-control" required />

									<span class="text-red m-t-5 display-b" ng-if="buyersinfo1.$submitted && buyersinfo1.buyersname.$invalid">Buyer's Name is required</span>
								</div>					
								<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo1.$submitted && buyersinfo1.buyersemail.$invalid]">
									<label class="control-label" for="sc_other_seller_address">Buyer's Email:</label>										
									<input type="text" ng-init="form.data.buyersemail = email" ng-model="form.data.buyersemail" name="buyersemail" id="buyersemail" class="form-control" required />

									<span class="text-red m-t-5 display-b" ng-if="buyersinfo1.$submitted && buyersinfo1.buyersemail.$invalid">Buyer's Email is required</span>
								</div>	
							</div>
						</div>	
					</div>
					<div class="col-md-8">		
						<h4 class="page-header">Buyer's Info - Step 2</h4>
						<div class="form-group">
							<label class="control-label">Do you need to sell your current home?<br><small class="font-w-normal">By selecting "yes" we will make the offer to buy contingent on the sale of your current home. This means if you do not accept an offer by a specified date and close the sale of your current home by a specified date than this offer to buy can be cancelled.</small></label>
							<div class="form-group">
								<input type="radio" ng-model="s2BuyerVar" value="yes" >Yes

								<input type="radio" ng-model="s2BuyerVar" value="no">No

								<input type="radio" ng-model="s2BuyerVar" value="notsureattorneyhelp">Not Sure Request Attorney Help
							</div>
							<div class="animate-switch" ng-show="s2BuyerVar === 'yes'">
								<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo2.$submitted && buyersinfo2.sc_current_address.$invalid]">
									<label class="control-label" for="sc_current_address">What is the address of your current home?</label>
									<input type="text"  ng-init="form.data.sccurrentaddress = address" ng-model="form.data.sccurrentaddress" name="sc_current_address" id="sc_current_address" class="form-control" placeholder="Enter the address of your current home" required g-places-autocomplete autocomplete="off" />

									<span class="text-red m-t-5 display-b" ng-if="buyersinfo2.$submitted && buyersinfo2.sc_current_address.$invalid">This field is required</span>
								</div>
								<div class="form-group">
									<label class="control-label">Do you have a current accepted offer to sell your current house?</label>
									<div class="form-group">
										<input type="radio" ng-model="s2quest1BuyerVar" value="yes" >yes
										<input type="radio" ng-model="s2quest1BuyerVar" value="no">No
									</div>
									<div ng-show="s2quest1BuyerVar === 'yes'">
										<label class="control-label" for="sc_current_address">The date you want in the contract that you must sell your house by. <br><small class="font-w-normal">If you are unable to sell your home by this date then you will either need to get an extension from the seller or cancel the contract to buy the house. Otherwise you will be at risk to forfeit your earnest money.</small></label>
										<input type="text" ng-model="form.data.contract_date" name="contract_date" id="contract_date" class="form-control" date-picker required readonly />	
									</div>
									<div ng-show="s2quest1BuyerVar === 'no'">
										<label class="control-label" for="sc_current_address">Have you already listed your home for sale?</label>
										<div class="form-group">

											<input type="radio" ng-model="s2quest2BuyerVar" value="yes" >yes
											<input type="radio" ng-model="s2quest2BuyerVar" value="no">No
										</div>

										<div ng-show="s2quest2BuyerVar === 'yes'">
											<label class="control-label" for="sc_current_address">With Whom?</label>
										
												<div class="form-group">

											<input type="radio" ng-model="s2quest3BuyerVar" value="turbohomes" >Turbo Homes
											<input type="radio" ng-model="s2quest3BuyerVar" value="realtor">Realtor
											<input type="radio" ng-model="s2quest3BuyerVar" value="self">Self
											<input type="radio" ng-model="s2quest3BuyerVar" value="others">Others

										<div ng-show="s2quest3BuyerVar === 'realtor'">
							<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo4.$submitted && buyersinfo4.whom_realtor.$invalid]">
															<label class="control-label" for="sc_realtor_name">Realtor's Name:</label>							
															<input type="text" ng-model="form.data.whom_realtor" name="whom_realtor" id="whom_realtor" class="form-control" required />	
							
															<span class="text-red m-t-5 display-b" ng-if="buyersinfo4.$submitted && buyersinfo4.whom_realtor.$invalid">Realtor's Name is required</span>
														</div>
														
														<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo4.$submitted && buyersinfo4.whom_realstate.$invalid]">
															<label class="control-label" for="sc_brokerage_address">Real Estate Brokerage:</label>				
															<input type="text" ng-model="form.data.whom_realstate" name="whom_realstate" id="whom_realstate" class="form-control" required />	
							
															<span class="text-red m-t-5 display-b" ng-if="buyersinfo4.$submitted && buyersinfo4.whom_realstate.$invalid">Real Estate Brokerage is required</span>
														</div>
										</div>
										<div ng-show="s2quest3BuyerVar === 'others'">
											<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo4.$submitted && buyersinfo4.whom_other.$invalid]">
															<label class="control-label" for="whom_other">Other Description</label>
															<input type="text" name="whom_other" ng-model="form.data.whom_other" class="form-control" required />	
							
															<span class="text-red m-t-5 display-b" ng-if="buyersinfo4.$submitted && buyersinfo4.whom_other.$invalid">Other Description is required</span>
														</div>
										</div>
											<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo4.$submitted && buyersinfo4.accept_offer_date.$invalid]">
														<label class="control-label" for="sc_current_address">The date you want in the contract that you must accept an offer to sell your house. 
															<!-- <br><small>If you are unable to to accept an offer by this date then you will either need to get an extension from the seller or cancel the contract to buy the house. Otherwise you will be at risk to forfeit your earnest money.</small> -->
														</label>
														<input type="text" ng-model="form.data.accept_offer_date" name="accept_offer_date" id="accept_offer_date" class="form-control" date-picker required readonly />	
							
														<span class="text-red m-t-5 display-b" ng-if="buyersinfo4.$submitted && buyersinfo4.accept_offer_date.$invalid">This field is required</span>	
													</div>
													<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo4.$submitted && buyersinfo4.must_sell_current_house_contract.$invalid]">
														<label class="control-label" for="sc_current_address">The date you want in the contract that you must sell your house by. 
															<!-- <br><small>If you are unable to to accept an offer by this date then you will either need to get an extension from the seller or cancel the contract to buy the house. Otherwise you will be at risk to forfeit your earnest money.</small> -->
														</label>
														<input type="text" ng-model="form.data.must_sell_current_house_contract" name="must_sell_current_house_contract" id="must_sell_current_house_contract" class="form-control" date-picker required readonly />	
							
														<span class="text-red m-t-5 display-b" ng-if="buyersinfo4.$submitted && buyersinfo4.must_sell_current_house_contract.$invalid">This field is required</span>	
													</div>
										</div>
										</div>

										<div ng-show="s2quest2BuyerVar === 'no'">

<label class="control-label">Do you want to sell your home with Turbo and save thousands?</label>
											<div class="form-group">
											
											<input type="radio" ng-model="s2quest4BuyerVar" value="yes" >yes
											<input type="radio" ng-model="s2quest4BuyerVar" value="no">No
										</div>

								<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo4.$submitted && buyersinfo4.accept_offer_date.$invalid]">
														<label class="control-label" for="sc_current_address">The date you want in the contract that you must accept an offer to sell your house. 
															<!-- <br><small>If you are unable to to accept an offer by this date then you will either need to get an extension from the seller or cancel the contract to buy the house. Otherwise you will be at risk to forfeit your earnest money.</small> -->
														</label>
														<input type="text" ng-model="form.data.accept_offer_date" name="accept_offer_date" id="accept_offer_date" class="form-control" date-picker required readonly />	
							
														<span class="text-red m-t-5 display-b" ng-if="buyersinfo4.$submitted && buyersinfo4.accept_offer_date.$invalid">This field is required</span>	
													</div>
													<div class="form-group" ng-class="{true: 'has-error'}[buyersinfo4.$submitted && buyersinfo4.must_sell_current_house_contract.$invalid]">
														<label class="control-label" for="sc_current_address">The date you want in the contract that you must sell your house by. 
															<!-- <br><small>If you are unable to to accept an offer by this date then you will either need to get an extension from the seller or cancel the contract to buy the house. Otherwise you will be at risk to forfeit your earnest money.</small> -->
														</label>
														<input type="text" ng-model="form.data.must_sell_current_house_contract" name="must_sell_current_house_contract" id="must_sell_current_house_contract" class="form-control" date-picker required readonly />	
							
														<span class="text-red m-t-5 display-b" ng-if="buyersinfo4.$submitted && buyersinfo4.must_sell_current_house_contract.$invalid">This field is required</span>	
													</div>
										</div>

									</div>

								</div>
							</div>
						</div>
					</div>
					<!-- Financing -->
					<div class="col-md-12">
						<h4 class="page-header">Financing</h4>
					</div>		
					<div class="col-md-8">
						<h4 class="page-header">Financing - Step 1</h4>
						

						<div class="form-group" ng-class="{true: 'has-error'}[financing1.$submitted && financing1.sc_financing_amt.$invalid]">
							<label class="control-label" for="sc_financing_amt">How much do you want to offer?</label>										
							<input type="text" ng-model="form.data.scfinancingamt" name="sc_financing_amt" id="sc_financing_amt" class="form-control" placeholder="Amount" ng-init="form.data.scfinancingamt = 0" required mask-money/>	
							
							<span class="text-red m-t-5 display-b" ng-if="financing1.$submitted && financing1.sc_financing_amt.$invalid">This field is required</span>	
						</div>	
					</div>
					<div class="col-md-8">
						<h4 class="page-header">Financing - Step 2</h4>
						<div class="form-group" ng-class="{true: 'has-error'}[financing2.$submitted && financing2.sc_financing_earnest_amt.$invalid]">
							<label class="control-label" for="sc_financing_earnest_amt">How much money do you want to offer in earnest? <small class="font-w-normal">Typically 1%</small></label>

							<input type="text" ng-model="form.data.scfinancingearnestamt" name="sc_financing_earnest_amt" id="sc_financing_earnest_amt" class="form-control" placeholder="Amount" required mask-money with-decimal />	
							
							<span class="text-red m-t-5 display-b" ng-if="financing2.$submitted && financing2.sc_financing_earnest_amt.$invalid">This field is required</span>	
						</div>	
						<label class="control-label">Earnest money is deposited in escrow account with a title/escrow company.<br> Do you have a title company you want to work with?</label>	
						<div class="form-group">

							<input type="radio" ng-model="s2quest1FinancingVar" value="yes" >yes
							<input type="radio" ng-model="s2quest1FinancingVar" value="no">No
						</div>
						<div ng-show="s2quest1FinancingVar === 'yes'">
							<div class="form-group" ng-class="{true: 'has-error'}[financing2.$submitted && financing2.sc_financing_escrow_name.$invalid]">
								<label class="control-label" for="sc_financing_escrow_name">Company Name:</label>
								<input type="text" ng-model="form.data.scfinancingescrowname" name="sc_financing_escrow_name" id="sc_financing_escrow_name" class="form-control" required />	

								<span class="text-red m-t-5 display-b" ng-if="financing2.$submitted && financing2.sc_financing_escrow_name.$invalid">Company Name is required</span>
							</div>	

							<div class="form-group" ng-class="{true: 'has-error'}[financing2.$submitted && financing2.sc_financing_escrow_officer_name.$invalid]">
								<label class="control-label" for="sc_financing_escrow_officer_name">Officer Name:</label>
								<input type="text" ng-model="form.data.scfinancingescrowofficername" name="sc_financing_escrow_officer_name" id="sc_financing_escrow_officer_name" required class="form-control" />	

								<span class="text-red m-t-5 display-b" ng-if="financing2.$submitted && financing2.sc_financing_escrow_officer_name.$invalid">Officer Name is required</span>
							</div>

							<div class="form-group" ng-class="{true: 'has-error'}[financing2.$submitted && financing2.sc_financing_escrow_tel.$invalid]">
								<label class="control-label" for="sc_financing_escrow_tel">Telephone:</label>
								<input type="text" ng-model="form.data.scfinancingescrowtel" name="sc_financing_escrow_tel" id="sc_financing_escrow_tel" class="form-control" required />	

								<span class="text-red m-t-5 display-b" ng-if="financing2.$submitted && financing2.sc_financing_escrow_tel.$invalid">Telephone is required</span>
							</div>
						</div>
					</div>

					<div class="col-md-8">
						<h4 class="page-header">Financing - Step 3</h4>
						<div class="form-group">
							<label class="control-label">How do you plan on paying for the house?</label>	

							<div class="form-group" ng-class="{true: 'has-error'}[financing3.$submitted && financing3.scfinancingearnestamt.$invalid]">
								<label class="control-label" for="sc_property_address">Earnest Money</label>										
								<input type="text" ng-model="form.data.scfinancingearnestamt" name="scfinancingearnestamt" id="scfinancingearnestamt" class="form-control" required mask-money with-decimal />	

								<span class="text-red m-t-5 display-b" ng-if="financing3.$submitted && financing3.scfinancingearnestamt.$invalid">Earnest Money is required</span>
							</div>

							<div class="form-group" ng-class="{true: 'has-error'}[financing3.$submitted && financing3.down_payment.$invalid]">
								<label class="control-label" for="sc_property_address" ng-init="form.data.down_payment = (form.data.scfinancingamt * 0.02)">Cash/Down Payment</label>										
								<input type="text" ng-model="form.data.down_payment" name="down_payment" id="down_payment" class="form-control" required mask-money />	

								<span class="text-red m-t-5 display-b" ng-if="financing3.$submitted && financing3.down_payment.$invalid">Cash/Down Payment is required</span>
							</div>

							<div class="form-group">
								<label class="control-label" for="sc_property_address">Mortgage/Loan</label>										
								<input type="text" ng-model="form.data.mortgage" name="mortgage" id="mortgage" ng-init="form.data.mortgage = (form.data.scfinancingamt * 0.97)" class="form-control" mask-money />
							</div>

							<div class="form-group">
								<label class="control-label" for="sc_property_address">Other</label>										
								<input type="text" ng-model="form.data.planforpayingother" name="planforpayingother" id="planforpayingother" class="form-control" mask-money ng-init="form.data.planforpayingother = 0"/>
							</div>
							<span class="text-red m-t-5 display-b" ng-if="financing3.$submitted && financing3.down_payment.$invalid">Have you been prequalified?</span>
							<div class="form-group">
								<input type="radio" ng-model="s3FinancingVar" value="yes" >yes
								<input type="radio" ng-model="s3FinancingVar" value="no">No
							</div>
						</div>	
					</div>
					<div class="col-md-8">
						<h4 class="page-header">Financing - Step 4</h4>

						<div class="form-group">
							<label class="control-label">Type of Financing <br><small class="font-w-normal">Most are conventional but a lender can help you determine the best kind for you</small></label>	
						</div>
						<div>
							<div class="form-group">
								<input type="radio" ng-model="s4FinancingVar" value="yes" >Conventional
								<input type="radio" ng-model="s4FinancingVar" value="va">VA
								<input type="radio" ng-model="s4FinancingVar" value="usda">USDA
								<input type="radio" ng-model="s4FinancingVar" value="assumption">Assumption
								<input type="radio" ng-model="s4FinancingVar" value="selercarryback">Seller Carryback
								<input type="radio" ng-model="s4FinancingVar" value="others" >Others
								<input type="radio" ng-model="s4FinancingVar" value="notsurequestforattorneyhelp">Not Sure Request For Attorney Help							
							</div>
							<div class="form-group" ng-show="s4FinancingVar === 'others'" ng-class="{true: 'has-error'}[financing4.$submitted && financing4.type_of_financing_other.$invalid]">									
								<input type="text" ng-model="form.data.type_of_financing_other" id="type_of_financing_other" name="type_of_financing_other" class="form-control" placeholder="Specify" required />	

								<span class="text-red m-t-5 display-b" ng-if="financing4.$submitted && financing4.type_of_financing_other.$invalid">This field is required</span>
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<h4 class="page-header">Financing - Step 5</h4>

						<p>Closing Costs</p>
						<label class="control-label">Do you want the seller to help cover closing costs?</label>	
						<div class="form-group">
							<input type="radio" ng-model="s5FinancingVar" value="yes" >yes
							<input type="radio" ng-model="s5FinancingVar" value="no">No
						</div>
						<div class="form-group" ng-show="s5FinancingVar === 'yes'">
							<div class="form-group" ng-class="{true: 'has-error'}[financing5.$submitted && financing5.closingcostamount.$invalid]">
								<label class="control-label" for="closingcostamount">Amount:</label>										
								<input type="text" ng-model="form.data.closingcostamount" name="closingcostamount" id="closingcostamount" class="form-control" placeholder="Amount" required  />	
								<span class="text-red m-t-5 display-b" ng-if="financing5.$submitted && financing5.closingcostamount.$invalid">Amount is required</span>
							</div>
							
						</div>
					</div>
					<div class="col-md-8">
						<h4 class="page-header">Financing - Step 6</h4>

						<p>Home Warranty</p>
						
						<div class="form-group">
							<label class="control-label">Do you want a home warranty included?  <br><small class="font-w-normal">It is not uncommon to have the seller pay for this.</small></label>
						</div>
						<div class="form-group">
							<input type="radio" ng-model="s6FinancingVar" value="yes" >yes
							<input type="radio" ng-model="s6FinancingVar" value="no">No
						</div>
						<div class="form-group" ng-show="s6FinancingVar === 'yes'">
							<div class="form-group">
								<label class="control-label">Who will you have pay for the home warranty? <br><small class="font-w-normal">Although it is not uncommon for the seller to pay for this it is one more thing that will make the offer less attractive to the seller.</small></label>
							</div>
							<div class="form-group">
								<input type="radio" ng-model="s6warrantyFinancingVar" value="buyer" >Buyer
								<input type="radio" ng-model="s6warrantyFinancingVar" value="seller">Seller
								<input type="radio" ng-model="s6warrantyFinancingVar" value="splitevenly">Split Evenly
							</div>
							<div class="form-group" ng-class="{true: 'has-error'}[financing6.$submitted && financing6.costnotexceed.$invalid]">
								<label class="control-label" for="costnotexceed">Cost to not exceed <br><small class="font-w-normal">Typically $400-$500 depending on the size of house and coverage desired. You could quickly call a home warranty company in the Provider Marketplace to learn more.</small></label>	
								<input type="text" ng-model="form.data.costnotexceed" name="costnotexceed" id="costnotexceed" class="form-control" placeholder="Amount" required  />	

								<span class="text-red m-t-5 display-b" ng-if="financing6.$submitted && financing6.costnotexceed.$invalid">This field is required</span>	
							</div>
						</div>
					</div>
					<div class="col-md-12">
						<h4 class="page-header">Important Dates</h4>
					</div>		
					<div class="col-md-8">
						<p>These dates create the timeline leading to the closing of your house.</p>
						<h4 class="page-header">Important Dates - Step 1</h4>
						<label class="control-label">When do you want to hear back from the seller regarding this offer?</label>
						<div class="form-group">
							<input type="radio" ng-model="s1ImportantDatesVar" value="24hours" >24 Hours
							<input type="radio" ng-model="s1ImportantDatesVar" value="48hours">48 Hours
							<input type="radio" ng-model="s1ImportantDatesVar" value="72hours">72 Hours
							<input type="radio" ng-model="s1ImportantDatesVar" value="others">Others
						</div>
						<div ng-show="s1ImportantDatesVar === 'others'">

							<input type="text" ng-model="form.data.importlistoneanswerother" name="importlistoneanswerother" id="importlistoneanswerother" class="form-control" placeholder="Other Specify" required date-picker />	
							
							<span class="text-red m-t-5 display-b" ng-if="importdates1.$submitted && importdates1.importlistoneanswerother.$invalid">This field is required</span>
						</div>
					</div>
					<div class="col-md-8">
						<h4 class="page-header">Important Dates - Step 2</h4>

						<div class="form-group" ng-class="{true: 'has-error'}[importdates2.$submitted && importdates2.inspectiondeadline.$invalid]">
							<label class="control-label">Due Diligence/Inspections Deadline <small class="font-w-normal">Typically 10 days</small></label>
							<input type="text" ng-model="form.data.inspectiondeadline" name="inspectiondeadline" id="inspectiondeadline" class="form-control" required  />	
							
							<span class="text-red m-t-5 display-b" ng-if="importdates2.$submitted && importdates2.inspectiondeadline.$invalid">This field is required</span>
						</div>
					</div>
					<div class="col-md-8">
						<h4 class="page-header">Important Dates - Step 3</h4>
						<div class="form-group">
							<label class="control-label">Do you have an inspector selected? <br><small class="font-w-normal">Having the home professionally inspected is not required but we always recommend it. We have some great recommendations in the Provider Marketplace.</small></label>
						</div>
						<div class="form-group">
							<input type="radio" ng-model="s3ImportantDatesVar" value="yes" >Yes
							<input type="radio" ng-model="s3ImportantDatesVar" value="no">No
						</div>
					</div>
					<div class="col-md-8">
						<h4 class="page-header">Important Dates - Step 4</h4>
						<div class="form-group">
							<label class="control-label">Closing Date <br><small class="font-w-normal">If you require a mortgage than this must be at least 45 days from this offer. Unless you put a specific date we will round to the nearest Monday through Thursday. Trying to close on a Friday can often not fund in time making you wait the weekend to get the keys. We don't want that.</small></label>
						</div>
						<div class="form-group">
							<input type="radio" ng-model="s4ImportantDatesVar" value="24hours" >46 Days
							<input type="radio" ng-model="s4ImportantDatesVar" value="48hours">60 Days
							<input type="radio" ng-model="s4ImportantDatesVar" value="72hours">90 Days
							<input type="radio" ng-model="s4ImportantDatesVar" value="others">Others
						</div>
						<div ng-show="s4ImportantDatesVar === 'others'">
							<input type="text" ng-model="form.data.closingdateother" name="closingdateother" id="closingdateother" class="form-control" placeholder="Other Specify" required date-picker />	
							
							<span class="text-red m-t-5 display-b" ng-if="importdates4.$submitted && importdates4.closingdateother.$invalid">This field is required</span>
						</div>
					</div>
					<div class="col-md-12">
						<h4 class="page-header">Inclusions</h4>
					</div>		
					<div class="col-md-8">
						<div class="form-group o-hidden">
							<label class="control-label">What "personal property" items do you want included in the sale? <br><small class="font-w-normal">Include a description of the items so they don't swap them out for something worse, e.g., make, model, color. Remeber that "fixtures" are included in the sale unless specifically excluded. Fixtures are property attached/affixed to the premises.</small></label>
						</div>
						<div class="form-group">
							<input type="radio" ng-model="inclusionsRef" value="refrigerator" >Refrigerator
							<input type="radio" ng-model="inclusionsWasher" value="washer">Washer
							<input type="radio" ng-model="inclusionsDryer" value="dryer">Dryer
							<input type="radio" ng-model="inclusionsSpa" value="abovergroundsspahottub">Above-Grounds spa/hot tub
							<input type="radio" ng-model="inclusionsOthers" value="others">Other
						</div>
						<div ng-show="inclusionsRef === 'refrigerator'">
							<div class="form-group">
								<label class="control-label" for="sc_financing_escrow_name">Refrigerator Description (i.e. Make, Model, Color)</label>
								<input type="text" ng-model="form.data.inclusion.refrigeratordesc" class="form-control" />
							</div>
						</div>

						<div ng-show="inclusionsWasher === 'washer'">
							<div class="form-group">
								<label class="control-label" for="sc_financing_escrow_name">Washer Description (i.e. Make, Model, Color)</label>
								<input type="text" ng-model="form.data.inclusion.washerdesc" class="form-control" />
							</div>
						</div>

						<div ng-show="inclusionsDryer === 'dryer'">
							<div class="form-group">
								<label class="control-label" for="sc_financing_escrow_name">Dryer Description (i.e. Make, Model, Color)</label>
								<input type="text" ng-model="form.data.inclusion.dryerdesc" class="form-control" />
							</div>
						</div>

						<div ng-show="inclusionsSpa === 'abovergroundsspahottub'">
							<div class="form-group">
								<label class="control-label" for="sc_financing_escrow_name">spa/hot tub Description (i.e. Make, Model, Color)</label>
								<input type="text" ng-model="form.data.inclusion.abovegrounddesc" class="form-control" />
							</div>
						</div>

						<div ng-show="inclusionsOthers === 'others'">
							<div class="form-group">
								<label class="control-label" for="sc_financing_escrow_name">Other Description (i.e. Make, Model, Color)</label>
								<input type="text" ng-model="form.data.inclusion.otherdesc" class="form-control" />
							</div>
						</div>
						<p>You are almost done. Let us know if you would like to discuss this offer with a Turbo Attorney before moving forward.</p>

						<div class="checkbox">
							<label class="control-label"><input type="checkbox" ng-model="form.data.discusswithattorny">Discuss with a Turbo Attorney</label>
						</div>

						<div class="form-group">
							<label class="control-label" for="sc_financing_escrow_name">Any additional terms and conditions that you want to include</label>
							<textarea class="form-control" rows="8" ng-model="form.data.additionalcondition"></textarea>
						</div>
						
						<div id="sc_spinner_container" class="ng-cloak" ng-if="onProgress">
							<div class="spinner">
								<div class="bounce1"></div>
								<div class="bounce2"></div>
								<div class="bounce3"></div>
							</div>
						</div>


					</div>

					<div class="col-md-8">
						<button type="submit" class="btn btn-primary btn_submit btn-block btn-lg login-btn" >Submit</button>
					</div>

				</div>	
			</form>
		</div>
	</section>