
<section class="content-header" id="gototop">
	<h1>
		Edit Home List
	</h1>
	<ol class="breadcrumb">
		<li><a href="#/dashboard"><i class="fa fa-dashboard"></i> Main</a></li>
		<li class="active"><a href="">Edit Home List</a></li>
	</ol>
</section>

<section class="content offer-details">
	
	<div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">{[{alert.msg}]}</div>

	<form name="form" ng-submit="save(form)" novalidate="">
    <div class="row">
		<div class="col-md-6">
			<div class="box box-info">
				<div class="box-header with-border">
		      		<h3 class="box-title">Property</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						 <label>Home Address</label>
						 <span class="text-danger pull-right" ng-messages="form.homeaddress.$error" ng-if="setError(form, 'homeaddress')">
						 	<span ng-message="required"> Home Address is required</span>
						 </span>
						 <input type="text" ng-model="field.homeaddress" name="homeaddress" class="form-control" placeholder="Enter the address of the home you want to buy" required g-places-autocomplete autocomplete="off" />
					</div>
					<br/>
					<div class="form-group">
						<label>Features Want to Highlight</label>
						<textarea class="form-control" ng-model="field.features" rows="5">
							
						</textarea>
					</div>
					<br/>
					<div class="form-group">
						<label>
							<input type="checkbox" name="mls" class="flat-blue" ng-true-value="1" 
    						ng-false-value="0"  ng-model="field.mls" icheck>
							&nbsp;List home on the MLS
						</label>
						<div class="form-group" ng-if="field.mls === 1">
					    	<label>Sales Price Percent Pay to buyer’s agent (typically 3%)</label>
					    	<span class="text-danger pull-right" ng-messages="form.percentpay.$error" ng-if="setError(form, 'percentpay')">
						 	<span ng-message="required"> Percent Pay required</span>
						    </span>
						    <div class="form-group">
						    	<label>
									<input type="radio" name="percentpay" class="flat-blue" value="2" ng-model="field.percentpay" icheck required>
									&nbsp;2%
								</label>
								&nbsp;
								<br/>
								<label>
									<input type="radio" name="percentpay" class="flat-blue" value="2.5" ng-model="field.percentpay" icheck required>
									&nbsp;2.5%
								</label>
								&nbsp;
								<br/>
								<label>
									<input type="radio" name="percentpay" class="flat-blue" value="3" ng-model="field.percentpay" icheck required>
									&nbsp;3%
								</label>
								<br/>
								<label>
									<input type="radio" name="percentpay" class="flat-blue" value="other" ng-model="field.percentpay" icheck required>
									&nbsp;Other
								</label>
								<div class="form-group" ng-if="field.percentpay === 'other'">
									<input type="text"  class="form-control" name="percentpayother" ng-model="percentpayother" required="">
									<small class="text-danger" ng-messages="form.percentpayother.$error" ng-if="setError(form, 'percentpayother')">
									 	<span ng-message="required">Required
									 	</span>
								    </small>
								</div>
							</div>
					    </div>
					</div>
				</div>
			</div>

			<div class="box box-info">
				<div class="box-header with-border">
		      		<h3 class="box-title">Pricing</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label>Buyer with No Agent</label>
						<span class="text-danger pull-right" ng-messages="form.buyernoagent.$error" ng-if="setError(form, 'buyernoagent')">
					 	<span ng-message="required"> 
					 	   Buyer with no agent required</span>
					    </span>
					    <input type="text" class="form-control" name="buyernoagent" ng-blur="checkIfEmpty('buyernoagent')" ng-keyup="setBuyerWAgentVal($event);checkIfEmpty('buyernoagent', 'keyup');checkIfEmpty('buyerwithagent', 'keyup');" ng-model="field.buyernoagent" required mask-money with-decimal>
					</div>
					<div class="form-group">
						<label>Buyer with Agent</label>
						<span class="text-danger pull-right" ng-messages="form.buyerwithagent.$error" ng-if="setError(form, 'buyerwithagent')">
					 	<span ng-message="required"> 
					 	   Buyer with agent required</span>
					    </span>
					    <input type="text" class="form-control"  name="buyerwithagent" ng-blur="checkIfEmpty('buyerwithagent')" ng-keyup="checkIfEmpty('buyerwithagent', 'keyup');" ng-model="field.buyerwithagent" required mask-money with-decimal>
					</div>
				</div>
			</div>

			

		    <div class="box box-info">
				<div class="box-header with-border">
		      		<h3 class="box-title">Other Info</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						<label>Contact Via</label>
						<div class="text-danger" ng-messages="form.contactvia.$error" ng-if="setError(form, 'contactvia')">
						 	<span ng-message="required"> Please Select One</span>
						</div>
						<div>
						<label>
							<input type="radio" name="contactvia" class="flat-blue" value="phone" ng-model="field.contactvia" icheck required ng-change="setMask(field.contactvia)">
							&nbsp;By Phone
						</label>
						</div>
						<label>
							<input type="radio" name="contactvia" class="flat-blue" value="text" ng-model="field.contactvia" icheck required ng-change="setMask(field.contactvia)">
							&nbsp;By Text
						</label>
						<br/>
						<label>
							<input type="radio" name="contactvia" class="flat-blue" value="email" ng-model="field.contactvia" icheck required ng-change="setMask(field.contactvia)">
							&nbsp;By Email
						</label>
						<br/>
						<div class="form-group" ng-if="via_placeholder && field.contactvia">
							<small>Please enter your {[{ via_placeholder }]} below.</small>
							<input type="text"  class="form-control" ng-model="field.contact" name="contact" ui-mask="{[{mask}]}"  ui-mask-placeholder="" ui-mask-placeholder-char="{[{char}]}" model-view-value="true" required="">
							<small class="text-danger pull-right" ng-if="setError(form, 'contact')" ng-messages="form.contact.$error">
								<span ng-message="required">Required
								</span>
								<span ng-message="mask">Invalid</span>
							</small>
						</div>
					</div>
					<div class="form-group">
						<label>Select those that apply:</label>
						<input type="hidden" name="apply" ng-model="field.apply" required="">
						<div class="text-danger" ng-messages="form.apply.$error" ng-if="setError(form, 'apply')">
						 	<span ng-message="required"> Please Select One</span>
						</div>
                        <div>  
						<label>
							<input type="checkbox" class="flat-blue" ng-model="field.homevacant" icheck ng-change="setApply()" ng-true-value="true">
							&nbsp;Home is vacant
						</label>
					    </div>
						<label>
							<input type="checkbox" class="flat-blue" ng-model="field.tenantoccupied" icheck ng-change="setApply()" ng-true-value="true">
							&nbsp;Tenant occupied
						</label>
						<br/>
						<label>
							<input type="checkbox" class="flat-blue" ng-model="field.permrequired" icheck ng-change="setApply()" ng-true-value="true">
							&nbsp;Permission required for showings
						</label>
						<br/>						
						<label>
							<input type="checkbox" class="flat-blue"  ng-model="field.gatecode" icheck ng-change="setApply()" ng-true-value="true">
							&nbsp;Gate Code
						</label>
						<br/>
						<div class="form-group" ng-if="field.gatecode" style="margin-left: 10px">
							<div class="text-danger" ng-messages="form.gatecodeprov.$error" ng-if="setError(form, 'gatecodeprov')">
						 	 	<span ng-message="required"> Please Select One</span>
						    </div>
						    <label>
								<input type="radio" name="gatecodeprov" class="flat-blue" value="w_provide" ng-model="field.gatecodeprov" icheck required>
								&nbsp;Will provide code when showing appointment is made.
								</label>
							<br/>
							<label>
								<input type="radio" name="gatecodeprov" class="flat-blue" value="provide_gcode" ng-model="field.gatecodeprov" icheck required>
								&nbsp;Provide gate code
							</label>
							<div class="form-group" ng-if="field.gatecodeprov === 'provide_gcode'" style="margin-left: 10px;">
								<input type="text" placeholder="Provide your gate code here" name="providedcode" class="form-control" ng-model="field.providedcode" required="">
								<small class="text-danger pull-right" ng-if="setError(form, 'providedcode')" ng-messages="form.providedcode.$error">
									<span ng-message="required">Required
									</span>
								</small>
							</div>
						</div>
						<label>
							<input type="checkbox" class="flat-blue" ng-model="field.alarmactivated" icheck ng-change="setApply()" ng-true-value="true">
							&nbsp;Alarm activated
						</label>
						<br/>
						<div class="form-group" style="margin-left: 10px;" ng-if="field.alarmactivated">
							<div class="text-danger" ng-messages="form.willalarm.$error" ng-if="setError(form, 'willalarm')">
						 	 	<span ng-message="required"> Please Select One</span>
						    </div>
							<label>
								<input type="radio" name="willalarm" class="flat-blue" value="w_provide" ng-model="field.willalarm" icheck required>
								&nbsp;Will provide instructions to deactivate alarm.
							</label>
							<br/>
							<label>
								<input type="radio" name="willalarm" class="flat-blue" value="provide_alarm_ins" ng-model="field.willalarm" icheck required>
								&nbsp;Provide alarm deactivation instructions
							</label>
							<div class="form-group" ng-if="field.willalarm === 'provide_alarm_ins'" style="margin-left: 10px;">
								<input type="text" placeholder="Provide your alarm code here" name="alarmcode" class="form-control" ng-model="field.alarmcode" required="">
								<small class="text-danger pull-right" ng-if="setError(form, 'alarmcode')" ng-messages="form.alarmcode.$error">
									<span ng-message="required">Required
									</span>
								</small>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label>How potential buyers gain access to enter the house.</label>
						<div class="text-danger" ng-messages="form.buyeraccess.$error" ng-if="setError(form, 'buyeraccess')">
						 	 	<span ng-message="required"> Please Select One</span>
						</div>
						<div class="form-group">
							<label>
								<input type="radio" name="buyeraccess" class="flat-blue" value="combination_lbox" ng-model="field.buyeraccess" icheck required>
								&nbsp;Combination Lockbox
							</label>
							<div style="margin-left: 10px" class="form-group" ng-if="field.buyeraccess === 'combination_lbox'">
								<div class="text-danger" ng-messages="form.lockbox.$error" ng-if="setError(form, 'lockbox')">
						 	 		<span ng-message="required"> Please Select One</span>
								</div>
								<label>
									<input type="radio" name="lockbox" class="flat-blue" value="will_provide_lbox" ng-model="field.lockbox" icheck required>
									&nbsp;Provide lockbox code now for buyer agents view only
								</label>
								<br/>
								<div class="form-group" ng-if="field.lockbox === 'will_provide_lbox'" style="margin-left: 10px;">
									<input type="text" placeholder="Provide lockbox code here" name="lockboxdesc" class="form-control" ng-model="field.lockboxdesc" required="">
									<small class="text-danger pull-right" ng-if="setError(form, 'lockboxdesc')" ng-messages="form.lockboxdesc.$error">
										<span ng-message="required">Required
										</span>
									</small>
								</div>
								<label>
									<input type="radio" name="lockbox" class="flat-blue" value="provide_lbox" ng-model="field.lockbox" icheck required>
									&nbsp;Provide lockbox code personally at time of showing confirmation
								</label>
								
							</div>
							<div>
								<label>
									<input type="radio" name="buyeraccess" class="flat-blue" value="unlocked" ng-model="field.buyeraccess" icheck required>
									&nbsp;Personally let them in
								</label>
						    </div>
						</div>						
					</div>
				</div>
			</div>
		</div> <!-- /.col-md-4 -->

		<div class="col-md-6">
			<div class="box box-info">
				<div class="box-header with-border">
		      		<h3 class="box-title">Seller's Information</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group">
						 <label>Full Name</label>
						 <span class="text-danger pull-right" ng-messages="form.fullname.$error" ng-if="setError(form, 'fullname')">
						 	<span ng-message="required"> Fullname is required</span>
						 </span>
						 <input type="text" ng-model="field.fullname" name="fullname" class="form-control" required="" />
					</div>
					<br/>
					<div class="form-group">
						 <label>Email</label>
						 <span class="text-danger pull-right" ng-messages="form.owneremail.$error" ng-if="setError(form, 'owneremail')">
						 	<span ng-message="required"> Email is required</span>
						 	<span ng-message="email"> Email is invalid</span>
						 </span>
						 <input type="text" ng-model="field.owneremail" name="owneremail" ng-change="validateEmail(field.owneremail,'owneremail')" class="form-control" required="" />
					</div>
					<br/>
					<div class="form-group">
						 <label>Phone</label>
						 <input type="text" ng-model="field.ownerphone" name="ownerphone" class="form-control" ui-mask="(999) 999-9999"  ui-mask-placeholder ui-mask-placeholder-char="_" model-view-value="true" />
					</div>
				</div>
			</div>

			<div class="box box-info">
				<div class="box-header with-border">
		      		<h3 class="box-title">Other Owner's Information</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div class="form-group" ng-repeat="other in field.otherowner">
						<div class="form-group">
							Owner {[{ $index + 1 }]}
							<button class="btn btn-danger btn-sm pull-right" type="button" ng-click="removeOtherOwner($index)"><i class="fa fa-times"></i></button>
						</div>
						<div class="form-group">
						 <label>Full Name</label>
						 <span class="text-danger pull-right" ng-messages="form['name_' + $index ].$error"  ng-if="setError(form,'name_' + $index)">
						 	<span ng-message="required"> Fullname is required</span>
						 </span>
						 <input type="text" ng-model="other.name" name="name_{[{ $index }]}" class="form-control" required="" />
						</div>
						<br/>
						<div class="form-group">
							 <label>Email</label>
							 <span class="text-danger pull-right" ng-messages="form['email_' +  $index ].$error" ng-if="setError(form, 'email_' + $index)">
							 	<span ng-message="required"> Email is required</span>
							 	<span ng-message="email"> Email is invalid</span>
							 </span>
							 <input type="text" ng-model="other.email" name="email_{[{ $index }]}" ng-change="validateEmail(other.email, 'email_' + $index)" class="form-control" required="" />
						</div>
						<br/>
						<div class="form-group">
							 <label>Phone</label>
							 <input type="text" ng-model="other.phone" name="phone_{[{ $index }]}" class="form-control" ui-mask="(999) 999-9999"  ui-mask-placeholder ui-mask-placeholder-char="_" model-view-value="true" />
						</div>
					</div>
					<div class="form-group pull-right">
						<button class="btn btn-primary" type="button" ng-click="addOtherOwner()"><i class="fa fa-plus"></i></button>
					</div>
				</div>
			</div>


			<div class="box box-info">
				<div class="box-header with-border">
		      		<h3 class="box-title">Special instructions</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="form-group">
						    <textarea class="form-control" rows="10" ng-model="field.specialinstruction">
						    	
						    </textarea>
						</div>
					</div>
				</div>
			</div>

			<div class="box box-info">
				<div class="box-header with-border">
		      		<h3 class="box-title">Paypal Transaction Status</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						<div class="form-group">
							Current Status : {[{ field.status }]}
							<div ng-if="field.status === 'Pending'" class="form-group text-center" style="margin-top: 10px; ">
								<button type="button" class="btn btn-primary" style="padding-left: 30px; padding-right: 30px; font-size: 16px;" ng-click="setStatus()">Set As Paid</button>
							</div>
						</div>
					</div>
				</div>
			</div>

		</div> <!-- /.col-md-4 -->
		<div class="col-md-12">
			 <div class="box box-default">
	             <div class="box-footer clearfix">
             	    <button class="btn btn-primary pull-right" style="margin-left:  5px">
                    	Save
                	</button>
             	    <a href="#" ui-sref="manageseller" class="btn btn-default pull-right m-l-15">
             	    	Cancel
             	    </a>
                 </div>
			 </div>
		</div>
	</div> <!-- /.row -->
	</form>
</section>

