<script type="text/ng-template" id="useredit">
	<div ng-include="'/bemodal/useredit.html'"></div>
</script>

<script type="text/ng-template" id="sellerdelete">
	<div ng-include="'/bemodal/userdelete.html'"></div>
</script>

<section class="content-header" id="gototop">
	<h1>
		List Of Home
		<!-- <small>Add New User</small> -->
	</h1>
	<ol class="breadcrumb">
		<li><a href="#/dashboard"><i class="fa fa-dashboard"></i> Main</a></li>
		<li class="active">Admin</li>
		<li class="active">List Of Home</li>
	</ol>
</section>

<section class="content">
	<div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">{[{alert.msg}]}</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="box">
				<div class="box-header">
					<h3 class="box-title">List Of Home</h3>
				</div>

				<div class="row">
					<div class="box-header">
					<div class="col-sm-6">
						<div id="example1_filter" class="dataTables_filter">
							<label>Search:</label>
							<div class="input-group">
								<input type="text" name="table_search" class="form-control input-sm pull-right" placeholder="Search" ng-model="keyword" ng-enter="search(keyword)">
								<div class="input-group-btn">
									<button class="btn btn-sm btn-default" ng-click="search(keyword)"><i class="fa fa-search"></i></button>
									<button class="btn btn-sm btn-default" ng-click="reset()"><i class="fa fa-refresh"></i></button>
								</div>
							</div>
						</div>
					</div>
					</div>
				</div>

				<div class="box-body table-responsive no-padding">
					<!-- <button type="button" ng-click="exportAction('excel')">Excel</button> -->
					<div class="col-md-12">
						
						<div class="form-group pull-right">
						<a href="#" class="btn btn-default" ui-sref="createseller">Create Home List</a>
						<button ng-json-export-excel data="dataList" report-fields='{id : "ID", userid : "User ID", homeaddress : "Property Address", features : "Features", mls : "MLS", percentpay : "Percent Pay", percentpayother : "Other Percent Pay", buyernoagent : "Price(W/out Agent)", buyerwithagent : "Price(With Agent)", buyerwithagentplus : "Agent Commission", fullname : "Full Name", owneremail :"Email",ownerphone : "Phone", contactvia: "Contact Via", apply : "Apply", created_at : "Date Listed"}' filename =" 'seller_list' " separator="," class="btn btn-primary">Export To Excel</button>
					    </div>
					</div>
					
					
					<table class="table table-hover export-table">
						<thead>
							<tr>
								<th style="width:5%;border:solid 1px #f4f4f4;border-left:none"></th>
								<th style="width:15%;border:solid 1px #f4f4f4;" ng-repeat="col in columns" ng-click="sort(sorting[col.value])">
									{[{ col.col }]}
									<span class="pull-right" ng-if="sortBy == sorting[col.value]['sortBy']"><i ng-class="{'fa fa-caret-up': sorting[col.value]['order'] == 'asc','fa fa-caret-down': sorting[col.value]['order'] == 'desc'}"></i></span>
								</th>
								<th style="width:20%;border:solid 1px #f4f4f4;border-right:none">Action</th>
							</tr>
						</thead>
						<tbody>
						<tr ng-repeat="list in listofseller">
							<td>{[{ index + $index }]}</td>
							<td>{[{list.fullname}]}</td>
							<td>{[{list.owneremail}]}</td>
							<td>{[{list.ownerphone}]}</td>
							<td>{[{list.created_at  |
                              amDateFormat:'ddd, MMM. Do YYYY' }]}</td>
                            <td>{[{ list.status }]}</td>
							<td align="center">
								<button class="btn btn-warning btn-sm" title="View" ui-sref="viewseller({id: list.id})">
									<i class="fa fa-align-justify"></i>
								</button>
								<button class="btn btn-primary btn-sm" ng-click="editseller(list.id)">
											<i class="glyphicon glyphicon-pencil"></i>
								</button>
								<button class="btn btn-danger btn-sm" title="Delete" ng-click="deleteseller(list.id)">
									<i class="glyphicon glyphicon-trash"></i>
								</button>
							</td>
						</tr>
						</tbody>
					</table>
				</div><!-- /.box-body -->
				<div class="box-footer text-center clearfix">
                <ul uib-pagination total-items="bigTotalItems" ng-model="bigCurrentPage" max-size="maxSize"  class="pagination-sm m-t-sm m-b" boundary-links="true" ng-click="setPage(bigCurrentPage)"></ul>
                </div>
                <div class="overlay" ng-show="listloading">
                	<i class="fa fa-refresh fa-spin" style="font-size:50px;"></i>
                </div>
			</div><!-- /.box -->
		</div>
	</div>
</section> <!-- end of content -->
