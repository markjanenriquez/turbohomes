
<section class="content-header">
	<h1>
		Home Details
	</h1>
	<ol class="breadcrumb">
		<li><a href="#/dashboard"><i class="fa fa-dashboard"></i> Main</a></li>
		<li class="active"><a href="">Home Details</a></li>
	</ol>
</section>

<section class="content offer-details">
	<div class="row m-b-10">
		<div class="col-md-12">
			<button type="button" class="btn btn-primary" ng-if="seller.viewdata.contract_status === constant.pending" ng-click="setContract()">Set Under Contract</button>
			<button type="button" class="btn btn-primary" ng-if="seller.viewdata.contract_status !== constant.pending" ng-click="viewContract()">View Contract</button>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6">
			<div class="box box-info">
				<div class="box-header with-border">
		      		<h3 class="box-title">Property</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div><strong>Home #:</strong> {[{ seller.viewdata.id }]}</div>
					<div><strong>Property's Address:</strong> {[{ seller.viewdata.homeaddress}]}</div>
				</div>
			</div>

			<div class="box box-info">
				<div class="box-header with-border">
		      		<h3 class="box-title">Pricing</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div><strong>Buyer with No Agent : {[{ seller.viewdata.buyernoagent | currency }]}</strong></div>
				    <div><strong>Buyer with Agent : {[{ seller.viewdata.buyerwithagent | currency }]}</strong></div>
				    <div><strong>Agent Commission: {[{ seller.viewdata.buyerwithagentplus | currency }]}</strong></div>
				</div>
			</div>

			<div class="box box-info"  ng-if="seller.viewdata.features">
				<div class="box-header with-border">
		      		<h3 class="box-title">Features</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						 {[{ seller.viewdata.features }]}
					</div>
				</div>
			</div>

		    <div class="box box-info">
				<div class="box-header with-border">
		      		<h3 class="box-title">Other Info</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div>
						<strong>Contact Via : {[{ seller.viewdata.contactvia }]}</strong>
					</div>
					<div class="m-l-15">
						 Contact :  {[{seller.viewdata.contact }]}
					</div>
					<div>
						<strong>Apply : </strong>
					</div>
					<div class="m-l-15">
						<div ng-if="seller.viewdata.homevacant">
							Home is vacant
						</div>
						<div ng-if="seller.viewdata.tenantoccupied">
							Tenant occupied  
						</div>
						<div ng-if="seller.viewdata.permrequired">
							Permission required for showings  
						</div>
						<div ng-if="seller.viewdata.gatecode">
							Gate Code
							<div class="m-l-15">
								<span ng-if="seller.viewdata.gatecodeprov === 'w_provide'">
									Will provide code when showing appointment is made.
								</span>
								<span ng-if="seller.viewdata.gatecodeprov === 'provide_gcode'">
									Provided gate code : {[{ seller.viewdata.providedcode }]}
								</span>
							</div>
						</div>
						<div ng-if="seller.viewdata.willalarm">
							Alarm activated
							<div class="m-l-15">
								<span ng-if="seller.viewdata.willalarm === 'w_provide'">
									Will provide instructions to deactivate alarm.
								</span>
								<span ng-if="seller.viewdata.willalarm === 'provide_alarm_ins'">
									Provided alarm deactivation instructions : {[{ seller.viewdata.alarmcode }]}
								</span>
							</div>
						</div>
					</div>
					<div>
						<strong>Buyer Access  : {[{ seller.viewdata.buyeraccess === "unlocked"? "Unlocked" : "Combination Lockbox" }]}</strong>
					</div>
					<div class="m-l-15" ng-if="seller.viewdata.buyeraccess === 'combination_lbox'">
						 <span ng-if="seller.viewdata.Lockbox === 'will_provide_lbox'">
						 	Provide lockbox code now for buyer agents view only
						 </span>
						 <span ng-if="seller.viewdata.lockbox === 'provide_lbox'">
						 	Lockbox Code : {[{ seller.viewdata.lockboxdesc }]}
						 </span>
					</div>
				</div>
			</div>
		</div> <!-- /.col-md-4 -->

		<div class="col-md-6">
			<div class="box box-info">
				<div class="box-header with-border">
		      		<h3 class="box-title">Seller's Information</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div><strong>Name:</strong> {[{ seller.viewdata.fullname }]}</div>
					<div><strong>Email:</strong> {[{ seller.viewdata.owneremail }]}</div>
					<div><strong>Contact:</strong> {[{ seller.viewdata.ownerphone }]}</div>
				</div>
			</div>

			<div class="box box-info" ng-if="seller.viewdata.otherbuyer.length">
				<div class="box-header with-border">
		      		<h3 class="box-title">Other Owner's Information</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body" ng-repeat="otherowner in seller.viewdata.otherbuyer">
					<div><strong>Name:</strong> {[{ otherowner.name }]}</div>
					<div><strong>Email:</strong> {[{ otherowner.email }]}</div>
					<div><strong>Contact:</strong> {[{ otherowner.phone }]}</div>
				</div>
			</div>


			<div class="box box-info"  ng-if="seller.viewdata.specialinstruction">
				<div class="box-header with-border">
		      		<h3 class="box-title">Special instructions</h3>
		    	</div><!-- /.box-header -->
				<div class="box-body">
					<div class="col-md-12">
						 {[{ seller.viewdata.specialinstruction }]}
					</div>
				</div>
			</div>

		</div> <!-- /.col-md-4 -->


		<div class="col-md-6" ng-if="seller.viewdata.contract_status !== constant.pending">
			<div class="box box-info">
				<div class="box-header with-border">
					<h3 class="box-title">Sent Mail Status</h3>
				</div><!-- /.box-header -->
				<div class="box-body">
               	    <div class="table-responsive">
               	    	<table class="table table-hover">
               	    		<thead>
               	    			<tr>
               	    				<th style="border:solid 1px #f4f4f4;">Type</th>
               	    				<th style="border:solid 1px #f4f4f4;">Status</th>
               	    				<th style="border:solid 1px #f4f4f4;">Date Sent</th>
                   	    			<th style="border:solid 1px #f4f4f4;">Action</th>
               	    			</tr>
               	    		</thead>
               	    		<tbody>
               	    			<tr ng-repeat="email in seller.emailstatuslist">
               	    				<td>{[{ email.type }]}</td>
               	    				<td>{[{ email.status }]}</td>
               	    				<td>{[{ email.date_sent }]}</td>
                   	    			<th><button type="button" class="btn btn-primary" ng-click="sendEmail(email.model)">Send Email</button></th>
               	    			</tr>
               	    		</tbody>
               	    	</table>
               	    </div>
				</div>
			</div>
		</div><!-- /.col-md-6 -->
	</div> <!-- /.row -->
</section>

<script type="text/ng-template" id="setContract.html">
	<form name="contractForm" novalidate ng-submit="setContract(contractForm)">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="icon-info"></i> Set Contract Info</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-offset-2 col-sm-8">
						<div class="form-group">
							<label for="contractdate">Contract Date</label>
							<input type="text" class="form-control" name="contractdate" ng-model="data.contractdate" date-picker required readonly>
						</div>
						<div class="form-group">
							<label for="inspectiondate">Inspection Date</label>
							<input type="text" class="form-control" name="inspectiondate" ng-model="data.inspectiondate" date-picker required readonly>
						</div>
						<div class="form-group">
							<label for="closingdate">Closing Date</label>
							<input type="text" class="form-control" name="closingdate" ng-model="data.closingdate" date-picker required readonly>
						</div>
						<!-- <div class="form-group">
							<label for="closingdate">Home # <small>(optional: can be found at the listed home details)</small></label>
							<input type="text" class="form-control" name="homeid" ng-model="data.homeid">
						</div> -->
					</div>
				</div>
			</div>
			<footer class="panel-footer">
				<div class="pull-right">
					<button type="button" class="btn btn-default" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn btn-primary">Submit</button>
				</div>
				<div style="clear:both;"></div>
			</footer>
		</div>
	</form>
</script>

<script type="text/ng-template" id="viewContract.html">
	<form name="viewContractForm" novalidate ng-submit="updateContract(viewContractForm)">
		<div class="panel panel-default">
			<div class="panel-heading"><i class="icon-info"></i> View Contract Info</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-sm-offset-2 col-sm-8">
						<div class="form-group">
							<label for="contractdate">Contract Date</label>
							<input type="text" class="form-control" name="contractdate" ng-model="contract.contractdate" date-picker required readonly min-date="minDate">
						</div>
						<div class="form-group">
							<label for="inspectiondate">Inspection Date</label>
							<input type="text" class="form-control" name="inspectiondate" ng-model="contract.inspectiondate" date-picker required readonly min-date="minDate">
						</div>
						<div class="form-group">
							<label for="closingdate">Closing Date</label>
							<input type="text" class="form-control" name="closingdate" ng-model="contract.closingdate" date-picker required readonly min-date="minDate">
						</div>
						<!-- <div class="form-group">
							<label for="closingdate">Home #</label>
							<input type="text" class="form-control" name="homeid" ng-model="contract.homeid" disabled>
						</div> -->
					</div>
				</div>
			</div>
			<footer class="panel-footer">
				<div class="pull-right">
					<button class="btn btn-default" ng-click="cancel()">Cancel</button>
					<button type="submit" class="btn btn-primary">Save</button>
				</div>
				<div style="clear:both;"></div>
			</footer>
		</div>
	</form>
</script>