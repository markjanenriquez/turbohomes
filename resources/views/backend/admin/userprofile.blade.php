
<section class="content-header" id="gototop">
	<h1>
		Profile
		<!-- <small>Add New User</small> -->
	</h1>
	<ol class="breadcrumb">
		<li><a href="#/dashboard"><i class="fa fa-dashboard"></i> Main</a></li>
		<li class="active">Admin</li>
		<li class="active">Profile</li>
	</ol>
</section>

<section class="content">
	<!-- SELECT2 EXAMPLE -->
	<alert ng-repeat="alert in alerts" type="{[{alert.type}]}" close="closeAlert($index)">{[{alert.msg}]}</alert>
	<div class="box {[{boxtype}]}">
		<form name="userform" ng-submit="saveuser(user)">
			<div class="box-header with-border">
				<h3 class="box-title">User Info</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<h4 class="page-header">Account Information</h4>
						<div class="profilecenter">
							<div class="profilepiccontainer">

								<div class="profilepic" id="profilepic" style="background-image: url('/img/{[{user.photo}]}');"></div>​
								<label for="profile_pic" class="label_profile_pic fadeinup animated">Change Picture</label>
							</div>
							<!-- <img src="/img/defaultprofilepicture.png" id="profilepic" class="profilepic"  alt="your image" width="150px" height="150px"> -->
							<input type="file" nv-file-select="" id="profile_pic" class="filestyle" data-buttonName="btn-primary" uploader="uploader" onchange="readURL(this);" accept="image/x-png, image/gif, image/jpeg"/>
						</div>
						<div class="form-group {[{useridclasstype}]}" id="gotouserid">
							<label class="control-label" for="userid">User ID <em>(Numbers Only)</em> <span ng-bind="useridmsg"></span><i class="{[{useridicontype}]}"></i><i class="fa fa-fw fa-refresh fa-spin text-muted" ng-show="userspinner"></i></label>
							<input type="number" class="form-control" id="userid" placeholder="Enter User ID" ng-model="user.userid" ng-change="useridchange(user.userid)" disabled="">
						</div>
						<!-- <div class="form-group {[{usernameclasstype}]}" id="gotousername">
							<label class="control-label" for="username">Username <em>(Username must be unique)</em> <span ng-bind="usernamemsg"></span><i class="{[{usernameicontype}]}"></i></label>
							<div class="input-group">
								<span class="input-group-addon">@</span>
								<input type="text" class="form-control" id="username" placeholder="Enter Username" ng-model="user.username" ng-trim="false" ng-change="user.username = user.username.split(' ').join('')">
							</div>
						</div> -->
						<div class="form-group">
							<label class="control-label" for="email"><!-- <i class="fa fa-check"></i>  -->Email <em>(Optional)</em></label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<input type="email" class="form-control" id="email" placeholder="Enter Email" ng-model="user.email">
							</div>
						</div>
						<!-- <div class="form-group {[{passwordclasstype}]}">
							<label class="control-label" for="password">Password <em>(Minimum of 6 characters)</em> <span ng-bind="passwordmsg"></span><i class="{[{passwordicontype}]}"></i></label>
							<input type="password" class="form-control" id="password" placeholder="Enter Password" ng-model="user.password" ng-change="passwordchange(user.password)">
						</div>
						<div class="form-group" ng-show="value != 0">
							<label class="control-label" >Password Strength <em ng-if="value <= 0"></em><em class="text-light-blue" ng-if="value < 50 && value > 0">(Weak)</em><em class="text-yellow" ng-if="value < 83 && value >= 50">(Medium)</em><em class="text-green" ng-if="value >= 83">(Strong)</em></label>
							<div ng-password-strength="user.password" strength="value" strength="passStrength" inner-class="progress-bar" inner-class-prefix="progress-bar-" class="ng-isolate-scope"></div>
						</div>
						<div class="form-group {[{repasswordclasstype}]}">
							<label class="control-label" for="repassword">Re-enter Password <em>(Minimum of 6 characters)</em> <span ng-bind="repasswordmsg"></span><i class="{[{repasswordicontype}]}"></i></label>
							<input type="password" class="form-control" id="repassword" placeholder="Re-enter Password" ng-model="user.repassword" ng-change="repasswordchange(user.password, user.repassword)">
						</div> -->

						<label class="control-label" for="password"><!-- <i class="fa fa-check"></i>  -->User Role <em></em></label>
						<div ng-hide="userrole == 'Super User'" class="form-group" ng-init="user.role = 0">
							<label>
								<input type="radio" name="role" class="flat-blue" value="0" ng-model="user.role" icheck>
								Administrator
							</label>
							<br>
							<label>
								<input type="radio" name="role" class="flat-blue" value="1" ng-model="user.role" icheck>
								Teacher
							</label>
						</div>
						<input type="text" ng-show="userrole == 'Super User'" class="form-control text-bold" value="Super User (Principal)" disabled>
					</div>

					<div class="col-md-6">
						<h4 class="page-header">Basic Information</h4>

						<div class="form-group {[{lastnameclasstype}]}" id="gotouserid">
							<label class="control-label" for="lastname">Last Name <span ng-bind="lastnamemsg"></span><i class="{[{lastnameicontype}]}"></i></label>
							<input type="text" class="form-control" id="lastname" placeholder="Enter Last Name" ng-model="user.lastname" ng-change="lastnamechange(user.lastname)">
						</div>
						<div class="form-group {[{firstnameclasstype}]}" id="gotouserid">
							<label class="control-label" for="firstname">First Name <span ng-bind="firstnamemsg"></span><i class="{[{firstnameicontype}]}"></i></label>
							<input type="text" class="form-control" id="firstname" placeholder="Enter Last Name" ng-model="user.firstname" ng-change="firstnamechange(user.firstname)">
						</div>
						<div class="form-group">
							<label class="control-label" for="middlename"><!-- <i class="fa fa-check"></i>  -->Middle Name <em>(Optional)</em></label>
							<input type="text" class="form-control" id="middlename" placeholder="Enter Middle Name" ng-model="user.middlename">
						</div>
						<div class="form-group {[{addressclasstype}]}">
							<label>Address <span ng-bind="addressmsg"></span><i class="{[{addressicontype}]}"></i></label>
							<textarea class="form-control" rows="3" placeholder="Enter Address" ng-model="user.address" ng-change="addresschange(user.address)"></textarea>
						</div>
						<div class="form-group">
							<label class="control-label" for="contact"><!-- <i class="fa fa-check"></i>  -->Contact Number <em>(Optional)</em></label>
							<input type="text" class="form-control" id="contact" placeholder="Enter Middle Name" ng-model="user.contact">
						</div>
						<label class="control-label" for="password"><!-- <i class="fa fa-check"></i>  -->Gender <em></em></label>
						<div class="form-group" ng-init="user.gender = 0">
							<label>
								<input type="radio" name="gender" class="flat-blue" value="0" ng-model="user.gender" icheck>
								Male
							</label>
							<br>
							<label>
								<input type="radio" name="gender" class="flat-blue" value="1" ng-model="user.gender" icheck>
								Female
							</label>
						</div>
					</div>

				</div><!-- /.row -->
			</div><!-- /.box-body -->
			<div class="box-footer">
				<a type="submit" class="btn btn-default" ng-click="cancel()">Cancel</a>
				<button type="submit" class="btn btn-info">Save</button>
			</div>
		</form>

		<div class="overlay" ng-show="saveloading">
			<i class="fa fa-refresh fa-spin" style="font-size:50px;"></i>
		</div>

	</div><!-- /.box -->
	<div class="box box-warning">
		<form name="pwdFORM">
		<div class="box-header with-border">
			<h3 class="box-title">Change Password</h3>
		</div><!-- /.box-header -->
		<div class="box-body">
			<div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="		closeAlert($index)">{[{alert.msg}]}</div>
			<div class="form-group">
				<label>Old Password</label>
				<input type="password" placeholder="Old Password" class="form-control" ng-model="pwd.old" required>
			</div>
			<div class="form-group">
				<label>New Password</label>
				<input type="password" name="new" placeholder="New Password" class="form-control" ng-model="pwd.new" ng-minlength="6" required>
				<em class="text-red" ng-show="pwdFORM.new.$error.minlength">Minimum of 6 characters.</em>
			</div>
			<div class="form-group"6
				<label>Retype New Password</label>
				<input type="password" name="renew" placeholder="Retype New Password" class="form-control" ng-model="pwd.renew" ng-minlength="6" required>
				<em class="text-red" ng-show="pwdFORM.renew.$error.minlength">Minimum of 6 characters.</em>
			</div>
		</div>
		<div class="box-footer">
			<a class="btn btn-info" ng-click="changepassword(pwd, loading = true)" ng-disabled="pwdFORM.$invalid">Save Password</a>
		</div>
		</form>
		<div class="overlay" ng-show="loading">
			<i class="fa fa-refresh fa-spin" style="font-size:50px;"></i>
		</div>
	</div>
</section>

<script type="text/javascript">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
            	document.getElementById('profilepic').style.backgroundImage="url("+ e.target.result +")";
                // $('#profilepic')
                //     // .attr('background-image', e.target.result)

                //     .width(150)
                //     .height(150);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
