
<section class="content-header" id="gototop">
	<h1>
		Edit User
		<!-- <small>Add New User</small> -->
	</h1>
	<ol class="breadcrumb">
		<li><a href="#/dashboard"><i class="fa fa-dashboard"></i> Main</a></li>
		<li class="active">Admin</li>
		<li class="active">Edit User</li>
	</ol>
</section>

<section class="content">
	<!-- SELECT2 EXAMPLE -->
	<div uib-alert ng-repeat="alert in alerts" ng-class="'alert-' + (alert.type || 'warning')" close="closeAlert($index)">{[{alert.msg}]}</div>
	<div class="box {[{boxtype}]}">
		<form name="userform" ng-submit="saveuser(user)">
			<div class="box-header with-border">
				<h3 class="box-title">User Info</h3>
			</div><!-- /.box-header -->
			<div class="box-body">
				<div class="row">
					<div class="col-md-6">
						<h4 class="page-header">Account Information</h4>
						<div class="profilecenter">
							<div class="profilepiccontainer">

								<div class="profilepic" id="profilepic" style="background-image: url('/img/{[{user.photo}]}');"></div>​
								<label for="profile_pic" class="label_profile_pic fadeinup animated">Change Picture</label>
							</div>
							<!-- <img src="/img/defaultprofilepicture.png" id="profilepic" class="profilepic"  alt="your image" width="150px" height="150px"> -->
							<input type="file" nv-file-select id="profile_pic" class="filestyle" data-buttonName="btn-primary" uploader="uploader" onchange="readURL(this);" accept="image/x-png, image/gif, image/jpeg"/>
						</div>
						<!-- <div class="form-group {[{useridclasstype}]}" id="gotouserid">
							<label class="control-label" for="userid">User ID <em>(Numbers Only)</em> <span ng-bind="useridmsg"></span><i class="{[{useridicontype}]}"></i><i class="fa fa-fw fa-refresh fa-spin text-muted" ng-show="userspinner"></i></label>
							<input type="number" class="form-control" id="userid" placeholder="Enter User ID" ng-model="user.userid" ng-change="useridchange(user.userid)" disabled="">
						</div> -->
						<!-- <div class="form-group {[{usernameclasstype}]}" id="gotousername">
							<label class="control-label" for="username">Username <em>(Username must be unique)</em> <span ng-bind="usernamemsg"></span><i class="{[{usernameicontype}]}"></i></label>
							<div class="input-group">
								<span class="input-group-addon">@</span>
								<input type="text" class="form-control" id="username" placeholder="Enter Username" ng-model="user.username" ng-trim="false" ng-change="user.username = user.username.split(' ').join('')" readonly>
							</div>
						</div> -->
						<div class="form-group">
							<label class="control-label" for="email"><!-- <i class="fa fa-check"></i>  -->Email</label>
							<div class="input-group">
								<span class="input-group-addon"><i class="fa fa-envelope"></i></span>
								<input type="email" class="form-control" id="email" placeholder="Enter Email" ng-model="user.email">
							</div>
						</div>
						<div class="form-group">
							<a class="btn btn-sm btn-warning" ng-click="resetpassword()">Reset Password</a>
						</div>
						<label class="control-label">User Role {[{ user.role }]}</label>
						<div class="form-group">
							<label>
								<input type="radio" name="role" class="flat-blue" value="0" ng-checked="user.role == 0" ng-model="user.role" icheck >
								Administrator
							</label>
							<br>
							<label>
								<input type="radio" name="role" class="flat-blue" value="1"  ng-checked="user.role == 1" ng-model="user.role" icheck>
								Buyer
							</label>
							<br>
							<label>
								<input type="radio" name="role" class="flat-blue" value="2" ng-checked="user.role == 2" ng-model="user.role" icheck>
								Seller
							</label>
							<br>
							<label>
								<input type="radio" name="role" class="flat-blue" value="3" ng-checked="user.role == 3" ng-model="user.role" icheck>
								Both
							</label>
							<br>
						</div>

						<label class="control-label">Status <em></em></label>
						<div class="form-group">
							<label>
								<input type="radio" name="status" class="flat-blue" value="1" ng-model="user.status" ng-checked="user.status == 1" icheck>
								Active
							</label>
							<br>
							<label>
								<input type="radio" name="status" class="flat-blue" value="0" ng-model="user.status" ng-checked="user.status == 0" icheck>
								Inactive
							</label>
						</div>
					</div>

					<div class="col-md-6">
						<h4 class="page-header">Basic Information</h4>

						<div class="form-group {[{lastnameclasstype}]}" id="gotouserid">
							<label class="control-label" for="lastname">Last Name <span ng-bind="lastnamemsg"></span><i class="{[{lastnameicontype}]}"></i></label>
							<input type="text" class="form-control" id="lastname" placeholder="Enter Last Name" ng-model="user.lastname" ng-change="lastnamechange(user.lastname)">
						</div>
						<div class="form-group {[{firstnameclasstype}]}" id="gotouserid">
							<label class="control-label" for="firstname">First Name <span ng-bind="firstnamemsg"></span><i class="{[{firstnameicontype}]}"></i></label>
							<input type="text" class="form-control" id="firstname" placeholder="Enter Last Name" ng-model="user.firstname" ng-change="firstnamechange(user.firstname)">
						</div>
						<div class="form-group">
							<label class="control-label" for="middlename"><!-- <i class="fa fa-check"></i>  -->Middle Name <em>(Optional)</em></label>
							<input type="text" class="form-control" id="middlename" placeholder="Enter Middle Name" ng-model="user.middlename">
						</div>
						<div class="form-group {[{addressclasstype}]}">
							<label>Address <span ng-bind="addressmsg"></span><i class="{[{addressicontype}]}"></i></label>
							<textarea class="form-control" rows="3" placeholder="Enter Address" ng-model="user.address" ng-change="addresschange(user.address)"></textarea>
						</div>
						<div class="form-group">
							<label class="control-label" for="contact"><!-- <i class="fa fa-check"></i>  -->Contact Number <em>(Optional)</em></label>
							<input type="text" class="form-control" id="contact" placeholder="Enter Middle Name" ng-model="user.contact">
						</div>
						<label class="control-label" for="password"><!-- <i class="fa fa-check"></i>  -->Gender <em></em></label>
						<div class="form-group" ng-init="user.gender = 0">
							<label>
								<input type="radio" name="gender" class="flat-blue" value="0" ng-model="user.gender" icheck>
								Male
							</label>
							<br>
							<label>
								<input type="radio" name="gender" class="flat-blue" value="1" ng-model="user.gender" icheck>
								Female
							</label>
						</div>
					</div>

				</div><!-- /.row -->
			</div><!-- /.box-body -->
			<div class="box-footer">
				<a type="submit" class="btn btn-default" ng-click="cancel()">Cancel</a>
				<button type="submit" class="btn btn-info">Save</button>
			</div>
		</form>

		<div class="overlay" ng-show="saveloading">
			<i class="fa fa-refresh fa-spin" style="font-size:50px;"></i>
		</div>

	</div><!-- /.box -->

	<!-- <div class="box box-warning">
		<div class="box-header with-border">
			<h3 class="box-title">Reset Password</h3>
		</div> --><!-- /.box-header -->
		<!-- <div class="box-body">

			<div class="alert alert-{[{alert.type}]} alert-dismissable" ng-repeat="alert in pwdalerts">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				{[{alert.msg}]}
			</div>
			Click <a class="btn btn-sm btn-warning" ng-click="resetpassword()">here</a> to set the default password.
		</div>
	</div> -->
</section>

<script type="text/javascript">
	function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
            	document.getElementById('profilepic').style.backgroundImage="url("+ e.target.result +")";
                // $('#profilepic')
                //     // .attr('background-image', e.target.result)

                //     .width(150)
                //     .height(150);
            };

            reader.readAsDataURL(input.files[0]);
        }
    }
</script>
