<!DOCTYPE html>
<html data-ng-app="app"  lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Bootstrap 3.3.5 -->
        <link rel="stylesheet" href="/components/bootstrap/dist/css/bootstrap.min.css">
        <!-- angular bootstrap css -->
        <link rel="stylesheet" href="/components/angular-bootstrap/ui-bootstrap-csp.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="/dist/css/AdminLTE.min.css">
        <!-- animate style -->
        <link rel="stylesheet" href="/dist/css/becss/animate.css">
        <!-- custom style -->
        <link rel="stylesheet" href="/dist/css/becss/custom.css">
        <!-- AdminLTE Skins. Choose a skin from the css/skins folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="/dist/css/skins/_all-skins.min.css">
        <link rel="stylesheet" href="/plugins/daterangepicker/daterangepicker-bs3.css">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="/plugins/iCheck/all.css">
        <!-- Bootstrap Color Picker -->
        <link rel="stylesheet" href="/plugins/colorpicker/bootstrap-colorpicker.min.css">
        <!-- Bootstrap time Picker -->
        <link rel="stylesheet" href="/plugins/timepicker/bootstrap-timepicker.min.css">
        <!-- Select2 -->
        <link rel="stylesheet" href="/components/select2/dist/css/select2.min.css">
        <!-- ng-img-crop -->
        <link rel="stylesheet" href="/components/ng-img-crop/compile/minified/ng-img-crop.css">
        <!-- Font Awesome -->
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
        <link rel="stylesheet" href="/components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
        <link rel="stylesheet" href="/components/Ionicons/css/ionicons.min.css">
        <!-- jvectormap -->
        <link rel="stylesheet" href="/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
        <!-- iCheck for checkboxes and radio inputs -->
        <link rel="stylesheet" href="/plugins/iCheck/all.css">
        <link rel="stylesheet" href="/plugins/timepicker/bootstrap-timepicker.min.css">
        <link rel="stylesheet" href="/components/fancybox/dist/jquery.fancybox.min.css" >
        <link rel="stylesheet" href="/components/select2/dist/css/select2.min.css">
        <link rel="stylesheet" href="/libs/houzez-child/css/bootstrap-datepicker3.min.css">

    </head>
    <body class="hold-transition skin-blue sidebar-mini" ng-controller="layoutCtrl">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
                <a href="index2.html" class="logo">
                    <!-- mini logo for sidebar mini 50x50 pixels -->
                    <span class="logo-mini"><b>EL</b></span>
                    <!-- logo for regular state and mobile devices -->
                    <span class="logo-lg"><b>Admin</b></span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top" role="navigation">
                    <!-- Sidebar toggle button-->
                    <a href="" class="sidebar-toggle" data-toggle="offcanvas" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <!-- Navbar Right Menu -->
                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- Messages: style can be found in dropdown.less-->
                            <!-- Notifications: style can be found in dropdown.less -->
                            <!-- Tasks: style can be found in dropdown.less -->
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="" class="dropdown-toggle" data-toggle="dropdown">
                                    <img src="/img/{[{profilepicture}]}" class="user-image" alt="User Image">
                                    <span class="hidden-xs" ng-bind="fullname"></span>
                                </a>
                                <ul class="dropdown-menu">
                                    <!-- User image -->
                                    <li class="user-header">
                                        <img src="/img/{[{profilepicture}]}" class="img-circle" alt="User Image">
                                        <p>
                                            <span ng-bind="fullname"></span>
                                            <small>Role:
                                            <span ng-bind="userrole"></span>
                                            </small>
                                        </p>
                                    </li>
                                    <!-- Menu Body -->
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        <div class="pull-left">
                                            <a ng-click="profile()" class="btn btn-default btn-flat">Profile</a>
                                        </div>
                                        <div class="pull-right">
                                            <a href="" class="btn btn-default btn-flat" ng-click="logout()">Sign out</a>
                                        </div>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left image">
                            <img src="/img/{[{profilepicture}]}" class="img-circle" alt="User Image" style="width:50px!important; height:50px!important;">
                        </div>
                        <div class="pull-left info">
                            <p ng-bind="fullname">Username</p>
                            <a href=""><i class="fa fa-circle text-success"></i> Online</a>
                        </div>
                    </div>
                    <!-- /.search form -->
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="header">MAIN NAVIGATION</li>
                        <!-- <li>
                            <a href="" ui-sref="dashboard">
                                <i class="fa fa-th"></i> <span>Dashboard</span>
                            </a>
                        </li> -->
                        <!-- <li ng-show="userrole != 'Teacher' ">
                            <a href="" ui-sref="schoolyear">
                                <i class="fa fa-th"></i> <span>School Year</span>
                            </a>
                        </li> -->
                        <li class="treeview" ng-show="userrole != 'Teacher' " ui-sref-active="active">
                            <a href="">
                                <i class="fa fa-user"></i> <span>Users</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu" >
                                <li><a href="" ui-sref="usercreate" ><i class="fa fa-circle-o text-green"></i> New User</a></li>
                                <li><a href="" ui-sref="usermanage"><i class="fa fa-circle-o text-blue"></i> Manage User</a></li>
                            </ul>
                        </li>
                        <li class="treeview" ui-sref-active="active">
                            <a href="">
                                <i class="fa fa-user"></i> <span>Blog</span> <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu" >
                                <li><a href="" ui-sref="blogcreate" ><i class="fa fa-circle-o text-green"></i> New Blog</a></li>
                                <li><a href="" ui-sref="blogmanage"><i class="fa fa-circle-o text-blue"></i> Manage Blog</a></li>
                                <li><a href="" ui-sref="tag"><i class="fa fa-circle-o text-blue"></i> Tag</a></li>
                            </ul>
                        </li>
                        <li class="treeview" ui-sref="manageoffer" ui-sref-active="active">
                            <a href="" >
                                <i class="fa fa-circle-o"></i> <span>Offers</span> <i class="pull-right"></i>
                            </a>
                        </li>
                        <li class="treeview" ui-sref="manageseller" ui-sref-active="active">
                            <a href="" >
                                <i class="fa fa-circle-o"></i> <span>List Of Home</span>
                            </a>
                        </li>

                        <!-- <li class="treeview">
                            <a href="">
                                <i class="fa  fa-users"></i> <span>Students</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li ui-sref-active="active"><a href="" ui-sref="createstudent"><i class="fa fa-circle-o text-green"></i> New Student</a></li>
                                <li><a ui-sref="managestudents"><i class="fa fa-circle-o text-blue"></i> Manage Students</a></li>
                            </ul>
                        </li> -->
                        <!-- <li class="treeview" ng-show="userrole != 'Teacher' ">
                            <a href="">
                                <i class="glyphicon glyphicon-home"></i><span>Rooms</span><i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li><a ui-sref="newroom"><i class="fa fa-circle-o text-green"></i> New Room</a></li>
                                <li><a ui-sref="managerooms"><i class="fa fa-circle-o text-blue"></i> Manage Rooms</a></li>
                            </ul>
                        </li>
                        <li ng-show="userrole == 'Teacher' ">
                            <a href="" ui-sref="myrooms">
                                <i class="glyphicon glyphicon-home"></i> <span>My Rooms</span>
                            </a>
                        </li> -->
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="min-height: 916px;" >
                <div class="fadeIn animated" ui-view>
                   {{--  <section class="content-header">
                        <h1>
                            Seller Dashboard
                            <!-- <small>Add New User</small> -->
                        </h1>
                        <ol class="breadcrumb">
                            <li><a href="#/dashboard"><i class="fa fa-dashboard"></i> Main</a></li>
                        </ol>
                    </section>

                    <section class="content">
                           <h1>Congratulations, you are now <span style="color : #fb9e4b">Turbo Powered!</span></h1>
                           <p style="font-size: 20px">We work for you. Here’s what happens next. </p>
                           <h3 style="margin-top: 55px">The Paperwork...Boring</h3>
                           
                    </section> --}}
                </div>
            </div><!-- /.content-wrapper -->
            <footer class="main-footer">
                <div class="pull-right hidden-xs">
                    <b>Version</b> 1.0
                </div>
                <strong>Copyright &copy; 2015-2016</strong> All rights reserved.
            </footer>
        </div><!-- ./wrapper -->
        <!-- SCRIPT RESOURCES SCRIPT RESOURCES SCRIPT RESOURCES SCRIPT RESOURCES SCRIPT RESOURCES -->
        <!-- jQuery 2.1.4 -->
        <script src="/components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="/components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="/components/select2/dist/js/select2.min.js"></script>
        <!-- InputMask -->
        <script src="/plugins/input-mask/jquery.inputmask.js"></script>
        <script src="/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
        <!-- custom input mask -->
        <script src="/dist/js/inputmask.js"></script>
        <!-- date-range-picker -->
        <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script> -->
        <script src="/plugins/daterangepicker/daterangepicker.js"></script>
        <!-- bootstrap color picker -->
        <script src="/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>
        <!-- bootstrap time picker -->
        <script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <!-- SlimScroll 1.3.0 -->
        <script src="/plugins/slimScroll/jquery.slimscroll.min.js"></script>
        <!-- iCheck 1.0.1 -->
        <script src="/plugins/iCheck/icheck.min.js"></script>
        <!-- FastClick -->
        <script src="/plugins/fastclick/fastclick.min.js"></script>
        <!-- AdminLTE App -->
        <script src="/dist/js/app.min.js"></script>
        <!-- ChartJS 1.0.1 -->
        <script src="/plugins/chartjs/Chart.min.js"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!-- <script src="/dist/js/pages/dashboard2.js"></script> -->
        <script src="/plugins/timepicker/bootstrap-timepicker.min.js"></script>
        <!-- AdminLTE for demo purposes -->
        <!-- iCheck 1.0.1 -->
        <script src="/plugins/iCheck/icheck.min.js"></script>
        <script src="/components/fancybox/dist/jquery.fancybox.min.js"></script>

        <script src="/components/jquery-maskmoney/dist/jquery.maskMoney.min.js"></script>
        
        <!-- Components -->
        <script src="/components/angular/angular.min.js" type="text/javascript"></script>
        <script src="/components/angular-jwt/dist/angular-jwt.min.js" type="text/javascript"></script>
        <script src="/components/a0-angular-storage/dist/angular-storage.min.js" type="text/javascript"></script>
        <script src="/components/angular-ui-router/release/angular-ui-router.min.js" type="text/javascript"></script>
        <script src="/other_components/angular-ui-load/ui-load.js" type="text/javascript"></script>
        <script src="/components/angular-ui-router/release/angular-ui-router.min.js" type="text/javascript"></script>
        <script src="/components/angular-bootstrap/ui-bootstrap-tpls.min.js" type="text/javascript"></script>
        <script src="/components/ng-password-strength/dist/scripts/ng-password-strength.js"></script>
        <script src="/components/lodash/lodash.min.js"></script>
        <script src="/components/angular-file-upload/dist/angular-file-upload.min.js"></script>
        <script src="/components/ng-img-crop/compile/minified/ng-img-crop.js"></script>
        <script src="/components/moment/min/moment.min.js"></script>
        <script src="/components/angular-moment/angular-moment.min.js"></script>
        
        <script src="/components/json-export-excel/src/json-export-excel.js"></script>
        <script src="/components/file-saver/FileSaver.js"></script>
        <script src="/components/angular-animate/angular-animate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/1.1.0/toaster.min.js"></script>

        <script src="/components/ckeditor/ckeditor.js"></script>
      
        <script src="/components/ng-ckeditor/dist/ng-ckeditor.min.js"></script>

        <script src="/components/angular-messages/angular-messages.min.js"></script>

        <script type="text/javascript" src="/components/select2/dist/js/select2.min.js"></script>
        <script type="text/javascript" src="/components/angular-ui-select2/src/select2.js"></script>

        <script src="/components/angular-ckeditor/angular-ckeditor.min.js"></script>
        <script src="/components/angular-google-places-autocomplete/src/autocomplete.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDU8UPQsS0i1sJTCUAVGRs6xSqJVJaA04k&libraries=places"></script>
       <link rel="stylesheet" href="/components/angular-google-places-autocomplete/src/autocomplete.css">

       <script src="/components/angular-ui-mask/dist/mask.min.js"></script>

       <script src="/components/angular.uuid2/dist/angular-uuid2.min.js" type="text/javascript"></script>
        
        <!-- Satellizer Js -->
        <script src="/components/satellizer/dist/satellizer.min.js"></script>
        <!-- angular form ui validation -->
        <script src="/dist/js/angular-ui-form-validation.js"></script>
        <!-- Angular app & config -->
        <script src="/ng-be/ng-app.js" type="text/javascript"></script>
        <script src="/ng-be/ng-config.js" type="text/javascript"></script>
        <!-- Controllers -->
        <script src="/ng-be/controllers/layout.js" type="text/javascript"></script>
        <!-- Factory -->
        <script src="/ng-be/factory/general-factory.js" type="text/javascript"></script>
        <!-- Directive -->
        <script src="/ng-be/directives/directive.js" type="text/javascript"></script>
        <script src="/ng-be/directives/validations.js" type="text/javascript"></script>
        <script src="/ng-be/directives/icheck.js" type="text/javascript"></script>
        <script src="/ng-be/directives/ngenter.js" type="text/javascript"></script>
        <!-- Filter -->
        <script src="/ng-be/filter/filter.js" type="text/javascript"></script>
        <!-- Services -->
        <script src="/ng-be/services/anchorSmoothScroll.js" type="text/javascript"></script>
        <script src="/libs/houzez-child/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <!-- Page script -->
        <!-- FANCYBOX -->
        <script>
        $(document).ready(function() {
        /*
        *  Simple image gallery. Uses default settings
        */
        $('.fancybox').fancybox();
        /*
        *  Different effects
        */
        // Change title type, overlay closing speed
        $(".fancybox-effects-a").fancybox({
        helpers: {
        title: {
        type: 'outside'
        },
        overlay: {
        speedOut: 0
        }
        }
        });
        });
        </script>
        <!-- FANCYBOX end -->
    </body>
</html>