<!DOCTYPE html>
<html data-ng-app="app"  lang="{{ app()->getLocale() }}">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Forgot Password</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
    <link rel="stylesheet" href="/plugins/font-awesome-4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
    <link rel="stylesheet" href="/plugins/ionicons-2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="/dist/css/AdminLTE.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="/plugins/iCheck/square/blue.css">
    <!-- angular bootstrap css -->
    <link rel="stylesheet" href="/components/angular-bootstrap/ui-bootstrap-csp.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-box-body">

        <div class="logo m-b-20"><img src="/img/logo-01.png" alt=""></div>
        <p class="login-box-msg">Error!</p>
        <p clas="text-muted">Your Reset Password link is expired or invalid!.</p>
        </span>

      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

<!-- SCRIPT RESOURCES SCRIPT RESOURCES SCRIPT RESOURCES SCRIPT RESOURCES SCRIPT RESOURCES -->
    <!-- jQuery 2.1.4 -->
    <script src="/plugins/jQuery/jQuery-2.1.4.min.js"></script>
    <!-- Bootstrap 3.3.5 -->
    <script src="/bootstrap/js/bootstrap.min.js"></script>
    <!-- iCheck -->
    <script src="/plugins/iCheck/icheck.min.js"></script>

    <!-- Components -->
    <script src="/components/angular/angular.min.js" type="text/javascript"></script>
    <script src="/components/angular-jwt/dist/angular-jwt.min.js" type="text/javascript"></script>
    <script src="/components/a0-angular-storage/dist/angular-storage.min.js" type="text/javascript"></script>
    <script src="/components/angular-ui-router/release/angular-ui-router.min.js" type="text/javascript"></script>
    <script src="/other_components/angular-ui-load/ui-load.js" type="text/javascript"></script>
    <script src="/components/angular-bootstrap/ui-bootstrap.min.js" type="text/javascript"></script>
    <script src="/components/ng-password-strength/dist/scripts/ng-password-strength.min.js"></script>
    <script src="/components/lodash/lodash.min.js"></script>
    <script src="/components/angular-file-upload/dist/angular-file-upload.min.js"></script>
    <script src="/components/ng-img-crop/compile/minified/ng-img-crop.js"></script>
    <script src="/components/moment/min/moment.min.js"></script>
    <script src="/components/angular-moment/angular-moment.min.js"></script>

    <script type="text/javascript" src="/components/select2/dist/js/select2.min.js"></script>
    <script type="text/javascript" src="/components/angular-ui-select2/src/select2.js"></script>
    <script src="/components/angular-ckeditor/angular-ckeditor.min.js"></script>

    <script src="/components/angular-messages/angular-messages.min.js"></script>
        <script src="/components/angular-animate/angular-animate.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/angularjs-toaster/1.1.0/toaster.min.js"></script>

    <!-- angular form ui validation -->
    <script src="/dist/js/angular-ui-form-validation.js"></script>
     <script src="/components/json-export-excel/src/json-export-excel.js"></script>
    <script src="/components/file-saver/FileSaver.js"></script>

    <script src="/components/ckeditor/ckeditor.js"></script>
    <script src="/components/ng-ckeditor/dist/ng-ckeditor.min.js"></script>

    <script src="/components/angular-google-places-autocomplete/src/autocomplete.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDU8UPQsS0i1sJTCUAVGRs6xSqJVJaA04k&libraries=places"></script>
    <link rel="stylesheet" href="/components/angular-google-places-autocomplete/src/autocomplete.css">

    <script src="/components/angular-ui-mask/dist/mask.min.js"></script>
    
    <script src="/components/angular.uuid2/dist/angular-uuid2.min.js" type="text/javascript"></script>
    
    <!-- Satellizer Js -->
    <script src="/components/satellizer/dist/satellizer.min.js"></script>
    
    <!-- Angular app & config -->
    <script src="/ng-be/ng-app.js" type="text/javascript"></script>
    <script src="/ng-be/ng-config.js" type="text/javascript"></script>
    <!-- Controllers -->
    <script src="/ng-be/controllers/forgotpassword/forgotpassword.js" type="text/javascript"></script>
    <!-- Factory -->
    <script src="/ng-be/factory/forgotpassword.js" type="text/javascript"></script>
    <script src="/ng-be/factory/general-factory.js" type="text/javascript"></script>

    <!-- Filter -->
    <script src="/ng-fe/filter/filter.js" type="text/javascript"></script>

    <script>
      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });
    </script>
  </body>
</html>
